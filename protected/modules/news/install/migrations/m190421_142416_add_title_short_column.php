<?php

class m190421_142416_add_title_short_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{news_news}}', 'title_short', 'string');
    }
}