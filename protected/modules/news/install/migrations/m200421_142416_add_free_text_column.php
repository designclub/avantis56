<?php

class m200421_142416_add_free_text_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{news_news}}', 'free_text', 'text');
    }
}