<?php
/**
 * Форма остались вопросы
 */
class ReservationFormModal extends CFormModel
{
    public $name;
    public $phone;
    public $email;
    public $comment;
    public $smen;
    public $fio;
    public $verifyCode;

    public function rules()
    {
        return [
            ['name, phone', 'required'],
            ['email', 'email'],
            ['comment, smen', 'safe'],
            [
                'verifyCode',
                'yupe\components\validators\YRequiredValidator',
                'allowEmpty' => false,
                'message' => Yii::t('UserModule.user', 'Check code incorrect')
            ],
            ['verifyCode', 'captcha', 'allowEmpty' => false, 'captchaAction'=>'/site/captcha'],
            ['verifyCode', 'emptyOnInvalid'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'       => 'Ваше имя',
            'phone'      => 'Ваш телефон',
            'email'      => 'Ваш E-mail',
            'comment'    => 'Комментарий к заявке',
            'smen'       => 'Смены',
            'fio'       => 'ФИО Вашего ребенка',
            'verifyCode' => 'Код проверки',
        ];
    }

    public function afterValidate()
    {
        $mail = Yii::app()->mail;
        if (empty($this->getErrors())) {

            $to = Yii::app()->getModule('yupe')->email;
            $from = 'dc@dc56.ru';
            $theme = "Бронирование путевки в лагерь";

            $body = Yii::app()->controller->renderPartial('//mail/mail/_reservation', ['model' => $this], true);
            $mail->send($from, $to, $theme, $body);
        }
        return parent::afterValidate();
    }

    public function emptyOnInvalid($attribute, $params)
    {
        if ($this->hasErrors()) {
            $this->verifyCode = null;
        }
    }

    public function getSmenyList()
    {
        $model = Smeny::model()->published()->findAll();
        $array = [];
        foreach ($model as $key => $item) {
            $array[$item->id] = $item->nomer . ' - ' . $item->getDate() . ' (' . $item->price . ')';
        }
        return $array;
    }

    public function getSmenyName()
    {
        $data = $this->getSmenyList();
        if (isset($data[$this->smen])) {
            return $data[$this->smen];
        }
        return null;
    }
}
?>