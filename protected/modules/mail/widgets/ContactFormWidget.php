<?php

Yii::import('application.modules.mail.models.form.ContactFormModel');
/**
 *
 */
class ContactFormWidget extends \yupe\widgets\YWidget
{

    public function run()
    {
        $model = new ContactFormModel;
        if (isset($_POST['ContactFormModel'])) {
            $model->attributes = $_POST['ContactFormModel'];
            if ($model->validate()) {
                Yii::app()->user->setFlash('success', 'Ваше сообщение успешно отправлено');
                Yii::app()->controller->refresh();
            }
        }

        $this->render('contact-form-widget', [
            'model' => $model,
        ]);
    }
}
