<?php
/**
 * PagesWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesWidget
 */
class PagesWidget extends yupe\widgets\YWidget
{
    public $isUseDataProvider = 0;

    public $pageSize = 15;

    public $limit = null;
    /**
     * @var
     */
    public $pageStatus;
    /**
     * @var bool
     */
    public $topLevelOnly = false;
    /**
     * @var string
     */
    public $order = 't.order ASC, t.create_time ASC';
    /**
     * @var
     */
    public $parent_id;
    public $category_id;
    /**
     * @var string
     */
    public $view = 'pageswidget';
    /**
     * @var bool
     */
    public $visible = true;

    /**
     *
     */
    public function init()
    {
        parent::init();

        if (!$this->pageStatus) {
            $this->pageStatus = Page::STATUS_PUBLISHED;
        }

        $this->parent_id = (int)$this->parent_id;
        $this->category_id = (int)$this->category_id;
    }

    /**
     * @throws CException
     */
    public function run()
    {
        if ($this->visible) {
            $criteria = new CDbCriteria();
            $criteria->order = $this->order;
            $criteria->addCondition("status = {$this->pageStatus}");

            // if (!Yii::app()->user->isAuthenticated()) {
            //     $criteria->addCondition('is_protected = '.Page::PROTECTED_NO);
            // }
            if ($this->limit) {
                $criteria->limit = (int)$this->limit;
            }
             if ($this->category_id) {
                $criteria->addCondition("category_id = {$this->category_id}");
            }
            if ($this->parent_id) {
                $criteria->addCondition("parent_id = {$this->parent_id}");
            }

            //$criteria->addCondition("id != 20"); //услуга не учавствующая

            if ($this->topLevelOnly) {
                $criteria->addCondition("parent_id is null or parent_id = 0");
            }

            if ((int)$this->isUseDataProvider) {
                $pages = new CActiveDataProvider('Page', [
                    'criteria' => $criteria,
                    'pagination' => [
                        'pageSize' => $this->pageSize
                    ],
                ]);
            } else {
                $pages = Page::model()->cache($this->cacheTime)->findAll($criteria);
            }

            $this->render(
                $this->view,
                [
                    'pages' => $pages,
                ]
            );
        }
    }
}
