<?php

class m201300_051948_add_canonical_to_page extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn("{{page_page}}", 'canonical', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn("{{page_page}}", 'canonical');
    }
}
 