<?php

class m221300_061949_alter_meta_description_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->alterColumn("{{page_page}}", 'meta_description', "varchar(2000) NOT NULL");
    }
}
