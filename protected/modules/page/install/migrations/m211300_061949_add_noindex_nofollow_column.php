<?php

class m211300_061949_add_noindex_nofollow_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn("{{page_page}}", 'noindex_nofollow', "boolean NOT NULL DEFAULT '0'");
    }

    public function safeDown()
    {
        $this->dropColumn("{{page_page}}", 'noindex_nofollow');
    }
}
