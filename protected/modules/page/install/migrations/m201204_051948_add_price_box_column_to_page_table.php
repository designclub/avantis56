<?php

class m201204_051948_add_price_box_column_to_page_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn("{{page_page}}", 'price_box', 'text');
    }

    public function safeDown()
    {
        $this->dropColumn("{{page_page}}", 'price_box');
    }
}
