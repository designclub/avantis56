var syntax        = 'sass', // выберете используемый синтаксис sass или scss, и перенастройте нужные пути в файле gulp.js и папки в вашего шаблоне
		gulpversion   = '4'; // Выберете обязателньо свою версию Gulp: 3 или 4

var gulp          = require('gulp'),
    autoprefixer  = require('gulp-autoprefixer'),
    browsersync   = require('browser-sync'),
    concat        = require('gulp-concat'),
    cache         = require('gulp-cache'),
    cleancss      = require('gulp-clean-css'),
		imagemin      = require('gulp-imagemin'),
		notify        = require('gulp-notify'),
		pngquant      = require('imagemin-pngquant'),
		gutil         = require('gulp-util' ),
		rename        = require('gulp-rename'),
		rsync         = require('gulp-rsync'),
		sass          = require('gulp-sass'),
		uglify        = require('gulp-uglify');
		jsImport = require('gulp-js-import');


// Незабываем менять 'bul.loc' на свой локальный домен
gulp.task('browser-sync', function() {
	browsersync({
		proxy: "avantis56.loc",
		notify: false,
	})
});


// Обьединяем файлы sass, сжимаем и переменовываем
gulp.task('styles', function() {
	return gulp.src('sass/**/*.scss')
	.pipe(sass({ outputStyle: 'expand' }).on("error", notify.onError()))
	//.pipe(rename({ suffix: '.min', prefix : '' }))
	.pipe(concat('index.min.css'))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
	.pipe(gulp.dest('../../../public/css'))
	.pipe(browsersync.stream())

});


// Обьединяем файлы скриптов, сжимаем и переменовываем
gulp.task('scripts', function() {
	return gulp.src(
		'js/main.js'
		)
    .pipe(jsImport({hideConsole: true}))
	.pipe(concat('custom.min.js'))
	.pipe(uglify())
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
	.pipe(gulp.dest('../../../public/js/'))
	.pipe(browsersync.reload({ stream: true }))

});


	gulp.task('watch', function() {
		gulp.watch('sass/**/*.scss', gulp.parallel('styles')); // Наблюдение за sass файлами в папке sass в теме
		gulp.watch(['js/libs/**/*.js', 'js/main.js'], gulp.parallel('scripts')); // Наблюдение за JS файлами в папке js
    gulp.watch('./../views/**/*.php', browsersync.reload) // Наблюдение за sass файлами php в теме

	});
	gulp.task('default', gulp.parallel('styles', 'scripts', 'browser-sync', 'watch'));

