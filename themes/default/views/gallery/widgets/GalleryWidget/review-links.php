<div class="review-link__flex">
	<?php foreach ($dataProvider->getData() as $k => $data): ?>
		<div class="link__block">
			<a href="<?= $data->image->alt; ?>" class="link__flex-image" style="display: block;">
				<?= Chtml::image($data->image->getImageUrl()); ?>
			</a>
			<a href="<?= $data->image->alt; ?>" target="_blank" class="link__flex-title">
				<?= $data->image->name; ?>
			</a>		
		</div>
	<?php endforeach; ?>
</div>
