  <?php
  Yii::app()->getClientScript()->registerCssFile('/css/cocoen.min.css');
  Yii::app()->getClientScript()->registerScriptFile('/js/cocoen.min.js');
  ?>
<div class="works">
     <?php foreach ($dataProvider->getData() as $value):?>
             <div class="cocoen_wrap">
                <div class="cocoen">
                  <?= CHtml::image($value->image->getImageUrl());?>
                  <?= CHtml::image('/uploads/image/'.$value->image->alt.'.jpg');?>
               </div>
               <div class="detail">
                 <div class="before">До</div>
                 <div class="after">После</div>
               </div>
             </div>
    <?php endforeach; ?>

</div>
<script>
  document.querySelectorAll('.cocoen').forEach(function(element){
  new Cocoen(element);
});

</script>