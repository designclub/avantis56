<?php if ($dataProvider->itemCount): ?>

<div class="banner slik-<?= $this->galleryId ?>">

    <?php foreach ($dataProvider->getData() as $k => $data): ?>
    <div class="banner_item" style='position:relative;background-image:url(<?= $data->image->getImageUrl() ?>)'>
         <?php if(!empty($data->image->description)):?>
            <div class="bane-desc">
              <?=$data->image->description;?>

              <div class="btn-z">
                <a class="but-z repeat_captha" data-target="#callbackServiceModal" data-toggle="modal" href="#">Записаться на приём</a>
              </div>
            </div>
        <?php endif;?>

        <div class="banner_after" style="position:absolute">
            <img src="/img/bdots.svg" alt="">
        </div>

    </div>
    <?php endforeach; ?>
</div>


<?php endif; ?>

