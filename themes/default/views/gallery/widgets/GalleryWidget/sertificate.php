<div class="w-sertificate">
    <?php foreach ($dataProvider->getData() as $value): ?>
            <a href="<?= $value->image->getImageUrl() ?>" data-fancybox="group-<?= $this->galleryId?>">
                <div class="w-sertificate__item">
                    <span>+</span>
                        <?= CHtml::image($value->image->getImageUrl(0,0,true)) ?>
                </div>
            </a>
    <?php endforeach ?>
</div>

<?php $fancybox = $this->widget(
            'gallery.extensions.fancybox3.AlFancybox', [
                'target' => '[data-fancybox]',
                'lang'   => 'ru',
                'config' => [
                    'animationEffect' => "fade",
                    'buttons' => [
                        "rotateleft",
                        "close",
                    ]
                ],
            ]
        ); ?>
<?php Yii::app()->clientScript->registerScript("serts", "
    $('.w-sertificate').slick({
       infinite: true,
      slidesToShow: 4,
      arrows: true,
      dots:true,
      autoplay:true,
      autoplaySpeed:5000,
      responsive: [
    {
      breakpoint: 1200,
      settings: {
         slidesToShow: 3

      }
    },
    {
      breakpoint: 768,
      settings: {
         slidesToShow: 3
      }
    },
    {
      breakpoint: 549,
      settings: {
         slidesToShow: 2,

      }
    },
    {
      breakpoint: 361,
      settings: {
         slidesToShow: 1,
         centerMode: false
      }
    }
  ]

    });

"); ?>