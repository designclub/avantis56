

<div class="our_work">
     <?php foreach ($dataProvider->getData() as $value):?>       
          <div>       
          
              <div class="our_work_item">
                <?= CHtml::image($value->image->getImageUrl(160,160), $value->image->alt,
                   ['title' => $value->image->alt, 'href' => $value->image->getImageUrl(), 'class' => 'our_photo']); ?>
                  
             </div>
          
             </div>          
         
            
    <?php endforeach; ?>

</div>

    <script>
   
    $('.our_work').slick({
       infinite: true,
      slidesToShow: 4,
      arrows: false,
      dots:true,
      autoplay:true,
      autoplaySpeed:5000,   
      responsive: [
    {
      breakpoint: 1200,
      settings: {
         slidesToShow: 3
         
      }
    },
    {
      breakpoint: 768,
      settings: {
         slidesToShow: 3
      }
    },
    {
      breakpoint: 549,
      settings: {
         slidesToShow: 2,
        
      }
    },
    {
      breakpoint: 361,
      settings: {
         slidesToShow: 1,
         centerMode: false        
      }
    }
  ]

    });
    
   

    </script>