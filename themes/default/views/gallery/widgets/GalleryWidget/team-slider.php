<div class="team-slider js-photo-slider-vip">
 <?php foreach ($dataProvider->getData() as $value):?>
  <div>
    <div class="team-slider_item">
      <?= CHtml::image($value->image->getImageUrl(330,220), $value->image->alt,
      ['title' => $value->image->alt, 'href' => $value->image->getImageUrl(), 'class' => 'our_photo']); ?>
    </div>
  </div>
<?php endforeach; ?>
</div>

<?php Yii::app()->clientScript->registerScript("team-slider", "
 $('.team-slider').slick({

      infinite: true,
      slidesToShow: 3,
      arrows: true,
      dots:false,
      autoplay:false,
      responsive: [
    {
      breakpoint: 1200,
      settings: {
         slidesToShow: 3,
         centerMode: false
      }
    },
    {
      breakpoint: 991,
      settings: {
         slidesToShow: 1,
         centerMode: false
      }
    },
    {
      breakpoint: 480,
      settings: {
         slidesToShow: 1,
         centerMode: false
      }
    },
    {
      breakpoint: 361,
      settings: {
         slidesToShow: 1,
         centerMode: false
      }
    }
  ]
    });
");
 ?>


