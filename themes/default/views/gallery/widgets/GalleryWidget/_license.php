<li class="col-sm-3">
    <div class="thumbnail">
        <?= CHtml::image(
            $data->image->getImageUrl(250, 350),
            $data->image->alt,
            ['title' => $data->image->alt, 'href' => $data->image->getImageUrl(), 'class' => 'gallery-image']
        ); ?>
    </div>
</li>
