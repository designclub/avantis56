
<?php

$this->title = "Отзывы о стоматологах Оренбурга | Стоматология «Авантис»";
$this->description = "Отзывы о частной стоматологии «Авантис». Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов. Записаться на прием по телефону: ☎️ +7(3532)66-22-66";
$this->keywords = Yii::app()->getModule('yupe')->siteKeyWords;
// $this->breadcrumbs = $this->getBreadCrumbs();
$this->breadcrumbs = ["Отзывы"];
	Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js', CClientScript::POS_END);

Yii::app()->getClientScript()->registerScriptFile('https://yastatic.net/s3/front-maps-static/front-maps/build/client/index/chunks/orgpage/e68e3e3dff14b1b7dd9a.ru.js');
Yii::app()->getClientScript()->registerCssFile('https://yastatic.net/s3/front-maps-static/front-maps/build/client/index/chunks/orgpage/e68e3e3dff14b1b7dd9a.css');

Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/reviews-yandex.css');
require_once 'phpQuery.php';
?>
<style>
	.business-review-view__reactions-container,
	.business-review-view__commentator{
		display: none;
	}
	.business-review-view__info{
		margin-bottom: 40px;
	}
	.yandex-reviews__btn{
		background: #13699c;
	    padding: 10px 25px;
	    color: #fff;
	    border-radius: 5px;
	    border: none;
	    margin: 30px auto 30px auto;
	    width: 270px;
	    text-align: center;
	}
    .average-rating{
        color: #0A6C93;
        font-weight: 700;
        font-size: 40px;
        line-height: 45px;
    }
    .count-rating {
        color: #0A6C93;
        font-weight: 500;
        font-size: 20px;
        line-height: 30px;
        margin: 0 0 30px;
    }
</style>

<div class="container">
    <!-- в этом месте, если что,хлебн.крошки -->
    <!-- <h1 class="text-center">Отзывы и оценки Яндекс</h1> -->

<?php
// $url = 'https://yandex.ru/maps/org/avantis/121107211633/?ll=55.176528%2C51.815822&z=17';
//    $file = file_get_contents($url);
//    $doc = phpQuery::newDocument($file);
//    $tr = $doc->find('.reviews-view__review');
//    echo "<div class='col-md-12'>";
//    echo $tr;
//    echo " <a target='_blank' href='https://yandex.ru/maps/org/avantis/121107211633/?ll=55.176528%2C51.815822&z=17'>
//    			<div class='yandex-reviews__btn'>Больше отзывов на Яндекс</div>
//    	   </a>";
//    echo "</div>";

?>

    <h1 class="p7">Отзывы на сайте</h1>

    <div class="average-rating">
        Средняя оценка <?= round($model->averageRating(), 2); ?> из 5
    </div>
    <div class="count-rating">
        Всего <?= $model->countRating(); ?> отзывов об уборке
    </div>

    <?php $this->widget(
        'application.components.FtListView',
        [
            'dataProvider' => $dataProvider,
            'emptyText' => '',
            'id' => 'review-box',
            'itemView' => '_view',
            'summaryText' => '',
            'template'=>'{items} {pager}',
            'itemsCssClass' => 'review-page',
            'ajaxUpdate'=>'true',
            'pagerCssClass' => 'pagination-box',

            'emptyTagName' => 'div',
            'emptyCssClass' => 'empty-form',
            'ajaxUpdate'=>true,
            'enableHistory' => false,
            // 'pager' => [
            //     'header' => '',
            //     'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
            //     'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
            //     'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            //     'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            //     'maxButtonCount' => 5,
            //     'htmlOptions' => [
            //         'class' => 'pagination'
            //     ],
            // ]

            'pager' => [
                'class' => 'application.components.ShowMorePager',
                'buttonText' => 'Показать ещё',
                'wrapTag' => 'div',
                'htmlOptions' => [
                    'class' => 'but-link'
                ],
                'wrapOptions' => [
                    'class' => 'review-pagination'
                ],
            ]
        ]
    ); ?>

    <div class="review-link">
        <h2>Читайте больше отзывов о нас</h2>
        <?php $this->widget('application.modules.gallery.widgets.GalleryWidget', ['galleryId' => 12, 'view' => 'review-links']) ?>
    </div>
</div>

<div class="review-form_wrap">
	<div class="container">
		<div class="review-form">
            <h2>Оставьте свой отзыв о нашей клинике</h2>
            <?php $this->widget('application.modules.review.widgets.ReviewWidget',['view' => 'reviewformwidget']); ?>
		</div>
	</div>
</div>