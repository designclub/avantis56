<div class="review-page__items">
	<div class="review-page__info" itemscope itemtype="https://schema.org/Review">
		<div itemprop="itemReviewed" itemscope="" itemtype="https://schema.org/Organization">
			<meta itemprop="name" content="Avantis">
		</div>
		<div class="review-page__name" itemprop="author" itemscope itemtype="https://schema.org/Person">
			<span itemprop="name"><?php echo CHtml::encode( $data->username); ?></span>
			<?php
				$strtotime = strtotime($data->date_created);
				$data_create = date("d.m.yy", $strtotime);
			?>
			<div class="date"><?=$data_create; ?></div>
		</div>

		<meta itemprop="datePublished" content="<?=$data_create; ?>"/>
		<div itemprop="itemReviewed" itemscope="" itemtype="https://schema.org/Organization">
	        <meta itemprop="name" content="<?= $this->title; ?>">
	    </div>

	    <div class="review-box__raiting-preview review-raiting">
	        <div class="raiting-list" itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">
	            <?php for ($i=1; $i <= 5; $i++) : ?>
	                <div class="raiting-list__item <?= ($i <= $data->rating) ? 'active' : ''; ?>"></div>
	            <?php endfor; ?>
		        <meta itemprop="worstRating" content="1">
		        <meta itemprop="ratingValue" content="<?= $data->rating ?>">
		        <meta itemprop="bestRating" content="5"/>
	        </div>
	    </div>
		<div class="review-page__text" itemprop="reviewBody">
			<p><?php echo $data->text; ?></p>
		</div>
	</div>
</div>