<?php
    Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'action'      => Yii::app()->createUrl('/review/create/'),
            'id'          => 'review-form',
            'type'        => 'vertical',
            'htmlOptions' => [
                'class' => '', 
                'data-type' => 'ajax-form',
                'enctype' => 'multipart/form-data'
            ],        
        )
    ); ?>
    <?= $form->hiddenField($model, 'rating'); ?>
    <?= $form->error($model, 'rating'); ?>
    <div class="raiting-form">
        <div class="raiting-form__header">
            Оцените работу клиники от 1 до 5
        </div>
        <div class="raiting-form__co">
            <div class="raiting-form__list raiting-list raiting-list-form">
                <div class="raiting-list__item" data-id="1"></div>
                <div class="raiting-list__item" data-id="2"></div>
                <div class="raiting-list__item" data-id="3"></div>
                <div class="raiting-list__item" data-id="4"></div>
                <div class="raiting-list__item" data-id="5"></div>
            </div>
            <div class="raiting-form__mes js-raiting-mes"></div>
        </div>
    </div>
    <div class="row-input">
        <div class="input-wrapp">
            <?= $form->textFieldGroup($model, 'username', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'data-original-title' => $model->getAttributeLabel('username'),
                        'data-content'        => $model->getAttributeDescription('username'),
                        'autocomplete' => 'off'
                    ],
                ],
            ]); ?>
        </div>
        <div class="input-wrapp">
            <?= $form->textFieldGroup($model, 'useremail', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'data-original-title' => $model->getAttributeLabel('useremail'),
                        'data-content'        => $model->getAttributeDescription('useremail'),
                        'autocomplete' => 'off'
                    ],
                ],
            ]); ?>
        </div>
    </div>
<div class="row-textarea">
    <?= $form->textAreaGroup($model, 'text', [
        'widgetOptions' => [
            'htmlOptions' => [
                'data-original-title' => $model->getAttributeLabel('text'),
                'data-content'        => $model->getAttributeDescription('text'),
                'autocomplete' => 'off'
            ],
        ],
    ]); ?>


   
<!--     <div class="file-upload">
        <label class="vision">
            <?//= $form->fileField($model, 'image'); ?>
            <span><i class="fa fa-paperclip" aria-hidden="true"></i> 
                <div id="count_file">
                    Прикрепить фото
                </div></span>
        </label>
    </div>
    <script type="text/javascript">
        $(document).ready( function() {
            $(".file-upload input[type=file]").change(function(){
                var inputFile = document.getElementById('Review_image').files;
                if(inputFile.length > 0){
                    $("#count_file").text('Выбрано файлов ' + inputFile.length);
                }else{
                    $("#count_file").text('Прикрепить фото');
                }
            });
        });
    </script> -->
    
    <div class="politika-conf">
        * Нажимая на кнопку "Отправить", я даю согласие на обработку моих
        персональных данных в соответствии с <a target="_blank" rel="nofollow" href="https://avantis56.loc/politika-konfidencialnosti-personalnyh-dannyh">Соглашением об <br>обработке персональных данных</a>
    </div>

    <div class="form-bot">
        
        <div class="form-captcha">
            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key'] ?>">
            </div>
            <?= $form->error($model, 'verifyCode');?>
        </div>
        <div class="form-button">
            <?= CHtml::submitButton('Оставить отзыв', [
                'id' => 'reviewZayavka-button', 
                'class' => 'revBtn', 
                 'data-send'=>'ajax'
            ]) ?>    
        </div>
    </div>
    </div>

<?php if (Yii::app()->user->hasFlash('review-success')): ?>
    <div id="messageModal" class="modal fade in" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Закрыть">
                        <span aria-hidden="true">&#10006;</span>
                    </button>
                    <h4 class="modal-title">Благодарим за ваш отзыв</h4>
                </div>
                <div class="modal-body">
                    Для нас очень важно ваше мнение.<br> 
                    После проверки модератором ваш отзыв появится на сайте.
                </div>
                <button class="close modal-button" type="button" data-dismiss="modal"
                    aria-label="Закрыть">
                    <span aria-hidden="true">Закрыть</span>
                </button>
            </div>
        </div>
    </div>
    <script>
        $('#messageModal').modal('show');
        setTimeout(function(){
            $('#messageModal').modal('hide');
        }, 5000);
    </script>
<?php endif ?>
<?php $this->endWidget(); ?>




