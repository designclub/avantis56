
<div class="review-box review-carousel">
	<?php foreach ($model as $key => $item) : ?>

			<div class="review-box__item">

				    <div class="rev_img">
                                        <?php if($item->image){
                                                echo CHtml::image($item->getImageUrl(145, 127, true),'');
                                            }else{
                                                echo CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.jpg', '', ['class'=>'img-responsive']);
                                            }										        ?>
				   </div>
					<div class="rev_body_wrap">
						<div class="rev_body">
							<h3><?= $item->username; ?></h3>
                                                        <div class="rev_text"><?= $item->getShortText(300); ?></div>
						</div>
					</div>

			</div>

	<?php endforeach; ?>
</div>

<?php
	Yii::app()->clientScript->registerScript("sl", "
		$('.review-carousel').slick({
		    infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: false,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
       dots: true,
		responsive: [
            {
                breakpoint: 1000,
                settings: {
                   	slidesToShow: 1,
        			slidesToScroll: 1,
        			adaptiveHeight: true
                }
            },
        ]
	});

	");
 ?>
