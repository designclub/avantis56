<div id="reservationModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Закрыть">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Забронировать путевку в лагерь</h4>
            </div>
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                        'id'=>'reservation-form-modal',
                        'type' => 'vertical',
                        'htmlOptions' => ['class' => 'form', 'data-type' => 'ajax-form'],
                    ]); ?>

                    <?php if (Yii::app()->user->hasFlash('reservation-success')): ?>
                        <div class="modal-success-message" style="color: green; text-align: center; padding: 35px 0px; font-size: 16px;">
                            <?= Yii::app()->user->getFlash('reservation-success') ?>
                        </div>
                        <script>
                            $('.modal-body').hide();
                            $('.modal-footer').hide();
                            setTimeout(function(){
                                $('#reservationModal').modal('hide');
                            }, 4000);
                           setTimeout(function(){
                                $('.modal-success-message').remove();
                                $('.modal-body').show();
                                $('.modal-footer').show();
                            }, 5000);
                        </script>
                    <?php endif ?>

                    <div class="modal-body">
                        <?= $form->textFieldGroup($model, 'name'); ?>
                        <?= $form->maskedTextFieldGroup($model, 'phone', [
                            'widgetOptions' => [
                                'mask' => '+7(999)999-99-99',
                                'htmlOptions'=>[
                                    'class' => 'data-mask',
                                    'data-mask' => 'phone',
                                    'placeholder' => 'Ваш телефон',
                                ]
                            ]
                        ]); ?>
                        <?= $form->textFieldGroup($model, 'email'); ?>
                        <?= $form->textAreaGroup($model, 'comment'); ?>
                        <?= $form->dropDownListGroup($model, 'smen', [
                            'widgetOptions' => [
                                'data' => $model->getSmenyList(),
                                'htmlOptions' => [
                                    'empty' => '-- выберите смену --',
                                    'encode' => false,
                                ],
                            ],
                        ]); ?>
                        <?= $form->textFieldGroup($model, 'fio'); ?>
                        <div class="row">
                            <div class="captha-box col-sm-8">
                                <div class="captha-box__in" id="captha-box__in3">
                                    <?php $this->widget(
                                        'CCaptcha',
                                        [
                                            'captchaAction' => '/site/captcha',
                                            'showRefreshButton' => true,
                                            'imageOptions' => [
                                                'width' => '100',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default',
                                                'id'    => 'captha__in3'
                                            ],
                                            'buttonLabel' => '<i class="glyphicon glyphicon-repeat"></i>',
                                        ]
                                    ); ?>
                                </div>
                                <div class="captha-box__in2">
                                    <?= $form->textFieldGroup(
                                        $model,
                                        'verifyCode',
                                        ['hint' => '']
                                    ); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?= CHtml::submitButton('Отправить', ['id' => 'reservation-button', 'class' => 'btn btn-default reservation-form-button', 'data-send'=>'ajax']) ?>
                            </div>
                        </div>
                        <div class="terms_of_use"> * Нажимая на кнопку "Отправить", я даю согласие на обработку моих персональных данных в соответствии с Соглашением об обработке персональных данных</div>
                    </div>
                <?php $this->endWidget(); ?>
        </div>
    </div>
</div>