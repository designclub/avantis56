<noindex>
                <div class="modal-title" style="font-size: 20px;">Запись на приём</div>

                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                        'id'=>'callback-service-form-modal',
                        'type' => 'vertical',
                        'htmlOptions' => ['class' => 'form', 'data-type' => 'ajax-form'],
                    ]); ?>
                    <div class="modal-body">
                        <?= $form->textFieldGroup($model, 'name', [
                            'widgetOptions'=>[
                                'htmlOptions'=>[
                                    'class' => '',
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>

                       <div class="form-group">
                            <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                            <?php $this->widget('CMaskedTextFieldPhone', [
                                'model' => $model,
                                'attribute' => 'phone',
                                'mask' => '+7(999)999-99-99',
                                'htmlOptions'=>[
                                    'class' => 'data-mask form-control',
                                    'data-mask' => 'phone',
                                    'placeholder' => 'Телефон',
                                    'autocomplete' => 'off'
                                ]
                            ]) ?>
                        </div>

                        <?= $form->dropDownListGroup($model, 'services', [
                            'widgetOptions' => [
                                'data' => $model->getServicesList(),
                            ]
                        ]) ?>

                        <?= $form->hiddenField($model, 'verify'); ?>
                        <div class="form-bot">
                            <div class="form-captcha">
                                <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key'] ?>"></div>
                                <?= $form->error($model, 'verifyCode');?>
                            </div>
                            <div class="form-button">
                                <?= CHtml::submitButton('Отправить', [
                                    'id' => 'callback-modal-button',
                                    'class' => '',
                                    'data-send'=>'ajax'
                                ]) ?>
                            </div>
                        </div>
                         <a href="/politika-konfidencialnosti-personalnyh-dannyh" rel="nofollow" class="terms_of_use" target="_blank"> * Нажимая на кнопку "Отправить", я даю согласие на обработку моих персональных данных в соответствии с Соглашением об обработке персональных данных</a>
                    </div>


                                            <?php if (Yii::app()->user->hasFlash($sucssesId)): ?>
                                                <div id="messageModalservice" class="modal fade in" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Закрыть">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                <h4 class="modal-title">Уведомление!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <?= Yii::app()->user->getFlash($sucssesId) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script>
                                                    $('#messageModalservice').modal('show');
                                                    setTimeout(function(){
                                                        $('#messageModalservice').modal('hide');
                                                    }, 5000);
                                                </script>
                                            <?php endif ?>


                <?php $this->endWidget(); ?>
</noindex>
        </div>
    </div>
</div>
