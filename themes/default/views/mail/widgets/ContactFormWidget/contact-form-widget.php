<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id'=>'form-qwest',
    'type' => 'vertical',
    'htmlOptions' => ['class' => 'form', 'data-type' => 'ajax-form'],
]); ?>

<div class="row-flex">
    <noindex>
        <h2>Написать нам</h2>
        <div class="inputs-wrap">
            <div class="col-form">
                <?= $form->textFieldGroup($model, 'name', [
                        'widgetOptions'=>[
                        'htmlOptions'=>[
                            'class' => '',
                            'autocomplete' => 'off'
                        ]
                    ]
                ]); ?>
            </div>
            <div class="col-form">
                <div class="form-group">
                    <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                    <?php $this->widget('CMaskedTextFieldPhone', [
                            'model' => $model,
                            'attribute' => 'phone',
                            'mask' => '+7(999)999-99-99',
                            'htmlOptions'=>[
                            'class' => 'data-mask form-control',
                            'data-mask' => 'phone',
                            'placeholder' => 'Телефон',
                            'autocomplete' => 'off'
                        ]
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="txt-wrap">
            <div class="txtar">
                <?= $form->textAreaGroup($model, 'body') ?>
            </div>
            <div class="dflex">
                <?= $form->hiddenField($model, 'code'); ?>
                    <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key'] ?>"></div>
                <?= $form->error($model, 'verifyCode');?>
            </div>
            <div class="sub_btn">
                <?= CHtml::submitButton('Отправить', [
                    'id' => 'form-sender',
                    'class' => 'btn sub_but',
                    'data-send'=>'ajax'
                ]) ?>
                <div class="soglasie">
                    <input type="checkbox" style="margin-right: 10px;">Согласие на обработку <?= Chtml::link('персональных данных', 'https://avantis56.ru/politika-konfidencialnosti-personalnyh-dannyh', ['rel' => 'nofollow']) ?>
                </div>
            </div>
        </div>

        <div class="gurle"></div>
    </noindex>
</div>

<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div id="messageModal" class="modal fade in" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Закрыть">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-title" style="font-size: 20px;">Уведомление!</div>
                </div>
                <div class="modal-body">
                    <?= Yii::app()->user->getFlash('success') ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#messageModal').modal('show');
        setTimeout(function(){
            $('#messageModal').modal('hide');
        }, 5000);
    </script>
<?php endif ?>

<?php $this->endWidget() ?>


