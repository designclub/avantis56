<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title ?: $page->title;
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>
<div class="main-banner">
    <?php $this->widget('application.modules.gallery.widgets.GalleryWidget', ['galleryId'=>1,'view'=>'banner']) ?>
</div>
<div class="main-page">
    <h2 class="text-center">о клинике</h2>
    <div class="container">
        <div class="row-page">
            <div class="row-page_img">
                <?= CHtml::image($page->getImageUrl(378,343)); ?>
            </div>
            <div class="row-page_content">
                <?= $page->body; ?>
            </div>
        </div>
    </div>
</div>
<div class="pages">
    <div class="container">
        <h2 class="text-center">Направления работы</h2>
        <div class="row">
            <div class="col-md-12">
               <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => 2]) ?>
             </div>
        </div>
    </div>
</div>
<div class="advantages">
    <h2 class="text-center">
        Почему мы?
    </h2>
    <div class="container">
        <div class="row">
         <div class="col-md-12">
             <?php $this->widget('application.modules.gallery.widgets.GalleryWidget',['galleryId' => 3, 'view' => 'advents']); ?>
         </div>
        </div>
    </div>
</div>
<div class="team">
     <h2 class="text-center">Наша команда</h2>
    <div class="container">
        <div class="row">
            <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => 4,'view'=>'team']) ?>
            <div class="page-button">
                <a href="/team" rel="nofollow">Все специалисты</a>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="row-page">
        <?= $page->short_content; ?>
    </div>
</div>
 <div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php $this->widget('application.modules.mail.widgets.ContactFormWidget', ['view' => 'contact-form-widget']); ?>
            </div>
        </div>

    </div>
</div>


