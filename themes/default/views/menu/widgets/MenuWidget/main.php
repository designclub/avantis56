<?php
$this->widget(
        'zii.widgets.CMenu', [
    'items' => $this->params['items'],
    'htmlOptions' => [
        'id' => 'menu_header'
    ]
        ]
);
