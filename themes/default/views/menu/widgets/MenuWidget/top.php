<?php
$this->widget(
    'zii.widgets.CMenu',
    [
        'items' => $this->params['items'],
        'htmlOptions' => [
            'class'=>'menu_top',
            'id'=>'menuTop'
        ]
    ]
);
