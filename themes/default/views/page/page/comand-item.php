<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>

<div class="main-page">
    <h2 class="text-center"><?= $model->title; ?></h2>
    <div class="container">
        <div class="comand-page">
            <div class="comand_img">
                <?= CHtml::image($model->getImageUrl(285,329)); ?>
            </div>
            <div class="comand_content">
                <?= $model->body; ?>
            </div>
        </div>
    </div>
</div>
