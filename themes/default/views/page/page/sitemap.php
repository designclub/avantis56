<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center" id="sitemap_head"><span><?= $model->title; ?></span></h1>
        </div>
    </div>
</div>
<div class="page_content">
    <div class="container bg_map">
        <div class="row">
            <div class="col-md-12">
              <div class="sw">
                <ul class="sitemap_pages">
                    <div class="li_item_wrap">
                        <li class="level_top"><a href="/">Главная</a></li>

                        <li class="level_top">
                            <a href="services">Услуги</a>
                        <?php $this->widget('application.modules.page.widgets.PagesWidget',['parent_id' => 2, 'view' => 'sitemap']); ?>
                        </li>

                        <li class="level_top"><a href="actions">Акции</a></li>

                        <li class="level_top"><a href="prays">Прайс</a></li>

                        <li class="level_top"><a href="prays">Наши работы</a></li>
                   </div>
                   <div class="li_item_wrap">
                    <li class="level_top"><a href="prays">Команда</a>
                        <?php $this->widget('application.modules.page.widgets.PagesWidget',['parent_id' => 4, 'view' => 'sitemap']); ?>
                    </li>

                    <li class="level_top"><a href="news">Новости</a>
                      <?php $this->widget('application.modules.news.widgets.LastNewsWidget'); ?>
                    </li>

                    <li class="level_top"><a href="review">Отзывы</a></li>

                    <li class="level_top"><a href="contacti">Контакты</a></li>
                    <li class="level_top">Пациентам
                        <?php $this->widget('application.modules.page.widgets.PagesWidget',['category_id' => 1, 'view' => 'sitemap']); ?>
                    </li>
                    <li class="level_top">Врачам
                        <?php $this->widget('application.modules.page.widgets.PagesWidget',['category_id' => 2, 'view' => 'sitemap']); ?>
                    </li>
                </div>

                </ul>
               </div>
            </div>
        </div>
    </div>
</div>
<div class="contacts">
    <div class="container">
        <?php $this->widget('application.modules.mail.widgets.ContactFormWidget', ['view' => 'contact-form-widget']); ?>
    </div>
</div>