<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
	$this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center"><?= $model->title; ?></h1>
		</div>
	</div>
</div>
<div class="page_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<main>
					 <div class="our_contacts" itemscope itemtype="http://schema.org/Organization">
						<div class="adress"><p itemprop="name">Клиника эстетической и функциональной стоматологии Авантис</p></div>
						<div class="adress" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<div class="name">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
									Адрес
							</div>
							<div class="detail">
								<div itemprop="addressLocality">460021</div>
								<div itemprop="addressLocality">город: Оренбург</div>
								<div itemprop="streetAddress">проспект: Гагарина</div>
								<div itemprop="streetAddress">дом: 7/1</div>
							</div>
						</div>
						<div class="adress">
							<div class="name">
									<i class="fa fa-phone-square" aria-hidden="true"></i>
									Телефон
							</div>
							<div class="detail">
								<a href="tel:+73532662266" itemprop="telephone">+7(3532) 662266</a>
								<a href="tel:+73532972266" itemprop="telephone">+7(3532) 972266</a>
							</div>
						</div>
						<div class="adress">
							<div class="name">
									<i class="fa fa-envelope" aria-hidden="true"></i>
									Email
							</div>
							<div class="detail">
								<a href="mailto:avantisdent@gmail.com" itemprop="email">avantisdent@gmail.com</a>
							</div>
						</div>
						<div class="adress">
							<div class="name">
									Режим работы:
							</div>
							<div class="detail">
								будни с 8:00 до 20:00, суббота с 10:00 до 16:00
							</div>
						</div>
						<div class="adress">
							<div class="name-social">
									Мы в социальных сетях:
							</div>
							<div class="soc-detail">
								<div class="soc-detail-item">
									<?= CHtml::link(CHtml::image('/img/vkontakte.png'), 'https://vk.com/avantis_dent') ?>
								</div>
								<div class="soc-detail-item">
									<?= CHtml::link(CHtml::image('/img/instagram.png'), 'https://www.instagram.com/avantis_dent/') ?>
								</div>
							</div>
						</div>
						<div class="adress">
							<div class="name-social">
									Написать нам в мессенджерах:
							</div>
							<div class="soc-detail">
								<div class="soc-detail-item">
									<?= CHtml::link(CHtml::image('/img/viber.png'), 'viber://add?number=79878472266') ?>
								</div>
								<div class="soc-detail-item">
									<?= CHtml::link(CHtml::image('/img/watsap.png'), 'whatsapp://send?phone=+79878472266') ?>
								</div>
							</div>
						</div>
					 </div>
				<div class="avantis_img">
					<?= CHtml::image($model->getImageUrl()) ?>
				</div>
			</main>
			<div class="contact">
				<h3 class="contact__title">Реквизиты</h3>
				<div class="contact__info">
					<div class="contact__info-item">
						<div class="contact-info__title">
							Наименование
						</div>
						<div class="contact-info__desc">
							Общество с ограниченной ответственностью «Авантис»
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
							ОГРН
						</div>
						<div class="contact-info__desc">
							1185658003453
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
							ИНН
						</div>
						<div class="contact-info__desc">
							5610230406
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
							КПП
						</div>
						<div class="contact-info__desc">
							561001001
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
							Юридический адрес:
						</div>
						<div class="contact-info__desc">
							Оренбургская область, город Оренбург, улица Транспортная, дом 18/3 квартира 103
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
							Фактический адрес:
						</div>
						<div class="contact-info__desc">
							Оренбургская об-ласть, город Оренбург, проспект Гагарина, 7/1
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
							Расчетный счет
						</div>
						<div class="contact-info__desc">
							40702810146000006301
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
							БИК
						</div>
						<div class="contact-info__desc">
							045354601
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
							Корреспондентсткий счет
						</div>
						<div class="contact-info__desc">
							30101810600000000601
						</div>
					</div>
				</div><hr>

				<h3 class="contact__title">Общественный транспорт</h3>
				<div class="contact__info">
					<div class="contact__info-item">
						<div class="contact-info__title">
							Автобусы
						</div>
						<div class="contact-info__desc">
							28, 13, 20, 62, 59, 52, 51, 40, 49т, 67, 36т, 57, 39, 33т, 32, 41, 33, 61, 38, 30
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
							Тролейбусы
						</div>
						<div class="contact-info__desc">
							12
						</div>
					</div>
				</div>

				<h3 class="contact__title">Остановки рядом</h3>
				<div class="contact__info">
					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							ГИБДД (ул. Транспортная)
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							АТБ-7
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							МУП ПА-1
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							13-й микрорайон
						</div>
					</div>

					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							Магазин Уют
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							ТК Оренбург
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							Липовая
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							14-й микрорайон
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							Джангильдина
						</div>
					</div>
					<div class="contact__info-item">
						<div class="contact-info__title">
						</div>
						<div class="contact-info__desc">
							ТК Максимум
						</div>
					</div>
				</div>
			</div>
			<div class="map_box">
				<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A6d2563dee3195bb7a3fc9d94fca63491e009ba6b4d8c4b0fe3e5b45d7f5da1b8&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
			</div>
			</div>
		</div>
	</div>
</div>