<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>

<div class="container" style="position: relative">
    <div class="row">
      <div class="col-md-12">
        <div class="toggler_service">
          <div class="toggler_box">
            <span></span>
          </div>
        </div>
      </div>
        <div class="col-md-4 service-block">
           <?php if (Yii::app()->hasModule('menu')): ?>
                <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'blok-uslug','view' => 'service']); ?>
            <?php endif; ?>

        </div>
        <div class="col-md-8 service-content">
            <h1><?= $model->title; ?></h1>
               <?php if (!empty($model->short_content)): ?>
                   <div class="short_description">
                        <?= $model->short_content; ?>
                   </div>
               <?php endif ?>
                 <div class="btn-z">
                    <a class="but-z" data-target="#callbackServiceModal" data-toggle="modal" href="#">
                    Записаться на приём
                     </a>
                </div>
                <div class="service_full_description">
                     <?= $model->body; ?>
                </div>
        </div><!--/col-md-8 service-content-->
    </div>
</div>
<?php if (!empty($model->price_box)): ?>
    <div class="price_tables">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="prices">Прайс услуг</h2>
                <?= $model->price_box ?>
            </div>
        </div>
    </div>
</div>
<?php endif ?>
<div class="team team_in_pages">
      <div class="container">
           <div class="row">
               <div class="col-md-12">
                 <h2>Команда специалистов</h2>
               </div>
           </div>
       </div>

    <div class="container">
        <div class="row">
                <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => 4,'view'=>'team']) ?>
                <div class="page-button">
                    <a href="/team" rel="nofollow">Все специалисты</a>
                </div>
            </div>
    </div>
</div>
<div class="articles_wrap">
    <noindex>
    <div class="container">
       <div class="row">
           <div class="col-md-12">
                <h2>Полезные статьи</h2>
           </div>
       </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php $this->widget('application.modules.news.widgets.LastNewsWidget',['view' => 'articles', 'limit' => 3]); ?>
            </div>
        </div>
    </div>
    </noindex>
</div>
<div class="pages">
       <div class="container">
            <div class="row">
                <div class="col-md-12">
                 <h3><span>Смотрите также</span></h3>
                </div>
            </div>
        </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => 2, 'order' => 'rand()', 'limit' => 4]) ?>
             </div>
        </div>
    </div>
</div>
 <div class="contacts">
    <div class="container">
        <?php $this->widget('application.modules.mail.widgets.ContactFormWidget', ['view' => 'contact-form-widget']); ?>
    </div>
</div>
<?php $this->widget('application.modules.review.widgets.ReviewWidget',['view' => 'reviewmodalwidget']); ?>


         <?php /*
            <h2 class="rev-head">отзывы</h2>
            <div class="r_box">
                <?php $this->widget('application.modules.review.widgets.ReviewNewWidget',['view' => 'review-carousel']); ?>
            </div>
            <button type="button" class="btn-rev" data-toggle="modal" data-target="#reviewZayavkaModal">
                        Оставить отзыв
            </button>
           */;?>

<script>
    $('blockquote a').on('click',function(e){
    e.preventDefault();
    var target = $(this).attr('href'),
        targetBlock = $(target).offset().top;
        $('html,body').animate({
            scrollTop:targetBlock
        },1000)

})
</script>