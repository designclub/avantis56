<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center" style="color: #083753;"><?= $model->title_short; ?></h1>
        </div>
    </div>
</div>
<div class="page_content mb3rem">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $model->body; ?>
                <div class="team">
                <div class="page-button">
                    <a href="#" class="repeat_captha" data-target="#callbackServiceModal" data-toggle="modal" href="#" tabindex="0">
                        Записаться на приём
                    </a>
                </div>
                </div>
            </div>
        </div>

        <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => 2,'view'=>'price-link']) ?> 
    </div>
</div>
<script>
    function tableSearch() {
    var phrase = document.getElementById('search-text');
    var table = document.getElementById('info-table');
    var regPhrase = new RegExp(phrase.value, 'i');
    var flag = false;
    for (var i = 1; i < table.rows.length; i++) {
        flag = false;
        for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
            flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
            if (flag) break;
        }
        if (flag) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }

    }
}

$(document).on("scroll", function(){
    var scrollNow = $(this).scrollTop(),
        search = $('.search-container').offset().top;

    if (scrollNow > search) {
      $('.search-container').addClass('border').find('.search_container').addClass('border');
    }
    else {
      $('.search_container').removeClass('border').find('.search_container').removeClass('border');
    }
})

</script>