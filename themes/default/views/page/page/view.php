<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

if ($model->canonical) {
    $this->canonical = $model->canonical; 
}
$this->title = $model->meta_title ?: $model->title;

$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;

$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';

?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center"><?= $model->title; ?></h1>
        </div>
    </div>
</div>
<div class="page_content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $model->body; ?>
            </div>
        </div>
    </div>
</div>