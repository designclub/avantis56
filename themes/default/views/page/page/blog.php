<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center"><?= $model->title_short; ?></h1>
        </div>
    </div>
</div>
<div class="blog_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="blog_contents">                
                    <div class="blog_img">
                        <?= CHtml::image($model->getImageUrl()) ?>
                    </div>
                    <div class="body">
                        <?= $model->price_box ?>
                    </div>
               </div>  
               <?= $model->body ?>
             </div>
        </div>
    </div>
</div>
<div class="contacts">
    <div class="container">
        <?php $this->widget('application.modules.mail.widgets.ContactFormWidget', ['view' => 'contact-form-widget']); ?>
    </div>
</div>
<?php $this->widget('application.modules.review.widgets.ReviewWidget',['view' => 'reviewmodalwidget']); ?>