<div class="courses-box__item">
    <?php $url = $item->getUrl(); ?>
    <?php if($item->childPages): ?>
        <?php foreach ($item->childPages(['order' => 'position ASC']) as $key => $value) : ?>
            <?php if($key == 0): ?>
                <?php $url = $value->getUrl(); ?>
            <?php endif; ?>
        <?php endforeach;?>
    <?php endif; ?>
    <a class="courses-box__link" href="<?= $url; ?>">
        <div class="courses-box__img">
            <?= CHtml::image($item->getIconUrl(), ''); ?>
        </div>
        <div class="courses-box__name">
            <?= $item->title_short?>
        </div>
    </a>
    <?php if($item->childPages): ?>
        <div class="courses-box__children">
            <div class="courses-children">
                <?php foreach ($item->childPages(['order' => 'position ASC']) as $key => $value) : ?>
                    <div class="courses-children__item">
                        <a class="courses-children__link" href="<?= $value->getUrl(); ?>"><?= $value->title_short; ?></a>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    <?php else: ?>
        <div class="courses-box__children">
            <div class="courses-children">
                <div class="courses-children__item">
                        <a class="courses-children__link" href="<?= $item->getUrl(); ?>"><?= $item->title_short; ?></a>
                    </div>
                </div>
        </div>
    <?php endif; ?>
</div>