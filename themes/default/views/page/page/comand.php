<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->n_nofollow = $model->noindex_nofollow ? 'noindex, nofollow' : '';
?>

<div class="main-page">
    <h1 class="text-center" style="margin: 7rem 0 5rem 0;"><?= $model->title; ?></h1>
    <div class="container">
        <div class="comand-page">
            <div class="comand_img">
                <?= CHtml::image($model->getImageUrl(285,329)); ?>
            </div>
            <div class="comand_content">
                <?= $model->body; ?>
            </div>
        </div>
    </div>
</div>
<section class="sertificate-single">
    <?php if (!empty($model->gallery_name)): ?>
    <div class="container">
        <h2>Сертификаты</h2>
        <?php $this->widget('application.modules.gallery.widgets.GalleryWidget',[
            'galleryId' => $model->gallery_name,
            'view' => 'sertificate'
            ]); ?>
    </div>
    <?php endif ?>
</section>
