<div class="services_page">
    <?php foreach ($pages as $key => $page) : ?>
      <div class="s_item">
            <div class="parent">
                <div class="parent_img">
                    <?= CHtml::image($page->getImageUrl(69,64), $page->title) ?>
                </div>

                <a class="parent_title" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
                    <?= $page->title_short ?>
                </a>
            </div>
            <div class="childs">
                <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => $page->id, 'view' => 'childs']) ?>
            </div>
      </div>
    <?php endforeach; ?>
</div>