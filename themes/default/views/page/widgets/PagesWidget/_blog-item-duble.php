<a class="item" href="<?= Yii::app()->createUrl('/poleznye-stati/' . $data->slug) ?>">
    <div class="item_img">
    <?= CHtml::image($data->getImageUrl(200,132,true), $data->title) ?>
    </div>
    <div class="content_wrap">
        <div class="item_title">
             <?= $data->title ?>
        </div>
        <div class="short_text">
          <?= $data->short_content ?>
        </div>
        <div class="detail">
            Подробнее
             <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </div>
    </div>
</a>
