
<ul>

<?php foreach ($pages as $key => $page) : ?>
  <li class="page_item">
    <a class="page_item_title" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
    <?= $page->title ?>
    </a>
    <?php if (!empty($page->childPages)): ?>
        <ul>
        <?php foreach ($page->childPages as $key => $child): ?>
            <li>
               <a class="page_item_title" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
            </li>
        <?php endforeach ?>
        </ul>
    <?php endif ?>
  </li>
<?php endforeach; ?>
</ul>