
<div class="services_page">

<?php foreach ($pages as $key => $page) : ?>
  <div class="s_item">
        <div class="parent">
            <div class="parent_img">
            <?= CHtml::image($page->getImageUrl(69,64), $page->title) ?>
            </div>

            <a class="parent_title" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
            <?= $page->title_short ?>
            </a>
        </div>
  </div>
<?php endforeach; ?>
</div>