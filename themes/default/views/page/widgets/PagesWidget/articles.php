
<div class="articles">
<?php foreach ($pages as $key => $page) : ?>
  <a class="item" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
    <div class="wrapp">
      <div class="item_title">
        <?= $page->title ?>
     </div>
    <div class="item_img">
    <?= CHtml::image($page->getImageUrl(0,0), $page->title) ?>
    <div class="hover" style="position: absolute">
        <div class="link_img">
            <?= CHtml::image('/img/document.svg') ?>
        </div>
    </div>
    </div>
</div>
    <div class="item_short">
         <div class="short_text"><?= $page->short_content ?></div>
         <div class="link_to_page">
           Читать
         </div>
    </div>
  </a>
<?php endforeach; ?>
</div>