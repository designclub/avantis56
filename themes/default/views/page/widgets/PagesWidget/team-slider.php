<div class="listPages">
<?php foreach ($pages as $key => $page) : ?>
  <div class="list_item">
    <div class="list_item_img">
    <?= CHtml::image($page->getImageUrl(285,343), $page->title) ?>
    </div>
  </div>
<?php endforeach; ?>
</div>

<?php Yii::app()->clientScript->registerScript("slick2", "
 $('.listPages').slick({

      infinite: true,
      slidesToShow: 3,
      arrows: true,
      dots:false,
      autoplay:true,
      autoplaySpeed:5000,
      responsive: [
    {
      breakpoint: 1200,
      settings: {
         slidesToShow: 3

      }
    },
    {
      breakpoint: 768,
      settings: {
         slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
         slidesToShow: 1,
         centerMode: true,
        centerPadding: '40px'
      }
    },
    {
      breakpoint: 361,
      settings: {
         slidesToShow: 1,
         centerMode: false
      }
    }
  ]
    });
");
 ?>

