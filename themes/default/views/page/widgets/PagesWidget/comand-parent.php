<div class="team_box">
<?php foreach ($pages as $key => $page) : ?>
    <a class="team_item"  onclick="<?php if (empty($page->short_content)){echo 'return false';} ?>" style="<?php if (empty($page->short_content)){echo 'cursor: default;';} ?>" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
        <div class="team_item_img">
            <?= CHtml::image($page->getImageUrl(), $page->title) ?>
        </div>
        <div class="wrap">
            <div class="team_item_title">
                <?= $page->title ?>
            </div>
            <div class="team_item_desc">
                <?= $page->title_short ?>
            </div>
            <div class="team_link" style="<?= empty($page->short_content) ? 'display: none' : ''; ?>">
                <div class="link_to_page">Подробнее
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </a>
<?php endforeach; ?>
</div>


