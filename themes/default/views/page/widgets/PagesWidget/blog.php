<div class="list_blog">
    <?php $this->widget('bootstrap.widgets.TbListView', [
        'itemView' => '_blog-item',
        'dataProvider' => $pages,
        'summaryText' => "Статьи {start}-{end} из {count} ",
    ]) ?>
</div>