
<ul class="pages_list">

<?php foreach ($pages as $key => $page) : ?>
  <li class="page_item">
    <a class="page_item_title" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
    <?= $page->title_short ?>
    </a>
  </li>
<?php endforeach; ?>
</ul>