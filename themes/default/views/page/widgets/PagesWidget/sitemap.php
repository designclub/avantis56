
<ul class="site_map_box">

<?php foreach ($pages as $key => $page) : ?>
  <li class="item">
            <a class="parent_title" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
            <?= $page->title ?>
            </a>
        <ul class="childs">
            <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id' => $page->id, 'view' => 'sitemap_childs']) ?>
        </ul>
  </li>
<?php endforeach; ?>
</ul>