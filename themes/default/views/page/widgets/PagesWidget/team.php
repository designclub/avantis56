<div class="listPages">
    <?php foreach ($pages as $key => $page) : ?>
        <div class="list_item">
            <div class="list_item_img">
                <img src="" data-src="<?= $page->getImageUrl(285,343); ?>" alt="<?= $page->title; ?>">
            </div>
            <div class="wrap">
                <div class="list_item_title">
                    <?= $page->title ?>
                </div>
                <div class="list_item_desc">
                    <span><?= $page->title_short ?></span>
                </div>
                <div class="list_link" style="<?= empty($page->short_content) ? 'display: none' : ''; ?>">
                    <a  href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>" rel="nofollow">Подробнее
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>



