<div class="pages_block">
<?php foreach ($pages as $key => $page) : ?>
  <a class="page_item" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
    <div class="page_item_img">
    <?= CHtml::image($page->getImageUrl(69,64), $page->title) ?>    
    </div>
    <div class="page_item_title">
    <?= $page->title_short ?>
    </div>
  </a>
<?php endforeach; ?>
</div>