<div class="container">
	<div class="footer_flex">
		<div class="foot_brand">
			<a href="/">
				<?= CHtml::image($this->mainAssets . '/images/logo.png') ?>
			</a>
		</div>
		<div class="foot_links">
			<?php if (Yii::app()->hasModule('menu')): ?>
			<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu','view' =>'footer']); ?>
		<?php endif; ?>
	</div>
	<div class="foot_info">
		<div class="info_item">
			<div class="wr_item">
				<i class="fa fa-phone" aria-hidden="true"></i>
				<a href="tel:+7353266-22-66" class="tel">+7(3532)66-22-66</a>
			</div>
			<div class="wr_item">
				<a href="mailto:avantisdent@gmail.com" class="mail">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				avantisdent@gmail.com</a>
			</div>
			<div class="wr_item">
				<a class="but-z repeat_captha" data-target="#callbackModal" data-toggle="modal" href="#">
					<i class="fa fa-phone-square repeat_captha" aria-hidden="true"></i>
				Заказать звонок</a>
			</div>
		</div>
	</div>
</div>
</div>

<?php Yii::app()->clientScript->registerScript("one-job", "
	$('.one_job h4').click(function(){
		$(this).next('.hid_text').toggle();
	});
"); ?>


