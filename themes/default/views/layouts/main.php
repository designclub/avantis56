<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
	<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

	<link rel="preconnect" href="https://yastatic.net">
	<link rel="preconnect" href="https://www.google.com">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://yastatic.net">
	<link rel="preconnect" href="https://mc.yandex.ru">
	<link rel="preconnect" href="https://www.gstatic.com">

	<meta name="yandex-verification" content="cc6136030278620e" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Language" content="ru-RU" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title><?= $this->title;?></title>
	<meta name="description" content="<?= $this->description;?>" />

	<?php if ($this->canonical): ?>
		<link rel="canonical" href="<?= $this->canonical ?>" />
	<?php endif; ?>

	<?php if ($this->n_nofollow): ?>
		<meta name="robots" content="<?= $this->n_nofollow; ?>" />
	<?php endif; ?>

	<?php
	Yii::app()->getClientScript()->registerCssFile('/css/index.min.css');
	//Yii::app()->getClientScript()->registerCssFile('/slick/slick.css');
	//Yii::app()->getClientScript()->registerCssFile('/slick/slick-theme.css');
	//Yii::app()->getClientScript()->registerCssFile('https://yastatic.net/s3/frontend/ugc/v1.48.0/desktop.css');
	// Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js', CClientScript::POS_END);
	Yii::app()->getClientScript()->registerScriptFile('/js/custom.min.js', CClientScript::POS_END);
	//Yii::app()->getClientScript()->registerScriptFile('/slick/slick.min.js', CClientScript::POS_END);
	?>

	<!-- <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> -->
	<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>
	<script>
		function loadCSS(hf) {
			var ms=document.createElement("link");ms.rel="stylesheet";
			ms.href=hf;document.getElementsByTagName("head")[0].appendChild(ms);
		}
		loadCSS("slick/slick.css"); //Load Libs CSS: Animate CSS
		loadCSS("slick/slick-theme.css"); //Load Libs CSS: Animate CSS
	</script>
	<div class="wrapper">
		<div class="content">
			<div class="visible-lg eye" style="position: absolute; right: 25px; top: 25px; z-index: 255;">
				<?= CHtml::link(CHtml::image($this->mainAssets . '/images/eye-white.png', '', ['height' => '40px;']), 'https://see.avantis56.ru'.$_SERVER['REQUEST_URI'], ['target' => '_blank', 'title' => "Версия для слабовидящих", "alt" => "Версия для слабовидящих", "rel" => "nofollow"]); ?>
			</div>

			<?php $this->renderPartial('//layouts/_header'); ?>

			<div class="container">
				<div class="row">
                    <?php $this->widget('application.components.MyTbBreadcrumbs', [
                        'links' => $this->breadcrumbs,
                    ]); ?>
					<?php
                    /*$this->widget(
						'bootstrap.widgets.TbBreadcrumbs',
						[
							'links' => $this->breadcrumbs,
						]
					);*/
                    ?>
				</div>
			</div>
			<?= $this->decodeWidgets($content); ?>
		</div>
		<div class="footer">
			<!-- footer -->
			<?php $this->renderPartial('//layouts/_footer'); ?>
			<!-- footer end -->
		</div>
		<div class="footer-bottom">
			<?php $this->renderPartial('//layouts/_copyright'); ?>
		</div>
	</div>

	<?php $this->widget('application.modules.mail.widgets.CallbackWidget'); ?>
	<?php $this->widget('application.modules.mail.widgets.CallbackWidget', ['model' => 'CallbackServicesFormModal', 'view' => 'callback-service-widget']); ?>
	<?php $this->widget('application.modules.review.widgets.ReviewWidget', ['view' => 'reviewmodalwidget']); ?>

<!-- 	<script type="text/javascript" >
		(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(54013438, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true });
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/54013438" style="position:absolute; left:-9999px;" alt="" /></div>
	</noscript> -->
	
	<!-- Yandex.Metrika counter --> 
	<script type="text/javascript" > 
		(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter54013438 = new Ya.Metrika({ id:54013438, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); 
	</script> 
	<noscript><div><img src="https://mc.yandex.ru/watch/54013438" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
	<!-- /Yandex.Metrika counter -->

	<script type="text/javascript">
		var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
		var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
	</script>
	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="https://yastatic.net/highlightjs/8.2/highlight.min.js"></script>
	<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>
	<!-- Load CSS compiled without Bootstrap & Header styles (after release) -->
	<script>var ms=document.createElement("link");ms.rel="stylesheet";
		// ms.href="compiled.min.css";document.getElementsByTagName("head")[0].appendChild(ms);
	</script>

</body>
</html>
