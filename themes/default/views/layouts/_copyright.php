
    <div class="container">
        <div class="row">
            <div class="copy-footer">
                <div class="copyright">
                    <span>(c) &laquo; Авантис&raquo;&nbsp;-&nbsp;<?php echo date("Y");?></span>
                </div>
                <div class="social">
                    <div class="head">
                        Написать нам
                    </div>
                    <div class="links">
                        <a data-toggle="tooltip" title="Viber" href="viber://add?number=79878472266"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        <a data-toggle="tooltip" title="Whatsapp" href="whatsapp://send?phone=+79878472266"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="dclub">
                    <div class="dimg">
                        <?= CHtml::image($this->mainAssets . '/images/dc.png') ?>
                    </div>
                    <div class="dlink">
                        <a href="https://dcmedia.ru/">создание и продвижение<br>сайтов в Оренбурге</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
