<header>
<div class="container">
	<div class="top-block">
		<div class="menuTop">
			<?php if (Yii::app()->hasModule('menu')): ?>
				<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'pacientam','view' => 'top']); ?>
			<?php endif; ?>
		</div>
		<div class="datas">
			<div class="adresses">
				<div class="wrap">
					<div class="">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						 460021, Оренбург, проспект Гагарина, 7/1
					</div>
					<div class="">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<a href="mailto:avantisdent@gmail.com">avantisdent@gmail.com</a>
					</div>
					<div class="search">
						<form action="/search" style="position: relative">
							<input type="text" name="q" placeholder="Поиск по сайту">
							<button type="submit" name="search" class="zend_button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</form>
					</div>
				</div>
			</div>
			<div class="brand">
				<a href="/">
				<?= CHtml::image($this->mainAssets . '/images/logo.png') ?>
				</a>
			</div>
			<div class="callbacks">
				<div class="wrap">
				<div class="">
					<a href="tel:+7353266-22-66" class="tel">+7(3532)66-22-66</a>
				</div>
				<div class="">
				   <a class="but-z repeat_captha" data-target="#callbackModal" data-toggle="modal" href="#">Заказать звонок</a>
				</div>
				<div class="soc">
					<!-- <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a> -->
					<a href="https://vk.com/avantis_dent" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
					<a href="https://www.instagram.com/avantis_dent/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				</div>
				</div>
			</div>
		</div>
		<div class="menuTop">
		   <?php if (Yii::app()->hasModule('menu')): ?>
			 <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'vracham','view' =>'top']); ?>
		   <?php endif; ?>
		</div>
	 </div>
</div>
</header>
<div class="fluid">
	<div class="main-menu">
		<div class="container">
			<?php if (Yii::app()->hasModule('menu')): ?>
			   <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
		   <?php endif; ?>
		</div>
	</div>
</div>
<div class="mobile-menu">
	<div class="mobile-flex">
		<div class="mmenu">
			<?php if (Yii::app()->hasModule('menu')): ?>
				<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'pacientam','view' => 'top']); ?>
			<?php endif; ?>
		</div>
		<div class="mmenu">
			<span class="span">Меню
			<i class="fa fa-chevron-down" aria-hidden="true"></i>
			</span>

			<?php if (Yii::app()->hasModule('menu')): ?>
			  <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
		   <?php endif; ?>
		</div>
		<div class="mmenu">
			<?php if (Yii::app()->hasModule('menu')): ?>
			 <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'vracham','view' =>'top']); ?>
		   <?php endif; ?>
		</div>
		<div class="mmenu hide_btn">
			<div class="btn-z">
				<a class="but-z" data-target="#callbackServiceModal" data-toggle="modal" href="#">
				  Записаться на приём
				</a>
			</div>
		</div>
	</div>
</div>