<?php
$this->title = Yii::app()->getModule('news')->metaTitle ?: Yii::t('NewsModule.news', 'News');
$this->description = Yii::app()->getModule('news')->metaDescription;
$this->keywords = Yii::app()->getModule('news')->metaKeyWords;

$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>
<div class="page_content">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center"><?= Yii::t('NewsModule.news', 'News') ?></h1>

<div class="list_blog">
    <?php $this->widget('bootstrap.widgets.TbListView', [
        'itemView' => '_item',
        'dataProvider' => $dataProvider,
        'summaryText' => "Новости {start}-{end} из {count} ",
    ]) ?>
</div>
        </div>
    </div>
</div>

