<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = $model->meta_title ?: $model->title;
$this->description = $model->meta_description;
$this->keywords = $model->meta_keywords;
?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
            $this->breadcrumbs = [
                Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
                $model->title
            ];
            ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="blog-page">
        <h1 class="blog-page__title">
            <?= $model->title_short; ?>
        </h1>
        <div class="blog_contents">
            <?=$model->full_text; ?>
            <div class="blog_contents-freebody">
            <?=$model->free_text; ?>
            </div>
        </div>
    </div>
</div>

<div class="articles_wrap">
      <div class="container">
           <div class="row">
               <div class="col-md-12">
                    <h2 style="text-transform: none;">Полезные статьи</h2>
               </div>
           </div>
       </div>
    <div class="container">
        <?php $this->widget('application.modules.news.widgets.LastNewsWidget',['view' => 'articles', 'limit' => 3, 'not_id'=>$model->id]); ?>
    </div>
</div>
<div class="contacts">
    <div class="container">
        <?php $this->widget('application.modules.mail.widgets.ContactFormWidget', ['view' => 'contact-form-widget']); ?>
    </div>
</div>
