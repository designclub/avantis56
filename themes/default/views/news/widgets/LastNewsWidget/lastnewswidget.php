<?php Yii::import('application.modules.news.NewsModule'); ?>

    <div class='portlet-content'>
        <?php if (isset($models) && $models != []): ?>
            <ul class="site_map_box">
                <?php foreach ($models as $model): ?>
                    <li><?= CHtml::link(
                            $model->title,
                            ['/news/news/view/', 'slug' => $model->slug]
                        ); ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
