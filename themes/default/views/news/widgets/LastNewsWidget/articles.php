<div class="articles">
<?php foreach ($models as $key => $new) : ?>
    <div class="articles-item">
        <div class="articles-item__image">
            <?= CHtml::image($new->getImageUrl(360, 160), $new->title) ?>
        </div>
        <div class="articles-item__content">
            <div class="item__content-title">
                <?= Chtml::link($new->title, Yii::app()->createUrl('/news/news/view', ['slug'=>$new->slug])); ?>
            </div>
            <div class="item__content-short">
                <?= $new->short_text ?>
            </div>
            <div class="item__content-more">
                <?=Chtml::link('&#8594; Читать далее', Yii::app()->createUrl('/news/news/view', ['slug'=>$new->slug])); ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>