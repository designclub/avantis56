-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июл 17 2020 г., 16:35
-- Версия сервера: 5.7.28
-- Версия PHP: 7.0.33-0+deb9u8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `avantis`
--

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_blog_blog`
--

CREATE TABLE `avantis_blog_blog` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_blog_post`
--

CREATE TABLE `avantis_blog_post` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_blog_post_to_tag`
--

CREATE TABLE `avantis_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_blog_tag`
--

CREATE TABLE `avantis_blog_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_blog_user_to_blog`
--

CREATE TABLE `avantis_blog_user_to_blog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_callback`
--

CREATE TABLE `avantis_callback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `url` text,
  `agree` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_category_category`
--

CREATE TABLE `avantis_category_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_category_category`
--

INSERT INTO `avantis_category_category` (`id`, `parent_id`, `slug`, `lang`, `name`, `image`, `short_description`, `description`, `status`) VALUES
(1, NULL, 'pacientam', 'ru', 'Пациентам', NULL, '', '', 1),
(2, NULL, 'vracham', 'ru', 'Врачам', NULL, '', '', 1),
(3, NULL, 'komanda', 'ru', 'Команда', NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_comment_comment`
--

CREATE TABLE `avantis_comment_comment` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_contentblock_content_block`
--

CREATE TABLE `avantis_contentblock_content_block` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_contentblock_content_block`
--

INSERT INTO `avantis_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(1, 'прайс гигиена и профилактика', 'prays-gigiena-i-profilaktika', 3, '<table class=\"prices-table\">\r\n                <tbody><tr>\r\n                    <th>вид работы</th>\r\n                    <th>цена</th>\r\n                </tr>\r\n                <tr>\r\n                    <td>профессиональная гигиена полости рта(стандартная)</td>\r\n                    <td>6780 руб</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>профессиональная гигиена полости рта(парадонтологическая)+Vector-терапия</td>\r\n                    <td>от 20 000 руб</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>профессиональная гигиена полости рта(стандартная)</td>\r\n                    <td>6000 руб</td>\r\n                </tr>\r\n            </tbody></table>', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_feedback_feedback`
--

CREATE TABLE `avantis_feedback_feedback` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_feedback_feedback`
--

INSERT INTO `avantis_feedback_feedback` (`id`, `category_id`, `answer_user`, `create_time`, `update_time`, `name`, `email`, `phone`, `theme`, `text`, `type`, `answer`, `answer_time`, `is_faq`, `status`, `ip`) VALUES
(1, NULL, NULL, '2020-05-07 17:48:48', '2020-05-07 17:48:48', 'hTDmksFucYBQKU', 'copelandimogn@gmail.com', '', 'FuZClMKwiVqco', 'WFDhAQnbmxEHfc', 0, '', NULL, 0, 0, '183.88.209.24'),
(2, NULL, NULL, '2020-05-07 17:48:50', '2020-05-07 17:48:50', 'eoEILgJyUnOm', 'copelandimogn@gmail.com', '', 'sInhbdLeq', 'nGSVwKEUzZpLy', 0, '', NULL, 0, 0, '183.88.209.24'),
(3, NULL, NULL, '2020-06-15 17:18:14', '2020-06-15 17:18:14', 'pWLQZXvNqDUH', 'janemarshall7393@gmail.com', '', 'jSKkEPed', 'ClbuXWZqfti', 0, '', NULL, 0, 0, '113.173.7.163'),
(4, NULL, NULL, '2020-06-15 17:18:15', '2020-06-15 17:18:15', 'jdiCPAYbqG', 'janemarshall7393@gmail.com', '', 'UPQXGEZtMW', 'upiRHxGMWe', 0, '', NULL, 0, 0, '113.173.7.163');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_gallery_gallery`
--

CREATE TABLE `avantis_gallery_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_gallery_gallery`
--

INSERT INTO `avantis_gallery_gallery` (`id`, `name`, `description`, `status`, `owner`, `preview_id`, `category_id`) VALUES
(1, 'Банер на главной', '<p>Банер на главной</p>', 1, 1, NULL, NULL),
(2, 'Гигиена и профилактика', '<p>Гигиена и профилактика</p>', 1, 1, NULL, NULL),
(3, 'Преимущества на главной', '<p>Преимущества на главной</p>', 1, 1, NULL, NULL),
(4, 'Примеры работ', '<p>Примеры работ</p>', 1, 1, NULL, NULL),
(5, 'Лицензии', '<p>Лицензии</p>', 1, 1, NULL, NULL),
(6, 'Слайдер - наша команда', '<p>Слайдер - наша команда</p>', 1, 1, NULL, NULL),
(7, 'Щетинин', '<p>Щетинин дипломы</p>', 1, 1, NULL, NULL),
(8, 'Патрушев', '<p>Патрушев</p>', 1, 1, NULL, NULL),
(9, 'Попов', '<p>Попов</p>', 1, 1, NULL, NULL),
(10, 'Березина', '<p>Березина</p>', 1, 1, NULL, NULL),
(11, 'Сведения', '<p>Сведения</p>', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_gallery_image_to_gallery`
--

CREATE TABLE `avantis_gallery_image_to_gallery` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_gallery_image_to_gallery`
--

INSERT INTO `avantis_gallery_image_to_gallery` (`id`, `image_id`, `gallery_id`, `create_time`, `position`) VALUES
(1, 1, 1, '2019-01-14 17:51:14', 2),
(2, 2, 1, '2019-01-14 17:51:14', 3),
(3, 3, 1, '2019-01-14 17:51:14', 1),
(4, 4, 2, '2019-01-20 17:21:03', 6),
(5, 5, 2, '2019-01-20 17:21:03', 5),
(6, 6, 2, '2019-01-20 17:21:03', 7),
(7, 7, 2, '2019-01-20 17:21:03', 4),
(12, 12, 2, '2019-01-20 17:40:32', 9),
(13, 13, 2, '2019-01-20 17:40:32', 11),
(14, 14, 2, '2019-01-20 17:40:32', 10),
(15, 15, 2, '2019-01-20 17:40:32', 8),
(16, 16, 3, '2019-06-04 16:01:39', 14),
(17, 17, 3, '2019-06-04 16:01:39', 12),
(18, 18, 3, '2019-06-04 16:01:39', 13),
(19, 19, 4, '2019-06-09 12:27:14', 15),
(20, 20, 4, '2019-06-09 12:27:14', 16),
(21, 21, 4, '2019-06-09 12:27:14', 17),
(22, 61, 5, '2019-07-17 16:10:24', 18),
(23, 62, 5, '2019-07-17 16:11:04', 19),
(24, 66, 6, '2019-09-18 15:00:00', 20),
(25, 67, 6, '2019-09-18 15:00:01', 21),
(26, 68, 6, '2019-09-18 15:00:01', 22),
(27, 69, 6, '2019-09-18 15:00:02', 23),
(28, 83, 7, '2020-05-26 23:38:12', 25),
(29, 84, 7, '2020-05-26 23:38:12', 30),
(30, 85, 7, '2020-05-26 23:38:12', 29),
(31, 86, 7, '2020-05-26 23:38:13', 31),
(33, 88, 7, '2020-05-26 23:38:13', 32),
(34, 89, 7, '2020-05-26 23:38:13', 26),
(35, 90, 7, '2020-05-26 23:38:14', 33),
(36, 91, 7, '2020-05-26 23:38:14', 27),
(37, 92, 7, '2020-05-26 23:42:55', 34),
(39, 94, 8, '2020-05-27 00:03:28', 36),
(40, 95, 8, '2020-05-27 00:03:29', 37),
(41, 96, 8, '2020-05-27 00:03:29', 38),
(42, 97, 8, '2020-05-27 00:03:29', 39),
(43, 98, 8, '2020-05-27 00:03:29', 40),
(44, 99, 8, '2020-05-27 00:03:29', 35),
(45, 100, 9, '2020-05-27 00:05:03', 46),
(46, 101, 9, '2020-05-27 00:05:03', 52),
(47, 102, 9, '2020-05-27 00:05:03', 47),
(48, 103, 9, '2020-05-27 00:05:04', 48),
(49, 104, 9, '2020-05-27 00:05:04', 42),
(50, 105, 9, '2020-05-27 00:05:04', 49),
(51, 106, 9, '2020-05-27 00:05:04', 43),
(52, 107, 9, '2020-05-27 00:05:04', 53),
(54, 109, 9, '2020-05-27 00:05:05', 45),
(55, 110, 9, '2020-05-27 00:05:05', 41),
(56, 111, 9, '2020-05-27 00:05:05', 54),
(58, 113, 9, '2020-05-27 00:07:24', 50),
(59, 114, 9, '2020-05-27 00:07:24', 44),
(60, 115, 10, '2020-05-27 00:08:48', 59),
(61, 116, 10, '2020-05-27 00:08:49', 58),
(62, 117, 10, '2020-05-27 00:08:49', 55),
(63, 118, 10, '2020-05-27 00:08:49', 57),
(64, 119, 10, '2020-05-27 00:08:50', 56),
(65, 120, 10, '2020-05-27 00:08:50', 60),
(66, 121, 10, '2020-05-27 00:08:50', 61),
(67, 122, 10, '2020-05-27 00:08:50', 62),
(68, 123, 11, '2020-06-10 13:06:14', 63),
(69, 124, 11, '2020-06-10 13:06:15', 64),
(70, 125, 11, '2020-06-10 13:06:16', 65);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_image_image`
--

CREATE TABLE `avantis_image_image` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_image_image`
--

INSERT INTO `avantis_image_image` (`id`, `category_id`, `parent_id`, `name`, `description`, `file`, `create_time`, `user_id`, `alt`, `type`, `status`, `sort`) VALUES
(1, NULL, NULL, 'ban3.jpg', '<span class=\"title\">Безопасное лазерное<br />отбеливание зубов<br />за <span>8999</span> руб.!</span>', '31caf85cc2d55da1d7fc43d7ce957bb4.jpg', '2019-01-14 17:51:14', 1, 'ban3.jpg', 0, 1, 1),
(2, NULL, NULL, 'ban2.jpg', '<span class=\"title\">Профессиональная гигиена<br /> полости рта <br />«Air Flow» - 1990 руб.</span>', '50b66298b855ae171ae2c81a97e7be6c.jpg', '2019-01-14 17:51:14', 1, 'ban2.jpg', 0, 1, 2),
(3, NULL, NULL, 'ban.jpg', '<h1><span>Клиника эстетической <br />и функциональной стоматологии</span> <br />«Авантис»</h1>', '2d0419332bdb9ee89134deff731b30d2.jpg', '2019-01-14 17:51:14', 1, 'ban.jpg', 0, 1, 3),
(4, NULL, NULL, '3.jpg', '', '69f17492f0ab769b7874e51b327da28b.jpg', '2019-01-20 17:21:03', 1, '3.jpg', 0, 1, 4),
(5, NULL, NULL, '2.jpg', '', '3429163d475f58317b8edc81d2122db4.jpg', '2019-01-20 17:21:03', 1, '2.jpg', 0, 1, 5),
(6, NULL, NULL, '4.jpg', '', 'fc62e64e83a29fd0e99d0dde5dbe8e72.jpg', '2019-01-20 17:21:03', 1, '4.jpg', 0, 1, 6),
(7, NULL, NULL, '1.jpg', '', '5fdfc12fc9e0f75e76516fb922670ff4.jpg', '2019-01-20 17:21:03', 1, '1.jpg', 0, 1, 7),
(12, NULL, NULL, 'tads.jpg', '', '7104b3dbd8f7ce2966cc1b9370347290.jpg', '2019-01-20 17:40:32', 1, 'tads.jpg', 0, 1, 8),
(13, NULL, NULL, 'eryry.jpg', '', 'a6ab29d9f1fa38e9e2324ed4c3c392ee.jpg', '2019-01-20 17:40:32', 1, 'eryry.jpg', 0, 1, 9),
(14, NULL, NULL, 'eryey.jpg', '', '779a17e9e00bc912f88fec799707db5a.jpg', '2019-01-20 17:40:32', 1, 'eryey.jpg', 0, 1, 10),
(15, NULL, NULL, 'eryerr.jpg', '', 'f1ca1bbc3bbbc278acdcd02c1791f222.jpg', '2019-01-20 17:40:32', 1, 'eryerr.jpg', 0, 1, 11),
(16, NULL, NULL, 'Лечение без боли и стресса', '', 'adecdef0b4824bf51e833f431d1340ef.png', '2019-06-04 16:01:39', 1, 'Лечение без <br />боли и стресса', 0, 1, 12),
(17, NULL, NULL, 'Качественные материалы от надежных поставщиков', '', '1a1c7dd2bc3b4fa510f471ae48ab381c.png', '2019-06-04 16:01:39', 1, 'Качественные материалы <br />от надежных поставщиков', 0, 1, 13),
(18, NULL, NULL, 'Стоматологи высокой квалификации', '', '82b1d5500c03f97cd36d7f1d48b6ce2d.png', '2019-06-04 16:01:39', 1, 'Стоматологи<br /> высокой квалификации', 0, 1, 14),
(19, NULL, NULL, 'Наши работы', '', '6eabf481fc581ed476872212b05417c7.jpg', '2019-06-09 12:27:14', 1, 'down1', 0, 1, 15),
(20, NULL, NULL, 'Наши работы', '', '18c0f0ca86d618a031bec7aefd5c0d39.jpg', '2019-06-09 12:27:14', 1, 'down2', 0, 1, 16),
(21, NULL, NULL, 'Наши работы', '', '6c81c1a5d7402083b44e994974e24c3c.jpg', '2019-06-09 12:27:14', 1, 'down3', 0, 1, 17),
(22, NULL, NULL, 'Untitled-1.jpg', '', '9f6dae20b0eeb539d364e2ac5df903c3.jpg', '2019-06-21 01:18:45', 1, 'Untitled-1.jpg', 0, 1, 18),
(23, NULL, NULL, 'Untitled-1.jpg', '', '4902e88444e3d1f92aafc19482f4ee20.jpg', '2019-06-21 01:43:52', 1, 'Untitled-1.jpg', 0, 1, 19),
(24, NULL, NULL, 'Untitled-1.jpg', '', '0fe305b38a8c007ea1c56937975575f0.jpg', '2019-06-21 01:44:04', 1, 'Untitled-1.jpg', 0, 1, 20),
(25, NULL, NULL, 'Untitled-1.jpg', '', '0f9146772bfdaf96a1455cfce876b433.jpg', '2019-06-21 01:47:51', 1, 'Untitled-1.jpg', 0, 1, 21),
(26, NULL, NULL, 'Untitled-1.jpg', '', '5aa8615d714fe7916571cc8183008bf8.jpg', '2019-06-21 01:55:52', 1, 'Untitled-1.jpg', 0, 1, 22),
(27, NULL, NULL, 'Untitled-1.jpg', '', 'cd62db3a09625094f79fb77c6cda6d84.jpg', '2019-06-21 02:09:37', 1, 'Untitled-1.jpg', 0, 1, 23),
(28, NULL, NULL, 'Untitled-1.jpg', '', '4e4a7753d86a970f9cc37f3111833229.jpg', '2019-06-21 02:12:47', 1, 'Untitled-1.jpg', 0, 1, 24),
(29, NULL, NULL, 'Untitled-1.jpg', '', '8338f0125251eae0b25ef23ab8fd68ba.jpg', '2019-06-21 02:15:32', 1, 'Untitled-1.jpg', 0, 1, 25),
(30, NULL, NULL, 'Untitled-1.jpg', '', '3e73e0ab6d82250fb3053b79cd14f53c.jpg', '2019-06-21 02:21:02', 1, 'Untitled-1.jpg', 0, 1, 26),
(31, NULL, NULL, 'viniry.jpg', '', 'd546da5f8c0d3796f20753731b6e286b.jpg', '2019-07-01 13:00:56', 1, 'viniry.jpg', 0, 1, 27),
(32, NULL, NULL, 'bugeln.jpg', '', '009fc3b0d7517764dca428634784f37c.jpg', '2019-07-01 13:43:35', 1, 'bugeln.jpg', 0, 1, 28),
(33, NULL, NULL, 'semnoe.jpg', '', '687d049077ce5a6ada791fe0a5dd7aae.jpg', '2019-07-01 13:46:50', 1, 'semnoe.jpg', 0, 1, 29),
(34, NULL, NULL, 'nesem_prot.jpg', '', '9c60e8efd085ad0305d5a8eaaf252cd6.jpg', '2019-07-01 13:48:41', 1, 'nesem_prot.jpg', 0, 1, 30),
(35, NULL, NULL, 'nesem_prot.jpg', '', 'e7c2ebf89dac174c94baf5ef6af3f117.jpg', '2019-07-01 13:49:37', 1, 'nesem_prot.jpg', 0, 1, 31),
(36, NULL, NULL, 'remont.jpg', '', 'bfad41f870b59181161897e75c800a3c.jpg', '2019-07-01 13:54:38', 1, 'remont.jpg', 0, 1, 32),
(37, NULL, NULL, 'semnoe.jpg', '', 'e3968888cd9053bc1770e46d45d5d1fb.jpg', '2019-07-01 13:55:39', 1, 'semnoe.jpg', 0, 1, 33),
(38, NULL, NULL, 'vkladki.jpg', '', '6f8d938a429ca9f62d713e927e0c5d3f.jpg', '2019-07-01 13:57:01', 1, 'vkladki.jpg', 0, 1, 34),
(39, NULL, NULL, 'provizorn.jpg', '', '7467dbe995b48cbef3b2e255590b9322.jpg', '2019-07-01 13:58:23', 1, 'provizorn.jpg', 0, 1, 35),
(40, NULL, NULL, 'sniat_fix.jpg', '', '76be38ae2995ce9556beb65e398feb29.jpg', '2019-07-01 13:59:28', 1, 'sniat_fix.jpg', 0, 1, 36),
(41, NULL, NULL, 'iskustv_kor.jpg', '', 'de08f8069b0eae9cd4753af51f695ee7.jpg', '2019-07-01 14:00:46', 1, 'iskustv_kor.jpg', 0, 1, 37),
(42, NULL, NULL, 'brekety_damon_4.jpg', '', '0012a28ecd837c3c1de18c83baf5bc62.jpg', '2019-07-01 14:04:59', 1, 'brekety_damon_4.jpg', 0, 1, 38),
(43, NULL, NULL, 'opred_PR.jpg', '', '41dab29b2d13176af9f7c3ef2c6c9ffe.jpg', '2019-07-01 14:06:14', 1, 'opred_PR.jpg', 0, 1, 39),
(44, NULL, NULL, 'elair.jpg', '', '06c9366865bc50d0f9323796085a4d57.jpg', '2019-07-01 14:07:30', 1, 'elair.jpg', 0, 1, 40),
(45, NULL, NULL, 'registr_prik.jpg', '', 'f5bb2765acb01056a0f115b9a3ad12e4.jpg', '2019-07-01 14:09:14', 1, 'registr_prik.jpg', 0, 1, 41),
(46, NULL, NULL, 'opred_PR.jpg', '', '9d5da96164af3db38a44c0067ea8c9bc.jpg', '2019-07-01 14:10:23', 1, 'opred_PR.jpg', 0, 1, 42),
(47, NULL, NULL, 'registr_prik.jpg', '', '4b84c4b1ed38d5c7dc5038f5933a03b0.jpg', '2019-07-01 14:11:49', 1, 'registr_prik.jpg', 0, 1, 43),
(48, NULL, NULL, 'beozp_laz.jpg', '', 'be9bd2bf8e59d0344c73b511df9c1cde.jpg', '2019-07-01 14:12:42', 1, 'beozp_laz.jpg', 0, 1, 44),
(49, NULL, NULL, 'kappa.jpg', '', '2ea2aba770798bad76a595a8aeef99ec.jpg', '2019-07-01 14:14:07', 1, 'kappa.jpg', 0, 1, 45),
(50, NULL, NULL, 'ZOOM-4.jpg', '', '1bca061d20440117c614ab05e329904b.jpg', '2019-07-01 14:15:33', 1, 'ZOOM-4.jpg', 0, 1, 46),
(51, NULL, NULL, 'lazer.jpg', '', 'a2ab0d9e7c1a0b75762d456ea21ace90.jpg', '2019-07-01 14:16:44', 1, 'lazer.jpg', 0, 1, 47),
(52, NULL, NULL, 'endoot.jpg', '', '4214e6cbf64a16790d293961d4dc1440.jpg', '2019-07-01 14:18:31', 1, 'endoot.jpg', 0, 1, 48),
(53, NULL, NULL, 'elecktro.jpg', '', 'e35c23f36c0bcf091e62284442095cde.jpg', '2019-07-01 14:19:59', 1, 'elecktro.jpg', 0, 1, 49),
(54, NULL, NULL, 'kondolo.jpg', '', 'bf851de0b40fc58b9f0eccb7e3f0e731.jpg', '2019-07-01 14:21:22', 1, 'kondolo.jpg', 0, 1, 50),
(55, NULL, NULL, 'ortop.jpg', '', 'f7de9a0d162feb3294f766e8c4dfac2e.jpg', '2019-07-01 14:22:31', 1, 'ortop.jpg', 0, 1, 51),
(56, NULL, NULL, 'ultra_zv.jpg', '', '01b311eead988685a791ec099d0222cb.jpg', '2019-07-01 14:24:09', 1, 'ultra_zv.jpg', 0, 1, 52),
(57, NULL, NULL, 'air-flow.jpg', '', '25feec67aa7b0277f479687a8d7d9647.jpg', '2019-07-01 14:25:26', 1, 'air-flow.jpg', 0, 1, 53),
(58, NULL, NULL, 'bruxismo-y-bc3b3tox-rejuvenecimiento-facial-barcelona.jpg', '', 'ee33542729b1e160060a7438f5afa5ad.jpg', '2019-07-01 14:28:55', 1, 'bruxismo-y-bc3b3tox-rejuvenecimiento-facial-barcelona.jpg', 0, 1, 54),
(59, NULL, NULL, 'diagn.jpg', '', 'e2f9815029acbe81c06678b3a100f2c7.jpg', '2019-07-01 14:31:22', 1, 'diagn.jpg', 0, 1, 55),
(60, NULL, NULL, 'gigiena.jpg', '', 'b79c1e8bc778b7e3d5a5260f304ac89b.jpg', '2019-07-01 14:32:58', 1, 'gigiena.jpg', 0, 1, 56),
(61, NULL, NULL, 'Лицензия-главная страница', '', '17a63dcb806012e92e951eb5897fe777.jpg', '2019-07-17 16:10:24', 1, 'Лицензия-главная страница', 0, 1, 57),
(62, NULL, NULL, 'Лицензия-оборотная сторона', '', 'c4a030c3bf519f7f87389b9308f53845.jpg', '2019-07-17 16:11:04', 1, 'Лицензия-оборотная сторона', 0, 1, 58),
(63, NULL, NULL, 'detskij-ortodont.jpg', '', 'a9dd027a277e125787691c8a07401370.jpg', '2019-08-26 15:40:35', 1, 'detskij-ortodont.jpg', 0, 1, 59),
(64, NULL, NULL, '7f8edcee46ff2f97f801607b19170773.jpg', '', '91eecd93d33daa4c71fe8c65be761120.jpg', '2019-08-26 15:41:46', 1, '7f8edcee46ff2f97f801607b19170773.jpg', 0, 1, 60),
(65, NULL, NULL, 'cjDnRF4kzYk.jpg', '', '77079770ac40302b8154c69dbe87a537.jpg', '2019-09-13 15:33:14', 1, 'cjDnRF4kzYk.jpg', 0, 1, 61),
(66, NULL, NULL, 'DSC_1905.jpg', '', 'cf3936f6842818bead1f4534858db5c7.jpg', '2019-09-18 15:00:00', 1, 'DSC_1905.jpg', 0, 1, 62),
(67, NULL, NULL, 'DSC_1986.jpg', '', 'b371f33ca0479db2200724218eff2995.jpg', '2019-09-18 15:00:01', 1, 'DSC_1986.jpg', 0, 1, 63),
(68, NULL, NULL, 'DSC_1974.jpg', '', 'd359a693a33e21186fa3f728cad12756.jpg', '2019-09-18 15:00:01', 1, 'DSC_1974.jpg', 0, 1, 64),
(69, NULL, NULL, 'DSC_8761.jpg', '', '50d9b5ac0c8af1fcc89e00d0ef7f61d9.jpg', '2019-09-18 15:00:02', 1, 'DSC_8761.jpg', 0, 1, 65),
(70, NULL, NULL, 'chistka-breketov-min.jpg', '', '935cb7593c71085b0b725d2ac56864c2.jpg', '2019-09-20 12:57:35', 1, 'chistka-breketov-min.jpg', 0, 1, 66),
(71, NULL, NULL, '8c110a345d0ee638019ef7d435793e83.jpg', '', 'ec30e12bef1121fa91e329d2b1c2db87.jpg', '2019-09-23 11:40:34', 1, '8c110a345d0ee638019ef7d435793e83.jpg', 0, 1, 67),
(72, NULL, NULL, 'лечение-зубов-под-наркозом.jpg', '', '39c3a4aad5636584a2866d130629feee.jpg', '2019-09-23 11:41:02', 1, 'лечение-зубов-под-наркозом.jpg', 0, 1, 68),
(73, NULL, NULL, '4.jpg', '', '10f52a56092b21afa069e17a5c5e97ab.jpg', '2019-11-19 11:35:08', 1, '4.jpg', 0, 1, 69),
(74, NULL, NULL, '2.jpg', '', 'd94ee8e8790c1d9c7e979be8d957a485.jpg', '2019-11-19 11:40:52', 1, '2.jpg', 0, 1, 70),
(75, NULL, NULL, '2.jpg', '', '1358a8eb92a12877a4598da8243cd236.jpg', '2019-11-20 15:00:43', 1, '2.jpg', 0, 1, 71),
(76, NULL, NULL, 'кариес6.jpg', '', 'cb62120eb9b97cdd6d6786fede08da5f.jpg', '2020-02-14 12:29:42', 1, 'кариес6.jpg', 0, 1, 72),
(77, NULL, NULL, 'кариес6.jpg', '', 'bd5b5e7713374a0ce2f4bfd9c3301a9e.jpg', '2020-02-14 12:31:21', 1, 'кариес6.jpg', 0, 1, 73),
(78, NULL, NULL, 'кариес4.jpg', '', '1dd288db39619147f4de0e3f6416bbd8.jpg', '2020-02-14 12:31:46', 1, 'кариес4.jpg', 0, 1, 74),
(79, NULL, NULL, 'кариес3.jpg', '', 'eb4d52fb994786fb4e90ed7dee00da33.jpg', '2020-02-14 12:32:08', 1, 'кариес3.jpg', 0, 1, 75),
(80, NULL, NULL, 'дети1.jpg', '', '760f39f0bc802191f71060e1a11df9ae.jpg', '2020-02-20 13:48:20', 1, 'дети1.jpg', 0, 1, 76),
(81, NULL, NULL, 'дети3.jpg', '', '2bf02f41cc2aac75a80c2edf26fb9eff.jpg', '2020-02-20 13:48:50', 1, 'дети3.jpg', 0, 1, 77),
(82, NULL, NULL, 'дети4.jpg', '', '04f6b10940837d3ad733f6987976e336.jpg', '2020-02-20 13:49:36', 1, 'дети4.jpg', 0, 1, 78),
(83, NULL, NULL, 'img20200506_05520162_page-0001.jpg', '', '865e81792e0721243c4af48fb3af3a2b.jpg', '2020-05-26 23:38:12', 1, 'img20200506_05520162_page-0001.jpg', 0, 1, 79),
(84, NULL, NULL, 'img20200506_05550604_page-0001.jpg', '', '86bdd874f6034c59670bb7d55081fe43.jpg', '2020-05-26 23:38:12', 1, 'img20200506_05550604_page-0001.jpg', 0, 1, 80),
(85, NULL, NULL, 'img20200506_06040087_page-0001.jpg', '', '3ffc733f2299bb120195ee6b76bdade2.jpg', '2020-05-26 23:38:12', 1, 'img20200506_06040087_page-0001.jpg', 0, 1, 81),
(86, NULL, NULL, 'img20200506_05570524_page-0001.jpg', '', 'a38c07c89c1ce51575cb41723d603aad.jpg', '2020-05-26 23:38:13', 1, 'img20200506_05570524_page-0001.jpg', 0, 1, 82),
(88, NULL, NULL, 'img20200506_06082500_page-0001.jpg', '', '125824e9777ab12bc33218f839e9c4b9.jpg', '2020-05-26 23:38:13', 1, 'img20200506_06082500_page-0001.jpg', 0, 1, 84),
(89, NULL, NULL, 'img20200506_05532237_page-0001.jpg', '', '05c3a2df40339351961eabd301c00826.jpg', '2020-05-26 23:38:13', 1, 'img20200506_05532237_page-0001.jpg', 0, 1, 85),
(90, NULL, NULL, 'img20200506_06101023_page-0001.jpg', '', '1505b035aa0853d8c2868cba61ab08e7.jpg', '2020-05-26 23:38:14', 1, 'img20200506_06101023_page-0001.jpg', 0, 1, 86),
(91, NULL, NULL, 'img20200506_06070344_page-0001.jpg', '', '2e35ef019814e37243f6cf55d0d88acd.jpg', '2020-05-26 23:38:14', 1, 'img20200506_06070344_page-0001.jpg', 0, 1, 87),
(92, NULL, NULL, 'img20200506_06060208_page-0001.jpg', '', '5d5f6347be9f8d8ef64e8eb448e3109b.jpg', '2020-05-26 23:42:55', 1, 'img20200506_06060208_page-0001.jpg', 0, 1, 88),
(94, NULL, NULL, 'img20200506_06410571_page-0001.jpg', '', 'bec57b884b8a49f055b089d1809c230a.jpg', '2020-05-27 00:03:28', 1, 'img20200506_06410571_page-0001.jpg', 0, 1, 89),
(95, NULL, NULL, 'img20200506_06453818_page-0001.jpg', '', 'fdacb23e3aedeb4ad8336ae8b66f3242.jpg', '2020-05-27 00:03:29', 1, 'img20200506_06453818_page-0001.jpg', 0, 1, 90),
(96, NULL, NULL, 'img20200506_06432804_page-0001.jpg', '', '440948498aff34be2fe54cfb31830553.jpg', '2020-05-27 00:03:29', 1, 'img20200506_06432804_page-0001.jpg', 0, 1, 91),
(97, NULL, NULL, 'img20200506_06424762_page-0001.jpg', '', 'dd3165989d0101927d6da3595c6bde92.jpg', '2020-05-27 00:03:29', 1, 'img20200506_06424762_page-0001.jpg', 0, 1, 92),
(98, NULL, NULL, 'img20200506_06442761_page-0001.jpg', '', 'd66423c5e73d2cc35f2026154822b47b.jpg', '2020-05-27 00:03:29', 1, 'img20200506_06442761_page-0001.jpg', 0, 1, 93),
(99, NULL, NULL, 'img20200506_06420288_page-0001.jpg', '', '48dbbdd3609eae4f485a8ba5d98a4ba5.jpg', '2020-05-27 00:03:29', 1, 'img20200506_06420288_page-0001.jpg', 0, 1, 94),
(100, NULL, NULL, 'img20200506_06120116_page-0001.jpg', '', '1dc48e6a00dee4d751c71bf048571f73.jpg', '2020-05-27 00:05:03', 1, 'img20200506_06120116_page-0001.jpg', 0, 1, 95),
(101, NULL, NULL, 'img20200506_06132488_page-0001.jpg', '', '76f6a51538373d297b02b5a3bf286d55.jpg', '2020-05-27 00:05:03', 1, 'img20200506_06132488_page-0001.jpg', 0, 1, 96),
(102, NULL, NULL, 'img20200506_06153217_page-0001.jpg', '', '16da148257b0e83d8bc18ddfe55b4e7d.jpg', '2020-05-27 00:05:03', 1, 'img20200506_06153217_page-0001.jpg', 0, 1, 97),
(103, NULL, NULL, 'img20200506_06164111_page-0001.jpg', '', '4acf4c208d8621a8dc339abd9ceb7655.jpg', '2020-05-27 00:05:04', 1, 'img20200506_06164111_page-0001.jpg', 0, 1, 98),
(104, NULL, NULL, 'img20200506_06141491_page-0001.jpg', '', '740b2a1b92e0d76c918ec7279be4c5fc.jpg', '2020-05-27 00:05:04', 1, 'img20200506_06141491_page-0001.jpg', 0, 1, 99),
(105, NULL, NULL, 'img20200506_06174751_page-0001.jpg', '', '4f23acd6b65a4f7009d9334825d85c8a.jpg', '2020-05-27 00:05:04', 1, 'img20200506_06174751_page-0001.jpg', 0, 1, 100),
(106, NULL, NULL, 'img20200506_06210627_page-0001.jpg', '', 'b91e75f99ac29d14e4545beba06a81b6.jpg', '2020-05-27 00:05:04', 1, 'img20200506_06210627_page-0001.jpg', 0, 1, 101),
(107, NULL, NULL, 'img20200506_06215970_page-0001.jpg', '', '1bf490da69749686fa4969adf80eac44.jpg', '2020-05-27 00:05:04', 1, 'img20200506_06215970_page-0001.jpg', 0, 1, 102),
(109, NULL, NULL, 'img20200506_06240847_page-0001.jpg', '', '86cde19c49bc05e93ee6468e49eb67f1.jpg', '2020-05-27 00:05:05', 1, 'img20200506_06240847_page-0001.jpg', 0, 1, 104),
(110, NULL, NULL, 'img20200506_06245863_page-0001.jpg', '', '1aafbbf2827950c7836f924ee1fdaa67.jpg', '2020-05-27 00:05:05', 1, 'img20200506_06245863_page-0001.jpg', 0, 1, 105),
(111, NULL, NULL, 'img20200506_06254641_page-0001.jpg', '', 'ece3c5a3acdfcf262814211039c29f8f.jpg', '2020-05-27 00:05:05', 1, 'img20200506_06254641_page-0001.jpg', 0, 1, 106),
(113, NULL, NULL, 'img20200506_06231085_page-0001.jpg', '', '85fba15fc6ff9d3f6da567fef7db73a8.jpg', '2020-05-27 00:07:24', 1, 'img20200506_06231085_page-0001.jpg', 0, 1, 107),
(114, NULL, NULL, 'img20200506_06194315_page-0001.jpg', '', '278d8c647ae88cfd639712971faefbdf.jpg', '2020-05-27 00:07:24', 1, 'img20200506_06194315_page-0001.jpg', 0, 1, 108),
(115, NULL, NULL, 'img20200506_06313375_page-0001.jpg', '', '550dac13a0132f5d151f20857cb94d48.jpg', '2020-05-27 00:08:48', 1, 'img20200506_06313375_page-0001.jpg', 0, 1, 109),
(116, NULL, NULL, 'img20200506_06333323_page-0001.jpg', '', 'd7f130048141b3ef655d4f67bb9d0f4d.jpg', '2020-05-27 00:08:49', 1, 'img20200506_06333323_page-0001.jpg', 0, 1, 110),
(117, NULL, NULL, 'img20200506_06362986_page-0001.jpg', '', 'a31b6986ab7297a0810798f38f8533d8.jpg', '2020-05-27 00:08:49', 1, 'img20200506_06362986_page-0001.jpg', 0, 1, 111),
(118, NULL, NULL, 'img20200506_06344349_page-0001.jpg', '', 'dc533704974a333d6fc0177c409429e9.jpg', '2020-05-27 00:08:49', 1, 'img20200506_06344349_page-0001.jpg', 0, 1, 112),
(119, NULL, NULL, 'img20200506_06354763_page-0001.jpg', '', '0a4ecc76075b3999b00dc3b8cea6e01b.jpg', '2020-05-27 00:08:50', 1, 'img20200506_06354763_page-0001.jpg', 0, 1, 113),
(120, NULL, NULL, 'img20200506_06383645_page-0001.jpg', '', 'ab48b8aa5040dddc1a6a4f6cbd4ae61f.jpg', '2020-05-27 00:08:50', 1, 'img20200506_06383645_page-0001.jpg', 0, 1, 114),
(121, NULL, NULL, 'img20200506_06323492_page-0001.jpg', '', 'a3fa170f63cb1a0f3068bdc09fa002c7.jpg', '2020-05-27 00:08:50', 1, 'img20200506_06323492_page-0001.jpg', 0, 1, 115),
(122, NULL, NULL, 'img20200506_06372627_page-0001.jpg', '', '20b7e0ebb1673306e07a14f4d070c9ea.jpg', '2020-05-27 00:08:50', 1, 'img20200506_06372627_page-0001.jpg', 0, 1, 116),
(123, NULL, NULL, 'pdf_1.jpg', '', 'a4bf252299cbeac216527f08f19c5d45.jpg', '2020-06-10 13:06:14', 1, 'pdf_1.jpg', 0, 1, 117),
(124, NULL, NULL, 'pdf_2.jpg', '', 'a4cb988636946bd5b9ca933af3b38a77.jpg', '2020-06-10 13:06:15', 1, 'pdf_2.jpg', 0, 1, 118),
(125, NULL, NULL, 'pdf_3.jpg', '', '6fc62f208b751f8c1bc1d6496e2041fa.jpg', '2020-06-10 13:06:16', 1, 'pdf_3.jpg', 0, 1, 119),
(126, NULL, NULL, 'IMG_1752.jpg', '', '1bbc2253b47fbef3f15e5231dd3a9e23.jpg', '2020-06-30 16:57:26', 1, 'IMG_1752.jpg', 0, 1, 120),
(127, NULL, NULL, 'DSC_4890(1).jpg', '', '253b3f5b99852e0c3e2fcc222e0b32c6.jpg', '2020-06-30 16:59:40', 1, 'DSC_4890(1).jpg', 0, 1, 121),
(128, NULL, NULL, 'Авантис - цензура.jpg', '', '249bab9239117f36b20e898d78cbfc51.jpg', '2020-07-10 13:04:59', 1, 'Авантис - цензура.jpg', 0, 1, 122),
(129, NULL, NULL, 'миошина2.jpg', '', '8fb387ec87845b79e202249a93c7506b.jpg', '2020-07-13 11:30:39', 1, 'миошина2.jpg', 0, 1, 123),
(130, NULL, NULL, 'миошина3.jpg', '', '0e579ac48a732fc285bf92fd7bf464ea.jpg', '2020-07-13 11:32:00', 1, 'миошина3.jpg', 0, 1, 124);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_mail_mail_event`
--

CREATE TABLE `avantis_mail_mail_event` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_mail_mail_event`
--

INSERT INTO `avantis_mail_mail_event` (`id`, `code`, `name`, `description`) VALUES
(1, 'ostavit-zayavku', 'ОСТАВИТЬ ЗАЯВКУ', ''),
(2, 'zapis-na-priyom', 'Запись на приём', ''),
(3, 'obratnaya-svyaz-kontakty', 'Обратная связь контакты', 'Обратная связь контакты');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_mail_mail_template`
--

CREATE TABLE `avantis_mail_mail_template` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_mail_mail_template`
--

INSERT INTO `avantis_mail_mail_template` (`id`, `code`, `event_id`, `name`, `description`, `from`, `to`, `theme`, `body`, `status`) VALUES
(1, 'Оставить заявку', 1, 'Оставить заявку', '', 'dc@dc56.ru', 'avantisdent@gmail.com', 'Заявка с сайта Avantis', '<table>\r\n<tbody>\r\n<tr>\r\n	<td><strong>Имя:</strong>\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td><strong>Телефон:</strong>\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1),
(2, 'zapis-na-priyom', 2, 'Запись на приём', '', 'dc@dc56.ru', 'avantisdent@gmail.com', 'Запись на приём', '<table><tbody><tr><td><strong>имя</strong></td><td>name</td></tr><tr><td><strong>телефон</strong></td><td>phone</td></tr><tr><td><strong>услуга</strong></td><td>services</td></tr></tbody></table>', 1),
(3, 'obratnaya-svyaz-kontakty', 3, 'Обратная связь контакты', '', 'dc@dc56.ru', 'avantisdent@gmail.com', 'Отправка сообщения с сайта', '<table><tbody><tr><td>Имя</td><td>name</td></tr><tr><td>телефон</td><td>phone</td></tr><tr><td>сообщение</td><td>body</td></tr></tbody></table>', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_menu_menu`
--

CREATE TABLE `avantis_menu_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_menu_menu`
--

INSERT INTO `avantis_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(1, 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', 1),
(2, 'Пациентам', 'pacientam', 'Пациентам', 1),
(3, 'Врачам', 'vracham', 'Врачам', 1),
(4, 'Блок услуг', 'blok-uslug', 'Блок услуг', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_menu_menu_item`
--

CREATE TABLE `avantis_menu_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_menu_menu_item`
--

INSERT INTO `avantis_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`) VALUES
(13, 0, 2, 1, 'Пациентам', '#', '', '', '', '', '', '', '', 0, 2, 1),
(14, 0, 3, 1, 'Врачам', '#', '', '', '', '', '', '', '', 0, 3, 1),
(16, 0, 1, 1, 'Услуги', '/services', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 6, 1),
(17, 0, 1, 1, 'Акции', '/actions', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 7, 1),
(18, 0, 1, 1, 'Команда', '/team', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 11, 1),
(19, 0, 1, 1, 'Наши работы', '/our-work', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 10, 1),
(21, 0, 1, 1, 'Контакты', '/contacti', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 57, 1),
(22, 0, 1, 1, 'Полезные статьи', '/poleznye-stati', '', '', '', '', '', '', '', 0, 13, 1),
(24, 14, 3, 1, 'Вакансии', '/vakansii', '', '', '', '', '', '', '', 0, 16, 1),
(25, 13, 2, 1, 'Полезные статьи', '/poleznye-stati', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 17, 0),
(26, 14, 3, 1, '3D-стоматология', '/3d-stomatologia', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 18, 0),
(28, 0, 1, 1, 'Главная', '/', '', '', '', '', '', '', '', 0, 5, 1),
(30, 13, 2, 1, 'Вопросы и ответы', '/voprosy-i-otvety', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 20, 1),
(31, 13, 2, 1, 'Лицензии', '/licenzii', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 21, 1),
(32, 0, 4, 1, 'Гигиена и профилактика', '/gigiena-i-profilaktika', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 22, 1),
(33, 0, 4, 1, 'Диагностика', '/diagnostika', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 23, 1),
(34, 0, 4, 1, 'Отбеливание зубов', '/otbelivanie-zubov', '', '', '', '', '', '', '', 0, 24, 1),
(35, 69, 4, 1, 'Исправление прикуса', '/ispravlenie-prikusa', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 25, 1),
(36, 0, 4, 1, 'Имплантация зубов', '/implantaciya-zubov', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 26, 1),
(37, 0, 4, 1, 'Протезирование зубов', '/protezirovanie-zubov', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 27, 1),
(38, 0, 4, 1, 'Детская стоматология', '/detskaya-stomatologiya', '', '', '', '', '', '', '', 0, 28, 1),
(40, 0, 4, 1, 'Эстетическая реставрация зубов', '/esteticheskaya-restavraciya-zubov', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 30, 1),
(41, 0, 4, 1, 'Пародонтология', '/parodontologiya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 31, 1),
(42, 69, 4, 1, 'Лечение дисфункции ВНЧС', '/lechenie-disfunkcii-vnchs', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 32, 1),
(43, 32, 4, 1, 'Профессиональная гигиена полости рта «Air Flow»', '/professionalnaya-gigiena-polosti-rta-air-flow', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 33, 1),
(44, 32, 4, 1, 'Ультразвуковая чистка камней', '/ultrazvukovaya-chistka-kamney', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 34, 1),
(45, 33, 4, 1, 'Кондилография', '/kondilografiya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 35, 1),
(46, 33, 4, 1, 'Ортопедическая стоматология', '/ortopedicheskaya-stomatologiya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 36, 0),
(49, 0, 1, 1, 'Отзывы', '/otzyvy', '', '', '', '', '', '', '', 0, 14, 1),
(50, 33, 4, 1, 'Электромиография', '/elektromiografiya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 37, 1),
(51, 34, 4, 1, 'Эндоотбеливание', '/endootbelivanie', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 38, 1),
(52, 34, 4, 1, 'Клиническое отбеливание лазер Dr. Smail', '/klinicheskoe-otbelivanie-lazer-drsmail', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 39, 1),
(53, 34, 4, 1, 'Клиническое отбеливание ZOOM 4', '/klinicheskoe-otbelivanie-zoom-4', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 40, 1),
(54, 34, 4, 1, 'Каппы для отбеливания', '/kappa-dlya-otbelivaniya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 41, 1),
(55, 34, 4, 1, 'Безопасное лазерное отбеливание зубов', '/bezopasnoe-lazernoe-otbelivanie-zubov', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 42, 1),
(56, 69, 4, 1, 'Регистрация прикуса', '/registraciya-prikusa', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 43, 1),
(57, 69, 4, 1, 'Элайнеры', '/elaynery', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 44, 1),
(58, 69, 4, 1, 'Определение RP', '/opredelenie-rp', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 45, 1),
(60, 36, 4, 1, 'Искусственные коронки', '/iskusstvennye-koronki', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 47, 1),
(61, 36, 4, 1, 'Снятие и фиксация коронок', '/snyatie-i-fiksaciya-koronok', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 48, 1),
(62, 36, 4, 1, 'Провизорные коронки', '/provizornye-koronki', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 49, 1),
(63, 36, 4, 1, 'Коронки, вкладки', '/koronki-vkladki', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 50, 1),
(64, 37, 4, 1, 'Съемное протезирование', '/semnoe-protezirovanie', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 51, 1),
(65, 37, 4, 1, 'Починка протеза съёмного', '/pochinka-proteza-syomnogo', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 52, 1),
(66, 37, 4, 1, 'Несъемное протезирование', '/nesemnoe-protezirovanie', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 53, 1),
(67, 37, 4, 1, 'Бюгельное протезирование', '/byugelnoe-protezirovanie', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 54, 1),
(68, 37, 4, 1, 'Виниры', '/viniry', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 55, 1),
(69, 0, 4, 1, 'Ортодонт', '/ortodontiya', '', '', '', '', '', '', '', 0, 56, 1),
(70, 0, 1, 1, 'Прайс', '/prays', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 9, 1),
(71, 38, 4, 1, 'Лечение кариеса у детей', '/lechenie-kariesa-u-detey', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 58, 1),
(72, 38, 4, 1, 'Детский ортодонт', '/detskiy-ortodont', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 59, 1),
(73, 13, 2, 1, 'Прием у руководителя', '/priem-u-rukovoditelya', '', '', '', '', '', '', '', 0, 60, 1),
(74, 13, 2, 1, 'Запись на прием', '/pravila-zapisi-na-priem', '', '', '', '', '', '', '', 0, 61, 1),
(75, 13, 2, 1, 'Обслуживание по ДМС', '/obsluzhivanie-po-dms', '', '', '', '', '', '', '', 0, 62, 1),
(76, 13, 2, 1, 'Правила оказания услуг', '/pravila-okazaniya-platnyh-medicinskih-uslug', '', '', '', '', '', '', '', 0, 63, 1),
(77, 13, 2, 1, 'Способы оплаты', '/sposoby-oplaty', '', '', '', '', '', '', '', 0, 64, 1),
(78, 0, 4, 1, 'Лечение зубов', '/lechenie-zubov', '', '', '', '', '', '', '', 0, 65, 1),
(79, 78, 4, 1, 'Лечение кариеса', '/lechenie-kariesa', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 66, 1),
(80, 69, 4, 1, 'Брекеты', '/breket-sistema', '', '', '', '', '', '', '', 0, 67, 1),
(81, 13, 2, 1, 'Информация о правах и обязанностях граждан в сфере охраны здоровья', '/informaciya-o-pravah-i-obyazannostyah-grazhdan-v-sfere-ohrany-zdorovya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 68, 1),
(82, 13, 2, 1, 'Адреса и телефоны контролирующих органов', '/adresa-i-telefony-kontroliruyushchih-organov', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 69, 1),
(83, 13, 2, 1, 'Памятка для граждан о гарантиях бесплатного оказания медпомощи', '/uploads/files/2020/06/10/pamyatka_dlya_grazhdan_o_garantiyah_besplatnogo_okazaniya_med_pomoshchi_1591775910.pdf', '', '', '', '', '_blank', '', '', 0, 70, 1),
(84, 13, 2, 1, 'Дата государственной регистрации, сведения об учредителе', '/data-gosudarstvennoy-registracii-svedeniya-ob-uchreditele', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 71, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_migrations`
--

CREATE TABLE `avantis_migrations` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_migrations`
--

INSERT INTO `avantis_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1547369127),
(2, 'user', 'm131019_212911_user_tokens', 1547369128),
(3, 'user', 'm131025_152911_clean_user_table', 1547369133),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1547369135),
(5, 'user', 'm131106_111552_user_restore_fields', 1547369136),
(6, 'user', 'm131121_190850_modify_tokes_table', 1547369138),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1547369138),
(8, 'user', 'm150416_113652_rename_fields', 1547369139),
(9, 'user', 'm151006_000000_user_add_phone', 1547369139),
(10, 'yupe', 'm000000_000000_yupe_base', 1547369141),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1547369142),
(12, 'yupe', 'm150416_125517_rename_fields', 1547369142),
(13, 'yupe', 'm160204_195213_change_settings_type', 1547369142),
(14, 'category', 'm000000_000000_category_base', 1547369145),
(15, 'category', 'm150415_150436_rename_fields', 1547369145),
(16, 'image', 'm000000_000000_image_base', 1547369149),
(17, 'image', 'm150226_121100_image_order', 1547369150),
(18, 'image', 'm150416_080008_rename_fields', 1547369150),
(19, 'mail', 'm000000_000000_mail_base', 1547369153),
(20, 'notify', 'm141031_091039_add_notify_table', 1547369154),
(21, 'comment', 'm000000_000000_comment_base', 1547369158),
(22, 'comment', 'm130704_095200_comment_nestedsets', 1547369163),
(23, 'comment', 'm150415_151804_rename_fields', 1547369163),
(24, 'blog', 'm000000_000000_blog_base', 1547369183),
(25, 'blog', 'm130503_091124_BlogPostImage', 1547369184),
(26, 'blog', 'm130529_151602_add_post_category', 1547369187),
(27, 'blog', 'm140226_052326_add_community_fields', 1547369189),
(28, 'blog', 'm140714_110238_blog_post_quote_type', 1547369190),
(29, 'blog', 'm150406_094809_blog_post_quote_type', 1547369192),
(30, 'blog', 'm150414_180119_rename_date_fields', 1547369192),
(31, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1547369195),
(32, 'blog', 'm180421_143937_update_blog_meta_column', 1547369196),
(33, 'blog', 'm180421_143938_add_post_meta_title_column', 1547369197),
(34, 'page', 'm000000_000000_page_base', 1547369202),
(35, 'page', 'm130115_155600_columns_rename', 1547369203),
(36, 'page', 'm140115_083618_add_layout', 1547369204),
(37, 'page', 'm140620_072543_add_view', 1547369205),
(38, 'page', 'm150312_151049_change_body_type', 1547369206),
(39, 'page', 'm150416_101038_rename_fields', 1547369206),
(40, 'page', 'm180224_105407_meta_title_column', 1547369207),
(41, 'page', 'm180421_143324_update_page_meta_column', 1547369208),
(42, 'sitemap', 'm141004_130000_sitemap_page', 1547369208),
(43, 'sitemap', 'm141004_140000_sitemap_page_data', 1547369209),
(44, 'feedback', 'm000000_000000_feedback_base', 1547369212),
(45, 'feedback', 'm150415_184108_rename_fields', 1547369213),
(46, 'contentblock', 'm000000_000000_contentblock_base', 1547369214),
(47, 'contentblock', 'm140715_130737_add_category_id', 1547369214),
(48, 'contentblock', 'm150127_130425_add_status_column', 1547369215),
(49, 'callback', 'm150926_083350_callback_base', 1547369217),
(50, 'callback', 'm160621_075232_add_date_to_callback', 1547369217),
(51, 'callback', 'm161125_181730_add_url_to_callback', 1547369218),
(52, 'callback', 'm161204_122528_update_callback_encoding', 1547369218),
(53, 'callback', 'm180224_103745_add_agree_column', 1547369219),
(54, 'menu', 'm000000_000000_menu_base', 1547369222),
(55, 'menu', 'm121220_001126_menu_test_data', 1547369222),
(56, 'menu', 'm160914_134555_fix_menu_item_default_values', 1547369227),
(57, 'news', 'm000000_000000_news_base', 1547369232),
(58, 'news', 'm150416_081251_rename_fields', 1547369232),
(59, 'news', 'm180224_105353_meta_title_column', 1547369233),
(60, 'news', 'm180421_142416_update_news_meta_column', 1547369233),
(61, 'gallery', 'm000000_000000_gallery_base', 1547369237),
(62, 'gallery', 'm130427_120500_gallery_creation_user', 1547369239),
(63, 'gallery', 'm150416_074146_rename_fields', 1547369239),
(64, 'gallery', 'm160514_131314_add_preview_to_gallery', 1547369241),
(65, 'gallery', 'm160515_123559_add_category_to_gallery', 1547369243),
(66, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1547369244),
(67, 'page', 'm161028_090209_add_page_image_tbl', 1547526937),
(68, 'page', 'm181204_051948_add_image_column_to_page_table', 1547526939),
(69, 'review', 'm000000_000000_review_base', 1547980443),
(70, 'review', 'm000000_000001_review_add_column', 1547980443),
(71, 'review', 'm000000_000002_review_add_column_pos', 1547980444),
(72, 'page', 'm191204_051948_add_short_text_column_to_page_table', 1559732582),
(73, 'news', 'm190421_142416_add_title_short_column', 1559794381),
(74, 'store', 'm140812_160000_store_attribute_group_base', 1559912330),
(75, 'store', 'm140812_170000_store_attribute_base', 1559912330),
(76, 'store', 'm140812_180000_store_attribute_option_base', 1559912330),
(77, 'store', 'm140813_200000_store_category_base', 1559912330),
(78, 'store', 'm140813_210000_store_type_base', 1559912330),
(79, 'store', 'm140813_220000_store_type_attribute_base', 1559912330),
(80, 'store', 'm140813_230000_store_producer_base', 1559912331),
(81, 'store', 'm140814_000000_store_product_base', 1559912331),
(82, 'store', 'm140814_000010_store_product_category_base', 1559912331),
(83, 'store', 'm140814_000013_store_product_attribute_eav_base', 1559912332),
(84, 'store', 'm140814_000018_store_product_image_base', 1559912332),
(85, 'store', 'm140814_000020_store_product_variant_base', 1559912332),
(86, 'store', 'm141014_210000_store_product_category_column', 1559912332),
(87, 'store', 'm141015_170000_store_product_image_column', 1559912332),
(88, 'store', 'm141218_091834_default_null', 1559912333),
(89, 'store', 'm150210_063409_add_store_menu_item', 1559912333),
(90, 'store', 'm150210_105811_add_price_column', 1559912333),
(91, 'store', 'm150210_131238_order_category', 1559912333),
(92, 'store', 'm150211_105453_add_position_for_product_variant', 1559912333),
(93, 'store', 'm150226_065935_add_product_position', 1559912333),
(94, 'store', 'm150416_112008_rename_fields', 1559912333),
(95, 'store', 'm150417_180000_store_product_link_base', 1559912334),
(96, 'store', 'm150825_184407_change_store_url', 1559912334),
(97, 'store', 'm150907_084604_new_attributes', 1559912334),
(98, 'store', 'm151218_081635_add_external_id_fields', 1559912334),
(99, 'store', 'm151218_082939_add_external_id_ix', 1559912334),
(100, 'store', 'm151218_142113_add_product_index', 1559912334),
(101, 'store', 'm151223_140722_drop_product_type_categories', 1559912334),
(102, 'store', 'm160210_084850_add_h1_and_canonical', 1559912335),
(103, 'store', 'm160210_131541_add_main_image_alt_title', 1559912335),
(104, 'store', 'm160211_180200_add_additional_images_alt_title', 1559912335),
(105, 'store', 'm160215_110749_add_image_groups_table', 1559912335),
(106, 'store', 'm160227_114934_rename_producer_order_column', 1559912336),
(107, 'store', 'm160309_091039_add_attributes_sort_and_search_fields', 1559912336),
(108, 'store', 'm160413_184551_add_type_attr_fk', 1559912336),
(109, 'store', 'm160602_091243_add_position_product_index', 1559912336),
(110, 'store', 'm160602_091909_add_producer_sort_index', 1559912336),
(111, 'store', 'm160713_105449_remove_irrelevant_product_status', 1559912336),
(112, 'store', 'm160805_070905_add_attribute_description', 1559912336),
(113, 'store', 'm161015_121915_change_product_external_id_type', 1559912336),
(114, 'store', 'm161122_090922_add_sort_product_position', 1559912336),
(115, 'store', 'm161122_093736_add_store_layouts', 1559912336),
(116, 'payment', 'm140815_170000_store_payment_base', 1559912337),
(117, 'delivery', 'm140815_190000_store_delivery_base', 1559912337),
(118, 'delivery', 'm140815_200000_store_delivery_payment_base', 1559912337),
(119, 'order', 'm140814_200000_store_order_base', 1559912338),
(120, 'order', 'm150324_105949_order_status_table', 1559912338),
(121, 'order', 'm150416_100212_rename_fields', 1559912338),
(122, 'order', 'm150514_065554_change_order_price', 1559912338),
(123, 'order', 'm151209_185124_split_address', 1559912339),
(124, 'order', 'm151211_115447_add_appartment_field', 1559912339),
(125, 'order', 'm160415_055344_add_manager_to_order', 1559912339),
(126, 'order', 'm160618_145025_add_status_color', 1559912339),
(127, 'coupon', 'm140816_200000_store_coupon_base', 1559912339),
(128, 'coupon', 'm150414_124659_add_order_coupon_table', 1559912340),
(129, 'coupon', 'm150415_153218_rename_fields', 1559912340),
(130, 'page', 'm201204_051948_add_price_box_column_to_page_table', 1561005791),
(131, 'news', 'm200421_142416_add_free_text_column', 1561095657),
(132, 'page', 'm201300_051948_add_canonical_to_page', 1571652432),
(133, 'page', 'm211300_061948_add_gallery_name_column_to_page_table', 1591351853);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_news_news`
--

CREATE TABLE `avantis_news_news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `title_short` varchar(255) DEFAULT NULL,
  `free_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_news_news`
--

INSERT INTO `avantis_news_news` (`id`, `category_id`, `lang`, `create_time`, `update_time`, `date`, `title`, `slug`, `short_text`, `full_text`, `image`, `link`, `user_id`, `status`, `is_protected`, `meta_keywords`, `meta_description`, `meta_title`, `title_short`, `free_text`) VALUES
(1, NULL, 'ru', '2019-06-06 09:17:50', '2020-07-17 12:09:30', '2019-06-06', 'Зубные пасты с частицами угля', 'zubnye-pasty-s-chasticami-uglya', '<p>Ученые выяснили, что такие пасты плохо влияют на здоровье зубов, а маркетинговые описания не подтверждены исследованиями...</p>', '<p>Производители утверждают, что уголь помогает избавиться от патогенных организмов и следов от чая и кофе, оказывает бактерицидный эффект, улучшает pH-баланс и борется с неприятным запахом изо рта. Однако ученые выяснили, что такие пасты плохо влияют на здоровье зубов, а маркетинговые описания не подтверждены исследованиями.</p><h3>Почему пасты с древесным углем вредны</h3><ul>\r\n	<li>Эрозия зубов;</li>\r\n	<li>Риск развития кариеса;</li>\r\n	<li>Воспаление десен (гингивит) и пародонтит.</li>\r\n</ul><p>Британские исследователи изучили 50 образцов, и всего в 8% из них содержался фтор. Проведенная проверка показала, что в составе образцов не содержится достаточного количества отбеливающих веществ, а высокая абразивность негативно влияет на десны и эмаль.\r\n</p><h3>Нужно ли полностью от них отказываться?</h3><p>Активированный уголь давно используется в медицине и имеет много преимуществ, но применять его для чистки зубов ежедневно не стоит. Если у вас нет трещин на эмали, здоровые десны и вы не носите брекеты, то лучше это делать раз в месяц и тщательно полоскать ротовую полость. В таком случае удастся сохранить баланс между положительным и отрицательным влиянием на зубы.\r\n</p><h3>Можно ли использовать детям?</h3><h2></h2><p>Малышам и подросткам рекомендуется чистить зубы пастой с меньшим содержанием фтора и абразивных веществ и щадящим действием, поэтому стоит с осторожностью подходить к этому вопросу.\r\n</p><p>Стоматологи призывают воздержаться от покупки зубных паст с частичками угля, обосновав это тем, что микрочастицы могут проникнуть в щели между зубами и пломбами и остаться там навсегда. Дэмиен Волмсли назвал образцы «опасным способом отбеливания» и призвал людей с\r\n</p>', '36171e7f3377cd2eb60b6a06bcec7a8d.jpg', '', 1, 0, 0, '', 'Уголь активно используют при изготовлении масок для лица, булочек для бутербродов, в медицине и промышленности. Последний тренд - черные пасты и порошки с углем | Новости клиники «Авантис»', 'Ученые заявили о вреде зубных паст с частицами угля | Новости клиники «Авантис»', 'Ученые заявили о вреде зубных паст с частицами угля', '<p>Уголь активно используют при изготовлении масок для лица, булочек для \r\nбутербродов, в медицине и промышленности. Последний тренд - черные пасты\r\n и порошки с углем.\r\n</p><div><strong style=\"border-bottom:1px solid #232323\">Наибольшую популярность они имеют в:\r\n	</strong>\r\n</div><ul>\r\n	<div>\r\n		<div>\r\n			<li>Великобритании</li>\r\n			<li>Японии</li>\r\n			<li>США</li>\r\n			<li>Индии</li>\r\n			<li>Таиланде</li>\r\n		</div>\r\n		<div>\r\n			<li>Литве</li>\r\n			<li>Австрии</li>\r\n			<li>Китае</li>\r\n			<li>Южной Корее</li>\r\n			<li>Швейцарии.</li>\r\n		</div>\r\n	</div>\r\n</ul>'),
(2, NULL, 'ru', '2019-06-06 09:39:32', '2020-07-17 12:09:33', '2019-06-06', 'Отбеливающие полоски', 'otbelivayushchie-poloski', '<p>Американцы ежегодно тратят миллиарды долларов на средства, помогающие \r\nдобиться белоснежной улыбки. Однако в погоне за красотой...</p>', '<h3>Кто организовал исследование?</h3><p>Вызывают деструкцию зубов накладки с перекисью водорода. Выводы сделали работники Стоктонского университета, находящегося в Нью-Джерси. Они провели опыты, озвучив их итоги на собрании Американского общества биохимии и молекулярной биологии. В 2019 году оно проходило с 6 по 9 апреля в городе Орландо штата Флорида.\r\n</p><h3>Суть эксперимента</h3><p>В прошлые годы проверки ограничивались изучением воздействия отбеливающих лент на эмаль, поэтому процедура считалась безвредной. Это самая твердая ткань в организме человека. Ее сравнивают с алмазом, поэтому при процедуре она не повреждается. Страдает дентин. Биохимия твердых тканей зуба представлена четырьмя слоями:\r\n</p><ul>\r\n	<li>эмаль</li>\r\n	<li>дентин</li>\r\n	<li>десна</li>\r\n	<li>пульпа</li>\r\n</ul><p>Работы экспериментаторов были нацелены на то, чтобы определить, как сильно портится дентин после осветления (располагается он под эмалью и на 90% состоит из коллагена).\r\n</p><p>Для проведения испытания отобрали участников со здоровой ротовой полостью. Ученые пытались решить 2 задачи:\r\n</p><ol>\r\n	<li>Дать полноценную характеристику влияния перекиси на дентин.</li>\r\n	<li> Оценить степень разрушения его фибрилл.</li>\r\n</ol><p>Удалось выяснить, что коллагеновые соединения действительно страдают. Они не растворяются, а дробятся, делясь на мелкие фрагменты.\r\n</p><p>Резюме\r\n</p><h3>В целом, результаты получили неутешительные.<br> Оказалось, что молекулы H2O2 свободно проникают не только в эмаль.<br> Регулярное использование пластырей приводит к резкому снижению белка, в том числе, и в дентине.</h3><p>Удостовериться в правильности выводов помогла вторая работа. Ученые подвергли обработке чистый коллаген. Анализ, проведенный методом электрофореза, показал, что протеиновые связи стали слабыми. Это негативным образом отражается на состоянии зубов.\r\n</p><p>Исследователи не оценивали возможность восстановления соединительных тканей с течением времени. В будущем специалисты предпримут попытку понять, как перекись водорода влияет на остальные белковые компоненты дентина.\r\n</p>', '5ed7554bd8076627af62bed915ced625.jpg', '', 1, 0, 0, '', 'Американцы ежегодно тратят миллиарды долларов на средства, помогающие добиться белоснежной улыбки. Однако в погоне за красотой можно нанести вред здоровью. Этот факт доказан экспериментальным путем | Новости клиники «Авантис»', 'Отбеливающие полоски разрушают дентин | Новости клиники «Авантис»', 'Отбеливающие полоски разрушают дентин', '<p>Американцы ежегодно тратят миллиарды долларов на средства, помогающие \r\nдобиться белоснежной улыбки. Однако в погоне за красотой можно нанести \r\nвред здоровью. Этот факт доказан экспериментальным путем.</p>'),
(3, NULL, 'ru', '2020-07-17 15:59:03', '2020-07-17 16:09:38', '2020-07-17', 'Виниры или брекеты', 'viniry-ili-brekety', '<p>Одна из самых популярных услуг зубной коррекции – это брекеты, который \r\nбыстро восстанавливают нормальную форму коронок. Однако сейчас...\r\n</p>', '<p>Восстановление достигается с помощью металлических дуг. Сапфировые конструкции менее активно устраняют изменения.\r\n</p><p>Виниры при этом имеют скорее маскировочное, чем корректирующее действие. Они могут применяться только для устранения аномалий в передней части ряда – в области первых премоляров, клыков и резцов. Керамика позволяет удалить кривизну и убрать промежутки между ними.\r\n</p><h3>Красота</h3><p>Классические брекеты видны при разговоре. Конечно, они небольших размеров, но при этом все равно их линии видны на эмали. Более красивыми являются производные сапфира. Наименее заметны лингвальные устройства, располагающиеся на задней поверхности. Но при этом они неудобны в носке и имеют высокую цену.\r\n</p><p>В отличие от брекетов, винирные элементы сами формируют эстетику улыбки. Они маскируют все дефекты, корректируют трещины и сколы, заполняют щелочки. При этом можно устранить последствия кариеса и других заболеваний.\r\n</p><h3>Комфорт в использовании</h3><p>Скобки отличаются особым неудобством при ношении. К их недостаткам относится то, что:\r\n</p><ol>\r\n	<li>К ним нужно долго привыкать;</li>\r\n	<li>Они давят на зубы;</li>\r\n	<li>Изменяют произношение слов;</li>\r\n	<li>Из-за них пациенту приходится менять рацион, отказываясь от твердых и вязких продуктов;</li>\r\n	<li>После каждого приема пищи необходима чистка.</li>\r\n</ol><p>Виниры крепятся непосредственно к зубкоронкам. Они не нарушают артикуляцию и не вызывают неприятных ощущений. При установке виниров не требуется и специального питания. Поэтому в плане удобства и эстетичности они значительно превосходят брекетные системы.\r\n</p><p>Таким образом, применение винирных компонентов рекомендовано при маленьких нарушениях, которые не требуют резкой коррекции, а брекеты стоит устанавливать при сильных деформациях.\r\n</p>', '78f466761448b38c14a30fd03de67777.jpg', '', 1, 0, 0, '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 'Лучшие методы эстетической стоматологии | Статьи клиники «Авантис»', 'Лучшие методы эстетической стоматологии', '<p>Одна из самых популярных услуг зубной коррекции – это брекеты, \r\nкоторый быстро восстанавливают нормальную форму коронок. Однако сейчас в\r\n практику внедряются виниры – тонкие пластинки, которые улучшают внешний\r\n вид через маскировку щербинок и других проблемных участков. Разберемся,\r\n какой из способов более эффективный и эстетичный.\r\n</p><h3>Результаты</h3><p>Скобки обладают повышенной эффективностью – они убирают даже тяжелые проблемы. Лечение используется при следующих состояниях:\r\n</p><ol>\r\n	<li>Открытый прикус;</li>\r\n	<li>Отклонение угла нижней челюсти назад или вперед;</li>\r\n	<li>Скрученность.</li>\r\n</ol>'),
(4, NULL, 'ru', '2020-07-17 16:05:25', '2020-07-17 16:06:19', '2020-07-17', 'Соки', 'soki', '<p>В обществе бытует мнение, что фреши – это источник витаминов и \r\nминералов. И это действительно так. Однако последние \r\nэксперименты...\r\n</p>', '<p><strong style=\"text-decoration:underline\">Вред заключается в том, что в него входят:</strong>\r\n</p><ul>\r\n	<li>·      Кислоты;</li>\r\n	<li>·      Глюкоза;</li>\r\n	<li>·      Фруктоза;</li>\r\n	<li>·      Другие углеводы.</li>\r\n</ul><p>Данные элементы в напитках содержатся в большем количестве, чем в самих фруктах. Это вызывает разрушение поверхностного слоя оболочки зуба.\r\n</p><p>Со временем у человека возникает кариес и выраженные деструктивные изменения, которые впоследствии приводит к тяжелым осложнениям. Поэтому Ассоциация британских стоматологов выпустила специальное обращение к пациентам, в котором рекомендуется ограничить употребление свежевыжатых соков и не злоупотреблять ими на ежедневной основе.\r\n</p><h3>Так ли полезны смузи?</h3><p>Другим популярным трендом в здоровом питании могут считаться густые смеси, приготовленные из свежих бананов, яблок и апельсинов, часто с добавлением огурцов и зелени. В интернете представлены сотни рецептов, в каждом из которых подчеркивается полезность для метаболизма. К преимуществам смузи относится:\r\n</p><ol>\r\n	<li>Относительно низкое содержание сахара;</li>\r\n	<li>Высокий уровень клетчатки;</li>\r\n	<li>Множество необходимых для органов компонентов, участвующих в клеточном обмене веществ.</li>\r\n</ol><p>Однако исследования показали, что такие жидкости неблагоприятно сказываются на внешнем виде и составе эмали, покрывающей зубной ряд. Это связано с тем, что они являются более вязкой. Вследствие этого смузи имеют повышенную кислотность. Они быстро обволакивают поверхность зубов и создают на них сладкую пленку.\r\n</p><p>Таким образом, натуральные нектары – это питательные напитки, которые все же нужно употреблять в меру. Их можно считать десертами – а жажду можно утолять и водой. Каждый день стоит вводить в рацион не более 150 мл. При этом лучше использовать соломинку. Это позволит сохранить здоровье рта и снизить риск развития стоматологических заболеваний.\r\n</p>', '28ec610f02cd0f5d55fabee1aa8f746d.jpg', '', 1, 0, 0, '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 'Почему соки не всегда благоприятно влияют на зубы | Статьи клиники «Авантис»', 'Почему соки не всегда благоприятно влияют на зубы?', '<p>В обществе бытует мнение, что фреши – это источник витаминов и \r\nминералов. И это действительно так. Однако последние длительные \r\nэксперименты, проведенные в Англии, подтвердили их возможность \r\nпровоцировать патологические процессы.\r\n</p><h3>Результаты опытов</h3><p>Многие\r\n люди полагают, что фруктовые соки оказывают только положительное \r\nвлияние, поэтому стремятся пить их как можно больше. При этом они не \r\nучитывают то, что жидкие компоненты оказываются вредными для состояния \r\nважных структур.\r\n	<br>\r\n</p>'),
(5, NULL, 'ru', '2020-07-17 16:07:55', '2020-07-17 16:11:19', '2020-07-17', 'Витамины для зубов', 'vitaminy-dlya-zubov', '<p>Чтобы укрепить зубы и десна необходимо сбалансированное питание и употребление соответствующих витаминов. Полезные вещества...\r\n</p>', '<h3>Зачем пить витамины?</h3><div>В энергозатратных ситуациях, во время болезней или беременности организму нужна поддержка.\r\n</div><p><strong>Ведь, если некоторых элементов будет не хватать:</strong>\r\n</p><ol>\r\n	<li>Повышается чувствительность зубов, что говорит об истончении эмали.</li>\r\n	<li>Развиваются патологии, воспаляется слизистая оболочка.</li>\r\n	<li>Зубы становятся подвижными, если мало витамина С.</li>\r\n	<li>Эмаль покрывается черными пятнами, возникает кариес.</li>\r\n</ol><p>Авитаминоз вызывает кровоточивость десен. Он приводит к гингивиту, при котором они воспаляются. Это состояние сопровождается отеками, появлением кровянистых выделений.\r\n</p><p>Всех этих проблем можно избежать, если следить за рационом.\r\n</p><h3>Какие компоненты особенно важны</h3><p>Не обойтись без следующих:\r\n</p><ol>\r\n	<li>А. Они способствуют укреплению тканей.</li>\r\n	<li>Д. Стимулирует восстановительные процессы.</li>\r\n	<li>С. Восстанавливает поврежденные участки.</li>\r\n	<li>К. Участвует в кроветворении.</li>\r\n	<li>Кальций. Поддерживает эмаль.</li>\r\n	<li>Фосфор. С его помощью укрепляются кости.</li>\r\n	<li>Фтор. Обеспечивает достаточную белизну.</li>\r\n</ol><p>Витаминные средства нельзя принимать без ведома врача. Сначала кровь проверяют на уровень микроэлементов, ведь избыток их так же вреден, как и недостаток.  Специалист оценит качество ротовой полости, риск развития осложнений и подберет подходящий медикамент.\r\n</p><h3>Откуда можно получить?</h3><p>Наибольшее количество незаменимых элементов для здоровья содержится в молочных продуктах. Поэтому лучше употреблять молоко, творог, сметану. Эти вещества есть также в бобах и орехах.\r\n</p><p>Фосфор можно также получить из мяса, яиц и рыбы. Жирны морепродукты – это источник витамина Д, а С присутствует в цитрусовых, болгарском перце, моркови, редьке, картофеле, капусте, смородине, К получают из авокадо, бананов, яиц.<br>Множество полезных компонентов можно получить из фруктов, овощей и зелени. Нельзя обходить стороной печень трески, говяжью и гусиную.<br>Если сделать выбор в пользу укрепляющих лекарств, то стоит использовать  Асепту. Она подходит для детей и взрослых.<br>Здоровое питание – это лучшая профилактика заболеваний и поддержка. Если этого недостаточно, врач назначит препараты.\r\n</p>', '0e9b60df37be7378ee13a897075aa04c.jpg', '', 1, 0, 0, '', 'Чтобы укрепить зубы и десна необходимо сбалансированное питание и употребление соответствующих витаминов. Уделять этому вопросу больше внимания рекомендуют в начале весны и в конце осени| Статьи клиники «Авантис»', 'Витамины для зубов | Статьи клиники «Авантис»', 'Витамины для зубов', '<p>Чтобы укрепить зубы и десна необходимо сбалансированное питание и \r\nупотребление соответствующих витаминов. Полезные вещества можно получить\r\n из еды или пропить специальные препараты. Уделять этому вопросу больше \r\nвнимания рекомендуют в начале весны и в конце осени.\r\n</p>'),
(6, NULL, 'ru', '2020-07-17 16:09:14', '2020-07-17 16:11:21', '2020-07-17', 'Что такое миошина?', 'chto-takoe-mioshina', '<p>Применения миошин как подготовительный этап в исправлении прикуса.\r\n</p>', '<h2>Шинотерапия в стоматологии</h2><p>Лечебно-диагностическая шина - медицинский инструмент, изготавливаемый из полимерных материалов гипоаллергенного характера. Внешне она напоминает каппу боксеров, имеет изогнутый вид, соответствующий геометрии человеческих челюстей. Обладает «эффектом памяти». В зависимости от разновидности дисфункции челюстного аппарата, шина может быть надета как на верхнюю, так и на нижнюю челюсть пациента.\r\n</p><p>Основные характеристики шинотерапии:\r\n</p><ol>\r\n	<li>Высокие адаптивные показатели – пациент без последствий в оптимальные сроки привыкает к ношению аппарата, шина не вызывает дискомфорта.</li>\r\n	<li>Подтвержденная эффективность – в отличие от иных видов терапевтического лечения прикуса данный аппарат позволяет увидеть результаты в первые 30 дней лечения.</li>\r\n	<li>Многофункциональность – выравнивает челюстную дугу, распределяет нагрузку при жевании, помогает снизить напряжение в мышечной ткани, способна изменять положение конкретных зубных элементов.\r\n	</li>\r\n</ol><p>Конструкция изготавливается по индивидуальному оттиску, что обеспечивает не только комфорт при работе челюстей, но и надежную фиксацию самой «каппы». Основные виды шин в стоматологии:\r\n</p><h3>Артропатическая</h3><p>Применяется для лечения пациентов с дисфункцией височно-нижнечелюстного сустава, который хрустит, причиняет боль и дискомфорт при открывании рта.\r\n</p><h3>Комбинированная</h3><p>Необходима для инициальной терапии одновременно в двух направлениях – для изменения прикуса и решения проблем в нейромышечной стоматологии.\r\n</p><h3>Миошина</h3><p>Используется в случаях, когда нарушена функциональность лицевых мышц: при жевании пациент испытывает дискомфорт и скованность, наблюдает неконтролируемые спазмы или сжатия челюстей. Это наиболее часто используемая конструкция, которая обладает целым перечнем показаний к применению.\r\n</p><h2>Применение миошин</h2><p>Шины этой категории представляют собой небольшое устройство, созданное из полимеров и устанавливаемое в основном на верхнюю челюсть. Это дугообразный аппарат, снимающий гипертонус жевательных мышц. Они получают так называемую разгрузку, что заметно уменьшает истираемость зубов, пропадают боли и перенапряжение, усталость в момент жевания.\r\n</p><p>Показания:\r\n</p><ol>\r\n	<li>Щелчки, резкие звуки, хруст в челюстях при открывании рта.</li>\r\n	<li>Болевые синдромы – в том числе в области ротовой полости и даже головные боли.</li>\r\n	<li>Парафункции (неосознанная необходимость открывания, закрывания челюстей).</li>\r\n	<li>Бруксизм (неконтролируемый скрежет зубами).</li>\r\n	<li>Чрезмерная утомляемость жевательных мышц, в том числе при разговорах.</li>\r\n	<li>Нарушение прикуса.</li>\r\n</ol><p>Универсальность устройства позволяет устанавливать его практически любому пациенту независимо от возраста, половых признаков и анатомических особенностей. Врач подберет стабилизирующую миошину, которая не вызовет дискомфорта при постоянном ношении. Противопоказания: воспалительные процессы в ротовой полости, чрезмерная активность языка и губ, что приводит к ее смещению и отсутствию терапевтического эффекта.\r\n</p>', '73b4b0a93f29f9fb37fd38176010734b.jpg', '', 1, 0, 0, '', 'Миошина второй категории - стоматологи клиники «Авантис» рассказывают о современных стоматологических методах и технологиях.', 'Миошина второй категории: для чего она предназначена - Блог на сайте клиники «Авантис»', 'Методы исправления прикуса', '<p>Ряд ортопедических работ, таких как накладки виниров или установка коронок, требуют проведения инициальной или начальной терапии. Пациентам необходимо исправить прикус, решить проблемы неконтролируемых мышечных спазмов и перенапряжения мышц в челюстном суставе.\r\n</p><p> Только после этого можно проводить протезирование, будучи уверенным в том, что результатом процедуры станет бесперебойная функциональность зубов, высокие эстетические показатели и корректная работа челюстей.\r\n</p><p> В нейромышечной стоматологии за подготовительный этап отвечает шинотерапия.\r\n</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_notify_settings`
--

CREATE TABLE `avantis_notify_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_page_page`
--

CREATE TABLE `avantis_page_page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `short_content` text,
  `price_box` text,
  `canonical` varchar(255) DEFAULT NULL,
  `gallery_name` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_page_page`
--

INSERT INTO `avantis_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `short_content`, `price_box`, `canonical`, `gallery_name`) VALUES
(1, NULL, 'ru', NULL, '2019-01-13 15:12:09', '2020-07-03 12:45:49', 1, 1, 'Авантис', 'Главная', 'homepage', '<p>Современная стоматология - это не про «световые» пломбы и брекеты. Она может гораздо больше: избавить вас от головной боли, гайморита или храпа, вернуть овалу лица гармонию, а улыбке - красоту и функциональность.</p>  <p>Все болезни от нервов? Возможно. Но десятки популярнейших – от нарушений в зубочелюстной системе. Каждый день мы видим пациентов с проблемами, решить которые можно было без тонн лекарств и скитаний по врачам и косметологам.</p>  <p>Стоматология «Avantis» - это  клиника в Оренбурге, следующая принципам японского профессора Садао Сато и концепции австрийского профессора Рудольфа Славичека. Именно функциональность зубочелюстной системы мы ставим во главу угла. А эстетика - приятный бонус. Высококвалифицированные специалисты, уникальное оборудование, доказанные методики, проверенные материалы - будьте уверены, мы работаем по новейшим мировым стандартам.</p>  <p>Ведущие направления клиники: лечение дисфункции височно-нижнечелюстного ,диагностика заболеваний полости рта, ортодонтия для взрослых и детей, имплантация и реставрация зубов, протезирование, виниры.</p>', 'платная стоматология оренбург, стоматолог оренбург, частная стоматология оренбург, стоматология запись на приём оренбург, стоматологическая клиника оренбург, стоматологическая поликлиника оренбург', 'Частная стоматология в Оренбурге: запись на прием, адрес, телефон и цены. Стоматологическая поликлиника «Авантис» - услуги платного стоматолога и лечение зубов без страха и боли по доступным ценам!', 1, 0, 1, '', '', 'Стоматологическая клиника, частная стоматология в Оренбурге | «Авантис»', '01e0b60545028dd09714e1a64421f076.jpg', '<p>Стоматология «Авантис» - это комплексный подход к здоровью\r\nзубов. Мы смотрим на проблему под разными углами и часто работаем в связке со\r\nспециалистами смежных специальностей: лор-врачами, неврологами, остеопатами.\r\nБлагодаря такой работе и уникальному диагностическому оборудованию, мы\r\nискореняем проблему на корню, а не лечим последствия. Но главное, мы доводим\r\nлечение до конца и добиваемся стабильного результата. Даже у тех пациентов, кто\r\nдо нас прошел другие стоматологии и потратил немало времени и денег.\r\n</p><h3>Перечень оказываемых услуг\r\n</h3><ul><li>Консультация (первичная консультация стоматолога-терапевта, пародонтолога, ортодонта и ортопеда в нашей клинике бесплатна);</li><li><a href=\"https://avantis56.ru/diagnostika\">Диагностика</a>;</li><li><a href=\"https://avantis56.ru/lechenie-kariesa\">Лечение кариеса</a>;</li><li><a href=\"https://avantis56.ru/gigiena-i-profilaktika\">Профессиональная гигиена полости рта</a> (профессиональная чистка зубов, удаление камней);</li><li><a href=\"https://avantis56.ru/implantaciya-zubov\">Имплантация</a> и <a href=\"https://avantis56.ru/protezirovanie-zubov\">протезирование</a> (наши стоматологи помогут подобрать способ восстановления зубного ряда, идеально подходящий в вашем случае);</li><li><a href=\"https://avantis56.ru/ortodontiya\">Ортодонтическое лечение</a> (исправление прикуса с помощью брекетов, элайнеров);</li><li><a href=\"https://avantis56.ru/otbelivanie-zubov\">Отбеливание зубов</a> (лазерное отбеливание зубов Dr.Smile, химическое отбеливание);</li><li><a href=\"https://avantis56.ru/esteticheskaya-restavraciya-zubov\">Эстетическая реставрация зубов</a>;</li><li>Детская стоматология (детский ортодонт).</li></ul><h3>Наши преимущества\r\n</h3><p>Клиник много, стоматология «Авантис» одна. Почему стоит выбрать именно нас?</p><p>1. Доказательная стоматология и новейшие методы.</p><p>Мы единственная стоматология в Оренбурге, где к лечению зубов подходят комплексно и в строгом соответствии с европейскими протоколами и стандартами: мы лечим кариес с использованием коффердама,обрабатываем канал гипохлоритом и используем систему вертикальной конденсации гуттаперчи при депульпировании, исследуем и балансируем работу челюстного сустава и жевательных мышц при ортодонтическом лечении. Мы всегда работаем с применением оптики и новейших компьютерных технологий.</p><p>2. Прием врача-гнатолога.</p><p>Гнатолог знает все о зубочелюстном аппарате и его взаимосвязи с позвоночником, костным и мышечным скелетом тела. Ведь голова может болеть из-за дисфункции ВНЧС, а шея - из-за неправильного прикуса. Врач-гнатолог проводит диагностику, оценивает правильность смыкания зубов, выявляет аномалии и их причины. А после предлагает комплексное решение проблемы.</p><p>3. Мы объясняем.</p><p>Мы подробно и простым языком рассказываем и показываем каждому пациенту, что происходит на каждом этапе лечения и для чего каждый из них необходим.</p><p>4. Мы учимся у лучших в мире.</p><p>Мы не относимся к делу формально. Наши врачи повышают квалификацию несколько раз в год, в том числе, за рубежом. Мы осваиваем самые современные методики и наши кабинеты оснащены уникальной для Оренбурга техникой.</p><p>5. Стоимость лечения фиксирована.</p><p>Неприятные сюрпризы в стоматологии «Авантис» исключены. С каждым пациентом мы составляем договор, который включает индивидуальный план лечения. В нем прописаны все этапы и их стоимость.</p><p>Если для вас эти моменты тоже важны, добро пожаловать к нам. Запись на прием в стоматологию осуществляется через специальную форму ниже либо по звонку по телефону на +7(3532)66-22-66. Наш адрес: г. Оренбург, ул. Высотная, д. 2.</p>', '', 'https://avantis56.ru', NULL),
(2, NULL, 'ru', NULL, '2019-01-14 12:10:42', '2019-08-09 16:28:12', 1, 1, 'Услуги клиники «авантис»', 'Услуги', 'services', '<p>==[[w:PagesWidget|parent_id=2;view=services-page]]==</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику тех или иных заболеваний зубов | Платная стоматологическая поликлиника «Авантис»', 1, 0, 2, '', 'main-services', 'Цены на стоматологические услуги | Клиника «Авантис»', NULL, '', '', NULL, NULL),
(3, NULL, 'ru', NULL, '2019-01-14 12:11:19', '2020-06-18 12:44:36', 1, 1, 'Акции', 'Акции', 'actions', '<h3 style=\"text-align: center;\">Клиники эстетической и функциональной стоматологии «Авантис»</h3>\r\n<div class=\"actions_box\">\r\n	<p style=\"text-transform:uppercase\"><strong>Безопасное лазерное отбеливание зубов – 8999 руб\r\n		</strong>\r\n	</p>\r\n</div>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику тех или иных заболеваний зубов | Платная стоматология «Авантис»', 1, 0, 3, '', '', 'Акции и скидки на стоматологические услуги | Стоматология «Авантис»', NULL, '', '', '', NULL),
(4, NULL, 'ru', NULL, '2019-01-14 12:12:08', '2019-09-18 15:01:14', 1, 1, 'Команда', 'Команда', 'team', '<p>==[[w:PagesWidget|parent_id=4;view=comand-parent]]==</p><p>==[[w:GalleryWidget|galleryId=6;view=team-slider]]==<br></p>', '', 'В нашей команде работают лучшие стоматологи Оренбурга. Лечение зубов без страха и боли по доступным ценам | Клиника «Авантис»', 1, 0, 4, '', '', 'Команда клиники эстетической и функциональной стоматологии «Авантис» г. Оренбург', NULL, '', '', NULL, NULL),
(5, NULL, 'ru', NULL, '2019-01-14 12:13:02', '2019-08-09 16:31:04', 1, 1, 'Наши работы', 'Наши работы', 'our-work', '<p class=\"text-center\"><small><span style=\"color: rgb(127, 127, 127);\">используйте бегунок для просмотра</span></small></p><p>==[[w:GalleryWidget|galleryId=4;view=cocoen]]==\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов | Платная стоматология «Авантис»', 1, 0, 5, '', '', 'Наши работы | Частная стоматология «Авантис»', NULL, '', '', NULL, NULL),
(7, NULL, 'ru', NULL, '2019-01-14 12:23:29', '2019-09-16 10:45:44', 1, 1, 'Контакты', 'Контакты', 'contacti', '<p>65464</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Контактная информация стоматологии «Авантис». Адрес: ↪Оренбург, улица Высотная, дом 2 ☎Телефоны: 662266, 972266', 1, 0, 7, '', 'contacts', 'Адрес и телефон частной стоматологии «Авантис»', '92614b6d18c6d7282b56a27dc584fbd6.jpeg', '', '', NULL, NULL),
(8, NULL, 'ru', NULL, '2019-01-14 12:24:08', '2019-08-09 16:31:51', 1, 1, 'Новости', 'Новости', 'news', '<p>24344</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов | Стоматологическая поликлиника «Авантис»', 1, 0, 8, '', '', 'Новости по стоматологии для пациентов и врачей | Стоматология «Авантис»', NULL, '', '', NULL, NULL),
(11, 2, 'ru', NULL, '2019-01-14 13:34:34', '2019-09-05 14:08:00', 1, 1, 'Вакансии', 'Вакансии', 'vakansii', '<div class=\"jobs\">\r\n	<ul>\r\n		<li class=\"one_job\">\r\n		<h4>Администратор</h4>\r\n		<div class=\"hid_text\" style=\"display: block;\">\r\n			Гражданство РФ, опыт работы по специальности от 3 лет, владение оргтехникой, коммуникабельность, ответственность, дисциплинированность, умение работать в команде, легкая обучаемость.<br>График работы и заработная плата обсуждаются на собеседовании.\r\n		</div>\r\n		</li>\r\n	</ul>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов | Платная стоматология «Авантис»', 1, 0, 11, '', '', 'Вакансии стоматологической клиники «Авантис»', NULL, '', '', NULL, NULL),
(12, 1, 'ru', NULL, '2019-01-14 13:35:10', '2020-07-17 12:47:19', 1, 1, 'Полезные статьи', 'Полезные статьи', 'poleznye-stati', '<p>==[[w:PagesWidget|parent_id=12;view=blog;isUseDataProvider=1]]==</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов | Платная стоматология «Авантис»', 1, 0, 12, '', '', 'Полезные статьи | Частная стоматология «Авантис»', NULL, '', '', '', NULL),
(13, 2, 'ru', NULL, '2019-01-14 13:35:55', '2019-06-28 17:07:59', 1, 1, '3D-стоматология', '3D-стоматология', '3d-stomatologia', '<p>выывпвы</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника Авантис', 0, 0, 13, '', '', '3D-стоматология - Клиника «Авантис»', NULL, '', NULL, NULL, NULL),
(15, NULL, 'ru', 2, '2019-01-15 14:31:56', '2020-07-17 15:50:21', 1, 1, 'Гигиена и профилактика', 'Гигиена и профилактика', 'gigiena-i-profilaktika', '<p>Он включает:</p><ul><li>Сбор информации. Врач-стоматолог опрашивает пациента, выявляя жалобы и вредные привычки;</li><li>Осмотр. Врач-стоматолог обследует ротовую полость, выявляя возможные патологии и оценивая степень проблем;</li><li>Удаление зубного камня при помощи ультразвука (при необходимости);</li><li>Обработка по технологии AIR FLOW;</li><li>Полировка всех зубов;</li><li>Обучение гигиене – чистке зубов и языка, пользованию нитью, межзубными ершиками и другими полезными приспособлениями. Подбор средств для индивидуального ухода;</li><li>Осмотр через неделю: самостоятельная контролируемая чистка, окрашивание налета и оценка результата врачом.</li></ul><p>Посещение гигиениста в стоматологии в Оренбурге позволит сохранить красивую улыбку надолго без сложных и дорогостоящих вмешательств.</p><p style=\"text-align: justify;\">Домашний уход дает возможность убрать лишь мягкий налет и освежить дыхание. А вот удаление камней и пигментированного налета возможно лишь в условиях стоматологического кабинета с применением специализированных препаратов. Игнорирование этих правил приводит к потере зубов уже в молодом возрасте. Профессиональная гигиена включает:\r\n</p><ul>\r\n	<li>Удаление зубного камня при помощи ультразвука</li>\r\n	<li>Аппаратное очищение поверхности зуба</li>\r\n	<li>Полировка  и реминилизация эмали</li>\r\n	<li>Подбор средств для индивидуального ухода</li>\r\n</ul><p style=\"text-align: justify;\">Профессиональная гигиена полости рта – это необходимый комплекс мер. Проходить осмотр рекомендовано не реже одного раза в год, в идеале раз в 6 месяцев. Скопление налета в труднодоступных местах приводит к развитию кариеса, разрушению зуба и его последующей потере. При скоплении отложений зубного камня человека беспокоит неприятный запах и кровоточивость десен. Посещение гигиениста в стоматологии в Оренбурге позволит сохранить красивую улыбку надолго, без сложных и дорогостоящих вмешательств.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Записаться на гигиенические и профилактические процедуры для зубов по низким ценам. Гигиена и профилактика ротовой полости в Оренбурге | Платная стоматология «Авантис»', 1, 0, 15, '', 'services', 'Гигиена и профилактика заболеваний зубов в Оренбурге | Стоматология «Авантис»', '0f6b24216d7d0de85913baf70f8b246c.png', '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/b79c1e8bc778b7e3d5a5260f304ac89b.jpg\" title=\"Гигиена и профилактика\" alt=\"Гигиена и профилактика\">\r\n</div><p style=\"text-align: justify;\">Мы привыкли обращаться в стоматологию, когда уже есть проблема. А это в корне неправильно. Легче предупредить, чем лечить. И гигиена полости рта здесь играет решающую роль. Ведь именно зубной налет и камень подобны бомбе замедленного действия, которая в последствии приводит к заболеванию тканей пародонта и потере зубов. По нашим наблюдениям более 80% пациентов чистят зубы не так, как следует. Да, налет можно регулярно удалять с помощью профессиональной чистки. Но зачем, если мы можем научить вас чистить зубы так, что профессиональная гигиена полости рта понадобится вам не чаще раза в год.</p><p style=\"text-align: justify;\"> Именно поэтому в нашей клинике существует услуга «Урок\r\nгигиены».</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Снятие зубных отложений в области одного зуба ультразвуком\r\n	</td>\r\n	<td>100\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Полировка одного зуба аппаратом Air-Flow\r\n	</td>\r\n	<td>150\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(16, NULL, 'ru', 2, '2019-01-15 14:32:30', '2020-07-01 13:01:14', 1, 1, 'Диагностика', 'Диагностика', 'diagnostika', '<p>Первая консультация врача-стоматолога в клинике «Avantis» бесплатна.</p>  <p>Прием проходит по предварительной записи. Специалист осматривает полость рта, собирает анамнез пациента, определяет проблемы и ставит предварительный диагноз. В процессе пациент получает исчерпывающую информацию о возможных вариантах терапии и стоимости.</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Записаться на консультацию и диагностику к стоматологу по низким ценам. Диагностика заболеваний зубов в Оренбурге | Частная стоматология «Авантис»', 1, 0, 16, '', 'services', 'Записаться на консультацию и диагностику к зубному врачу | Стоматология «Авантис»', '1bfba08e52167ecb481626a81da9abfb.png', '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/e2f9815029acbe81c06678b3a100f2c7.jpg\" title=\"Диагностика\" alt=\"Диагностика\">\r\n</div>\r\n<p>Качественная диагностика в стоматологии – 70% успеха в лечении. Мы работаем, следуя принципам японского профессора Садао Сато и концепции австрийского профессора Рудольфа Славичека – передовых специалистов в области ортодонтии и гнатологии. Мы решаем проблему не только конкретного зуба, подходим к вопросу диагностики комплексно, добираясь до истинных причин.\r\n</p>\r\n<p>Сегодня в нашем арсенале уникальное для Оренбурга оборудование - кондилограф и электромиограф, которые не только позволяют выявить патологии зубочелюстной системы и поставить точный диагноз, но и грамотно спланировать лечение, а также корректировать его в процессе.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Клинические фотографии, консультация, заполнение эстетического анализа\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Расчет ТРГ\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Диагностическая модель (отдублированная)\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Слепок диагностический альгинатный\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Кондилография\r\n	</td>\r\n	<td>8 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Электромиография\r\n	</td>\r\n	<td>4 500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(17, NULL, 'ru', 2, '2019-01-15 14:33:03', '2020-04-06 11:10:56', 1, 1, 'Отбеливание зубов', 'Отбеливание зубов в Оренбурге', 'otbelivanie-zubov', '<p>Профессиональное отбеливание зубов в Оренбурге, обеспечивает оптимальные результаты за относительно короткое время. Этот метод, выполненный под наблюдением стоматолога, приобрел популярность среди тех, кто-либо недоволен безрецептурными продуктами, либо не хочет суетиться и беспокоиться о цвете.\r\n</p><p>Цена отбеливания зубов в Оренбурге является основным отличием при сравнении с другими вариантами. Процедура не является сложной, но требует умения избегать травм десны (десен). Кроме того, для подготовки и завершения процедуры возможно потребуется дорогостоящее оборудование. В общем, процедура занимает от 60 до 90 минут.\r\n</p><h2>Виды отбеливания:\r\n</h2><ol>\r\n	<li><a href=\"https://avantis56.ru/klinicheskoe-otbelivanie-lazer-drsmail\">Лазер Dr. Smail</a></li>\r\n	<li><a href=\"https://avantis56.ru/klinicheskoe-otbelivanie-zoom-4\">ZOOM 4</a></li>\r\n	<li><a href=\"https://avantis56.ru/bezopasnoe-lazernoe-otbelivanie-zubov\">Лазерное (безопасное)</a></li>\r\n	<li><a href=\"https://avantis56.ru/kappa-dlya-otbelivaniya\">Каппы для отбеливания</a>, фторирования</li>\r\n</ol><h2>Преимущества\r\n</h2><p>Отбеливание - это безопасный, улучшающий жизнь процесс, который оказывает положительное влияние как на внешность, так и на психологическое здоровье. Ниже перечислены 4 преимущества:\r\n</p><ul>\r\n	<li>Повышает уверенность в себе</li>\r\n	<p>С отбеливанием уверенность в себе будет стремительно расти. Будь человек на работе, на свидании, проводит презентацию или просто идёт по улице, яркую улыбку трудно пропустить! Блестящая улыбка также свидетельствует о том, что человек заботится о себе и о своей внешности.\r\n	</p>\r\n	<li>Улучшает внешность</li>\r\n	<p>У человека может быть самая шикарная улыбка, но он не застрахован от ущерба, который каждый получает от ежедневной еды и питья. Пища, кофе, чай и сода со временем окрашивают здоровый цвет. После правильной процедуры можно сразу заметить разницу без необходимости «фильтровать» и / или редактировать свои фотографии.\r\n	</p>\r\n	<li>Сводит к минимуму вид морщин</li>\r\n	<p>Если есть беспокойства о морщинах, данная процедура - это отличный вариант. Вместо того чтобы сосредоточиться на морщинах, люди с большей вероятностью сосредоточатся на ярко-белой улыбке. Это отвлечет внимание от появления окружающих морщин, шрамов и угревой сыпи.\r\n	</p>\r\n	<li>Не опустошает кошелек</li>\r\n	<p>Процедура не стоит как пластическая операция. Для продления эффекта, будет достаточно набора для подкраски после лечения.\r\n	</p>\r\n</ul><h2 style=\"text-align: justify;\">Есть ли риски?\r\n</h2><p style=\"text-align: justify;\">Однако многих пациентов эта приятная перспектива отпугивает, поскольку они полагают, что:\r\n</p><ul>\r\n	<li>это слишком дорого</li>\r\n	<li>цвет держится не долго, поэтому нет смысла вкладывать деньги</li>\r\n	<li>из-за отбеливания разрушается эмаль, а значит, повышается чувствительность</li>\r\n</ul><blockquote>В нашей стоматологической клинике применяется новейшая технология бережного отбеливания зубов Zoom 4, которая была разработана в США компанией Discus Dental. Это один из лидеров своей отрасли, специализирующийся именно на отбеливании.\r\n</blockquote><p style=\"text-align: justify;\">Отбеливание в руках опытных практиков – это не опасно. Наиболее распространенной опасностью при отбеливании является неправильное использование и чрезмерное использование или неправильное использование обычных отбеливающих продуктов, которые широко доступны на рынке.\r\n</p><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"dry-PtOEbW4\" data-ratio=\"16:9\">Отбеливание зубов\r\n</div>', 'отбеливание зубов цена оренбург, отбеливание зубов оренбург', 'Узнать стоимость профессионального отбеливания зубов в Оренбурге. Записаться на безопасное отбеливание зубов по низким ценам | Платная стоматология «Авантис»', 1, 0, 17, '', 'services', 'Отбеливание зубов - цены в Оренбурге | Стоматология «Авантис»', '8c16c3ff39c11945f7708b4140034074.png', '<div class=\"thumb\"><img src=\"http://avantis56.ru/uploads/image/0fe305b38a8c007ea1c56937975575f0.jpg\">\r\n</div><p style=\"text-align: justify;\">Привлекательная белоснежная улыбка подчеркивает не только заботу человека о своем здоровье, но и его финансовый статус. Белые зубы становятся своеобразной визитной карточкой, ведь собеседник в первую очередь интуитивно бросает взгляд на лицо и губы.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Клиническое отбеливание лазер Dr. Smail отбеливание\r\n	</td>\r\n	<td>15 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Клиническое отбеливание ZOOM 4\r\n	</td>\r\n	<td>17 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Безопасное лазерное отбеливание зубов</td>\r\n	<td>8 999\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Каппа для отбеливания, фторирования\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(18, NULL, 'ru', 66, '2019-01-15 14:33:51', '2019-09-09 17:11:41', 1, 1, 'Исправление прикуса', 'Исправление прикуса', 'ispravlenie-prikusa', '<h2 style=\"text-align: justify;\">Самолигирующиеся брекеты\r\n</h2><p style=\"text-align: justify;\">Эта технология основана на установке усовершенствованных брекетов. Операция очень проста, поэтому она занимает минимум времени и позволяет исправить прикус у взрослого за 1-1,5 года. Пациенту устанавливают металлические или керамические брекет-системы немецких производителей QuicKlear и Forestadent.\r\n</p><h2 style=\"text-align: justify;\">Лингвальные брекеты\r\n</h2><p style=\"text-align: justify;\">Это «брекеты-невидимки», которые монтируются на внутреннюю поверхность зубов. Они постепенно исправляют прикус и нисколько не мешают вам улыбаться собеседнику. В нашей клинике исправление прикуса у взрослого производится с помощью усовершенствованных брекетов поколения 2d от немецкой компании Forestadent.\r\n</p><h2 style=\"text-align: justify;\">Применение каппы\r\n</h2><p style=\"text-align: justify;\">Каппы (элайнеры) – это прозрачная съемная вставка, которая позволяет выполнять все привычные действия (принимать пищу, чистить зубы, улыбаться) без каких-либо посторонних ощущений. Такие вставки носят от 9 месяцев до 1,5 лет. В нашей клинике исправление прикуса осуществляется с помощью капп итальянского производителя AIRALIGN.\r\n</p>', 'исправление прикуса оренбург, исправление прикуса у взрослого оренбург, исправление прикуса цена', 'Узнать стоимость исправления прикуса в Оренбурге. Исправления прикуса у взрослых по лучшим ценам | Частная стоматология «Авантис»', 1, 0, 18, '', 'services', 'Исправление прикуса у взрослых - цены, исправить прикус в Оренбурге | Стоматология «Авантис»', 'c238418cbfffda083c7204aeb0b8a04a.png', '<div class=\"thumb\"><img src=\"http://avantis56.ru/uploads/image/0f9146772bfdaf96a1455cfce876b433.jpg\">\r\n</div><p style=\"text-align: justify;\">Последние исследования стоматологов показали, что количество людей с неправильным прикусом за последнее время увеличилось: поэтому 7 человек из 10 постоянно сдерживают свою улыбку, чтобы скрыть этот дефект.\r\n<br>\r\nОднако современная стоматология решает и куда более сложные задачи, поэтому исправление прикуса уже давно стало рутинной операцией, гарантирующей стопроцентный результат. В нашей клинике эта процедура проводится несколькими способами.</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Регистрация прикуса\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\"> Брекет система (MBT, ROTH, DAMON, ALEXANDER)\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (первая категория сложности на две челюсти)\r\n	</td>\r\n	<td>25 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (вторая категория сложности две челюсти)\r\n	</td>\r\n	<td>39 800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (третья категория сложности на две челюсти)\r\n	</td>\r\n	<td>55 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (четвертая категория сложности две челюсти)\r\n	</td>\r\n	<td>65 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Повторная фиксация одного брекета\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие аппаратуры две челюсти (без ретейнера, с чисткой)\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Защита композитная выступающих элементов ортодонтической аппаратуры\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы первая категория\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы вторая категория\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры третья категория\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры четвертая  категория\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена металлической дуги\r\n	</td>\r\n	<td>900\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена белой дуги\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MEAW дуга\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Эластичная тяга 1 пакетик\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\"> Ретейнер\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td> Исправление прикуса элайнерами  Invisalign ORTHOSNAP, STAR SMAIL\r\n	</td>\r\n	<td>от 120 000 до 280 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\"> Гнатология\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Определение RP конструктор прикуса (силикон, люксабайт)\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(19, NULL, 'ru', 2, '2019-01-15 14:34:27', '2020-04-06 13:45:50', 1, 1, 'Имплантация зубов', 'Имплантация зубов', 'implantaciya-zubov', '<p>Ничто не заменит естественные зубы, но имплантаты существенно улучшат ситуацию. Имплантаты похожи на винты, изготовленные из титана и других материалов, совместимых с человеческим телом. Имплантант  устанавливается хирургическим путем в верхнюю или нижнюю челюсть, где он заменяет корень отсутствующего.\r\n</p><p>По приемлемой цене имплантации в Оренбурге пациент получает:\r\n</p><p>Имплантат, который выглядит и действует как естественный. Он надежно сидит, даже когда человек жует и говорит. Кроме того, это поможет поддерживать кость.\r\n</p><h2>Какие бывают импланты, и сколько стоит вставить зуб в Оренбурге?<br></h2><p style=\"text-align: justify;\">Импланты бывают корневидными, пластиночными и комбинированными. Первые монтируются непосредственно в корень и могут заменить один или несколько зубов. Вторые устанавливаются под небом. Чтобы их прикрепить, не нужно разрушать целостность челюстной кости. Цену на импланты можно узнать на консультации.\r\n</p><h2 style=\"text-align: justify;\">Кому показана и противопоказана имплантация за один день?\r\n</h2><p style=\"text-align: justify;\">Операция проводится при наличии:\r\n</p><ul>\r\n	<li>Полной адентии</li>\r\n	<li>Дефектов в зубном ряду</li>\r\n</ul><p style=\"text-align: justify;\">Имплантация зубов противопоказана при наличии с сердечно-сосудистых, иммунных заболеваний, сахарного диабета, злокачественных опухолей.\r\n</p><h2>Этапы установки</h2><p>Способ имплантации зубов в Оренбурге зависит от анатомии или структуры кости, типа. Большинство имплантатов включают в себя 3 основных этапа:\r\n</p><ul>\r\n	<li>Размещение;</li>\r\n	<li>Процесс заживления;</li>\r\n	<li>Замена.</li>\r\n</ul><h2>Размещение имплантата</h2><p>Стоматолог хирургически помещает имплантат в челюстную кость. После операции могут наблюдаться отеки и / или болезненность, поэтому для облегчения дискомфорта будут назначены обезболивающие препараты. Специалист порекомендует диету из мягкой пищи во время процесса заживления.\r\n</p><p style=\"text-align: justify;\">Есть два способа проведения процедуры:\r\n</p><ul>\r\n	<li>Базальная. Применяется недавно, это новая технология, позволяющая вживить имплант в базальный слой и кортикальную пластину.</li>\r\n	<li>Традиционная. Используется с конца XIX столетия и доступен по цене. Вся процедура абсолютно безболезненна. Имплантация зубов происходит в несколько этапов и занимает от 2 до 4 дней.</li>\r\n</ul><h2>Процесс заживления\r\n</h2><p>Что делает имплантат таким сильным, так это то, что кость действительно растет вокруг него и удерживает его на месте. Этот процесс называется остеоинтеграцией и занимает много времени. Некоторым людям, возможно, придется подождать, пока объект полностью не интегрируется, прежде чем к имплантату можно будет прикрепить постоянный сменный материал. Это возможно займет несколько месяцев. Продолжительность процесса заживления зависит от местоположения замещающего объекта и типа используемого имплантата.\r\n</p><h2>Замена\r\n</h2><p>Для имплантата специалист изготовит новый, который называется зубной коронкой. После завершения искусственный объект прикрепляется к стойке или абатменту имплантата.\r\n</p><p>Стоматолог поможет решить, сможет ли клиент получить зубной имплантат. <strong>Требования</strong>:\r\n</p><ul>\r\n	<li>Клиент в целом здоров. Некоторые хронические заболевания могут замедлить заживление после операции и помешать успешной установке.\r\n	</li>\r\n	<li>Человек не использует табак в любой форме. Курение или жевание табака - причина частого отказа.</li>\r\n	<li>Хорошая гигиена полости рта очень важна для успеха</li>\r\n	<li>Нужно убедиться, что область вокруг имплантата особенно чистая. Стоматологи рекомендуют использовать специальную зубную щетку, называемую межзубной щеткой, или полоскание для рта, чтобы предотвратить проблемы с деснами.</li>\r\n</ul><div class=\"lazyYT\" data-youtube-id=\"ii6J06OZhd0\" data-ratio=\"16:9\">Имплантация зубов\r\n</div>', 'имплантация зубов оренбург, имплантация зубов цены оренбург', 'Вставить зубы по доступным ценам. Качественная имплантация зубов в Оренбурге | Стоматологическая клиника «Авантис»', 1, 0, 19, '', 'services', 'Имплантация зубов - цена, вставить зубы в Оренбурге | Стоматология «Авантис»', 'd0fa24b531b4be5e8d9a2e430be7ae2a.png', '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/5aa8615d714fe7916571cc8183008bf8.jpg\" title=\"Имплантация зубов\" alt=\"Имплантация зубов\" \"=\"\">\r\n</div><p style=\"text-align: justify;\">Имплантация зубов - процедура, предполагающая установку штифта в челюстную ткань, с последующей установкой коронки. Операция позволяет быстро и недорого восстановить  потерянный зуб. Для этого требуется натуральная костная ткань.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Трепанация зуба , искусственные коронки имплантация\r\n	</td>\r\n	<td>150\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\">Снятие и фиксация коронок\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Снятие коронки штампованной (1ед)\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие коронки литой, металлокерамической (1ед.), разделение 2х литых коронок\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Фиксация 1ой коронки на стеклоиномерный цемент\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Фиксация одной коронки, вкладки и т.п. на композитный цемент\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\">Провизорные коронки\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Провизорная коронка, изготовленная прямым способом\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории\r\n	</td>\r\n	<td>1 200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории армированные\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории на имплантате MIS, Semados, Nobel\r\n	</td>\r\n	<td>3 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Починка провизорной коронки, изготовленной в лаборатории\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\">Коронки, вкладки\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка на основе КХС 1 категория\r\n	</td>\r\n	<td>7 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Вкладка керамическая on-iey, in-ley, over-ley\r\n	</td>\r\n	<td>14 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка (винир) на основе оксида циркония, Е МАХ, рефрактор\r\n	</td>\r\n	<td>17 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Коронка на имплантате винтовая фиксация 1 категория\r\n	</td>\r\n	<td>22 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент индивидуальный циркониевый\r\n	</td>\r\n	<td>8 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент индивидуальный титановый\r\n	</td>\r\n	<td>10 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент стандартный, локатор на имплантат Miss, Semados\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(20, NULL, 'ru', 2, '2019-01-15 14:35:05', '2020-04-29 11:34:28', 1, 1, 'Протезирование зубов', 'Протезирование зубов', 'protezirovanie-zubov', '<p> Протезирование зуба позволит забыть об ограничениях в еде и тщательном измельчении продуктов. Улыбка становится красивой, белоснежной, придавая человеку уверенность в себе. Современное оборудование и новейшие разработки зарубежных дантистов, применяемые в клинике «Авантис», гарантируют лечение на высоком уровне. Качественно проведенная работа протезиста предотвратит смещение ряда, нарушение речи и жевательных функций, сохранит овал лица.\r\n</p><h2>Как проходит протезирование</h2><p>Врач-стоматолог осматривает ротовую полость, лечит и пломбирует, удаляет отложения. Далее делается слепок, по которому в лаборатории изготавливают нужную конструкцию. Компьютерные методики автоматизируют процесс. Готовое изделие фиксируют после примерки и только в том случае, если он подходит пациенту и не портит прикус. В зависимости от общего состояния зубов протезист подберет необходимый вариант лечения.\r\n</p><h3>Съемное протезирование</h3><p>Рекомендовано людям со значительными потерями зубного ряда или с противопоказаниями к имплантации. Фиксация происходит специализированным гелем. Вынимают на ночь для очищения.\r\n</p><p><a href=\"https://avantis56.ru/semnoe-protezirovanie\">Съемные протезы</a> изготавливают из акрила или нейлона. Преимущества: полностью возвращают возможность жевать пищу, а цена на  услуги относительно доступна. Фиксируются гипоаллергенным гелем или крючками-кламмерами. На ночь их необходимо снимать. Возможна комбинация съемных и несъемных элементов. Вначале в челюсть вживляются импланты, к которым крепятся протезы. При необходимости их можно снимать самостоятельно. Крепления могут быть шаровидные, балочные или замочного типа.\r\n</p><h3>Несъемное протезирование</h3><p>Рекомендовано при отсутствии как одной, так и нескольких единиц. <a href=\"https://avantis56.ru/nesemnoe-protezirovanie\">Несъемные протезы</a>  почти не отличимы от натуральных, если правильно выбрать состав. Цена протеза в Оренбурге во многом зависит от материалов:\r\n</p><ol>\r\n	<li>Металл. Плюсы - надежность и низкая стоимость. Недостаток - непривлекательный внешний вид.</li>\r\n	<li>Металлокерамика. Внутренняя часть из металла, а наружная — из керамики, внешне похожей на эмаль. Недостаток - предварительная обточка.</li>\r\n	<li>Керамика. Практически невозможно отличить от настоящих. Материал смотрится очень гармонично, его обычно применяют для видимой части улыбки. Характерна невысокая прочность, сильная нагрузка противопоказана.</li>\r\n	<li>Акрил. Прекрасно имитирует десну. Однако жесткость и микропористая структура при неправильном использовании приводит к неприятным ощущениям и развитию болезнетворных бактерий.</li>\r\n	<li>Нейлон. Не натирает и не травмирует десны. Бактериальное обсеменение минимально из-за структуры. Единственный минус - меньшая нагрузка на кость, чем у акрила. В результате костная ткань нижней челюсти быстрее атрофируется.</li>\r\n</ol><p style=\"text-align: right;\" rel=\"text-align: right;\"><img src=\"https://avantis56.ru/uploads/image/10f52a56092b21afa069e17a5c5e97ab.jpg\" width=\"797\" height=\"319\" alt=\"\" style=\"width: 797px; height: 319px; float: right; margin: 0px 0px 10px 10px;\">\r\n</p><h3>Виды несъемных протезов</h3><ol>\r\n	<li><a href=\"https://avantis56.ru/implantaciya-zubov\">Имплантаты</a> В десну вживляют титановый элемент, на который крепится протез, имитирующий сразу несколько элементов. Имплантация проводится в один или два этапа. Второй вариант более физиологичен, промежуток между установкой стержня и постоянных коронок занимает несколько месяцев. За это время вокруг титанового корня нарастает костная ткань, которая прочно удерживает имплант.</li>\r\n	<li>All-on-4 рекомендуют при полном отсутствии зубов. Мост крепят на четыре импланта в разных концах челюсти.  Этот метод замещает зубной ряд за 1-3 суток.</li>\r\n	<li><a href=\"https://avantis56.ru/iskusstvennye-koronki\">Коронки</a>. Керамику часто применяют для видимой части улыбки. При сильной нагрузке выбирают металлокерамику или диоксид циркония. Крепление к искусственному корню возможно специальным составом или винтовой нарезкой.</li>\r\n	<li>Микропротезы. Исправляют незначительные дефекты. При сохраненном корне кариозные полости, не подлежащие пломбированию, заполняют вкладками. Они незаметны, устойчивы к жеванию. Нездоровый цвет эмали исправят тонкие накладки: <a href=\"https://avantis56.ru/viniry\">виниры</a> или люминиры. Гарантийный срок службы более десяти лет, несмотря на толщину накладки менее миллиметра. Минус - стачивание поверхности перед установкой накладок, но не такое сильное, как для коронок.\r\nЦены на протезирование зубов  различаются в стоматологических центрах Оренбурга. Всё зависит от уровня клиники в целом - компетенции врачей, технического оснащения, медицинских брендов. Специалисты высокой квалификации экономят время и деньги пациента, качественные материалы минимизируют риск развития отторжения и аллергических реакций.\r\n	</li>\r\n</ol><div class=\"lazyYT\" data-youtube-id=\"2KuelrdGMKU\" data-ratio=\"16:9\">Протезирование зубов\r\n</div>', 'протезирование зубов оренбург, протезирование зубов цены оренбург', 'Протезирование зубов по низким ценам: запись на прием, адрес, телефон и цены. Записаться на протезирование зубов в Оренбурге | Частная стоматология «Авантис»', 1, 0, 20, '', 'services', 'Протезирование зубов - цены в Оренбурге | Стоматология «Авантис»', '2d2f844c3482c41f373fe87f42ebfd9d.png', '<div class=\"thumb\"><img src=\"http://avantis56.ru/uploads/image/cd62db3a09625094f79fb77c6cda6d84.jpg\">\r\n</div><p>Протезирование зубов в Оренбурге пользуется регулярным спросом в медицинских центрах. Инновационные ортопедические конструкции восстанавливают отдельные единицы, исправляют небольшие дефекты - трещины, сколы. Они превосходно имитируют натуральную эмаль.</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Изготовление композитного винира (на один зуб)\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\"> Восковое моделирование\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>WaxUp восковое моделирование стандартное 1 зуб\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MockUp перенос  в полость рта макета 1 зуба\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>3D SetUp коррекция положения зубов 1 чел\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\"><strong>Съемное протезирование</strong>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Частичный съемный протез\r\n		</p>\r\n	</td>\r\n	<td>7500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полный съемный протез с акриловыми зубами однослойными\r\n		</p>\r\n	</td>\r\n	<td>\r\n		10000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полный съемный протез с акриловыми зубами Ivoclar-Vivadent Германия\r\n		</p>\r\n	</td>\r\n	<td>\r\n		15000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Металлический базис для полного съемного протеза (дополнение к 94)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		13000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\"><strong>Починка протеза съёмного</strong>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Починка протеза съемного\r\n		</p>\r\n	</td>\r\n	<td>2000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Добавление одного зуба в съемный протез\r\n		</p>\r\n	</td>\r\n	<td>2000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Перебазировка съемного протеза\r\n		</p>\r\n	</td>\r\n	<td>\r\n		1500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Замена пластиковой матрицы в замковом соединении (за 1 матрицу)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		1000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\"><strong>Бюгельное протезирование</strong>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Бюгельное протезирование\r\n		</p>\r\n	</td>\r\n	<td>\r\n		30000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL);
INSERT INTO `avantis_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `short_content`, `price_box`, `canonical`, `gallery_name`) VALUES
(21, NULL, 'ru', 2, '2019-01-15 14:35:40', '2020-04-22 14:11:36', 1, 1, 'Детская стоматология', 'Детская стоматология в Оренбурге', 'detskaya-stomatologiya', '<h2>ОСОБЕННОСТИ ЛЕЧЕНИЯ ДЕТЕЙ И ПОДРОСТКОВ</h2><ul>\r\n<li>Поверхностный слой (эмаль) более склонен к развитию кариеса и его стремительному протеканию.</li>\r\n<li>Корневая система недостаточно сформирована, поэтому требуется большая осторожность для предотвращения травм.</li>\r\n<li>Специальный набор инструментов, который отличается от инструментов для взрослых.</li>\r\n<li>Деликатный подход и терпение.</li>\r\n</ul><h2><img src=\"https://avantis56.ru/uploads/image/04f6b10940837d3ad733f6987976e336.jpg\"><br></h2><p>Детская платная стоматология в Оренбурге предоставляет услуги по следующим направлениям:</p><h3>Лечение молочных и постоянных зубов</h3><p>Частный медицинский центр “Авантис” использует щадящие методы лечения, мягкую анестезию. Для <a href=\"https://avantis56.ru/lechenie-kariesa-u-detey \">лечения кариеса у детей</a>  применяют серебрение, реминерализацию, озонотерапию, пломбирование, депофорез, препарирование.</p><h3>Безболезненное удаление</h3><p>Выпадение молочных зубов бывает довольно болезненным. В нашей клинике помогут избавиться от боли и неприятных ощущений. Отметим, что самостоятельное удаление может быть привести к инфицированию раны.</p><h3>Восстановление молочных зубов</h3><ul>\r\n<li>нормальная работа челюсти, эффективное пережевывание пищи;</li>\r\n<li>отработка речи, дефектов дикции;</li>\r\n<li>устранение комплексов, которые связаны с некрасивой улыбкой;</li>\r\n<li>снижение вероятности деформации прикуса.</li>\r\n</ul><h2>ОРТОДОНТИЯ</h2><p>Платный прием избавит от неправильного прикуса — проблемы, с которой сталкивается большая часть населения. В детстве <a href=\"https://avantis56.ru/detskiy-ortodont\">коррекция</a>  проходит гораздо легче. Устранять недостаток нужно в силу следующих факторов:</p><ul>\r\n<li>неравномерное распределение нагрузки во время жевания - причина пародонтоза;</li>\r\n<li>риск появления дефектов дикции, нарушение устной речи;</li>\r\n<li>развитие бруксизма - скрежета;</li>\r\n<li>микро травмы слизистой, головные боли, щелканье челюсти;</li>\r\n<li>появления комплексов, снижающих самооценку.</li>\r\n</ul><h3><img src=\"https://avantis56.ru/uploads/image/2bf02f41cc2aac75a80c2edf26fb9eff.jpg\"><br></h3><h3>Ортодонтические пластинки</h3><p>Конструкция из пластика и проволоки исправит мезиальный прикус, устранит диастему, смещение ряда, неверное направление роста.</p><p>Пластинки ставят как на верхнюю, так и на нижнюю челюсть. Достаточный корректирующий эффект достигается при их ношении детьми с 6 до 15 лет.</p><h3>Брекеты</h3><p><a href=\"https://avantis56.ru/breket-sistema\">Установка брекет-систем</a> — самый популярный и эффективный метод. Скобы, соединенные силовой дугой, крепятся к передней поверхности специальным клеем. В результате единицы ряда занимают вынужденное положение под давлением дуг.</p><h3>Трейнер</h3><p>Съемная система, которую устанавливают только на ночь в течение 6-12месяцев. <a href=\"https://avantis56.ru/detskiy-ortodont\">Ортодонт</a> рекомендует трейнеры детям в возрасте 4-10 лет, когда происходит формирование челюсти.</p><p><img src=\"https://avantis56.ru/uploads/image/760f39f0bc802191f71060e1a11df9ae.jpg\">\r\n</p><h2>ОБЕЗБОЛИВАНИЕ</h2><p>Ребенок – особенный пациент. Он не знает, как вести себя на приеме. Обычные уговоры здесь не помогают, а страх перед врачом только возрастает из-за болевых ощущений.</p><p>Детская частная стоматология в Оренбурге все болезненные манипуляции проводит под анестезией – проводниковой, инфильтрационной или аппликационной. Для маленьких пациентов разработаны особые обезболивающие средства – цветные гели с приятным вкусом и препараты, не содержащие адреналин.</p><p>Лечение во сне – платные услуги, позволяющие провести санацию рта без малейшего дискомфорта. Однако не стоит путать наркоз и поверхностную седацию – состояние искусственной «полудремы». Эта процедура безопасна. Медицинский специалист оценивает и полностью контролирует состояние больного.</p><h2>КАК ЧАСТО НЕОБХОДИМО ПОСЕЩАТЬ ДЕТСКОГО СТОМАТОЛОГА?</h2><ol>\r\n<li>Посетить ортодонта рекомендуют в 6 месяцев или после прорезывания первого зуба. Этот визит в большей части необходим для родителей: стоматолог информирует о <a href=\"https://avantis56.ru/gigiena-i-profilaktika\">гигиене ротовой полости</a> и о влиянии питания на ее здоровье.</li>\r\n<li>В 12-18 месяцев возможен бутылочный кариес, поэтому важно вовремя обследовать резцы. В дальнейшем рекомендовано посещение 1 раз в полугодие для профилактики.</li>\r\n<li>В 3 года можно оценить правильность сформированного прикуса.</li>\r\n</ol><p>Для детей мы всегда выбираем лучшее. Детский доктор должен обладать навыками психолога, быть предельно аккуратным, внимательным в своих словах и действиях. Частная клиника “Авантис” гарантирует, что плановое посещение дантиста не будет вызывать негативные эмоции.</p><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"4C_qOcWQi1A\" data-ratio=\"16:9\">Детская стоматология\r\n</div>', 'частная детская стоматология оренбург, платная детская стоматология оренбург, детская стоматология оренбург', 'Записаться на прием в платную детскую стоматологию в Оренбурге. Лечение зубов детям без страха и боли по доступным ценам | Частная детская стоматология «Авантис»', 1, 0, 21, '', 'services', 'Платная детская стоматология в Оренбурге | Авантис', '1079d2e3e3e71dff1c11656afc55984b.png', '<div class=\"thumb\"><img src=\"/uploads/image/4e4a7753d86a970f9cc37f3111833229.jpg\">\r\n</div><p style=\"text-align: justify;\">\r\n	Детская стоматология в Оренбурге специализируется на диагностике и лечении зубов у детей. Врачи, работающие в этой сфере, обладают знаниями по стоматологии, педиатрии и психологии. Они умеют найти подход к ребенку - рассказать интересную историю, отвлечь.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (первая категория сложности на две челюсти)\r\n	</td>\r\n	<td>25 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (вторая категория сложности две челюсти)\r\n	</td>\r\n	<td>39 800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (третья категория сложности на две челюсти)\r\n	</td>\r\n	<td>55 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (четвертая категория сложности две челюсти)\r\n	</td>\r\n	<td>65 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Повторная фиксация одного брекета\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие аппаратуры две челюсти (без ретейнера, с чисткой)\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Защита композитная выступающих элементов ортодонтической аппаратуры\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы первая категория\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы вторая категория\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры третья категория\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры четвертая  категория\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена металлической дуги\r\n	</td>\r\n	<td>900\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена белой дуги\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MEAW дуга\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Эластичная тяга 1 пакетик\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(23, NULL, 'ru', 2, '2019-01-15 14:37:02', '2020-04-06 11:19:06', 1, 1, 'Эстетическая реставрация зубов', 'Эстетическая реставрация зубов', 'esteticheskaya-restavraciya-zubov', '<h2>Основные показания к проведению</h2><p>• изменение цвета эмали или ее потемнение, которое невозможно скорректировать ежедневной чисткой, профессиональным очищением и отбеливанием;\r\n</p><p>• чрезмерное стачивание эмали и дентина, которое привело к изменению формы, размера и нарушило естественную функцию зуба;\r\n</p><p>• появление дефектов - щелей, структурных повреждений, сколов;\r\n</p><p>• травмы, кариозное повреждение, последствия хирургического или терапевтического лечения;\r\n</p><p>• деструктивные повреждения тканей, к которым относится гипоплазия, флюороз и т.д.\r\n</p><h2>Методы эстетической реставрации</h2><h3>Прямой метод</h3><p>Восстановление проводится непосредственно в ротовой полости, врач формирует коронку зуба во время процесса лечения. За один визит можно отреставрировать до 6 единиц зубного ряда с помощью свето-отверждаемых композитных материалов или стеклоиномерного цемента.\r\n</p><p>На восстановление одной единицы в среднем уходит от 15 до 60 минут. Главный недостаток — постепенное потускнение материала, утрата первоначального блеска, который был виден после шлифовки. Пациентам, у которых использовался прямой метод, необходимо тщательно выбирать зубную пасту и соблюдать гигиену.\r\n</p><h3>Непрямой метод</h3><p>Виниры-тонкие композитные или фарфоровые пластинки, которые накладываются на препарированные зубы, замещая собой верхний слой эмали. Главная особенность этих накладок — они покрывают всю поверхность зуба, а не только его фронтальную сторону.\r\n</p><p>Проведение процедуры состоит из нескольких этапов:\r\n</p><p>• подготовка-проведение лечения, чистка каналов, их пломбирование (при необходимости);\r\n</p><p>• снятие слепка для изготовления виниров;\r\n</p><p>• установка\r\n</p><p>Виниры показаны пациентам с недостаточно ровными и белоснежными зубами. За счет пластинок улыбка получается идеальной, однако при неправильном прикусе от методики лучше отказаться. Также виниры противопоказаны пациентам, у которых отсутствуют жевательные зубы или они занимаются достаточно травматичными видами спорта (к примеру, бокс).\r\n</p><p>При использовании непрямого метода возможно сочетание коронки и накладок, если они изготавливаются из одного материала.\r\n</p><h3>Отличие от пломбирования</h3><p>С помощью эстетической реставрации восстанавливается анатомическая форма зуба, его целостность и функции. С одной стороны с такой же целью проводится пломбирование, однако различия между этими двумя методиками все же есть:\r\n</p><p>• При установке пломбы в первую очередь восстанавливают функционирование коронки, а реставрация затрагивает и эстетическую составляющую, так как при ее проведении выбираются специальные материалы, которые имитируют дентин и эмаль, подстраиваясь под их цвет, прозрачность.\r\n</p><p>• Установка пломбы возможна, если разрушение затронуло не более 1/3 коронки. Восстановление зуба может выполняться при более выраженном ее повреждении в ситуациях, когда внешне она не повреждена, однако нуждается в исправлении формы, коррекции цвета или устранении широких промежутков между ними.\r\n</p><div class=\"lazyYT\" data-youtube-id=\"X2zC0Wm34jI\" data-ratio=\"16:9\">Эстетическая реставрация зубов\r\n</div><h3>Материалы: имитатор эмали с дентином</h3><p>В стоматологических клиниках с современным оборудованием используются композитные составы. Они застывают только под тепловым или световым воздействием. Поэтому материал может долго оставаться эластичным, позволяя стоматологу создавать натуральный рельеф, воспроизводя естественную форму. Разнообразие цветов имитатора эмали с дентином сделает улыбку естественной.\r\n</p><p>Для композитов характерна прочность и на данный момент они являются идеальным вариантом для выполнения эстетической реставрации.\r\n</p><h3>Виниры, люминиры, коронки и брекеты</h3><p>В стоматологической и ортодонтической практике используется несколько методик, направленных на восстановление зубов в Оренбурге, коррекцию прикуса и устранение эстетических дефектов.\r\n</p><p>Виниры — тонкая пластинка, толщина которой не превышает 0,2-0,6 мм. Она фиксируется на заранее отпрепарированную поверхность. В стоматологических клиниках с современным оборудованием выполняется незначительное обтачивание эмали — 0,3-1 мм. Использование виниров подойдет пациентам с чрезмерно крупными или выпирающими вперед зубами.\r\n</p><p>Люминиры — тонкие пластинки-накладки, толщина которых составляет 02,-0,3 мм. Их главное отличие — возможность фиксации без предварительно стачивания эмали.  Они подходят для пациентов с маленькими коронками и наличием щелей между ними.\r\n</p><p>Коронка — мини-протез, представляющий собой колпачок, который применяют, когда нельзя пломбировать. С его помощью можно устранить внешний дефект, вернуть коронке утраченную функцию, предотвратив ее дальнейшее разрушение.\r\n</p><p>Брекет-система — ортодонтическая конструкция, состоящая из брекетов и дуги, с помощью которой они растягиваются, выравнивая зубной ряд. Такой вид коррекции прикуса позволяет скорректировать функцию пережевывания пищи и улучшить красоту улыбки.\r\n</p><div class=\"lazyYT\" data-youtube-id=\"90lJucIrOHY\" data-ratio=\"16:9\">Эстетическая реставрация зубов\r\n</div><h3>Насколько процедура долговечна?</h3><p>Длительность сохранения эффекта напрямую зависит от образа жизни, рациона питания и соблюдения гигиены ротовой полости.\r\n</p><p>В среднем от одного месяца до одного года. Важно понимать, что спустя некоторое время толщина натуральных тканей начнет уменьшаться и пациенту потребуется непрямая реставрация. Помимо этого восстановительные работы могут затронуть соседние зубы, которые в дальнейшем также будут нуждаться в лечении.\r\n</p><p>Для сохранения эффекта на протяжении нескольких месяцев или даже лет нужно соблюдать определенные правила по уходу.<br>\r\n</p><p>1. На протяжении первых 48 часов оказаться от пищи, которая может окрасить эмаль. Табу также накладывается на использование зубных паст с красителем, губных помад и сигарет.\r\n</p><p>2. Дважды в год посещать стоматолога для плановой полировки.\r\n</p><p>3. Лучше всего отдавать преимущество щеткам с мягкой щетиной, чтобы композитный материал не стирался из-за агрессивного механического воздействия.\r\n</p><p>4. При выборе пасты нужно обращать внимание на pH (он должен быть нейтральным), содержание фтора (оно должно быть повышенным) и отсутствие абразивных частиц.\r\n</p><p>5. Цвет может изменяться при использовании средств для очищения ротовой полости, в которых содержится хлоргексидин или дифторид олова.\r\n</p><p>6. Материалы, используемые при восстановлении, могут растворяться при контакте со спиртом. Поэтому стоит с осторожностью использовать ополаскиватели на их основе и алкогольные напитки.\r\n</p><p>7. Пациентам с бруксизмом в анамнезе рекомендуется ношением специальных кап во время сна.\r\n</p><p>Восстановление разрушенных зубов с помощью эстетической реставрации позволяет справиться с множеством проблем, обусловленных нарушением целостности коронки, наличием щелей, неровностей или изменения цвета эмали. При соблюдении всех правил по уходу улыбка будет сохранять свою прекрасную форму и белизну на протяжении длительного срока.\r\n</p>', 'восстановление зубов оренбург, эстетическая реставрация зубов оренбург, частная стоматология оренбург, стоматологическая поликлиника оренбург, стоматологическая клиника оренбург, платная стоматология оренбург, стоматолог оренбург', 'Восстановление зубов в Оренбурге: лечение кариеса и некариозных поражений по доступным ценам. Эстетическая реставрация зубов в частной стоматологии | Стоматологическая клиника «Авантис»', 1, 0, 76, '', 'services', 'Эстетическая реставрация зубов в Оренбурге | Стоматология «Авантис»', '2ec937d96062d56b17680e5aefda37d4.png', '<div class=\"thumb\"><img src=\"http://avantis56.ru/uploads/image/8338f0125251eae0b25ef23ab8fd68ba.jpg\">\r\n</div><p style=\"text-align: justify;\">Реставрацией называется процедура, при которой восстанавливается форма и функциональность коренных или молочных зубов. Изменяется форма, длина, ширина, убираются внешние недостатки. Реставрация предполагает выравнивание зубного ряда. Часто до ее проведения проводится лечение пришеечного кариеса. Благодаря применению композитных материалов во время процедуры корректируется и восстанавливается эстетика зубного ряда.\r\n</p>', '<table class=\"bordered\">\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги<br>\r\n	</th>\r\n	<th>Цена в рублях<br>\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение одной\r\n            пломбы из композита светового\r\n            отверждения\r\n		</p>\r\n	</td>\r\n	<td>\r\n		1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение\r\n            пломбы при разрушении зуба более ½ \r\n            коронковой  части зуба\r\n		</p>\r\n	</td>\r\n	<td>\r\n		2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение\r\n            пломбы при полном разрушении коронковой\r\n            части зуба(без учета штифтов)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		2 200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Изготовление\r\n            композитного винира (на один зуб)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		2 500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(24, NULL, 'ru', 2, '2019-01-15 14:37:42', '2020-04-06 11:20:22', 1, 1, 'Пародонтология', 'Пародонтология', 'parodontologiya', '<p>Болезни пародонта занимают лидирующие позиции среди всех заболеваний полости рта. Начиная с 35-летнего возраста, у большинства пациентов наблюдаются разные степени поражения. Предоставляемые услуги пародонтолога в Оренбурге позволяют на ранних этапах <a href=\"https://avantis56.ru/diagnostika\">диагностировать</a> патологические процессы и с помощью высокоэффективных методик произвести лечение.<br>\r\n</p><p>Пародонтолог – это узкопрофильный стоматолог, специализирующийся на заболеваниях, затрагивающих пародонт. В компетенцию этого специалиста входит спектр болезней десен и ротовой полости: гингивит, пародонтоз и пародонтит. Согласно всемирной статистике, у более 35% обращающихся пациентов диагностируется поражение пародонта.<br>\r\n</p><h3>В каких случаях к нему обращаются?</h3><p>Консультация пародонтолога необходима, если у пациента имеется один из перечисленных симптомов:\r\n</p><p><img src=\"https://avantis56.ru/uploads/image/1358a8eb92a12877a4598da8243cd236.jpg\" width=\"390\" height=\"261\" alt=\"\" style=\"width: 390px; height: 261px; float: right; margin: 0px 0px 10px 10px;\">\r\n</p><ul>\r\n	<li>отечность десны;</li>\r\n	<li>расшатывание зубов;</li>\r\n	<li>оголение зубной шейки;</li>\r\n	<li>изменение цвета десны и ее кровоточивость;</li>\r\n	<li>чувствительность десен к холодному и горячему;</li>\r\n	<li>болевые ощущения во время гигиенических процедур;</li>\r\n	<li>гнойные образования на десне;</li>\r\n	<li>образование промежутка между зубом и десной;</li>\r\n	<li>неприятный запах.</li>\r\n</ul><h2>Пародонтология. Основные виды заболеваний.</h2><p>Пародонтология представляет собой науку о стоматологии, которая изучает болезни околозубной ткани. Главная ее задача - это устранение имеющихся патологий ротовой полости, укрепление десен и костной ткани, а также профилактика болезней пародонта.\r\n</p><p>Узкопрофильный врач в Оренбурге должен обладать комплексом знаний, необходимых для благоприятного лечения. От его компетентности зависит здоровье не только полости рта, но и всего организма. Запущенные проблемы могут привезти к потере зубов и попаданию гноя в кровеносную систему, что негативно скажется на общем состоянии здоровья. В таких проблемах бессилен стоматолог-терапевт и стоматолог-хирург, только врач пародонтолог способен с точностью поставить диагноз и подобрать оптимальные методы лечения.\r\n</p><p>В его специализацию входят следующие заболевания.\r\n</p><h3>Гингивит</h3><p>Первым и основным симптомом гингивита является кровоточивость десен. На первый взгляд безобидная проблема должна стать поводом к посещению специалиста. Если воспалительный процесс находится на ранней стадии, он затрагивает лишь мягкие ткани.\r\n</p><p>Если же игнорировать кровоточивость десен и верить рекламным роликам, призывающим купить чудодейственную пасту, гингивит стремительно перерастет в более опасную проблему – пародонтит.\r\n</p><h3>Пародонтит</h3><p>Для этого заболевания характерны изменения околозубных тканей, которое приводят к нарушениям соединения десны и зуба. При отсутствии принятых мер происходит его расшатывание с последующим выпадением.\r\n</p><p>В большинстве случаев такой диагноз ставится пациентам в возрасте от 30 лет. Диагностирование в раннем возрасте вызвано нарушенной работой щитовидной железы. В стоматологической практике принято различать пародонтит:\r\n</p><ul>\r\n	<li>юношеский;</li>\r\n	<li>во время беременности;</li>\r\n	<li>при сахарном диабете.</li>\r\n</ul><p>Распознать его можно по следующим симптомам:\r\n</p><ul>\r\n	<li>Кровоточивость десен, возникающая в области нескольких зубов или всего ряда.</li>\r\n	<li>Появление гноя в пародонтальных карманах.</li>\r\n	<li>Неприятный запах из ротовой полости, а также дискомфорт в процессе жевания;</li>\r\n	<li>Оголение шейки зуба.</li>\r\n</ul><h3>Пародонтоз</h3><p>Диагноз пародонтоз ставится при системном патологическом процессе, который охватывает пародонт, периодонт и костные ткани. Он носит невоспалительный характер, что значит отсутствие гнойных выделений и расшатывания зубов. Главными симптомами пародонтоза является обнажение шейки зуба и присутствие зуда в мягких тканях.\r\n</p><p>Наследственность, болезни сердечно-сосудистой системы, гормональные нарушения – все это может послужить развитию болезни. В случае диагностирования пародонтоза в стоматологии Оренбурга пародонтологи рекомендуют проводить рентгенографию для выявления стадии поражения и назначения эффективного лечения.\r\n</p><h2>Лечение</h2><p>Лечение патологических изменений пародонта в зависимости от тяжести формы и поставки диагноза может быть двух видов:\r\n</p><ul>\r\n	<li>Комплексная терапия – наиболее эффективная методика, применяемая на ранних стадиях. Она может включать в себя лечение ротовой полости <a href=\"https://avantis56.ru/professionalnaya-gigiena-polosti-rta-air-flow\">AirFlow</a>, удаление зубных отложений с использованием ультразвука, очистку пародонтальных карманов.</li>\r\n	<li>Хирургические методы. Подразумевают применение лоскутных операций, направленных на коррекцию края мягких тканей, а также глубокую очистку околозубных карманов.</li>\r\n</ul><ul>\r\n	<li>Кюретаж – процедура по глубокой очистке карманов около зуба. Подразумевает удаление имеющихся отложений специальными инструментами, после чего производится полировка корней.</li>\r\n	<li>Пересадка трансплантантов – процедура по пересадке мягких тканей из области неба в уязвимые или оголенные места. Позволяет защитить оголенные зубы от кариеса и зубного камня.</li>\r\n	<li>Гингивэктомия эффективна в запущенных случаях, а также при наличии зубодесневых карманов. Она подразумевает удаление хирургическим путем части десны, чтобы вернуть пациенту желание улыбаться.</li>\r\n</ul><ul>\r\n	<li>Общая терапия. Подразумевает тщательную диагностику пародонта с использованием компьютерных и лабораторных исследований. В данном случае могут назначаться антибактериальные препараты, чтобы остановить воспалительные процессы в полости рта.</li>\r\n	<li>Ортопедическая терапия. Она заключается в проведении шинирования и применении <a href=\"https://avantis56.ru/breket-sistema\">брекет-систем</a>.</li>\r\n</ul><h2><br></h2><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"R1vD7MZI4UI\" data-ratio=\"16:9\">Пародонтология\r\n</div><p><br>\r\n</p><h2>Часто задаваемые вопросы о пародонтологии</h2><p><i>Сколько стоят услуги пародонтолога?</i><i></i>\r\n</p><p>Цена услуг зависит от нескольких факторов: вид и степень тяжести заболевания, количество рекомендованных процедур и способ лечения. Точную стоимость озвучит врач после консультации и постановки диагноза.\r\n</p><p><i>Можно ли ставить протез при пародонтите?</i>\r\n</p><p>Протезирование возможно лишь после того, как будет решена проблема с деснами. В противном случае его результат будет недолговечным.\r\n</p><p><i>Лечение пародонтита болезненно?</i>\r\n</p><p>Врач пародонтолог в Оренбурге имеет в своем арсенале профессиональные знания и опыт. Инвазивные манипуляции проводятся под местной анестезией, способной избавить от любых болевых ощущений.\r\n</p><p><i>Какие преимущества лазерной терапии?</i>\r\n</p><p>Пародонтит в запущенных случаях подразумевает назначение хирургических методов. Использование лазера – это быстрый, эффективный и малотравматичный вариант. Его лучом специалист эффективно стерилизует околозубные карманы, полностью устраняя причину воспалительного процесса.\r\n</p><p><i>Важно ли лечение пародонта?</i>\r\n</p><p>Избавление от болезней пародонта – это эффективный вклад в Ваше здоровье! Оно позволяет вернуть здоровье полости рта и устранить риск выпадения зубов. Регулярное посещение стоматолога, правильная личная гигиена и адекватное лечение способно большинству пациентов сохранить свои зубы на всю жизнь!\r\n</p>', 'пародонтолог оренбург, стоматолог парадонтолог', 'Записаться на прием к стоматологу-пародонтологу по доступным ценам. Качественная пародонтология в Оренбурге | Стоматологическая клиника «Авантис»', 1, 0, 77, '', 'services', 'Пародонтолог - цены в Оренбурге | Стоматология «Авантис»', '5d8b0560c083dc93af43c0e6cdded665.png', '<div class=\"thumb\"><img src=\"http://avantis56.ru/uploads/image/3e73e0ab6d82250fb3053b79cd14f53c.jpg\">\r\n</div><p style=\"text-align: justify;\">Пародонтолог – это врач, специализирующийся на здоровье мягких тканей, окружающих зубы. Именно эти ткани обеспечивают нужное  положение зубов и удерживают их на своих местах. Десны подвержены воспалительным заболеваниям и дистрофии. Если игнорировать это состояние, может ослабнуть зубодесневая связка, что приводит к различным негативным эффектам вплоть до потери зубов. Профилактикой таких заболеваний и устранением последствий занимается пародонтолог.\r\n</p>', '<table class=\"bordered\">\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цена в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обследование полости рта с определением глубины карманов, пародонтальных и гигиенических индексов, составление плана лечения\r\n		</p>\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Медикаментозное лечение зубнодесневых карманов, (орошение, аппликация, инстиляция, повязка и т.д.) в области 1-5 зубов\r\n		</p>\r\n	</td>\r\n	<td>350\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Вскрытие пародонтального абсцесса\r\n		</p>\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Кюретаж пародонтальных карманов в области 1-2 зубов (закрытый)\r\n		</p>\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Избирательное пришлифовывание (сошлифовывание эмали со ската бугра одного зуба)\r\n		</p>\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Шинирование в области 1-6 зубов с помощью композиционных материалов\r\n		</p>\r\n	</td>\r\n	<td>3 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td c=\"\">\r\n		<p>Гингивэктомия (1-2 зуба)\r\n		</p>\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие зубных отложений в области одного зуба ультразвуком\r\n		</p>\r\n	</td>\r\n	<td>100\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полировка одного зуба профессиональными пастами и щетками\r\n		</p>\r\n	</td>\r\n	<td>20\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полировка одного зуба аппаратом Air-Flow\r\n		</p>\r\n	</td>\r\n	<td>150\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Первичная профессиональная гигиена полости рта-удаление налета и зубного камня. Шлифовка и полировка всех зубов. Профессиональное обучение гигиене полости рта\r\n		</p>\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Процедура гигиенической обработки пародонтальных карманов Vector/Aqva-Care 1 челюсть\r\n		</p>\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение лазером 1 сеанс на 1 челюсть\r\n		</p>\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение лазером 1 сеанс на зубодесневой карман, афта и т.п.\r\n		</p>\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение гигиене полости рта с применением ортодонтической щетки для брекетов\r\n		</p>\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение гигиене полости рта с применением межзубных ершиков\r\n		</p>\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Глубокое фторирование эмали с применением эмаль-герметизирующего ликвида (Германия) 1 зуб\r\n		</p>\r\n	</td>\r\n	<td>100\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(25, NULL, 'ru', 66, '2019-01-15 14:38:39', '2019-09-09 17:21:47', 1, 1, 'Лечение дисфункции ВНЧС', 'Лечение дисфункции ВНЧС', 'lechenie-disfunkcii-vnchs', '<h2>Симптомы заболевания</h2><p style=\"text-align: justify;\">Патология может быть связана с самыми разными причинами:\r\n</p><ul>\r\n	<li>Индивидуальные анатомические особенности сустава, строения черепа;</li>\r\n	<li>Травмы ВНЧС (разные формы вывихов и подвывихов);</li>\r\n	<li>Бруксизм – т.е. скрежетание зубами во время сна из-за слишком большого напряжения жевательных мышц;</li>\r\n	<li>Хруст и щелчки во время совершения движений;</li>\r\n	<li>Боли и другие посторонние ощущения (как в покое, так и при открывании рта).</li>\r\n</ul><h2 style=\"text-align: justify;\">Диагностика и лечения</h2><p style=\"text-align: justify;\">Основное условие эффективного лечения – правильно проведенная диагностика. В нашей клинике пациенты проходят кондилографию и аксиографию – современные безболезненные процедуры, которые позволяют получить точные данные о состоянии сустава. После этого врач понимает, как необходимо лечить пациента и назначает соответствующий курс терапии.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Лечение дисфункции височно-нижнечелюстного сустава в Оренбурге. Диагностика и лечение боли в челюстном суставе по лучшим ценам | Стоматологическая поликлиника «Авантис»', 1, 0, 25, '', 'services', 'Лечение дисфункции ВНЧС - цены в Оренбурге | Стоматология «Авантис»', '9fc3d653e1a670c74ce8b9800731742b.png', '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/ee33542729b1e160060a7438f5afa5ad.jpg\" title=\"Лечение дисфункции ВНЧС\" alt=\"Лечение дисфункции ВНЧС\">\r\n</div><p style=\"text-align: justify;\">Височно-нижнечелюстной сустав обеспечивает одну из важнейших функций – он позволяет пережевывать пищу, принимать напитки и говорить. Однако в некоторых случаях у пациентов наблюдаются затруднения в работе нижней челюсти, что проявляется болевым синдромом и другими неприятными симптомами.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Кондилография\r\n	</td>\r\n	<td>8 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Электромиография\r\n	</td>\r\n	<td>4 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Малый функциональный анализ (клинический осмотр, пальпация, аускультация, перенос данных для артикулятора с помощью анатомической лицевой дуги определение RP конструктор прикуса (силикон, люксабайт анализ разборных моделей в артикуляторе, анализ боковой ТРГ, окклюзограммы, фотографии, бруксчеккеры, планирование лечения)\r\n	</td>\r\n	<td>20 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Лечебно-диагностическая мичиганская (стабилизирующая) шина, миошина первая категория\r\n	</td>\r\n	<td>15 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Лечебно-диагностическая мичиганская (стабилизирующая) шина, миошина вторая категория\r\n	</td>\r\n	<td>20 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Лабораторная перебазировка шины\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Контрольное посещение пациентов с шиной. Коррекция шины\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Определение RP конструктор прикуса (силикон, люксабайт)\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Накладка стабилизирующая (композит) с фиксацией\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Фиксация одной стабилизирующей накладки на композит\r\n	</td>\r\n	<td>350\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Изготовление Брукс-чеккеров (одна пара, слепки и модели включены)\r\n	</td>\r\n	<td>4 700\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Изготовление одной единицы (коронка, винир, on-ley, in-ley из оксида циркония, поливошпатной керамики, Еmax) эстетико-функциональной, согласно индивидуальным особенностям ВНЧС. В концепции Р. Славичека. Включает предворительное восковое моделирование, Mook up, один комплект временных.\r\n	</td>\r\n	<td>25 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Брекет лингвальный In Ovation, один зуб\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Дуга для лингвальных брекетов\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL);
INSERT INTO `avantis_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `short_content`, `price_box`, `canonical`, `gallery_name`) VALUES
(26, NULL, 'ru', NULL, '2019-01-16 12:11:18', '2020-06-30 13:58:24', 1, 1, 'Прейскурант цен на стоматологические услуги <br /> ООО «Avantis»', 'Прайс', 'prays', '<p style=\"text-align: center;\" rel=\"text-align: center;\"><span style=\"color: rgb(89, 89, 89);\">(прейскурант цен пролонгируется ежемесячно до изменения директором Поповым А.С)\r\n	</span>\r\n</p>\r\n<h3 class=\"t_stomotologiya\">ТЕРАПЕВТИЧЕСКАЯ СТОМАТОЛОГИЯ</h3>\r\n<div class=\"search-container\">\r\n	<div class=\"search_container\">\r\n		<input class=\"form-control\" type=\"text\" placeholder=\"Поиск по таблице\" id=\"search-text\" onkeyup=\"tableSearch()\">\r\n	</div>\r\n</div>\r\n<table class=\"table table-bordered\" id=\"info-table\">\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<p>Наименование услуги\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>Цена в рублях\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\">\r\n		<p><strong>ОБЩИЕ ВИДЫ   РАБОТ</strong>\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>ТЕРАПИЯ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p><strong></strong>\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Осмотр.   Консультация терапевта (сбор анамнеза, заполнение зубной формулы, оформление   документации)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p><strong>0</strong>\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Осмотр   и консультация (с составлением плана лечения, чтение КТ)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Проведение   аппликации анестезии (гель, спрей, жидкость)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Инъекционная   анестезия (инфильтрационная, проводниковая, антралигаментарная,   внутрипульпарная)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   коффердама , optraGate\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>250\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие   пломбы\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Трепанация   зуба, искусственные коронки\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>150\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   временной пломбы\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   матричной системы\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   изолирующей, лечебной прокладки\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полировка   пломбы с пастой\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Финишная   обработка пломбы\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Эндоотбеливание   1 зуб\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Клиническое   отбеливание лазер Dr.Smile\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>15 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Клиническое   отбеливание ZOOM 4\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>17 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фиксация   crystal SWAROVSKI на зуб\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>ЛЕЧЕНИЕ КАРИЕСА И НЕКАРИОЗНЫХ   ПОРАЖЕНИЙ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   одной пломбы из композита светового отверждения\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   пломбы при разрушении зуба до ½ коронковой части\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение   пломбы при разрушении более ½ коронковой части зуба (без учета штифтов)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>3000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Изготовление   композитного винира (на один зуб)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полное   изготовление коронковой части зуба из композита светового отверждения (BuildUp) без учета штифтов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>3000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>ЭНДОДОНТИЧЕСКИЕ ВИДЫ РАБОТ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Механическая   и медикаментозная обработка одного корневого канала ручным способом\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>150\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Механическая   и медикаментозная обработка одного корневого канала машинным способом\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обтурация   одного корневого канала вертикальной конденсации гуттаперчи\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Временное   пломбирование одного корневого канала лечебной пастой\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Извлечение   инородного тела из корневого канала сложное\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Извлечение   инородного тела из корневого канала простое\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Распломбирование   одного корневого канала\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>600\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фиксация   стекловолоконного штифта в корневой канал\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фиксация   титанового штифта в корневой канал\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>700\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Закрытие   внутрикорневых перфораций и перфораций в области фуркаций\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Подготовка   корневого канала под ШКВ\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>600\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Медикаментозная   обработка корневого канала\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>ПАРОДОНТОЛОГИЧЕСКИЙ ПРИЕМ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обследование   полости рта с определением глубины карманов, пародонтальных и гигиенических   индексов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Медикаментозное   лечение зубнодеснеых карманов (орошение, аппликация, инстиляция, повязка и   т.д.) в области 1-5 зубов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>350\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Вскрытие   пародонтального абсцесса\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>250\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Кюретаж   пародонтальных карманов в области 1-2 зубов (закрытый)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>250\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Избирательное   пришлифовывание (сошлифовывание эмали со ската бугра одного зуба)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Шинирование   в области 1-6 зубов с помощью композитных материалов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>3000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Гингивэктомия   (1-2 зуба)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие   зубных отложений в области одного зуба ультразвуком\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полировка   одного зуба профессиональными пастами и щетками\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>20\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полировка   одного зуба аппаратом Air-Flow\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>150\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Первичная   профессиональная гигиена полости рта – удаление налета и зубного камня.   Шлифовка и полировка всех зубов. Профессиональное обучение гигиене полости   рта\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Шинирование   в области 1-5 зубов с помощью стекловолоконной ленты и композитных материалов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>5000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение   лазером 1 сеанс на 1 челюсть\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение   лазером 1 сеансна зубодесневой карман, афта и т.п.\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение   гигиене полости рта с применением ортодонтической щетки для брекетов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение   гигиене полости рта с применением межзубных ершиков\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Глубокое   фторирование эмали с применением эмаль-герметизирующего ликвида (Германия) 1   зуб\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Коррекция   десневого контура лазером\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение   гигиене полости рта ирригатором Revyline\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4750\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>ОРТОПЕДИЯ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Первичная   консультация стоматолога ортодонта, ортопеда\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>0\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   стоматолога ортопеда, ортодонта (осмотр, клинические фотографии,   консультация, заполнение первичной медицинской документации, составление   плана лечения)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   врача стоматолога-гнатолога, составление плана лечения при дисфункции ВНЧС\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Диагностика</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Клинические   фотографии, консультация, заполнение эстетического анализа\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Расчет   ТРГ\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Диагностическая   модель (отдублированная)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Слепок   диагностический альгинантный\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Снятие и фиксация коронок</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие   коронки штампованной (1 ед)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие   коронки литой, металлокерамической (1ед.), разделение двух литых коронок\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фиксация   1ой коронки на стеклоиномерный цемент\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фиксация   одной коронки, вкладки и т.п. на композитный цемент\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Снятие слепков</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Слепок,   снятый альгинантными массами (1 слепок)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>400\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Двойной   слепок, снятый силиконовыми массами (1 слепок)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>700\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Слепок,   снятый полиэфирными массами с использованием Pentamix (1 слепок)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Трансферт   чек. Абатмент чек\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1600\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Регистрация   прикуса\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лицевая   дуга. Перенос данных для артикулятора\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Индивидуальная   ложка\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>700\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Ретракционная   нить 1 нить\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>100\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Диагностическая   модель отдублирования 1 шт.\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>700\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Несъемное протезирование</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Восковое моделирование</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>WaxUp восковое моделирование стандартное 1   зуб\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>MockUp перенос в полость рта макета 1 зуба\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>3D SetUp\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Провизорные коронки</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Провизорная   коронка, изготовленная прямым способом\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Провизорные   коронки, изготовленные в лаборатории\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Провизорные   коронки, изготовленные в даборатории армированные\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1600\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Провизорные   коронки, изготовленные в лаборатории на имплантате MIS,   Semados, Nobel\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>3650\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Починка   провизорной коронки, изготовленной в лаборатории\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Провизорные   коронки, изготовленные в лаборатории на имплантате Semados\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4250\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Коронки, вкладки</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Керамическая   коронка на основе КХС 1 категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>7500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Вкладка   керамическая on-ley, over-ley\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>14 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Керамическая   коронка (винир) на основе оксида циркония, Е МАХ, рефрактор\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>17 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Коронка   на имплантате винтовая фиксация 1 категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>25 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Абатмент   индивидуальный титановый\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>8500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Абатмент   индивидуальный циркониевый\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Абатмент   стандартный, локатор на имплантат MIS, Semados\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>5000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Вкладка   штифто-культевая\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2700\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Съемное протезирование</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Частичный   съёмный протез\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>7500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полный   съемный протез с акриловыми зубами однослойными\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Полный   съемный протез с акриловыми зубами  Ivoclar-Vivadent Германия.\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>15 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Металлический   базис для полного съемного протеза (дополнение к 94)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>13 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Частичный   съемный протез от 1 до 4 зубов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Починка протеза съемного</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Починка   протеза съемного\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Добавление   одного зуба в съемный протез\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Перебазировка   съемного протеза\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Замена   пластиковой матрицы в замковом соединении (за 1 матрицу)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Бюгельное протезирование</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Бюгельное   протезирование\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>30 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Каппы</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Каппа   для отбеливания, фторирования\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Каппа   жесткая\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>ОРТОДОНТИЯ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Общие услуги</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Аппарат   Френкеля, Твин-Блок\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>15 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Трейнер   ортодонтический\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>8500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Аппарат   быстрое небное расширение\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>12 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Контрольный   прием, припасовка аппарата\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Аппарат   ортодонтический малый ORTHO-TAIN\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>7500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Брекет</strong><strong> </strong><strong>система</strong><strong> (</strong><strong>МВТ</strong><strong>, ROTH, DAMON, ALEXANDER) </strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Установка   брекет-системы металлической (вторая категория сложности,две челюсти)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>39 800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Установка   брекет-системы (третья категория сложности,две челюсти)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>55 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Установка   брекет-системы (четвертая категория сложности,две челюсти)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>65 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение   на брекет-системе многопетлевой техникой – 20 эстетических брекетов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>82 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Повторная   фиксация с заменой брекета\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Эстетическая   брекет система Edgewise при лечении   мультепетлевой техникой\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>85 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Брекет   система Edgewise эстетичный набор 20 ед.\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>92 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Брекет   система Edgewise при лечении техникой мультипетлевой дуги\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>112 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Брекет-система   Edgewise 6/6 комбинированная при лечении техникой мультепетлевой дуги\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>95 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>MEAW   дуга сложная\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Плановый   лечебный прием при лечении техникой многопетлевой дуги MEAW\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Брекет   система комбенированная 6/6\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>75 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Повторная   фиксация одного брекета\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие   аппаратуры (без ретейнера с чисткой)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>5000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Защита   композитная выступающих элементов ортодонтической аппаратуры\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Плановый   лечебный сеанс для активации брекет системы первая категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Плановый   лечебный сеанс для активации брекет системы вторая категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Плановый   лечебный сеанс для активации брекет системы третья категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Плановый   лечебный сеанс для активации брекет системы четвертая категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Замена   металлической дуги\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>900\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Замена   белой дуги\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1300\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>MEAW дуга простая\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Эластичная   тяга 1 пакетик\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>250\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта (ортодонтический воск)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Ретейнер </strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Несъемный   ретейнер на одну челюсть\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Починка   ретейнера в области одного зуба\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Съемный   ретейнер на одну челюсть типа OSAMU\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2350\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Плановый   лечебный сеанс Invisalign\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Снятие   брекет системы с установкой шинирующего ретейнера 1 челюсть\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>7000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Гнатология</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Ботулинотерапия   100 единиц\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>16 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Кондилография\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>от 12 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Электромиография\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>от 4500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Малый   функциональный анализ (клинический осмотр, пальпация, аускультация, перенос   данных для артикулятора с помощью анатомической лицевой дуги определение RP конструктор прикуса (силикон,   люксабайт анализ разборных моделей в артикуляторе , анализ боковой ТРГ,   окклюзограммы, фотографии, бруксчеккеры, планирование лечения)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p> 20 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечебно-диагностическая   мичиганская (стабилизирующая) шина, миошина первая категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>15 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечебно-диагностическая   мичиганская (стабилизирующая) шина, миошина вторая категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>20 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лабораторная   перебазировка шины\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Контрольное   посещение пациентов с шиной. Коррекция шины\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Определение   PR конструктор прикуса (силикон,   люксобайт)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Накладка   стабилизирующая (композит) с фиксацией\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фиксация   одной стабилизирующей накладки на композит\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>350\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечебно-диагностическая   дистракционная суставная шина\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>35 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Изготовление   Брукс-чеккеров (одна пара, слепки и модели включены)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4700\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Изготовление   одной единицы (коронка, винир, on-ley, in-ley из оксида циркония, поливошпатнойкерамики, Emax)   эстетико-функциональной, согласно индивидуальным особенностям ВНЧС. В   концепции Р.Славичека. включает предварительное восковое моделирование, Mock up,   один комплект временных\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>25 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Брекет   лингвальный In Ovation,   один зуб\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>5000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Дуга   для лингвальных брекетов\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Кондилография   3 категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>16 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Фотометрический   анализ, перенос данных в артикулятор\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>6000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечебно-диагностическая   антериоризирующая шина 3 категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>28 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Пришлифовка   миошины в центральном соотношении\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1500\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Диагностика   и планирование лечения дисфункции левого/правого ВНЧС с односторонним   блокированием\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>42 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Пришлифовка   шины в терапевтическом положении\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Кондилография   2 категория\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>12 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Лечение Элайнер-системой</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение   элайнер системой до 10 единиц\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>75 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение   элайнер системой до 30 единиц\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>220 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Лечение   Трейнер2 класс\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Пластинка   сложная\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>12 000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Гигиена</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение   гигиене полости рта с помощью щетки Curaprox 1006 single/1009 single\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>650\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Обучение   гигиене полости рта с помощью щетки Curaprox CS5460   ortho/softCS1560/supersoft CS/ultrasoft\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>650\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием ершиков межзубных CPS 06 prime/ CPS 07,08   prime\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1900\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием ершиков межзубных CPSprime plus   06/07/08/09/011/mix и CPS14\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>1000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием таблеток для индикации зубного налета   РСА 223 (12 шт.)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>530\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием набора скребков для языка СТС 203\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>800\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием зубной пасты BeYou single\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>2000\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием зубной пасты Enzycal\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>940\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консудьтация   по гигиене полости рта с использованием зубной пасты CURASEPT\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>850\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта (ортодонтический воск)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>200\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта, обучение (ирригатор Revyline)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>4750\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости рта с использованием электрической звуковой зубной щетки   Revyline\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>3750\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Консультация   по гигиене полости(набор зубных щеток   Revyline Premium 6 шт.)\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>600\r\n		</p>\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Платная стоматологическая поликлиника: цены, перечень услуг и отзывы пациентов. Запись на прием к стоматологу в Оренбурге. Телефон стоматологии: +7(3532) 66-22-66 | Частная стоматология «Авантис»', 1, 0, 26, '', 'price', 'Цены на стоматологические услуги в Оренбурге | Стоматология «Авантис»', NULL, '', '', '', NULL),
(27, NULL, 'ru', 4, '2019-01-16 15:08:11', '2020-05-26 12:18:44', 1, 1, 'медсестра, ассистент врача', 'Белогорцева Ольга Сергеевна', 'belogorceva', '<p>медсестра, ассистент врача</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника Авантис', 1, 0, 40, '', 'comand', 'Белогорцева Ольга Сергеевна - медсестра, ассистент врача|Клиника «Авантис»', '79f83c00d6435303d4d0559a6f1756c6.jpg', '', '', NULL, NULL),
(28, NULL, 'ru', 4, '2019-01-16 15:57:35', '2020-06-10 11:52:37', 1, 1, 'стоматолог-терапевт', 'Патрушев Александр Юрьевич', 'patrushev', '<p><strong>Стоматолог-терапевт</strong>\r\n</p><p>Диплом по\r\nспециальности «Стоматология» выдан 25 июня 2010г., Сертификат специалиста\r\n№005214 «Стоматология терапевтическая» выдача 24 декабря 2018г., Сертификат\r\nспециалиста №1304 «Стоматология хирургическая» выдан 16 апреля\r\n2016г., Сертификат №17120 «Стоматология общей практики» выдан 31 августа 2011г.\r\n	<br>\r\n</p><p><strong>График работы</strong><strong> и часы приема</strong><br></p><p>Понедельник, вторник, среда, четверг 14:00 – 20:00</p><p>Пятница 08:00 – 20:00</p><p>Суббота 10:00 – 16:00</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 29, '', 'comand', 'Патрушев Александр Юрьевич - врач стоматолог-терапевт|Клиника «Авантис»', '8294bf9033efe3907556396fdf0eb8c6.jpg', '', '', '', 8),
(29, NULL, 'ru', 4, '2019-01-16 15:59:57', '2020-06-10 11:53:07', 1, 1, 'стоматолог-терапевт', 'Попова Евгения Викторовна', 'popova', '<p><strong>Стоматолог-терапевт</strong>\r\n</p><p>Диплом с\r\nотличием по специальности «Стоматология» выдан 25 июня 2010г., Диплом о\r\nпрофессиональной переподготовке №234 «Стоматология терапевтическая» выдан 14\r\nдекабря 2013г., Сертификат специалиста №004157 «Стоматология терапевтическая»\r\nвыдан 20 ноября 2018 г., Сертификат №4375474 «Стоматология общей практики»\r\nвыдан 31 августа 2011г.\r\n	<br>\r\n</p><p><strong>График работы</strong> <strong>и часы приема</strong><br></p><p><span class=\"redactor-invisible-space\">Понедельник,\r\nвторник, среда, четверг 08:00 – 14:00<br></span></p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 28, '', 'comand', 'Попова Евгения Викторовна - врач стоматолог-терапевт|Клиника «Авантис»', '0aa402828d3b88deda099f8574e3454d.jpg', '', '', '', 10),
(30, NULL, 'ru', 4, '2019-01-16 16:01:28', '2020-06-10 11:52:52', 1, 1, 'стоматолог-ортопед', 'Щетинин Антон Владимирович', 'shetinin', '<p><strong>Стоматолог-ортопед</strong>\r\n</p><p>Диплом с отличием по специальности «Стоматология» выдан 26 июня 2012г «Государственное бюджетное образовательное учреждение высшего профессионального образования «Оренбургская государственная медицинская   академия» Министерства здравоохранения и социального развития Российской Федерации», Диплом № 201\\13 «Стоматология общей практики» выдача 31 августа 2013г., Диплом о профессиональной переподготовке № 0638 «Стоматология ортопедическая» выдача 27 декабря 2014г., Сертификат специалиста №1278\\19 «Стоматология ортопедическая» выдача 10 апреля 2019 г. действует до 2024г., Удостоверение о повышение квалификации №1902 «Стоматология ортопедическая» выдача 10 апреля 2019г.\r\n</p><p><strong>График работы</strong> <strong>и часы приема</strong><br></p><p><span class=\"redactor-invisible-space\">Среда, четверг:\r\nчетные – 15:00 – 20:00, нечетные - 08:00 – 13:00 <br></span></p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 30, '', 'comand', 'Щетинин Антон Владимирович - врач стоматолог-ортопед|Клиника «Авантис»', '4b31180ee5b34756c395ecd4ba28826f.jpg', '', '', '', 7),
(31, NULL, 'ru', 4, '2019-01-16 16:03:52', '2020-06-10 11:51:26', 1, 1, 'гнатолог, ортодонт, ортопед', 'Попов Антон Сергеевич', 'popov', '<p><strong>Гнатолог, ортодонт, ортопед</strong></p><p>Диплом с отличием по специальности «Стоматология» выдан 17 июня 2009 г. «Государственное образовательное учреждение высшего профессионального образования Оренбургская государственная медицинская академия Федерального агентства по здравоохранению и социальному развитию, Сертификат специалиста №0227-12 «Стоматология ортопедическая» выдан 27.02.2019г. действует до 2024 г, Диплом о профессиональной переподготовке №М 1902/27-003 «Стоматология ортопедическая» выдан 27 февраля 2019г, Диплом о профессиональной переподготовки по программе «Организация здравоохранения и общественное здоровье» выдан 19 ноября 2018г, Сертификат специалиста №004142 «Организация здравоохранения и общественное здоровье» выдан 19.11.2018, Сертификат специалиста № 01563101141673 «Ортодонтия», Удостоверение к диплому ВСА №0728942 выдан 31 августа 2012г «Ортодонтия», Сертификат А №4408447 «Стоматология общей практики» выдан 30.06.2010г, Сертификат специалиста №10732/17«Ортодонтия» выдан 24 октября 2017г.</p><p><strong>График работы</strong> <strong>и часы приема</strong></p><p>Понедельник 08:00 – 20:00</p><p>Вторник 08:00 – 20:00</p><p>Среда: четные – 8:00 – 13:00, нечетные 13:00 – 20:00</p><p>Четверг: четные – 8:00 – 13:00, нечетные 13:00 – 20:00</p><p>Пятница 08:00 – 20:00</p><p><strong>          </strong></p><p>Суббота 10:00 – 16:00</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 27, '', 'comand', 'Попов Антон Сергеевич - врач стоматолог, гнатолог, ортодонт, ортопед|Клиника «Авантис»', '58744dbeab437259f19e505ae70fa314.jpg', '', '', '', 9),
(32, 1, 'ru', NULL, '2019-01-17 10:13:11', '2019-07-17 17:08:18', 1, 1, 'Вопросы и ответы', 'Вопросы и ответы', 'voprosy-i-otvety', '<div class=\"jobs\">\r\n	<ul>\r\n		<li class=\"one_job\">\r\n		<h4>1. С чем связана высокая чувствительность зубов?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			В списке возможных причин – эрозия эмали как результат воздействия на нее кислот, содержащихся в пище, напитках. Также на истончение зубной пластины влияет степень жесткости щетки. Не стоит с силой тереть эмаль при чистке, достаточно будет плавных движений. У части пациентов обнажается зубной корень, в результате чего опускается десна. Это серьезно сказывается на чувствительности.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>2. Чем опасен неправильный прикус?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			Данная аномалия может спровоцировать быстрое стирание зубов, а в придачу вызвать различные заболевания десен. Неисправленный прикус грозит человеку серьезными изменениями в нижнечелюстном суставе.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>3. Можно ли удалять несколько зубов одновременно?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			При отсутствии необходимости врачи стараются не вырывать у пациента более одной штуки.  Ведь для организма это очевидный стресс. Послеоперационная ранка может заживать достаточно долго. В крайних случаях, если оба зуба, подлежащие удалению, располагаются вплотную друг к другу, то удаление возможно. Хотя лучше запастись терпением и посетить стоматолога для очередной операции, выждав определенное время.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>4. После удаления зуба посинела десна. Как быть?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			Синий цвет объясняется возникновением гематомы. У многих пациентов это реакция на обезболивающий укол, что не катастрофично: через несколько дней десна примет первичный окрас. А вот если гематома долго не исчезает, десна распухла и есть боль, то, возможно, удаление было произведено некачественно. Следует немедленно обратиться в клинику за помощью.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>5. Зуб сильно разрушен. Есть ли надежда его сохранить?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			Степень разрушения имеет немалое значение. Если она невелика, стоматолог пытается максимально сохранить зуб, завершив предварительное лечение установкой коронки. Если нет смысла восстанавливать разрушенный объект, его удаляют. Пациенту предлагают вживить имплант.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>6. Стоит ли проводить профессиональную чистку полости рта?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			Желательно пройти подобный комплекс процедур. Его применение избавит от зубного камня и налета. Вследствие ряда гигиенических манипуляций эмаль станет значительно светлее. До проведения серьезного лечения полости рта врач, при отсутствии противопоказаний, назначает профессиональную чистку зубов. У процедуры существуют особенности. Она не назначается пациентам моложе 16 лет, - ведь ткань зубов в этом возрасте не окончательно сформирована. И людям с особо чувствительной эмалью не предложат профчистку. При наличии воспаления десен или сложного кариеса врач назначит предварительное лечение. Некоторые сердечные недуги и эпилепсия также находятся в списке противопоказаний для проведения процедуры.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>7. Можно ли лечить зубы во время беременности?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			Разрешается, но в определенные периоды. Врачи рекомендуют посещать стоматолога на 4-ом, 5-ом и 6-ом месяце беременности. Указанный период - самый безопасный. Если в медицинской карте не отмечены противопоказания, то лечение, включая применение обезболивания, разрешено. Период лактации относительно благоприятен для стоматологических процедур.\r\n		</div>\r\n		</li>\r\n		<li class=\"one_job\">\r\n		<h4>8. Опасно ли отбеливание зубов?</h4>\r\n		<div class=\"hid_text\" style=\"display: none;\">\r\n			Когда работу делает профессионал, опасаться не стоит. Врач должен правильно рассчитать длительность воздействия на эмаль, чтобы избежать ожога тканей. Интересно, что различные методы отбеливания имеют неоспоримые преимущества и особенности. В частности, в отбеливании AmazingWhite применяемый гель обогащен полезными веществами. А методика LumiBrite защищает эмаль и качественно изменяет ее оттенок. Популярным является фотоотбеливание с применением ультрафиолетовой лампы. Лазерное осветление параллельно удаляет налет и камень. Все подобные стоматологические манипуляции качественно и безопасно изменяют оттенок и состояние зубной эмали.\r\n		</div>\r\n		</li>\r\n	</ul>\r\n</div>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 32, '', '', 'Вопросы и ответы - Клиника «Авантис»', NULL, '', '', NULL, NULL),
(33, 1, 'ru', NULL, '2019-01-17 10:13:38', '2020-06-10 12:56:42', 1, 1, 'Лицензии', 'Лицензии', 'licenzii', '<p>==[[w:GalleryWidget|galleryId=5;view=gallerylicense]]==</p><h2 style=\"text-align: center;\">Ссылки на Законы</h2><h3>Закон РФ от 07.02.1992 № 2300-1 \"О защите прав потребителей\"</h3><p><a href=\"http://www.consultant.ru/document/cons_doc_LAW_305/\">http://www.consultant.ru/document/cons_doc_LAW_305/</a>\r\n</p><p><a href=\"http://base.garant.ru/10106035/\">http://base.garant.ru/10106035/</a>\r\n</p><p><br>\r\n</p><h3>Федеральный закон \"Об основах охраны здоровья граждан в Российской Федерации\" от 21.11.2011 № 323-ФЗ</h3><p><a href=\"http://www.consultant.ru/document/cons_doc_LAW_121895/\">http://www.consultant.ru/document/cons_doc_LAW_121895/</a>\r\n</p><p><a href=\"http://base.garant.ru/12191967/\">http://base.garant.ru/12191967/</a>\r\n</p><p><br>\r\n</p><h3>Постановление Правительства РФ от 04.10.2012 № 1006 \"Об утверждении Правил предоставления медицинскими организациями платных медицинских услуг\"</h3><p><a href=\"http://base.garant.ru/70237118/\">http://base.garant.ru/70237118/</a></p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 33, '', 'licenzii', 'Лицензии и сертификаты|Клиника «Авантис»', NULL, '', '', '', NULL),
(34, NULL, 'ru', 15, '2019-01-18 17:02:54', '2020-06-30 15:03:00', 1, 1, 'Профессиональная гигиена полости рта «Air Flow»', 'Профессиональная гигиена полости рта «Air Flow»', 'professionalnaya-gigiena-polosti-rta-air-flow', '<h2 style=\"text-align: justify;\" rel=\"text-align: justify;\">7 преимуществ метода</h2><p>Благодаря профилактической очистке можно предупредить развитие или избавиться от многих неприятных явлений:</p><ul><li>неприятный запах</li><li>воспаление десен</li><li>кариес</li></ul><p>К тому же чистка обладает легким отбеливающим эффектом - возвращает зубам природный цвет эмали.</p><p>После процедуры доктор проконсультирует вас по профилактическим мероприятиям и правильной чистке зубов, что позволит поддерживать здоровье полости рта.</p><div class=\"lazyYT\" data-youtube-id=\"oA06foaa9Xg\" data-ratio=\"16:9\">Air Flow\r\n</div>', '«Авантис»', 'Профессиональная гигиена полости рта Air Flow в Оренбурге. Безопасная чистка зубов аппаратом Эйр Флоу по лучшим ценам | Частная стоматология «Авантис»', 1, 0, 34, '', 'services', 'Профессиональная гигиена полости рта «Air Flow» по низким ценам | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/25feec67aa7b0277f479687a8d7d9647.jpg\" title=\"Профессиональная гигиена полости рта «Air Flow»\" alt=\"Профессиональная гигиена полости рта «Air Flow»\">\r\n</div><p>Эта процедура позволяет мягко очистить эмаль от налета, мягкого зубного камня и других отложений. Налет удаляется направленной под давлением струей воды, воздуха и специального порошка.</p><p>Наш кабинет оснащен новейшим аппаратом KaVo PROPHYflex и NSK prophymate. Мы применяем порошки премиум-качества KaVo prophypearls. Благодаря специальному составу они отлично очищают, но при этом бережно относятся к тканям зуба.</p><p>Преимущества процедуры:</p><ul><li>Не повреждается эмаль;</li><li>Не травмируются десны;</li><li>Порошки обладают противовоспалительным и противокариозным эффектом;</li><li>Процедура практически безболезненна.</li></ul><p>Показания:</p><ul><li>Зубной налет;</li><li>Предрасположенность к воспалительным процессам мягких тканей (гингивит, пародонтит);</li><li>Наличие брекет-системы;</li><li>Подготовка к отбеливанию, лечению зубов;</li><li>Профилактика стоматологических заболеваний,</li><li>Подходит курильщикам, любителям чая и кофе.</li></ul><p>Чтобы достичь наилучшего результата, процедура чистки Air Flow в нашей клинике занимает не менее часа. Прием включает в себя:</p><ul><li>Удаление зубного камня при помощи ультразвука (при необходимости);</li><li>Обработка по технологии AIR FLOW;</li><li>Полировка всех зубов – обязательный этап, который, к сожалению, соблюдают далеко не все стоматологи. Полировка пастой делает поверхность зуба зеркально-гладкой и предотвращает образование нового налета.</li></ul>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Полировка одного зуба аппаратом Air-Flow\r\n	</td>\r\n	<td>150\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(35, NULL, 'ru', 15, '2019-01-20 12:49:29', '2020-06-30 15:08:54', 1, 1, 'Ультразвуковая чистка камней', 'Ультразвуковая чистка камней', 'ultrazvukovaya-chistka-kamney', '<h2>ПРЕИМУЩЕСТВА УЛЬТРАЗВУКОВОЙ ЧИСТКИ:</h2><ul>\r\n	<li>Эффективно устраняет твердый зубной камень, позволяет достичь абсолютной гладкости эмали и возвращает эстетичный внешний вид элементам зубного ряда.</li>\r\n	<li>Такой способ очистки комфортен для человека и дает хороший результат. Дискомфорт может быть, если отложения проникли под десну. В этом случае доктор обязательно применит щадящую анестезию.</li>\r\n	<li>Подходит для аллергиков, т.к. не используются химические средства и порошки.</li>\r\n	<li>Нередко ультразвуковая чистка проводится совместно с чисткой Air Flow.</li>\r\n</ul><h2>ЭФФЕКТ ОТ ПРОЦЕДУРЫ:\r\n</h2><ul>\r\n	<li>полное очищение от имеющихся загрязнений;</li>\r\n	<li>предотвращение кариеса;</li>\r\n	<li>осветление зубов на 1-2 тона;</li>\r\n	<li>профилактика заболеваний полости рта.</li>\r\n</ul><p>По имеющимся противопоказаниям проконсультирует врач.\r\n</p><p>После процедуры доктор даст вам рекомендации по уходу за полостью рта, чтобы продлить эффект чистки на длительное время и, при желании, подберет средства индивидуального ухода.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Сделать ультразвуковую чистку зубов от камня по доступным ценам. Качественная чистка зубного камня ультразвуком в Оренбурге | Частная стоматология «Авантис»', 1, 0, 35, '', 'services', 'Ультразвуковая чистка камней по низким ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/01b311eead988685a791ec099d0222cb.jpg\" title=\"Ультразвуковая чистка камней\" alt=\"Ультразвуковая чистка камней\">\r\n</div><p>15-17 часов – столько времени необходимо, чтобы налет начал превращаться в зубной камень. А видимые твердые отложения проявляются примерно\r\n через 3-6 месяцев. Твердый камень не очистить даже самой современной щеткой. Раньше процедура удаления зубного камня была только механической – болезненной и достаточно травматичной. Современная альтернатива – ультразвуковая чистка. Это удаление минерализованных образований с помощью новейшего ультразвукового скалера.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Снятие зубных отложений в области одного зуба ультразвуком\r\n	</td>\r\n	<td>100\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL);
INSERT INTO `avantis_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `short_content`, `price_box`, `canonical`, `gallery_name`) VALUES
(36, NULL, 'ru', 16, '2019-01-20 13:37:15', '2020-06-30 17:08:40', 1, 1, 'Ортопедическая стоматология', 'Ортопедическая стоматология', 'ortopedicheskaya-stomatologiya', '<h2 style=\"text-align: justify;\">Основные методы</h2><p style=\"text-align: justify;\">Сегодня используется несколько методов ортопедической диагностики:\r\n</p><ul>\r\n	<li>опрос и осмотр пациента – ортопед собирает и анализирует жалобы</li>\r\n	<li>исследование чувствительности зубов на перепады температур - термометрия</li>\r\n	<li>рентген (получение снимка)</li>\r\n	<li>компьютерная томография</li>\r\n	<li>электрометрия</li>\r\n	<li>ортопантомограмма</li>\r\n	<li>кариниометрия</li>\r\n	<li>сбор общих анализов и др.</li>\r\n</ul><p style=\"text-align: justify;\">Все исследования проводятся в условиях клиники. На основании полученных данных врач ставит точный диагноз и определяет наиболее эффективные способы лечения. Все методы диагностики современной стоматологии абсолютно безболезненны и обычно проводятся за несколько минут.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Сделать ортопедическую диагностику по доступным ценам. Услуги диагностики врача-ортопеда в Оренбурге | Стоматологическая поликлиника «Авантис»', 0, 0, 36, '', '', 'Ортопедическая диагностика зубов в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/f7de9a0d162feb3294f766e8c4dfac2e.jpg\" title=\"Ортопедическая стоматология\" alt=\"Ортопедическая стоматология\">\r\n</div><p style=\"text-align: justify;\">Современная стоматологическая ортопедия обладает большим количеством эффективных диагностических методов, позволяющих надежно установить вид заболевания, его причины, а главное – методы лечения.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Клинические фотографии, консультация, заполнение эстетического анализа\r\n	</td>\r\n	<td>1000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Расчет ТРГ\r\n	</td>\r\n	<td>1000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Диагностическая модель (отдублированная)\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Слепок диагностический альгинатный\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n\r\n</tbody>\r\n</table>', '', NULL),
(37, NULL, 'ru', 16, '2019-01-20 13:38:06', '2020-07-10 13:05:19', 1, 1, 'Кондилография', 'Кондилография', 'kondilografiya', '<h2>Зачем нужна кондилография?\r\n</h2><p>Это исследование позволяет оценить движения ВНЧС во всех плоскостях: данные о работе челюстного сустава, связок и жевательных мышц. На основе полученных данных врач планирует ортодонтическое или ортопедическое лечение, балансируя зубочелюстную систему.\r\n</p><h2>Показания к диагностике</h2><p>Диагностика обязательно проводится в таких случаях:\r\n</p><ul>\r\n	<li>ортодонтическое лечение\r\n	</li>\r\n	<li>тотальное протезирование\r\n	</li>\r\n	<li>нарушение функции сустава\r\n	</li>\r\n	<li>болевые ощущения, дискомфорт\r\n	</li>\r\n	<li>хрусты и щелчки в челюсти\r\n	</li>\r\n	<li>для контроля процесса лечения патологий сустава</li>\r\n</ul><h2>Как проходит кондилография</h2><p>Для исследования ВНЧС в стоматологической клинике обычно применяется 2 современных метода исследования – это аксиография и кондилография. Работа проводится с помощью кондилографа, который аккуратно крепится к челюсти, щекам и височным поверхностям.\r\n</p><p>В процессе диагностики пациент совершает несколько движений челюстью по команде врача. Прибор фиксирует все данные в электронном виде (угол движения, наклон, коэффициент смещения и др.). В результате врач получает исключительно точную картину, благодаря которой можно назначить индивидуальный курс лечения.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Пройти аксиографию в Оренбурге. Качественная кондилография по лучшим ценам | Частная стоматологическая клиника «Авантис»', 1, 0, 37, '', 'services', 'Сделать кондилографию по низким ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/249bab9239117f36b20e898d78cbfc51.jpg\">\r\n</div><p>В ортодонтическом или ортопедическом лечении десятки нюансов. Это не просто выровнять прикус или установить имплантаты. В этом деле десятки нюансов. Ведь, к примеру, меняя прикус, мы перестраиваем всю работу зубочелюстной системы, затрагиваем суставы и мышцы. И все это нужно балансировать в процессе лечения.Если не учесть какой-то компонент, есть риск получить неполноценное лечение, или результат не будет стабильным. Именно поэтому перед началом лечения мы обследуем каждого пациента с помощью кондилографа и делаем электромиографию.\r\n</p><p>Принцип исследования – регистрация движений сустава с применением электронных датчиков. Информация выводится на монитор компьютер и затем анализируется врачом-стоматологом.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Кондилография\r\n	</td>\r\n	<td>от 12 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(38, NULL, 'ru', NULL, '2019-01-20 15:44:22', '2020-02-26 11:29:33', 1, 1, 'Отзывы', 'Отзывы', 'otzyvy', '<p>сысы\r\n</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 38, '', '', 'Отзывы о стоматологах Оренбурга|Клиника «Авантис»', NULL, '', '', '', NULL),
(39, NULL, 'ru', 4, '2019-06-04 17:51:04', '2020-05-26 12:18:44', 1, 1, 'медсестра, ассистент врача', 'Патрушева Татьяна Сергеевна', 'patrusheva', '<p>медсестра, ассистент врача</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 41, '', '', 'Патрушева Татьяна Сергеевна - медсестра, ассистент врача|Клиника «Авантис»', 'bd159570948d62e8d300c06acec08867.jpg', '', '', NULL, NULL),
(40, NULL, 'ru', 4, '2019-06-04 17:52:07', '2020-05-26 12:18:44', 1, 1, 'медсестра, ассистент врача', 'Щербакова Елена Сергеевна', 'shcherbakova-elena-sergeevna', '<p>медсестра, ассистент врача</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 42, '', '', 'Щербакова Елена Сергеевна - медсестра, ассистент врача|Клиника «Авантис»', '5f644b33f9ed10c0626d097b499c0b4a.jpg', '', '', NULL, NULL),
(41, NULL, 'ru', 4, '2019-06-04 17:52:55', '2020-05-26 12:18:44', 1, 1, 'администратор-кассир', 'Борисова Юлия Владиковна', 'borisova-yuliya-vladikovna', '<p>администратор-кассир</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника Авантис', 1, 0, 39, '', '', 'Борисова Юлия Владиковна - администратор-кассир|Клиника «Авантис»', 'd20313ddc097ca6874b01f5d6264b1de.jpg', '', '', NULL, NULL),
(42, NULL, 'ru', 4, '2019-06-04 17:53:42', '2020-05-26 12:18:44', 1, 1, 'администратор-кассир', 'Токарева Виктория Сергеевна', 'tokareva-viktoriya-sergeevna', '<p>администратор-кассир</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 31, '', '', 'Токарева Виктория Сергеевна - администратор-кассир|Клиника «Авантис»', '7a3a2e44fea1983c879d22b5c8132f54.jpg', '', '', NULL, NULL),
(43, NULL, 'ru', 12, '2019-06-05 15:25:45', '2020-07-17 11:52:19', 1, 1, 'Лучшие методы эстетической стоматологии', 'Виниры или брекеты', 'viniry-ili-brekety', '<p>Восстановление достигается с помощью металлических дуг. Сапфировые конструкции менее активно устраняют изменения.\r\n</p><p>Виниры при этом имеют скорее маскировочное, чем корректирующее действие. Они могут применяться только для устранения аномалий в передней части ряда – в области первых премоляров, клыков и резцов. Керамика позволяет удалить кривизну и убрать промежутки между ними.\r\n</p><h3>Красота</h3><p>Классические брекеты видны при разговоре. Конечно, они небольших размеров, но при этом все равно их линии видны на эмали. Более красивыми являются производные сапфира. Наименее заметны лингвальные устройства, располагающиеся на задней поверхности. Но при этом они неудобны в носке и имеют высокую цену.\r\n</p><p>В отличие от брекетов, винирные элементы сами формируют эстетику улыбки. Они маскируют все дефекты, корректируют трещины и сколы, заполняют щелочки. При этом можно устранить последствия кариеса и других заболеваний.\r\n</p><h3>Комфорт в использовании</h3><p>Скобки отличаются особым неудобством при ношении. К их недостаткам относится то, что:\r\n</p><ol>\r\n	<li>К ним нужно долго привыкать;</li>\r\n	<li>Они давят на зубы;</li>\r\n	<li>Изменяют произношение слов;</li>\r\n	<li>Из-за них пациенту приходится менять рацион, отказываясь от твердых и вязких продуктов;</li>\r\n	<li>После каждого приема пищи необходима чистка.</li>\r\n</ol><p>Виниры крепятся непосредственно к зубкоронкам. Они не нарушают артикуляцию и не вызывают неприятных ощущений. При установке виниров не требуется и специального питания. Поэтому в плане удобства и эстетичности они значительно превосходят брекетные системы.\r\n</p><p>Таким образом, применение винирных компонентов рекомендовано при маленьких нарушениях, которые не требуют резкой коррекции, а брекеты стоит устанавливать при сильных деформациях.\r\n</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 43, '', 'blog', 'Лучшие методы эстетической стоматологии | Статьи клиники «Авантис»', '98e64fef30a00be7cc29edb2bd97d3e3.jpg', '<p>Одна из самых популярных услуг зубной коррекции – это брекеты, который \r\nбыстро восстанавливают нормальную форму коронок. Однако сейчас...</p>', '<p>Одна из самых популярных услуг зубной коррекции – это брекеты, \r\nкоторый быстро восстанавливают нормальную форму коронок. Однако сейчас в\r\n практику внедряются виниры – тонкие пластинки, которые улучшают внешний\r\n вид через маскировку щербинок и других проблемных участков. Разберемся,\r\n какой из способов более эффективный и эстетичный.</p><h3>Результаты</h3><p>Скобки обладают повышенной эффективностью – они убирают даже тяжелые проблемы. Лечение используется при следующих состояниях:</p><ol><li>Открытый прикус;</li><li>Отклонение угла нижней челюсти назад или вперед;</li><li>Скрученность.</li></ol>', '', NULL),
(44, NULL, 'ru', 12, '2019-06-05 15:41:35', '2019-07-02 09:46:38', 1, 1, 'Почему соки не всегда благоприятно влияют на зубы?', 'Соки', 'soki', '<p><strong style=\"text-decoration:underline\">Вред заключается в том, что в него входят:</strong>\r\n</p><ul>\r\n	<li>·      Кислоты;</li>\r\n	<li>·      Глюкоза;</li>\r\n	<li>·      Фруктоза;</li>\r\n	<li>·      Другие углеводы.</li>\r\n</ul><p>Данные элементы в напитках содержатся в большем количестве, чем в самих фруктах. Это вызывает разрушение поверхностного слоя оболочки зуба.\r\n</p><p>Со временем у человека возникает кариес и выраженные деструктивные изменения, которые впоследствии приводит к тяжелым осложнениям. Поэтому Ассоциация британских стоматологов выпустила специальное обращение к пациентам, в котором рекомендуется ограничить употребление свежевыжатых соков и не злоупотреблять ими на ежедневной основе.\r\n</p><h3>Так ли полезны смузи?</h3><p>Другим популярным трендом в здоровом питании могут считаться густые смеси, приготовленные из свежих бананов, яблок и апельсинов, часто с добавлением огурцов и зелени. В интернете представлены сотни рецептов, в каждом из которых подчеркивается полезность для метаболизма. К преимуществам смузи относится:\r\n</p><ol>\r\n	<li>Относительно низкое содержание сахара;</li>\r\n	<li>Высокий уровень клетчатки;</li>\r\n	<li>Множество необходимых для органов компонентов, участвующих в клеточном обмене веществ.</li>\r\n</ol><p>Однако исследования показали, что такие жидкости неблагоприятно сказываются на внешнем виде и составе эмали, покрывающей зубной ряд. Это связано с тем, что они являются более вязкой. Вследствие этого смузи имеют повышенную кислотность. Они быстро обволакивают поверхность зубов и создают на них сладкую пленку.\r\n</p><p>Таким образом, натуральные нектары – это питательные напитки, которые все же нужно употреблять в меру. Их можно считать десертами – а жажду можно утолять и водой. Каждый день стоит вводить в рацион не более 150 мл. При этом лучше использовать соломинку. Это позволит сохранить здоровье рта и снизить риск развития стоматологических заболеваний.\r\n</p>', '', 'Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов - Клиника «Авантис»', 1, 0, 44, '', 'blog', 'Почему соки не всегда благоприятно влияют на зубы | Статьи клиники «Авантис»', '71df38405956f2f930d931724c98dfe9.jpg', '<p>В обществе бытует мнение, что фреши – это источник витаминов и \r\nминералов. И это действительно так. Однако последние \r\nэксперименты...</p>', '<p>В обществе бытует мнение, что фреши – это источник витаминов и \r\nминералов. И это действительно так. Однако последние длительные \r\nэксперименты, проведенные в Англии, подтвердили их возможность \r\nпровоцировать патологические процессы.</p><h3>Результаты опытов</h3><p>Многие\r\n люди полагают, что фруктовые соки оказывают только положительное \r\nвлияние, поэтому стремятся пить их как можно больше. При этом они не \r\nучитывают то, что жидкие компоненты оказываются вредными для состояния \r\nважных структур.<br></p>', NULL, NULL),
(45, NULL, 'ru', 12, '2019-06-05 15:42:58', '2019-07-02 09:45:31', 1, 1, 'Витамины для зубов', 'Витамины для зубов', 'vitaminy-dlya-zubov', '<h3>Зачем пить витамины?</h3><div>В энергозатратных ситуациях, во время болезней или беременности организму нужна поддержка.\r\n</div><p><strong>Ведь, если некоторых элементов будет не хватать:</strong>\r\n</p><ol>\r\n	<li>Повышается чувствительность зубов, что говорит об истончении эмали.</li>\r\n	<li>Развиваются патологии, воспаляется слизистая оболочка.</li>\r\n	<li>Зубы становятся подвижными, если мало витамина С.</li>\r\n	<li>Эмаль покрывается черными пятнами, возникает кариес.</li>\r\n</ol><p>Авитаминоз вызывает кровоточивость десен. Он приводит к гингивиту, при котором они воспаляются. Это состояние сопровождается отеками, появлением кровянистых выделений.\r\n</p><p>Всех этих проблем можно избежать, если следить за рационом.\r\n</p><h3>Какие компоненты особенно важны</h3><p>Не обойтись без следующих:\r\n</p><ol>\r\n	<li>А. Они способствуют укреплению тканей.</li>\r\n	<li>Д. Стимулирует восстановительные процессы.</li>\r\n	<li>С. Восстанавливает поврежденные участки.</li>\r\n	<li>К. Участвует в кроветворении.</li>\r\n	<li>Кальций. Поддерживает эмаль.</li>\r\n	<li>Фосфор. С его помощью укрепляются кости.</li>\r\n	<li>Фтор. Обеспечивает достаточную белизну.</li>\r\n</ol><p>Витаминные средства нельзя принимать без ведома врача. Сначала кровь проверяют на уровень микроэлементов, ведь избыток их так же вреден, как и недостаток.  Специалист оценит качество ротовой полости, риск развития осложнений и подберет подходящий медикамент.\r\n</p><h3>Откуда можно получить?</h3><p>Наибольшее количество незаменимых элементов для здоровья содержится в молочных продуктах. Поэтому лучше употреблять молоко, творог, сметану. Эти вещества есть также в бобах и орехах.\r\n</p><p>Фосфор можно также получить из мяса, яиц и рыбы. Жирны морепродукты – это источник витамина Д, а С присутствует в цитрусовых, болгарском перце, моркови, редьке, картофеле, капусте, смородине, К получают из авокадо, бананов, яиц.<br>Множество полезных компонентов можно получить из фруктов, овощей и зелени. Нельзя обходить стороной печень трески, говяжью и гусиную.<br>Если сделать выбор в пользу укрепляющих лекарств, то стоит использовать  Асепту. Она подходит для детей и взрослых.<br>Здоровое питание – это лучшая профилактика заболеваний и поддержка. Если этого недостаточно, врач назначит препараты.\r\n</p>', '', 'Чтобы укрепить зубы и десна необходимо сбалансированное питание и употребление соответствующих витаминов. Уделять этому вопросу больше внимания рекомендуют в начале весны и в конце осени| Статьи клиники «Авантис»', 1, 0, 45, '', 'blog', 'Витамины для зубов | Статьи клиники «Авантис»', '63c5ec4f128a2fe13c7f2b95ba1b54b9.jpg', '<p>Чтобы укрепить зубы и десна необходимо сбалансированное питание и употребление соответствующих витаминов. Полезные вещества...</p>', '<p>Чтобы укрепить зубы и десна необходимо сбалансированное питание и \r\nупотребление соответствующих витаминов. Полезные вещества можно получить\r\n из еды или пропить специальные препараты. Уделять этому вопросу больше \r\nвнимания рекомендуют в начале весны и в конце осени.</p>', NULL, NULL),
(46, NULL, 'ru', 16, '2019-06-07 13:53:46', '2020-07-03 12:46:17', 1, 1, 'Электромиография', 'Электромиография', 'elektromiografiya', '<h2>Показания для исследования:\r\n</h2><ul>\r\n	<li>Аномалии прикуса</li>\r\n	<li>Полное или частичное отсутствие зубов</li>\r\n	<li>Заболевания челюстных суставов</li>\r\n	<li>Бруксизм</li>\r\n	<li>Миозиты жевательной и мимической мускулатуры</li>\r\n	<li>Системный пародонтит</li>\r\n	<li>Артриты и артрозы ВНЧС</li>\r\n	<li>Для контроля в процессе ортодонтического лечения</li>\r\n</ul><h2>Принцип исследования:\r\n</h2><p>Прибор фиксирует мышечную патологию при неправильном прикусе, бруксизме, заболеваниях ВНЧС, гипертонусе жевательных мышц, воспалительных процессах в самих мышцах (миозитах) и ряде других заболеваний в челюстно-лицевой области.А также врач-гнатолог может исключить или подтвердить диагноз мигрень.\r\n</p><h2>Как проходит исследование:\r\n</h2><p>Электромиография проводится при помощи накладных электродов и абсолютно безболезненно. Прибор регистрирует биопотенциал мышц и выводит данные на экран компьютера. Врач оценивает электромиограммы при жевании, в состоянии покоя, при различных смещениях нижней челюсти вперед и в стороны, анализирует данные и составляет индивидуальный план лечения.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Сделать ЭМГ в Оренбурге по доступным ценам. Пройти электромиографию жевательных мышц | Платная стоматология «Авантис»', 1, 0, 46, '', 'services', 'Электромиография - цены и отзывы, пройти ЭМГ в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/253b3f5b99852e0c3e2fcc222e0b32c6.jpg\" width=\"30%\" \"=\"\">\r\n</div><p>Качественное ортодонтическое лечение сегодня немыслимо без электромиографии. Это важный диагностический метод в работе гнатолога, ортодонта, ортопеда. Он помогает изучать функционально-динамические расстройства жевательного аппарата, что незаменимо при подборе терапии.\r\n<br>Электромиография – это регистрация электрической активности жевательных мышц.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Электромиография\r\n	</td>\r\n	<td>от 4 500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(47, NULL, 'ru', 17, '2019-06-07 13:55:46', '2019-08-09 15:52:12', 1, 1, 'Эндоотбеливание', 'Эндоотбеливание', 'endootbelivanie', '<p style=\"text-align: justify;\">Если классические методы предполагают воздействие снаружи, то здесь полость заполняется специальным составом. Процедура довольно простая:\r\n</p><ul>\r\n	<li>Специалист должен выяснить причину изменения цвета. Для этого используют рентген\r\n	</li>\r\n	<li>Второй момент – вскрывается и осматривается полость\r\n	</li>\r\n	<li>При наличии вторичного кариеса проводится лечение\r\n	</li>\r\n	<li>Корневой канал изолируют\r\n	</li>\r\n	<li>Полость заполняется  гелем\r\n	</li>\r\n	<li>Устанавливается временная пломба\r\n	</li>\r\n	<li>На следующем приеме оценивается результат. При необходимости вводится еще порция геля. Повторять операцию можно до пяти раз</li>\r\n</ul><p style=\"text-align: justify;\">Сегодня эта процедура является новинкой, но уже с успехом применяется. В стоматологии специалисты готовы провести процедуру эндоотбеливания зубов и вернуть улыбке белизну. Ее эффективность выше, чем у чистки и полировки, а результат сохраняется на долгие годы.\r\n</p>', 'отбеливание зубов цена оренбург, отбеливание зубов оренбург', 'Сделать эндоотбеливание зубов по доступным ценам. Безопасное эндо отбеливание зубов в Оренбурге | Частная стоматология «Авантис»', 1, 0, 47, '', 'services', 'Эндоотбеливание зубов в Оренбурге по низким ценам | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/4214e6cbf64a16790d293961d4dc1440.jpg\" title=\"Эндоотбеливание\" alt=\"Эндоотбеливание\">\r\n</div><p style=\"text-align: justify;\">Глубокий кариес или пульпит заканчивается тем, что корень у зуба удаляется. Депульпированная коронка темнеет, независимо от пищевых предпочтений и ухода. Сегодня все чаще врачи рекомендуют закрывать его металлокерамикой, не дожидаясь разрушения истонченных стенок. В этом случае зуб стачивается, он него остается лишь основа. Сегодня есть альтернативное решение – внутриканальное отбеливание.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Эндоотбеливание  1 зуб\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(48, NULL, 'ru', 17, '2019-06-07 13:56:19', '2019-08-09 15:51:10', 1, 1, 'Клиническое отбеливание лазер Dr.Smail', 'Клиническое отбеливание лазер Dr. Smail', 'klinicheskoe-otbelivanie-lazer-drsmail', '<p style=\"text-align: justify;\">Стоматологи со всей уверенностью заявляют, что этот метод безопасен для эмали, а результат сохраняется на несколько лет. Это оправдывает и более высокую стоимость, по сравнению с другими методами. </p><p style=\"text-align: justify;\"> Этапы:\r\n</p><ul>\r\n	<li>Врач осматривает пациента, выясняет возможные противопоказания и выносит заключение о возможности отбеливания</li>\r\n	<li>Гигиеническая обработка ротовой полости и поверхности зубов</li>\r\n	<li>Десны покрываются защитным веществом</li>\r\n	<li>На эмаль наносят отбеливающий состав</li>\r\n	<li>Лазерный луч выступает как катализатор. Благодаря ему гель вступает в реакцию с эмалью, расщепляя нежелательные белковые соединения.</li>\r\n</ul><p style=\"text-align: justify;\">Современные препараты разработаны с учетом того, чтобы не наносить вреда эмали. Многочисленные исследования подтверждают, что и гель и лазерные лучи не приведут к ее повреждению. Это способ стать немного красивей. Посещение стоматолога – гигиениста в Оренбурге поможет вам сделать шаг навстречу своей мечте.\r\n</p>', 'отбеливание зубов цена оренбург, отбеливание зубов оренбург', 'Клиническое отбеливание лазером Dr. Smail в Оренбурге. Качественное отбеливание зубов лазером доктор Смайл по лучшим ценам | Частная стоматология «Авантис»', 1, 0, 48, '', 'services', 'Клиническое лазерное отбеливание Dr. Smail в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/a2ab0d9e7c1a0b75762d456ea21ace90.jpg\" title=\"Клиническое отбеливание лазер Dr. Smail\" alt=\"Клиническое отбеливание лазер Dr. Smail\">\r\n</div><p style=\"text-align: justify;\">Из всех существующих методов, популярным является лазерное отбеливание зубов. Раньше эта процедура требовала постоянной сосредоточенности от врача и пациента. Теперь лазер делает ее быстрее, полностью лишая травматизма. Экономя время пациенты выигрывают в качестве.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Клиническое отбеливание лазер Dr. Smail\r\n	</td>\r\n	<td>15 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(49, NULL, 'ru', 17, '2019-06-07 13:57:10', '2019-08-09 15:50:08', 1, 1, 'Клиническое отбеливание ZOOM 4', 'Клиническое отбеливание ZOOM 4', 'klinicheskoe-otbelivanie-zoom-4', '<h2>Как проходит отбеливание</h2><p style=\"text-align: justify;\">Эта процедура осуществляется в течение 45 минут. Система ZOOM – это метод безболезненной очистки специальным прибором. Перед этим врач проводит профессиональную чистку ротовой полости, удаляя камень и налет. Все манипуляции проводятся совершенно безболезненно и не нарушают структуру зуба. Благодаря этому пациент получает не только белоснежную улыбку, но и очищение зубов от отложений.\r\n</p><p style=\"text-align: justify;\">Преимуществ у этой процедуры довольно много:\r\n</p><ul>\r\n	<li>Эмаль становится светлее на 7-9 тонов</li>\r\n	<li>Чувствительность зубов снижается до 66%</li>\r\n	<li>Результат долговечен (до 2 лет)</li>\r\n	<li>Процедура быстрая и безболезненная</li>\r\n</ul>', 'отбеливание зубов оренбург, отбеливание зубов цена оренбург', 'Сделать безопасное отбеливание ZOOM 4 в Оренбурге. Качественное отбеливание зубов ЗУМ 4 по доступным ценам | Частная стоматология «Авантис»', 1, 0, 49, '', 'services', 'Клиническое отбеливание ZOOM 4 по низким ценам | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/1bca061d20440117c614ab05e329904b.jpg\" title=\"Клиническое отбеливание ZOOM 4\" alt=\"Клиническое отбеливание ZOOM 4\">\r\n</div><p style=\"text-align: justify;\">Профессиональное отбеливание зубов позволяет сделать их не только красивыми, но и здоровыми. В нашей клинике применяется современная технология ZOOM 4, которая обеспечивает бережное отбеливание эмали с сохранением устойчивого результата на 1-2 года.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Клиническое отбеливание ZOOM 4\r\n	</td>\r\n	<td>17 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(50, NULL, 'ru', 17, '2019-06-07 13:57:59', '2019-08-09 15:49:16', 1, 1, 'Каппа для отбеливания', 'Каппы для отбеливания', 'kappa-dlya-otbelivaniya', '<p style=\"text-align: justify;\">Капа – это пластиковый резервуар, который носят на нижней и верхней челюстях. В него добавляют специальный состав, который помогает постепенно осветлить зубы.\r\n</p><ul>\r\n	<li>Это  щадящий способ\r\n	</li>\r\n	<li>Не нужно ходить на приемы много месяцев\r\n	</li>\r\n	<li>Капы очень тонкие, ношение не ощущается\r\n	</li>\r\n	<li>Подгоняются они индивидуально</li>\r\n	<li>Сохраняют герметичность в течение всего периода использования</li>\r\n</ul><p style=\"text-align: justify;\">Насколько сильным будет отбеливание, сложно сказать заочно. Это зависит от множества факторов. Гель-отбеливатель содержит перекись водорода. Процедура будет эффективной при возрастном потемнении, применении лекарств, которые вызывают аналогичный эффект или злоупотреблении чаем и кофе. Но могут быть и другие причины, которые влияют на цвет, от особенностей пищеварения до сбоев. Поэтому и необходима консультация специалиста. Посещение гигиениста в Оренбурге позволит подобрать способ отбеливания эмали для каждого пациента.\r\n</p>', 'отбеливание зубов цена оренбург, отбеливание зубов оренбург', 'Заказать каппы для отбеливания зубов по доступным ценам. Качественные и безопасные отбеливающие каппы для зубов в Оренбурге | Частная стоматология «Авантис»', 1, 0, 50, '', 'services', 'Каппы для отбеливания зубов - цены в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/2ea2aba770798bad76a595a8aeef99ec.jpg\" title=\"Каппы для отбеливания\" alt=\"Каппы для отбеливания\">\r\n</div><p style=\"text-align: justify;\">Красивая улыбка не теряет своей притягательности. Несмотря на обилие средств, люди продолжают искать новые варианты подчеркнуть ее очарование. Отбеливающие капы – это хороший  потому, что не предполагают многократного посещения стоматолога.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Каппа для отбеливания, фторирования\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(51, NULL, 'ru', 17, '2019-06-07 13:58:44', '2019-08-09 15:48:07', 1, 1, 'Безопасное лазерное отбеливание зубов', 'Безопасное лазерное отбеливание зубов', 'bezopasnoe-lazernoe-otbelivanie-zubov', '<p style=\"text-align: justify;\">Отбеливание при помощи лазера считается самым эффективным. Кардинальное изменение тона происходит в несколько этапов:\r\n</p><ul>\r\n	<li>Подготовка. Чаще всего это гигиеническая чистка</li>\r\n	<li>Защита десен. Для этого стоматолог наносит на них специальный состав</li>\r\n	<li>Нанесение геля</li>\r\n	<li>Активация состава</li>\r\n	<li>Укрепление эмали</li>\r\n</ul><p style=\"text-align: justify;\">Процедура занимает около часа. При этом пациент может чувствовать легкое покалывание, но не более. Чувствительность зубов не нарушается, не наблюдается болезненности десен. Если соблюдать рекомендации врача, использовать качественные средства для ухода за полостью рта, то хороший результат сохраняется на 5 – 6 лет. Безопасное лазерное отбеливание зубов в Москве проводится по предварительной записи, после осмотра терапевтом. Если требуется лечение кариеса, его выполняют до процедуры.\r\n</p>', 'отбеливание зубов оренбург, отбеливание зубов цена оренбург', 'Безопасное лазерное отбеливание зубов по доступным ценам. Качественное отбеливание зубов лазером в Оренбурге | Частная стоматология «Авантис»', 1, 0, 51, '', 'services', 'Безопасное лазерное отбеливание зубов в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/be9bd2bf8e59d0344c73b511df9c1cde.jpg\" title=\"Безопасное лазерное отбеливание зубов\" alt=\"Безопасное лазерное отбеливание зубов\">\r\n</div><p style=\"text-align: justify;\">О красивой улыбке мечтает большинство людей. Поэтому так востребованно отбеливание зубов. Но при этом важно еще и не навредить здоровью. Пищевые пристрастия и вредные привычки приводят к тому, что постепенно эмаль приобретает желтый оттенок. Избавится от него в домашних условиях непросто. Лишь стоматолог подберет и выполнит процедуру отбеливания без вреда для эмали.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Безопасное лазерное отбеливание\r\n	</td>\r\n	<td>8 999\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(52, NULL, 'ru', 66, '2019-06-07 13:59:56', '2019-09-09 17:07:36', 1, 1, 'Регистрация прикуса', 'Регистрация прикуса', 'registraciya-prikusa', '<h2>Суть метода</h2><p style=\"text-align: justify;\">Методика лечения заключается в следующем:\r\n</p><ol>\r\n	<li>Врач создает двусторонний оттиск зубов пациента, располагающихся напротив друг друга на верхней и нижней челюсти (антагонисты).</li>\r\n	<li>Переносит полученную информацию на модель.</li>\r\n	<li>Затем стоматолог формирует ортопедическую конструкцию для исправления прикуса, которая идеально соответствует анатомическим особенностям конкретного человека.</li>\r\n	<li>При создании используется материал на основе смеси высокопрочных силиконов, что обеспечивает возможность использования брекетов в течение длительного срока.</li>\r\n	<li>Смесь очень быстро застывает и вынимается изо рта. Таким образом, все манипуляции проводятся в условиях клиники за несколько минут.</li>\r\n</ol>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Качественная регистрация прикуса в Оренбурге, регистрация прикуса - фото и цены | Частная стоматологическая поликлиника «Авантис»', 1, 0, 52, '', 'services', 'Регистрация прикуса по доступным ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/9d5da96164af3db38a44c0067ea8c9bc.jpg\" title=\"Регистрация прикуса\" alt=\"Регистрация прикуса\">\r\n</div><p style=\"text-align: justify;\">В современной стоматологии используются разные способы лечения этого дефекта. Одним из наиболее эффективных и надежных является регистрация прикуса.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Регистрация прикуса\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(53, NULL, 'ru', 66, '2019-06-07 14:01:19', '2019-09-09 17:09:38', 1, 1, 'Элайнеры', 'Элайнеры', 'elaynery', '<p style=\"text-align: justify;\">Это, по сути чехлы, которые надеваются на весь ряд. Внутренняя полость повторяет все анатомические особенности челюсти. День за днем ортодонтическая каппа оказывает незначительное давление на зубы. В течение курса необходимо периодически посещать врача, который меняет элайнеры. Это позволит добиться желаемых улучшений в короткий срок.\r\n</p><p>Достоинствами метода является:\r\n</p><ul>\r\n	<li>Предсказуемый результат</li>\r\n	<li>Короткие сроки. Не нужно годами носить брекеты</li>\r\n	<li>Комфортное использование, не травмирует десны и эмаль зубов</li>\r\n	<li>Отсутствует неприятный привкус</li>\r\n</ul><p style=\"text-align: justify;\">Есть и минусы. С ними сложней следить за гигиеной полости рта. Простая чистка становится длительной процедурой.  Не подходит детям, используют для исправления прикуса только коренных зубов. Их стоимость выше, чем у привычных металлических брекетов. Со всеми плюсами и минусами врач дополнительно познакомит в кабинете.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Элайнеры для выравнивания зубов по лучшим ценам. Качественные и безопасные элайнеры в Оренбурге | Стоматологическая поликлиника «Авантис»', 1, 0, 53, '', 'services', 'Элайнеры для зубов по низким ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/06c9366865bc50d0f9323796085a4d57.jpg\" title=\"Элайнеры\" alt=\"Элайнеры\">\r\n</div><p style=\"text-align: justify;\" rel=\"text-align: justify;\">Брекеты – конструкция знакомая, которая  справляется с поставленными задачами, но внешний вид отталкивает. Из-за этого люди отказываются от них, предпочитая мириться с несовершенствами зубного ряда. Сегодня есть альтернативный вариант – <i>элайнеры</i>. Почти невидимые, они справляются с задачей на пять\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td> Исправление прикуса элайнерами  Invisalign ORTHOSNAP, STAR SMAIL\r\n	</td>\r\n	<td>От 120 000  до 280 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(54, NULL, 'ru', 66, '2019-06-07 14:02:38', '2019-09-09 17:10:15', 1, 1, 'Определение RP', 'Определение RP', 'opredelenie-rp', '<h2>Материалы для слепка</h2><p style=\"text-align: justify;\">В клинике используются только современные слепочные материалы, которые обеспечивают максимальную надежность и долговечность брекета:\r\n</p><ol>\r\n	<li>Luxabite (люксабайт) – это наиболее твердый и прочный материал для получения слепков; изготавливается на основе акрилатов. Не имеет вкуса, запаха и отлично режется, при этом не деформируется.</li>\r\n	<li>O-Bite – прочный силикон, предназначенный для длительного использования. Дает аромат спелого апельсина.</li>\r\n	<li>Futar – эластомерный материал, который наносится с помощью удобного пистолета-дозатора.</li>\r\n	<li>А-силикон для слепков немецкой компании DMG. Отличается надежностью и невысокой стоимостью.</li>\r\n</ol>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Определение RP, конструктор прикуса по доступным ценам. Конструктор прикуса (силикон, люксабайт) в Оренбурге | Платная стоматология «Авантис»', 1, 0, 54, '', 'services', 'Определение RP, конструктор прикуса по доступным ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/4b84c4b1ed38d5c7dc5038f5933a03b0.jpg\" title=\"Определение RP\" alt=\"Определение RP\">\r\n</div><p style=\"text-align: justify;\">Современная стоматология использует довольно много надежных методов лечения прикуса, и один из них – создание оттиска, который идеально соответствует анатомии зубов пациента. Эта процедура называется регистрацией прикуса: для получения оттискного слепка используются современные материалы. Они наносятся на зубной ряд, быстро застывают благодаря высокой вязкости и становятся брекетом.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Определение RP конструктор прикуса (силикон, люксабайт)\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(55, NULL, 'ru', 66, '2019-06-07 14:03:13', '2020-04-06 11:05:31', 1, 1, 'Брекеты', 'Брекеты', 'breket-sistema', '<h2>Как система выравнивает зубы?\r\n</h2><p>Брекеты оказывают постоянное давление в течение продолжительных периодов времени. Ортодонтические дуги, являющиеся частью конструкции брекетов, имеют память формы, и при фиксации дуга сопротивляется, что и способствует смещению под действием тепла ротовой полости. Форма вашей челюсти постепенно адаптируется к этому давлению.\r\n</p><p>Многие склонны думать о том, что зубы связаны непосредственно с челюстной костью, что затрудняет представление о том, как их можно двигать. Но под деснами есть мембрана, окруженная вашими костями, которая оказывает прижимающее воздействие к челюсти. Эта мембрана контролирует положение и реагирует на давление, оказываемое брекетами.\r\n</p><p>Установка брекетов в Оренбурге, занимает от одного до двух часов и выполняется опытными специалистами-ортодонтами.\r\n</p><p><strong>Виды конструкций:</strong>\r\n</p><ul>\r\n	<li>Вестибулярные</li>\r\n	<li>Лингвальные</li>\r\n	<li>Сапфировые</li>\r\n	<li>Керамические</li>\r\n	<li>Пластиковые</li>\r\n	<li>Из драгметаллов</li>\r\n</ul><p>Брекеты чаще используются в подростковом возрасте, но все больше взрослых получают корректирующие брекеты на более поздних этапах жизни.\r\nОни изготовлены из металла, керамики, проволоки и связующего материала.\r\nПоказатели брекетов, которые доступны для установки в городе Оренбург, варьируются в зависимости от возраста, когда начинаются процедуры, и каковы ваши цели лечения.\r\nЭффективность брекетов зависит от человека и его способности тщательно следовать инструкциям стоматолога. Также стоит отметить и относительно низкую цену установки в Оренбурге.\r\n</p><h2>Типы</h2><p>Тип, который порекомендует ортодонт, будет зависеть от нескольких факторов, таких как ваш возраст и наличие у вас прикуса в дополнение к кривизне. Брекеты изготавливаются на заказ и индивидуально под потребности каждого клиента.\r\n</p><p>Классические брекеты, которые приходят на ум большинству людей, сделаны из металла, которые приклеиваются индивидуально к каждому из зубов. Арочный провод оказывает давление, а эластичные уплотнительные кольца соединяют арку с кронштейнами.\r\n</p><p>Арочный провод периодически регулируется, так как он оказывает медленное перемещающее воздействие, а резинки отключаются при назначении специалиста.\r\n</p><p>Клиника предлагает следующие брекет-системы:\r\n</p><ul>\r\n	<li>MBT;\r\n	</li>\r\n	<li>ROTH;</li>\r\n	<li>DAMON;\r\n	</li>\r\n	<li>ALEXANDER.</li>\r\n</ul><h2>Как быстро вы получите результат?</h2><p>Продолжительность лечения варьируется отдельно для каждого человека, но обычно люди носят брекет-системы от одного до трех лет. Тщательно следуя инструкциям своего стоматолога, клиент должен быть уверен, что носит данные устройства в течение максимально короткого промежутка времени.\r\n</p><p style=\"text-align: justify;\">Подобрать оптимальный вариант лечения может только врач в ходе осмотра. Учитываются пожелания пациента - стоимость, эстетические характеристики и другие факторы.\r\n</p><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"ycUjIwW_AIk\" data-ratio=\"16:9\">Брекеты\r\n</div>', 'брекеты оренбург, брекеты цена оренбург, установка брекетов оренбург', 'Установка брекетов в Оренбурге по низким ценам. Брекеты из безопасных материалов: записаться на прием, установить брекет-систему и заменить дугу | Частная стоматологическая поликлиника «Авантис»', 1, 0, 55, '', 'services', 'Брекеты - цена, установка брекетов в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/0012a28ecd837c3c1de18c83baf5bc62.jpg\" title=\"Брекет системы\" alt=\"Брекет системы\">\r\n</div><p style=\"text-align: justify;\">Брекетами называют ортодонтические конструкции, призванные корректировать неправильное положение зубов, связанное с неровностью зубного ряда или нарушением прикуса. Выравнивается в нужном направлении зубной ряд медленно, но неуклонно. Крепятся несъемные скобы к наружной или внутренней поверхности зубов при помощи специального клея.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (первая категория сложности на две челюсти)\r\n	</td>\r\n	<td>25 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (вторая категория сложности две челюсти)\r\n	</td>\r\n	<td>39 800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (третья категория сложности на две челюсти)\r\n	</td>\r\n	<td>55 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (четвертая категория сложности две челюсти)\r\n	</td>\r\n	<td>65 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Повторная фиксация одного брекета\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие аппаратуры две челюсти (без ретейнера, с чисткой)\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Защита композитная выступающих элементов ортодонтической аппаратуры\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы первая категория\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы вторая категория\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры третья категория\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры четвертая  категория\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена металлической дуги\r\n	</td>\r\n	<td>900\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена белой дуги\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MEAW дуга\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Эластичная тяга 1 пакетик\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(56, NULL, 'ru', 19, '2019-06-07 14:04:00', '2019-08-09 15:44:20', 1, 1, 'Искусственные коронки', 'Искусственные коронки', 'iskusstvennye-koronki', '<h2>Виды коронок:\r\n</h2><ul>\r\n	<li>временные;\r\n	</li>\r\n	<li>постоянные.</li>\r\n</ul><p>Изготавливаются коронки из разных материалов – от керамики до металла. Подбор проводится индивидуально для каждого пациента.\r\n</p><p><strong>На этапе снятия слепка</strong> стоматолог выполняет все работы по порядку, чтобы будущие протезы не натирали и были по уникальной форме зубов. Для этого используется современное оборудование. Для получения штампов используется качественный гипс.\r\n</p><p><strong>На завершающем этапе</strong> обязательно проводят примерку и шлифовку мест, где пациенту мешает протез. Только после этого проводится окончательная припасовка и фиксация изделия.\r\n</p><p><strong>Результат</strong> - протезирование, которое прослужит долгие годы.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Качественные искусственные коронки по лучшим ценам. Поставить искусственные коронки для зубов в Оренбурге | Платная стоматология «Авантис»', 1, 0, 56, '', 'services', 'Искусственные коронки для зубов по низким ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/de08f8069b0eae9cd4753af51f695ee7.jpg\" title=\"Искусственные коронки\" alt=\"Искусственные коронки\">\r\n</div><p>Искусственные коронки используются для укрепления зуба, который разрушается. Такой вид протезирования возвращает зубам приятный внешний вид, обеспечивает жевательную функцию. В работе используются изделия от надежных поставщиков и современные технологии. Устанавливаются одиночные или спаянные друг с другом, последние восполняют отсутствующие зубы.\r\n</p><p>Такая технология протезирования применяется, чтобы восстановить анатомическую форму зуба, при аномалиях цвета, а также в качестве основы для установки других протезов.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Трепанация зуба , искусственные коронки\r\n	</td>\r\n	<td>150\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(57, NULL, 'ru', 19, '2019-06-07 14:04:31', '2019-08-09 15:43:29', 1, 1, 'Снятие и фиксация коронок', 'Снятие и фиксация коронок', 'snyatie-i-fiksaciya-koronok', '<h2>Показания</h2><p style=\"text-align: justify;\">Замена коронок (метод перепротезирования) осуществляется в 2 этапа:\r\n</p><ol>\r\n	<li>Демонтаж старой конструкции.</li>\r\n	<li>Монтаж на постоянный цемент новой коронки из более прочного материала для обеспечения максимального комфорта.</li>\r\n</ol><p style=\"text-align: justify;\">Протезирование новых коронок применяется редко, поскольку этот способ лечения считается крайней мерой. Он используется, если:\r\n</p><ul>\r\n	<li>Пациент постоянно жалуется на боли, появляющиеся во время приема пищи или при разговоре</li>\r\n	<li>Коронка расшатывается и разрушает соседние зубы – в этом случае рекомендуется ставить протез</li>\r\n	<li>В установленной коронке обнаружен брак, поэтому она требует срочной замены</li>\r\n	<li>Под ней сформировалась киста, для удаления которой придется снимать и саму коронку</li>\r\n	<li>Для лечения зубной боли также понадобится удалить материал</li>\r\n</ul>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Удаление и установка зубных коронок в Оренбурге. Снять зубную коронку с последующей фиксацией по лучшим ценам | Частная стоматология «Авантис»', 1, 0, 57, '', 'services', 'Снятие и фиксация коронок по доступным ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/76be38ae2995ce9556beb65e398feb29.jpg\" title=\"Снятие и фиксация коронок\" alt=\"Снятие и фиксация коронок\">\r\n</div><p style=\"text-align: justify;\">В некоторых случаях пациенты, испытывающие зубную боль и другие посторонние ощущения, нуждаются в срочном удалении коронок с последующей фиксацией новой конструкции.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Снятие коронки штампованной (1ед)\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие коронки литой, металлокерамической (1ед.), разделение 2х литых коронок\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Фиксация 1ой коронки на стеклоиномерный цемент\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Фиксация одной коронки, вкладки и т.п. на композитный цемент\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL);
INSERT INTO `avantis_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `short_content`, `price_box`, `canonical`, `gallery_name`) VALUES
(58, NULL, 'ru', 19, '2019-06-07 14:05:02', '2019-08-09 15:42:52', 1, 1, 'Провизорные коронки', 'Провизорные коронки', 'provizornye-koronki', '<h2 style=\"text-align: justify;\">Преимущества метода</h2><p style=\"text-align: justify;\">Временные коронки в основном монтируются для того, чтобы пациент не ощущал психологического дискомфорта, связанного с отсутствием зубов накануне монтажа протеза. Они изготавливаются из прочных материалов, поэтому обеспечивают все базовые функции – человек может принимать пищу, жевать, говорить, улыбаться.\r\n</p><p style=\"text-align: justify;\">Основные преимущества метода в том, что провизорные коронки отличаются:\r\n</p><ul>\r\n	<li>Большой прочностью (и это несмотря на то, что они временные)</li>\r\n	<li>Надежной установкой (просадка минимальная)</li>\r\n	<li>Стабильностью цвета (сохраняются один и тот же оттенок)</li>\r\n	<li>Комфортной установкой и использованием</li>\r\n	<li>Невысокой стоимостью</li>\r\n</ul>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Качественные провизорные коронки для зубов по лучшим ценам. Поставить провизорные коронки в Оренбурге | Стоматологическая поликлиника «Авантис»', 1, 0, 58, '', 'services', 'Провизорные коронки по доступным ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/7467dbe995b48cbef3b2e255590b9322.jpg\" title=\"Провизорные коронки\" alt=\"Провизорные коронки\">\r\n</div><p style=\"text-align: justify;\">Провизорными называют временные коронки, которые монтируются на небольшое время перед установкой протеза (съемного или несъемного). Этот метод стоматологического лечения стали применять особенно часто благодаря надежности и экономичности материала.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Провизорная коронка, изготовленная прямым способом\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории\r\n	</td>\r\n	<td>1 200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории армированные\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории на имплантате MIS, Semados, Nobel\r\n	</td>\r\n	<td>3 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Починка провизорной коронки, изготовленной в лаборатории\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(59, NULL, 'ru', 19, '2019-06-07 14:05:34', '2019-08-09 15:42:00', 1, 1, 'Коронки, вкладки', 'Коронки, вкладки', 'koronki-vkladki', '<h2 style=\"text-align: justify;\">Установка коронки</h2><p style=\"text-align: justify;\">Восстановление зуба с помощью коронки – один из самых распространенных современных методов стоматологического лечения. Вначале врач ставит обезболивающее, после чего прочищает каналы зуба, сверлит их, устанавливает коронку и регулирует ее высоту. Эта процедура проводится безболезненно, однако исключить неприятные ощущения на 100% нельзя.</p><h2 style=\"text-align: justify;\">Установка вкладки</h2><p style=\"text-align: justify;\">Протезирование вкладками проводится, если коронки повреждены на 25-50% и более. Установка пломбы (в том числе металлокерамической) в этом случае не будет эффективной, поэтому предпочтительнее выбрать именно вкладки на зубы. Они обойдутся дороже, однако такие материалы намного надежнее. Они идеально подходят к анатомической форме зуба и практически не дают усадки. Вкладка становится небольшим микропротезом, который надежно защищает его от кариеса и других заболеваний.</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Коронки и вкладки для зубов по доступным ценам. Качественные коронки и вкладки в Оренбурге | Платная стоматология «Авантис»', 1, 0, 59, '', 'services', 'Коронки и вкладки для зубов по доступным ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/6f8d938a429ca9f62d713e927e0c5d3f.jpg\" title=\"Коронки, вкладки\" alt=\"Коронки, вкладки\">\r\n</div><p style=\"text-align: justify;\">Основные виды протезирования зубов – это установка коронок и вкладок. Именно они отличаются большей надежностью по сравнению с пломбами. Поэтому они пользуются большей популярностью, несмотря на более высокую стоимость.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка на основе КХС 1 категория\r\n	</td>\r\n	<td>7 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Вкладка керамическая ON-iey, IN-ley, OVER-ley\r\n	</td>\r\n	<td>14 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка (винир) на основе оксида циркония, Е МАХ, рефрактор\r\n	</td>\r\n	<td>17 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Коронка на имплантате винтовая фиксация 1 категория\r\n	</td>\r\n	<td>22 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент индивидуальный титановый\r\n	</td>\r\n	<td>8 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент индивидуальный циркониевый\r\n	</td>\r\n	<td>10 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент стандартный, локатор на имплантат Miss, Semados\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(60, NULL, 'ru', 20, '2019-06-07 14:06:43', '2019-08-09 15:41:04', 1, 1, 'Съемное протезирование', 'Съемное протезирование', 'semnoe-protezirovanie', '<h2>Показания</h2><p>Установка съемной конструкции понадобится в таких случаях:\r\n</p><ul>\r\n	<li>Полное отсутствие зубов</li>\r\n	<li>Частичное присутствие зубов в небольшом количестве</li>\r\n	<li>Пародонтит (зубы расшатываются)</li>\r\n	<li>Отсутствие последних зубов в ряду</li>\r\n	<li>Нежелание человека устанавливать несъемный протез по любой причине</li>\r\n</ul><p style=\"text-align: justify;\">Именно съемный протез может стать единственным способом решения проблемы. Например, у некоторых людей зубы, которые могли бы стать опорой для несъемной конструкции, имеют такие анатомические особенности, которые не позволяют провести эту операцию.\r\n</p><h2>Виды протезирования</h2><p>Сегодня существует несколько видов протезирования:\r\n</p><ul>\r\n	<li>Полное съемное (протез можно постоянно снимать и надевать)</li>\r\n	<li>Частично-съемное (для восстановления небольшого количества зубов)</li>\r\n	<li>Условно-съемное (протез снимать не нужно)</li>\r\n</ul>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Съемные зубные протезы по лучшим ценам. Качественное съемное протезирование в Оренбурге | Платная стоматология «Авантис»', 1, 0, 60, '', 'services', 'Съемное протезирование по доступным ценам в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/e3968888cd9053bc1770e46d45d5d1fb.jpg\" title=\"Съемное протезирование\" alt=\"Съемное протезирование\">\r\n</div><p style=\"text-align: justify;\">Съемное протезирование зубов – это наиболее экономичный вариант решения сразу нескольких проблем с зубным рядом. Изготовление съемного протеза осуществляется в соответствии с анатомическими особенностями челюсти конкретного человека, поэтому привыкнуть к такой конструкции можно очень быстро.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Частичный съемный протез\r\n	</td>\r\n	<td>7 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Полный съемный протез с акриловыми зубами однослойными\r\n	</td>\r\n	<td>10 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Полный съемный протез с акриловыми зубами Ivoclar-Vivadent Германия\r\n	</td>\r\n	<td>15 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Металлический базис для полного съемного протеза (дополнение к 94)\r\n	</td>\r\n	<td>13 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(61, NULL, 'ru', 20, '2019-06-07 14:07:18', '2019-08-09 15:39:24', 1, 1, 'Починка протеза съёмного', 'Починка протеза съёмного', 'pochinka-proteza-syomnogo', '<p>Ремонт проводится при разных видах поломки (10% случаев регистрируется в 1 год использования протеза):\r\n</p><ul>\r\n	<li>Сколы</li>\r\n	<li>Разломы</li>\r\n	<li>Дефекты креплений</li>\r\n	<li>Трещины</li>\r\n	<li>Зазоры между протезом и поверхностью десен</li>\r\n</ul><h2>Как проходит лечение</h2><p>Обычно процесс проходит в несколько этапов:\r\n</p><ol>\r\n	<li>Пациент вводит сломанный протез в ротовую полость</li>\r\n	<li>Затем ему снимают оттиск поверхности зубов</li>\r\n	<li>Модель отливают и заполняют пластмассой либо воском</li>\r\n	<li>Новый протез шлифуют и полируют</li>\r\n</ol><p style=\"text-align: justify;\">В зависимости от сложности процесс может занять как несколько часов, так и 2-3 дня. Сам пациент должен поприсутствовать только в начале. Благодаря получению оттиска протез удается скорректировать так, что он будет максимально соответствовать поверхности зубов.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Ремонт съемных зубных протезов по доступным ценам. Коррекция и починка съемных протезов для зубов в Оренбурге | Частная стоматология «Авантис»', 1, 0, 61, '', 'services', 'Починить съемный зубной протез в Оренбурге | Стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/bfad41f870b59181161897e75c800a3c.jpg\" title=\"Починка протеза съёмного\" alt=\"Починка протеза съёмного\">\r\n</div><p style=\"text-align: justify;\">Несмотря на то, что съемные протезы изготавливаются из особо прочных материалов, иногда они ломаются. В таких случаях пациенты нуждаются в срочном восстановлении протеза. Ремонт производится в условиях клиники в день обращения, благодаря чему человек получит возможность снова пользоваться протезом, а не устанавливать новый (что обойдется значительно дороже).\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Починка протеза съемного\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Добавление одного зуба в съемный протез\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Перебазировка съемного протеза\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена пластиковой матрицы в замковом соединении (за 1 матрицу)\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(62, NULL, 'ru', 20, '2019-06-07 14:07:58', '2019-08-09 15:38:42', 1, 1, 'Несъемное протезирование', 'Несъемное протезирование', 'nesemnoe-protezirovanie', '<h2 style=\"text-align: justify;\">Как проходит процедура</h2><p>Изготовление и установка осуществляется в несколько этапов:\r\n</p><ol>\r\n	<li>Обследование пациента</li>\r\n	<li>Монтаж титанового винта в кость (он полностью срастается с челюстью за 3-6 месяцев). Спустя это время он зарастает мягкими тканями и становится полностью невидимым</li>\r\n	<li>Установка формирователя десны</li>\r\n	<li>Установка мостовидного протеза – сначала он монтируется на временные коронки, затем – на постоянные</li>\r\n</ol><p style=\"text-align: justify;\">Протезирование зубов – дорогостоящая услуга. Однако она позволяет окончательно решить проблему и поставить долговечный качественный имплант, который будет заменять природные зубы в течение всей жизни.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Качественное несъемное протезирование зубов по доступным ценам. Поставить несъемный протез для зубов в Оренбурге | Стоматологическая клиника «Авантис»', 1, 0, 62, '', 'services', 'Несъемное протезирование зубов в Оренбурге | Частная стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/e7c2ebf89dac174c94baf5ef6af3f117.jpg\" title=\"Несъемное протезирование\" alt=\"Несъемное протезирование\">\r\n</div><p style=\"text-align: justify;\">Протезирование – одна из самых распространенных услуг современной стоматологии. Установка протеза позволяет провести полное замещение естественных зубов имплантами. Очевидное преимущество несъемной конструкции – ее надежность и долговечность, отсутствие неудобств, связанных с гигиеной и приемом пищи. Внешне протезы тоже ничем не отличаются от природных зубов. Более того – они не меняют цвет, поэтому всегда выглядят очень привлекательно.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n\r\n<tr>\r\n	<th colspan=\"2\"> Восковое моделирование\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>WaxUp восковое моделирование стандартное 1 зуб\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MockUp перенос  в полость рта макета 1 зуба\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>3D SetUp коррекция положения зубов 1 чел\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\"> Провизорные коронки\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Провизорная коронка, изготовленная прямым способом\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории\r\n	</td>\r\n	<td>1 200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории армированные\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Провизорные коронки, изготовленные в лаборатории на имплантате MIS, Semados, Nobel\r\n	</td>\r\n	<td>3 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Починка провизорной коронки, изготовленной в лаборатории\r\n	</td>\r\n	<td>1 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th colspan=\"2\"> Коронки, вкладки\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка на основе КХС 1 категория\r\n	</td>\r\n	<td>7 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Вкладка керамическая on-iey, in-ley, over-ley\r\n	</td>\r\n	<td>14 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка (винир) на основе оксида циркония, Е МАХ, рефрактор\r\n	</td>\r\n	<td>17 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Коронка на имплантате винтовая фиксация 1 категория\r\n	</td>\r\n	<td>22 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент индивидуальный титановый\r\n	</td>\r\n	<td>8 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент индивидуальный циркониевый\r\n	</td>\r\n	<td>10 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Абатмент стандартный, локатор на имплантат Miss, Semados\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(63, NULL, 'ru', 20, '2019-06-07 14:08:24', '2019-08-09 15:37:52', 1, 1, 'Бюгельное протезирование', 'Бюгельное протезирование', 'byugelnoe-protezirovanie', '<p style=\"text-align: justify;\">Опора может быть на здоровые моляры или импланты, что обеспечивает распределение нагрузки по всей челюсти и возвращает способность нормально питаться.\r\n</p><p style=\"text-align: justify;\">Бюгельные протезы различаются по материалу, из которого они изготовлены, а также способом фиксации. </p><p>Основные виды:\r\n</p><ul>\r\n	<li>На кламмерах или крючках, которыми конструкции удерживаются</li>\r\n	<li>На аттачментах. Это крохотные замки, одна часть которого встраивается в опорный зуб и покрывается коронкой, а второй находится в самом протезе</li>\r\n	<li>На телескопических коронках</li>\r\n</ul><p>Бюгельные конструкции имеют ряд преимуществ перед аналогами:\r\n</p><ul>\r\n	<li>Красота</li>\r\n	<li>Высокая прочность</li>\r\n	<li>Человек продолжает чувствовать вкус пищи</li>\r\n	<li>Не страдает дикция</li>\r\n	<li>Можно носить круглосуточно</li>\r\n</ul><p style=\"text-align: justify;\">Посетив стоматологическую клинику в Оренбурге можно получить полный комплекс услуг по протезированию зубов. Врач разрабатывает анатомически точную модель челюсти при помощи компьютерных технологий, что позволяет избежать лишних примерок.\r\n</p>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Бюгельное протезирование зубов по доступным ценам. Качественные бюгельные зубные протезы в Оренбурге | Стоматологическая клиника «Авантис»', 1, 0, 63, '', 'services', 'Бюгельное протезирование зубов в Оренбурге | Частная стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/009fc3b0d7517764dca428634784f37c.jpg\" title=\"Бюгельное протезирование\" alt=\"Бюгельное протезирование\">\r\n</div><p>Если большая часть зубного ряда утрачена, а установка моста или имплантов невозможна, то стоматологи рекомендуют установить бюгельный протез. Это съемная конструкция состоит из:\r\n</p><ul>\r\n	<li>Металлической или пластмассовой дуги</li>\r\n	<li>Жевательной поверхности</li>\r\n	<li>Креплений</li>\r\n</ul>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th> Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Бюгельное протезирование\r\n	</td>\r\n	<td>30 000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(64, NULL, 'ru', 20, '2019-06-07 14:08:55', '2019-09-13 16:58:59', 1, 1, 'Виниры', 'Виниры', 'viniry', '<h2>Особенности установки</h2>\r\n<p>Клиника предлагает услуги по установке следующих видов виниров:\r\n</p>\r\n<ol>\r\n	<li><strong>Композитные.</strong> Их накладывают прямо на зубную пластину способом нанесения специального пастообразного материала. Срок службы подобной конструкции – до 6 лет.\r\n	</li>\r\n	<li><strong>Керамические.</strong> Данные пластины с успехом заменят популярные ранее коронки, а основная их цель – завуалировать межзубные щели и кривизну. Керамика будет радовать владельца около 15 лет.</li>\r\n</ol>\r\n<p>Благодаря новейшим технологиям и высококачественным материалам посетители нашего центра гарантированно обретают:\r\n</p>\r\n<ul>\r\n	<li>Улучшение внешнего вида зубного ряда\r\n	</li>\r\n	<li>Долговременную белоснежную улыбку\r\n	</li>\r\n	<li>Безоперационное исправление формы зубов</li>\r\n</ul>\r\n<p>Наши стоматологи привнесут эстетику в жизнь каждого пациента, кто мечтал о безупречности своего имиджа. Улыбаться «по-голливудски» - теперь не преимущество избранных, а доступная реальность для большинства.\r\n</p><br>\r\n<iframe src=\"https://vk.com/video_ext.php?oid=-174309102&id=456239019&hash=20b259b683c6a53d\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen></iframe>', 'стоматология запись на приём оренбург, стоматологическая поликлиника оренбург, стоматология оренбург, частная стоматология оренбург, стоматология цены оренбург, стоматология телефон оренбург', 'Поставить виниры на зубы в Оренбурге, качественные и безопасные виниры по доступным ценам | Частная стоматологическая клиника «Авантис»', 1, 0, 64, '', 'services', 'Виниры на зубы по доступным ценам, установка виниров в Оренбурге | Стоматология «Авантис»', NULL, '<p><img src=\"https://avantis56.ru/uploads/image/d546da5f8c0d3796f20753731b6e286b.jpg\" title=\"Виниры\">\r\n</p><p>В эстетической стоматологии применяются особые тонкие накладки на переднюю поверхность зуба для маскировки несовершенств эмали.\r\n</p><p>Многолетний опыт специалистов нашей клиники позволяет осуществлять установку виниров на высоком уровне.  Работа с лучшими производителями микропротезов различной сложности делает возможным реставрацию поверхности зубов с любыми дефектами.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Изготовление композитного винира (на один зуб)\r\n	</td>\r\n	<td>2500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Керамическая коронка (винир) на основе оксида циркония, Е МАХ, рефрактор\r\n	</td>\r\n	<td>17000\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL),
(66, NULL, 'ru', 2, '2019-06-07 16:07:41', '2020-04-24 15:47:12', 1, 1, 'Ортодонт', 'Ортодонт в Оренбурге', 'ortodontiya', '<p>Врач-ортодонт - это специалист, который специализируется на выравнивании челюстей и зубов. Боль в челюсти, нарушения речи, апноэ во сне, заболевания десен, трудности с жеванием, кривые зубы и деформация зубного ряда создают дискомфорт и болезненные ощущения. Даже если нет боли или дискомфорта, возможно просто улучшить свою внешность с помощью более ровного набора. Пациентов обычно направляют к ортодонту от общего врача, который первым обнаруживает нарушение.\r\n</p><p>Специалисты имеют дело с аппаратными средствами, такими как фиксаторы, с целью выравнивания, чтобы улучшить внешний вид или способность человека жевать и глотать пищу без затруднений или боли.\r\n</p><p>В целом, ортодонтическое лечение длится от шести до тридцати месяцев. Время будет в значительной степени зависеть от классификации неправильного прикуса, типа зубных устройств, используемых для его исправления.\r\n</p><p class=\"tcs\"><strong>С какими проблемами нужно посетить стоматолога-ортодонта: </strong>\r\n</p><ul>\r\n	<div class=\"cont\">\r\n		<div class=\"part\">\r\n			<li>Неровный зубной ряд или аномалии отдельных зубов</li>\r\n			<li>Патологически неправильный или некрасивый прикус</li>\r\n			<li>Проблемы при прорезывании</li>\r\n			<li>Щель между резцами</li>\r\n			<li>Непропорциональные челюсти</li>\r\n		</div>\r\n		<div class=\"part\">\r\n			<li>Аномальное строение челюстной части лица</li>\r\n			<li>Выступ средней части лица</li>\r\n			<li>Смещения и выход зубов вне ряда</li>\r\n			<li>Адентия частичная или полная</li>\r\n			<li>Травмы</li>\r\n		</div>\r\n	</div>\r\n</ul><p>Ортодонт не только создает «голливудскую» улыбку, но и обеспечивает функциональность зубов. В работе врач-ортодонт совмещает знания стоматолога и зубного техника. Основываясь на диагностических инструментах, которые включают в себя полный медицинский и стоматологический анамнез, клинический осмотр, гипсовые модели зубов, а также специальные панорамные снимки и фотографии, ортодонт разработает план лечения.\r\n</p><p>Работа включает три основных этапа:\r\n</p><ul>\r\n	<li>Стадия планирования</li>\r\n	<p>Первые несколько посещений могут включать некоторые из следующих оценок:\r\n	</p>\r\n	<p>Медицинские и стоматологические осмотры. Необходимо убедиться, что предшествующие физиологические проблемы полностью контролируются до начала процедур.\r\n	</p>\r\n	<p>Модель исследования (отливки / впечатления от укуса). Человека просят прикусить специализированный поддон, заполненный гелевым веществом.\r\n	</p>\r\n	<p>Панорамные рентгеновские снимки - рентгеновские снимки - это инструменты для просмотра потенциальных осложнений или уже существующего повреждения челюстного сустава. Рентген также позволяет увидеть точное положение полости.\r\n	</p>\r\n	<li>Активная фаза</li>\r\n	<p>Все вышеперечисленные диагностические инструменты будут использоваться для диагностики и разработки индивидуального плана терапии. Далее, следует изготовление на заказ ортодонтические устройства, его установка и последующие корректировка фиксатора.\r\n	</p>\r\n	<li>Завершение</li>\r\n	<p>Когда всё будет правильно выровнено, фиксированные брекеты и съемные устройства будут удалены и сняты с производства. Самая громоздкая часть ортодонтических процедур уже закончена. Затем профессионал создаст специальный фиксатор. Целью фиксатора является обеспечение того, чтобы зубы не начинали возвращаться в исходное положение.\r\n	</p>\r\n</ul><p style=\"text-align: justify;\">Длительность лечения и качество результата зависят от выполнения рекомендаций пациентом, а также от профессионализма врача. В стоматологии «Авантис» работают специалисты с многолетним опытом, вы можете ознакомиться с <a href=\"https://avantis56.ru/review/\">отзывами</a> пациентов и <a href=\"https://avantis56.ru/our-work\">примерами лечения</a>.<br>\r\n</p><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"ezdCYsAi2zE\" data-ratio=\"16:9\">Ортодонт\r\n</div>', 'ортодонт оренбург, стоматолог оренбург, стоматология запись на приём оренбург, стоматологическая клиника оренбург, стоматологическая поликлиника оренбург, лечение зубов оренбург, лечить зубы оренбург, платная стоматология оренбург', 'Услуги стоматолога-ортодонта по лучшим ценам. Качественная ортодонтия в Оренбурге: запись на прием, адрес, телефон и цены | Стоматологическая поликлиника «Авантис»', 1, 0, 65, '', 'services', 'Записаться на прием к ортодонту в Оренбурге | Частная стоматология «Авантис»', 'f2f9d24dda81f67c0e1210b8d1f3228b.png', '<p><img src=\"http://avantis56.ru/uploads/image/9f6dae20b0eeb539d364e2ac5df903c3.jpg\">\r\n</p><p style=\"text-align: justify;\">Ортодонт занимается созданием красивой  и симметричной улыбки. Врач-ортодонт исправляет патологии челюсти, врожденные и приобретенные. Помогает восстановить правильный прикус. В специализацию входит исправление асимметрии лица, подготовка пациента к установке имплантов или протезов. Проблемы с дыханием или речью, вызванные челюстными дефектами также решает этот специалист.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Аппарат Френкеля, Твин-Блок\r\n	</td>\r\n	<td>15000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Трейнер ортодонтический\r\n	</td>\r\n	<td>8500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Аппарат быстрое небное расширение\r\n	</td>\r\n	<td>12000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Контрольный прием, припасовка аппарата\r\n	</td>\r\n	<td>500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Аппарат ортодонтический малый ORTHO-TAIN\r\n	</td>\r\n	<td>7500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(67, NULL, 'ru', NULL, '2019-06-21 11:45:07', '2019-06-21 11:46:35', 1, 1, 'Карта сайта', 'Карта сайта', 'sitemap', '<p>11111</p>', '', '', 1, 0, 66, '', 'sitemap', '', NULL, '', '', NULL, NULL),
(68, NULL, 'ru', 21, '2019-08-26 15:04:26', '2020-04-06 10:41:37', 1, 1, 'Лечение кариеса у детей', 'Лечение кариеса у детей', 'lechenie-kariesa-u-detey', '<p>Поэтому лечение кариеса у ребенка – необходимая процедура. В Оренбурге открыта детская стоматология, где принимают врачи, умеющие найти подход к любому, даже самому маленькому пациенту.\r\n</p><h2>Особенности развития кариеса у детей</h2><p>Кариес молочных зубов отличается агрессивностью. Поражение происходит намного быстрее. И связано это с особенностями зубной эмали. У молочных зубов она пористая, благодаря чему микроорганизмы легче в нее проникают. А из-за того, что слой очень тонкий, они быстро добираются до рыхлого дентина. Тогда зуб без должного лечения и правильной гигиены разрушается в течение нескольких месяцев. Вот почему необходимо посещать детского стоматолога каждые 3-4 месяца.\r\n</p><h2>Особенности лечения кариеса у детей</h2><p>В детской терапии стоматолог придерживается нескольких основных правил при лечении кариеса:\r\n</p><ul>\r\n	<li>двухэтапное обезболивание, при котором сначала используют аппликационную анестезию, чтобы ребенок не почувствовал укола, а потом – инъекционную;</li>\r\n	<li>концентрация анестетика в детской дозе, она подбирается в соответствии с весом малыша;</li>\r\n	<li>препараты с адреналином не используются до 4-х летнего возраста пациента;</li>\r\n	<li>кариозную полость у маленьких детей зачищают вручную с использованием кюреток и стоматологических экскаваторов;</li>\r\n	<li>для закрытия полости применяют специальные пломбировочные материалы, содержащие фториды и минералы;</li>\r\n	<li>прием стоматолога должен длиться не более получаса. </li>\r\n</ul><p>С таким подходом ребенок не будет бояться посещения стоматолога. И последующие профилактические осмотры врача он перенесет спокойно.\r\n</p><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"8H7sXPaJxY8\" data-ratio=\"16:9\">Лечение кариеса молочных зубов\r\n</div>', 'лечение кариеса у ребёнка оренбург, лечение кариеса оренбург, частная детская стоматология оренбург, платная детская стоматология оренбург', 'Лечение кариеса у детей в частной детской стоматологии. Лечение кариеса у ребенка по низким ценам в Оренбурге | Платная детская стоматология «Авантис»', 1, 0, 67, '', 'services', 'Лечение кариеса у ребёнка в Оренбурге | Детская стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/91eecd93d33daa4c71fe8c65be761120.jpg\">\r\n</div><p>\r\n	Кариес\r\n– заболевание ротовой полости, при котором происходит размягчение зубной эмали\r\nи образование полости в коронке. Некоторые родители полагают, что лечение\r\nмолочных зубов, пораженных этим патологическим процессом, необязательно. Однако\r\nэто мнение ошибочно. Нелеченый кариес опасен своими осложнениями – пульпитом и\r\nпериодонтитом, которые сопровождаются сильными болями. Кроме того, может\r\nпроизойти повреждение и даже гибель зачатка постоянного зуба, что в свою\r\nочередь повлечет нарушение прикуса и прочие ортодонтические проблемы. И все\r\nиз-за небольшой дырочки в зубе, которую проигнорировали родители.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (первая категория сложности на две челюсти)\r\n	</td>\r\n	<td>25 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (вторая категория сложности две челюсти)\r\n	</td>\r\n	<td>39 800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (третья категория сложности на две челюсти)\r\n	</td>\r\n	<td>55 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (четвертая категория сложности две челюсти)\r\n	</td>\r\n	<td>65 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Повторная фиксация одного брекета\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие аппаратуры две челюсти (без ретейнера, с чисткой)\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Защита композитная выступающих элементов ортодонтической аппаратуры\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы первая категория\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы вторая категория\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры третья категория\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры четвертая  категория\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена металлической дуги\r\n	</td>\r\n	<td>900\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена белой дуги\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MEAW дуга\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Эластичная тяга 1 пакетик\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(69, NULL, 'ru', 21, '2019-08-26 15:07:18', '2020-04-06 11:03:31', 1, 1, 'Детский ортодонт', 'Детский ортодонт', 'detskiy-ortodont', '<h2>Способы ортодонтической коррекции</h2><p>Метод исправления прикуса у ребенка и выравнивания зубного ряда подбирается исключительно в индивидуальном порядке. При этом детский ортодонт учитывает степень сложности аномалии и возраст пациента. Основные способы коррекции:\r\n</p><ul>\r\n	<li>Аппаратные методики. Это применение ортодонтических конструкций – капп, трейнеров, пластинок, брекет-систем.</li>\r\n</ul><ul>\r\n	<li>Миотерапия. В большинстве случаев это вспомогательный метод коррекции прикуса, который рекомендуют наряду с ношением кап и брекетов. Это комплекс упражнений для тренировки мышечного аппарата, окружающего зубной ряд. В качестве самостоятельной процедуры используется только на начальной стадии заболевания. </li>\r\n	<li>Хирургическое лечение. К нему прибегают, когда ортодонтические конструкции не в состоянии исправить аномалию. Кроме того, оно показано при выявлении врожденных патологий и грубых зубочелюстных аномалий, не диагностированных в раннем возрасте.</li>\r\n</ul><p>После коррекции ребенок проходит завершающую стадию лечения, называющуюся ретенционным периодом. В это время он должен носить специальные капы – ретейнеры для закрепления результатов лечения. Дело в том, что даже после нескольких месяцев коррекции зубы все равно будут стремиться занять свое прежнее неправильное положение. Чтобы этого не случилось, врач подбирает ретейнеры, которые используются пациентом еще в течение нескольких месяцев.\r\n</p><p>Чтобы своевременно выявить проблемы с прикусом у ребенка, родители должны регулярно посещать ортодонта. В Оренбурге такой специалист принимает в нашем отделении детской стоматологии. Поэтому записаться на прием к нему не составит труда. Главное, позаботиться о красивой и здоровой улыбке малыша.\r\n</p><p><br>\r\n</p><div class=\"lazyYT\" data-youtube-id=\"OUpoOxk9jsc\" data-ratio=\"16:9\">Детский ортодонт\r\n</div>', 'детский ортодонт оренбург, исправление прикуса у ребёнка оренбург, частная детская стоматология оренбург, платная детская стоматология оренбург', 'Исправление прикуса у ребёнка по низким ценам. Детский ортодонт в Оренбурге: фото, отзывы и цены | Платная детская стоматология «Авантис»', 1, 0, 68, '', 'services', 'Детский ортодонт - цена, исправление прикуса у ребенка в Оренбурге | Детская стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/a9dd027a277e125787691c8a07401370.jpg\"><br>\r\n</div><p>Детский ортодонт – специалист, занимающийся исправлением аномального прикуса. Именно его усилия способны подарить ребенку красивую улыбку. Главное, чтобы родители своевременно привели малыша на прием к врачу. Благодаря регулярным профилактическим осмотрам ортодонт сможет выявить патологию на ранних стадиях. Лечить проблемы с прикусом во взрослом возрасте намного дольше и дороже, чем в детском.\r\n</p>', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цены в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (первая категория сложности на две челюсти)\r\n	</td>\r\n	<td>25 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет-системы металлической (вторая категория сложности две челюсти)\r\n	</td>\r\n	<td>39 800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (третья категория сложности на две челюсти)\r\n	</td>\r\n	<td>55 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Установка брекет системы (четвертая категория сложности две челюсти)\r\n	</td>\r\n	<td>65 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Повторная фиксация одного брекета\r\n	</td>\r\n	<td>300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Снятие аппаратуры две челюсти (без ретейнера, с чисткой)\r\n	</td>\r\n	<td>5 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Защита композитная выступающих элементов ортодонтической аппаратуры\r\n	</td>\r\n	<td>200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы первая категория\r\n	</td>\r\n	<td>800\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации брекет системы вторая категория\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры третья категория\r\n	</td>\r\n	<td>2 000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Плановый лечебный сеанс для активации аппаратуры четвертая  категория\r\n	</td>\r\n	<td>2 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена металлической дуги\r\n	</td>\r\n	<td>900\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Замена белой дуги\r\n	</td>\r\n	<td>1 300\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>MEAW дуга\r\n	</td>\r\n	<td>1 500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Эластичная тяга 1 пакетик\r\n	</td>\r\n	<td>250\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(70, 1, 'ru', NULL, '2019-09-13 15:25:09', '2020-06-10 12:50:56', 1, 1, '', 'Прием у руководителя', 'priem-u-rukovoditelya', '<p><br>\r\n</p><div class=\"row\">\r\n	<div class=\"col-md-8\">\r\n		<p>Уважаемые пациенты стоматологии «Авантис»!\r\n		</p>\r\n		<p><strong>Дни и часы приема граждан в ООО «Авантис»</strong><br></p><p>Прием граждан проводится : понедельник, среда, пятница с 18:00 – 19:00</p><p>по адресу г. Оренбург, ул. Высотная, 2</p><p>Прием граждан ведет директор ООО «Авантис» - Попов Антон Сергеевич</p><p>            E mail: <a href=\"mailto:avantisdent@gmail.com\">avantisdent@gmail.com</a><br></p>\r\n	</div>\r\n	<div class=\"col-md-4\"><img src=\"uploads/image/77079770ac40302b8154c69dbe87a537.jpg\">\r\n		<p style=\"text-align: center;\">Главный врач клиники ООО «Авантис» <br>Попов Антон Сергеевич\r\n		</p>\r\n	</div>\r\n</div><p><br><br><br>\r\n</p>', '', '', 1, 0, 69, '', '', '', NULL, '', '', '', NULL),
(71, 1, 'ru', NULL, '2019-09-13 15:43:17', '2020-02-26 10:45:54', 1, 1, 'Запись на прием', 'Запись на прием', 'pravila-zapisi-na-priem', '<p>Клиника «Авантис» предлагает полный комплекс стоматологического лечения и процедур для взрослых и детей:\r\n</p>\r\n<ul>\r\n	<li><a href=\"https://avantis56.ru/ultrazvukovaya-chistka-kamney\">Ультразвуковая чистка камней</a></li>\r\n	<li><a href=\"https://avantis56.ru/otbelivanie-zubov\">Отбеливание </a></li>\r\n	<li><a href=\"https://avantis56.ru/implantaciya-zubov\">Имплантация </a>и <a href=\"https://avantis56.ru/protezirovanie-zubov\">протезирование </a></li>\r\n	<li><a href=\"https://avantis56.ru/ortodontiya\">Ортодонтия</a> и <a href=\"https://avantis56.ru/ispravlenie-prikusa\">исправление прикуса</a></li>\r\n	<li><a href=\"https://avantis56.ru/lechenie-kariesa\">Лечение кариеса</a></li>\r\n	<li><a href=\"https://avantis56.ru/lechenie-zubov\">Пародонтология</a></li>\r\n	<li><a href=\"https://avantis56.ru/esteticheskaya-restavraciya-zubov\">Восстановление и эстетическая реставрация </a> и др.</li>\r\n</ul>\r\n<p>Предварительная запись на прием ведется по телефону : +7 (3532) 66-22-66\r\n</p>\r\n<p>В день первого приема необходимо прийти за  10-15 минут до назначенного времени и обратиться в регистратуру  для оформления договора платных услуг и карты пациента, не забудьте ПАСПОРТ.\r\n</p>\r\n<p>Об отмене или переносе ПРОСЬБА СООБЩИТЬ  минимум за 2 часа до приема по телефону.\r\n</p>\r\n<h2>График работы  </h2>\r\n<p>Понедельник - Пятница: 08 - 20\r\n</p>\r\n<p>Суббота: 10 - 16\r\n</p>\r\n<p>Воскресенье: выходной\r\n</p>\r\n<p><br>\r\n</p>\r\n<div class=\"service-content\" style=\"margin-bottom: 30px;\">\r\n	<div class=\"btn-z\">\r\n		<a class=\"but-z\" data-target=\"#callbackServiceModal\" data-toggle=\"modal\" href=\"#\" tabindex=\"0\" style=\"margin: 0 auto;\">Записаться на приём</a>\r\n	</div>\r\n</div>', '', 'Записаться на прием к стоматологу по доступным ценам. Запись к врачу-стоматологу в Оренбурге | Стоматологическая клиника «Авантис»', 1, 0, 70, '', '', 'Запись на прием к стоматологу в Оренбурге | Частная стоматология «Авантис»', NULL, '', '', '', NULL),
(72, 1, 'ru', NULL, '2019-09-13 15:55:14', '2019-09-13 16:42:47', 1, 1, '', 'Обслуживание по ДМС', 'obsluzhivanie-po-dms', '<p>Стоматологическая клиника «Авантис» работает со страховыми организациями по системе ДМС.\r\n</p>\r\n<p>Перечень страховых организаций ДМС, сотрудничающих с ООО «Авантис» можно уточнить по телефону регистратуры: +7(3532)66-22-66\r\n</p>\r\n<p>Условия обслуживания по ДМС:\r\n</p>\r\n<ol>\r\n	<li>Наличие полиса ДМС, при предъявлении паспорта.</li>\r\n	<li>Наличие Программы страхования с указанием наименования медицинской организации с адресом: ООО «Авантис» - г. Оренбург, ул. Высотная, 2.</li>\r\n	<li>Наличие гарантийного письма на медицинское обслуживание застрахованного.</li>\r\n	<li>Прием врача проводится по предварительной записи через регистратуру по телефону: +7(3532)66-22-66.</li>\r\n	<li>Медицинские услуги проводятся с Информированного согласия пациента.</li>\r\n	<li>Медицинские услуги оказываются согласно индивидуальной программы страхования. При необходимости проведения услуг не входящих в данную программу пациент может их получить за наличный или безналичный расчет, заключив договор на оказание платных медицинских услуг.</li>\r\n</ol>\r\n<p><br>\r\n</p>\r\n<div class=\"btn-y\">\r\n	<a class=\"but-z\" data-target=\"#callbackServiceModal\" data-toggle=\"modal\" href=\"#\" tabindex=\"0\">Записаться на приём</a>\r\n</div>', '', '', 1, 0, 71, '', '', '', NULL, '', '', NULL, NULL);
INSERT INTO `avantis_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `short_content`, `price_box`, `canonical`, `gallery_name`) VALUES
(73, 1, 'ru', NULL, '2019-09-13 16:02:10', '2019-09-13 16:10:33', 1, 1, '', 'Правила оказания платных медицинских услуг', 'pravila-okazaniya-platnyh-medicinskih-uslug', '<h2 style=\"font-size: 22px;\">Положение о предоставлении платных медицинских услуг и гарантийных сроках в ООО «Авантис»</h2>\r\n<h3>Общие положения</h3>\r\n<p>1. Настоящее Положение разработано в соответствии с Законом РФ « О защите прав потребителей», Постановлением Правительства РФ от 04.10.2012г. № 1006 «Об утверждении Правил предоставления медицинскими организациями платных медицинских услуг», Федеральным законом от 21.11.2011г. № 323-ФЗ «Об основах охраны здоровья граждан в РФ», Гражданским кодексом РФ от 30.11.1994г. № 51-ФЗ\r\n</p>\r\n<p>2. Настоящее Положение определяет порядок и условия предоставления платных медицинских услуг населению в ООО «Авантис».\r\n</p>\r\n<h3>Условия предоставления платных медицинских услуг:</h3>\r\n<p>1. Платные медицинские услуги населению в ООО «Авантис» предоставляются на основании перечня работ (услуг), составляющих медицинскую деятельность и указанных в Лицензии на осуществление медицинской деятельности.\r\n</p>\r\n<p>2. Платные медицинские услуги населению осуществляются специалистами ООО «Авантис» в рамках Договора оказания стоматологических услуг по соглашению сторон с гражданами или организациями, работникам, застрахованным или членам их семей.\r\n</p>\r\n<p>3. При заключении Договора потребителю (заказчику) предоставляется информация о возможности получения соответствующих видов и объемов медицинской помощи без взимания платы в рамках «Территориальной программы государственных гарантий бесплатного оказания гражданам медицинской помощи».\r\n</p>\r\n<p>4. Договор заключается потребителем (заказчиком) в письменной форме. Договор, регламентирующий условия получения платных медицинских услуг должен отражать порядок расчетов, права, обязанности и ответственность сторон.\r\n</p>\r\n<p>5. При предоставлении платных медицинских услуг должны соблюдаться Порядки оказания стоматологической помощи населению.\r\n</p>\r\n<p>6. Медицинское учреждение обязано обеспечить соответствие предоставляемых услуг требованиям, предъявляемым к методам диагностики, лечения и профилактики стоматологических заболеваний, разрешенным на территории Российской Федерации.\r\n</p>\r\n<p>7. Цены на медицинские услуги устанавливаются согласно калькуляции и утверждаются директором ООО «Авантис».\r\n</p>\r\n<p>8. При предоставлении медицинских услуг оформляется и ведется медицинская карта стоматологического больного формы 025/У.\r\n</p>\r\n<h3>Информация об исполнителе:</h3>\r\n<p>1. Исполнитель обязан предоставить посредством размещения на сайте, информационных стендах или стойках следующую информацию: наименование юридического лица, адрес местонахождения, сведения о лицензии, перечень услуг с указанием цен в рублях, сведения о медицинских работниках, режим работы учреждения, адреса и телефоны органов исполнительной власти субъекта РФ в сфере здравоохранения, территориального органа Федеральной службы по надзору  и защиты прав потребителя в сфере здравоохранения.\r\n</p>\r\n<p>2. По требованию потребителя или заказчика исполнитель предоставляет для ознакомления: копию учредительных документов, копию лицензии, образовательные документы специалистов.\r\n</p>\r\n<p>3. До заключения Договора исполнитель уведомляет потребителя о том, что несоблюдение указаний (рекомендаций) врача, предоставившего медицинскую услугу, может снизить качество предоставляемой услуги, повлечь за собой невозможность ее завершения в срок или отрицательно сказаться на состоянии здоровья потребителя.\r\n</p>\r\n<h3>Порядок предоставления платных медицинских услуг:</h3>\r\n<p>1. Платные медицинские услуги предоставляются при наличии Договора на стоматологические услуги, информированного добровольного согласия потребителя (законного представителя).\r\n</p>\r\n<p>2. Исполнитель предоставляет потребителю или его законному представителю по его требованию информацию: о состоянии его здоровья, включая сведения о результатах обследования, диагнозе, методах лечения, связанном с ним риске, возможных вариантах и последствиях вмешательства, ожидаемых результатах лечения, об используемых лекарственных препаратах, противопоказаниях к их применению.\r\n</p>\r\n<p>3. Медицинское учреждение обязано вести статистический и бухгалтерский учет результатов платных медицинских услуг, составлять требуемую отчетность и предоставлять ее вовремя в порядке и сроки, установленные Законом и правовыми актами РФ.\r\n</p>\r\n<p>4. Медицинское учреждение обязано вести всю медицинскую документацию, в соответствии с профилем деятельности.\r\n</p>\r\n<p>5. Расчеты с населением производится в учреждениях банков или в ООО «Авантис» с применением контрольно-кассовой машины.\r\n</p>\r\n<p>6. Потребителю после исполнения услуг выдается документ подтверждающий оплату предоставленных медицинских услуг (контрольно-кассовый чек, квитанцию). По просьбе потребителя возможна выдача детализированного счета или Акта выполненных работ на оказанные услуги.\r\n</p>\r\n<p>7. Потребитель, пользующейся платными медицинскими услугами обязан:\r\n</p>\r\n<ul>\r\n	<li>оплатить стоимость оказанных услуг в срок, соответствующий Договору на оказание стоматологических услуг;</li>\r\n	<li>выполнять требования, обеспечивающие качественное оказание услуг, включая сообщение всей необходимой информации, соблюдать правила ООО «Авантис».</li>\r\n</ul>\r\n<h3>Ответственность исполнителя за предоставление платных медицинских услуг:</h3>\r\n<p>1. В соответствии с законодательством РФ медицинское учреждение несет ответственность перед потребителем за неисполнение или ненадлежащее исполнение условий договора, несоблюдение требований, предъявляемых к методам диагностики, лечения, профилактики стоматологических заболеваний, разрешенным на территории РФ, а также в случае причинения вреда здоровью и жизни потребителя.\r\n</p>\r\n<p>2. Потребитель, пользующийся платными медицинскими услугами вправе требовать возмещения убытков, причиненных неисполнением или ненадлежащим исполнением условий договора, возмещения ущерба, в случае  причинения вреда здоровью и жизни, а также о компенсации за причинение морального вреда в соответствии с законодательством РФ.\r\n</p>\r\n<p>3. При несоблюдении сроков оказанного лечения потребитель вправе по своему выбору:\r\n</p>\r\n<ul>\r\n	<li>назначить новый срок оказания услуг;</li>\r\n	<li>потребовать уменьшения стоимости предоставляемой услуги;</li>\r\n	<li>потребовать предоставления услуги другим специалистом;</li>\r\n	<li>расторгнуть договор и потребовать возмещения убытков.</li>\r\n</ul>\r\n<p>4. При наступлении гарантийного случая (период в течение которого, в случае обнаружения недостатков выполненной работе) потребитель вправе по своему выбору:\r\n</p>\r\n<ul>\r\n	<li>безвозмездно устранить недостатки выполненной работы врачом оказавшим ранее услугу или другим специалистом исполнителя;</li>\r\n	<li>потребовать уменьшения цены за выполненную работу;</li>\r\n	<li>потребовать возмещения убытков, без повторного лечения.</li>\r\n</ul>\r\n<p>5. Претензии и споры, возникшие между потребителем и медицинским учреждением, разрешаются по соглашению сторон или в судебном порядке в соответствии с законодательством Российской Федерации.\r\n</p>\r\n<p>6. ООО «Авантис» освобождается от ответственности за неисполнение или ненадлежащее исполнение платной медицинской услуги, если докажет, что неисполнение или ненадлежащее исполнение произошло вследствие несоблюдения пациентом предписаний или вследствие непреодолимой силы, а также по иным основаниям, предусмотренным законом.\r\n</p>\r\n<h3>Гарантия при выполнении платных медицинских услуг:</h3>\r\n<p>1. В соответствии с Гражданским кодексом РФ и Законом РФ «О защите прав потребителей» и Положения «Об установлении гарантийного срока на работу при оказании стоматологической помощи в государственных, муниципальных, частных медицинских учреждениях на территории Оренбургской области», утвержденного на ССА Оренбургской области от 10.12.2008г., в  ООО «Авантис» устанавливаются сроки гарантии на проведенное стоматологическое лечение:\r\n</p>\r\n<ul>\r\n	<li>Пломба из композита химического или светового отвердения – 1 год;</li>\r\n	<li>Пломба из стеклоиономерного цемента – 6 мес;</li>\r\n	<li>Шинирование зубов при заболеваниях пародонта  – 1 год;</li>\r\n	<li>Протезы металлокерамические и цельнолитые – 1,5 года;</li>\r\n	<li>Протезы из безметалловой керамики – 1 год</li>\r\n	<li>Съемный бюгельный металлический  протез – 2 года;</li>\r\n	<li>Съемный бюгельный пластмассовый протез – 1 год;</li>\r\n	<li>Съемный безметалловый пластиночный протез – 1 год;</li>\r\n	<li>Коронка из диоксид циркония – 2 года;</li>\r\n	<li>Виниры, вкладки – 1 года.</li>\r\n</ul>\r\n<p>Гарантия на эндодонтическое, пародонтологическое,  лечение и имплантацию не предусмотрена, так как вышеуказанное лечение связано с большой степенью риска возникновения осложнения после проведенного лечения. Возникающие осложнения лечатся в общем порядке, на возмездной основе.\r\n</p>\r\n<p>2. Гарантийные сроки исчисляются с момента завершения оказания услуг (наложения пломбы, установки протеза).\r\n</p>\r\n<p>3. Лечащий врач имеет право установить индивидуальный срок гарантии, исходя из конкретной клинической картины, получив предварительно согласие пациента.\r\n</p>\r\n<p>4. При оказании услуг по добровольному медицинскому страхованию, в случае установления страховой компанией гарантийных сроков выше установленных ООО «Авантис», дефекты, возникающие по истечению  гарантийных сроков, установленных последним, устраняются за счет средств страховой компании.\r\n</p>\r\n<div><br>\r\n</div>', '', '', 1, 0, 72, '', '', '', NULL, '', '', NULL, NULL),
(74, 1, 'ru', NULL, '2019-09-13 16:12:29', '2019-09-13 16:17:58', 1, 1, '', 'Способы оплаты', 'sposoby-oplaty', '<p>Оплату за оказанные услуги возможно произвести наличными средствами через контрольно-кассовый аппарат организации, банковской картой VISA, MASTER CARD или по безналичному расчету на счет организации.\r\n</p>\r\n<hr>\r\n<h3>Реквизиты </h3>\r\n<p>Общество с ограниченной ответственностью «Авантис» </p>\r\n<p>ОГРН 1185658003453\r\n</p>\r\n<p>ИНН 5610230406\r\n</p>\r\n<p>КПП 561001001\r\n</p>\r\n<p>Банковские реквизиты:\r\n</p>\r\n<p>р/сч 40702810146000006301 в ПАО Сбербанк отделение №8623\r\n</p>\r\n<p>БИК 045354601\r\n</p>\r\n<p>к/сч 30101810600000000601\r\n</p>\r\n<hr>\r\n<p><br><br>\r\n</p>', '', '', 1, 0, 73, '', '', '', NULL, '', '', NULL, NULL),
(75, NULL, 'ru', 2, '2019-09-16 16:13:11', '2020-04-06 11:17:01', 1, 1, 'Лечение зубов', 'Лечение зубов в Оренбурге', 'lechenie-zubov', '<h2>Лечение всех видов болезней зубов</h2><p>Посещение стоматолога – процедура, которую боятся многие и это главная причина того, что здоровье зубов очень часто запускается. Причиной этого становится страх перед болью. Частная клиника «Авантис» гарантирует безболезненное лечение:\r\n</p><ul>\r\n	<li>Кариеса, как одного из самого распространенного заболевания. Причины возникновения – вымывания минералов из состава эмали, что ведет к деструкции ее целостности.</li>\r\n	<li>Пульпита – одна из стадий осложнения кариеса вследствие попадания инфекции. Сопряженной сильным болевым синдромом.</li>\r\n	<li>Периодонтита – еще одна разновидность осложнения кариеса. Следствие – прогрессирующая деструкция тканей. Возникает вследствие отсутствия или неправильного лечения пульпита. Где инфекцией поражаются косные структуры, расположенные более глубоко (например, в прикорневой зоне).</li>\r\n	<li>Киста – очаговое видоизменение деструкция костных тканей овальной формы, расположенной у самой верхушки корня. Причина – хронический инфекционный периодонтит, который длительное время не лечился. В обостренной форме может приводить к образованию флюса (отека щеки с нагноением).</li>\r\n</ul><p>Каждое из перечисленный заболеваний очень опасно для здоровых зубов-соседей и здоровья самого человека. Ведь инфекция, попавшая (проникшая) в зуб свободно попадает в ЖКТ, а потом, кровяным потоком по всему организму.\r\n</p><h2>Безболезненное лечение зубов в Оренбурге</h2><p>Стоматология в клинике «Авантис» – полностью безболезненная терапия. Лечить зубы в Оренбурге больше не больно. Современное оборудование и сертифицированные препараты стопроцентно избавляют пациентов от болевого синдрома. Виды анестезии, которые практикуются в нашем заведении:\r\n</p><ol>\r\n	<li>Аппликационная. Применяется для пациентов, записавшихся на чистку десен, удаления зубного налета (камня), а также для других манипуляций при повышенной чувствительности зуба.</li>\r\n	<li>Инфильтрационная. Манипуляция, предусматривающая обезболивание необходимой зоны, инъекцией с анестетиком.</li>\r\n</ol><p>Лекарства, которые используются клиникой сертифицированные, разрешенные Минздравом, поэтому никаких побочных эффектов или аллергий не вызывают.\r\n</p><blockquote>Тем, кто панически боится уколов, специалист обезболит зону прокола анестетиком.\r\n</blockquote><h2>Преимущества лечения зубной боли в частной клинике</h2><p>Статистика свидетельствует, что к частным учреждениям доверие выше, чем к муниципальным. Это проясняется рядом положительных моментов:\r\n</p><ul>\r\n	<li>Сервис – на самом высоком уровне</li>\r\n	<li>Оснащение – современное, модернизированное, новейшее</li>\r\n	<li><a href=\"https://avantis56.ru/team\">Врачи</a> – с высокой степенью квалификации, регулярно повышающие свой уровень</li>\r\n	<li>Подход к пациентам – индивидуальный, внимательный</li>\r\n	<li>Очереди – полное отсутствие</li>\r\n</ul><h2>Перечень услуг терапевтического отделения стоматологии «Авантис»</h2><ul>\r\n	<li>предварительная запись и прием пациента врачом-стоматологом;</li>\r\n	<li><a href=\"https://avantis56.ru/lechenie-kariesa\">лечение кариозного</a> и не кариозного поражения зубов;</li>\r\n	<li>установка пломбировочного материала высокого качества при разрушениях масштабом меньше 50%;</li>\r\n	<li>терапия пульпита (воспалительный процесс в нервах зубов);</li>\r\n	<li>лечение периодонтита любой сложности;</li>\r\n	<li>механическое избавление от зубного камня как сверху зуба, так и под деснами;</li>\r\n	<li>терапия воспалений ротовой полости, слизистой;</li>\r\n	<li>лечение гиперчувствительности зубов.</li>\r\n</ul><p>Решая, куда пойти лечить зубы в Оренбурге, обратите внимание на профессионализм врачей клиники, ознакомьтесь с <a href=\"https://avantis56.ru/review\">отзывами пациентов</a>, узнайте об оснащении учреждения. Мы уверены, что в числе лучших из вашего списка будет клиника «Авантис». Нам уже доверяют многого жителей города. Администрация и персонал клиники заботятся о том, чтобы каждый посетитель остался довольным не только сервисом обслуживания, но и качеством лечения зубов.\r\n</p><div class=\"lazyYT\" data-youtube-id=\"8tmj7pWjT0Y\" data-ratio=\"16:9\">Лечение зубов\r\n</div>', 'лечение зубов оренбург, лечить зубы оренбург', 'Лечить зубы в Оренбурге по низким ценам. Лечение зубов и терапевтическая стоматология  без страха и боли. Записаться на прием к стоматологу-терапевту | Частная стоматология «Авантис»', 1, 0, 74, '', 'services', 'Лечение зубов в Оренбурге | Частная стоматология «Авантис»', 'c451b4d4835cf69bf2bcdf1b3b1412b3.png', '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/39c3a4aad5636584a2866d130629feee.jpg\" alt=\"Лечение зубов\">\r\n</div><p>Клиника «Авантис» предоставляет качественное и оперативное лечение зубов в Оренбурге. Задачи терапевтического отделения: избавление от кариеса; лечение пульпитов; терапия десен, слизистой ротовой полости, зубных каналов; профессиональное <a href=\"https://avantis56.ru/ultrazvukovaya-chistka-kamney\">удаление зубного камня</a> и прочих отложений. Возможности нашей клиники довольно-таки широкие. Каждый житель Оренбурга может записаться на посещение терапевтического кабинета с самым современным оснащением. Врачи высшей категории окажут любой вид врачебной помощи в полном объеме.\r\n</p>', '<table class=\"bordered\">\r\n<tbody>\r\n<tr>\r\n	<th>Наименование услуги\r\n	</th>\r\n	<th>Цена в рублях\r\n	</th>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение одной пломбы из композита светового отверждения\r\n		</p>\r\n	</td>\r\n	<td>1500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение пломбы при разрушении зуба более ½ коронковой части зуба\r\n		</p>\r\n	</td>\r\n	<td>2000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Наложение пломбы при полном разрушении коронковой части зуба(без учета штифтов)\r\n		</p>\r\n	</td>\r\n	<td>2200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Изготовление композитного винира (на один зуб)\r\n		</p>\r\n	</td>\r\n	<td>2500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(76, NULL, 'ru', 75, '2019-09-16 16:21:37', '2020-04-29 12:02:53', 1, 1, 'Лечение кариеса', 'Лечение кариеса', 'lechenie-kariesa', '<p>Основным этиологическим агентом являются стрептококки: они расщепляют сахара и создают кислую среду. В результате происходит деминерализация дентина. Цена лечения кариеса во многом зависит от того, когда удалось диагностировать заболевание. На первых порах нет сильных повреждений, поэтому стоимость будет ниже.\r\n</p><h2>Диагностика кариеса</h2><p>На начальном этапе распознать патологию без специального оборудования сложно. Рекомендуется  <a href=\"https://avantis56.ru/pravila-zapisi-na-priem\">посещать стоматолога</a> 1-2 раза в год для диагностики нарушений. Врач выявит пораженную область с помощью рентгена, электродиагностики, ультрафиолетового излучения.\r\n</p><h3>Явные признаки</h3><ol>\r\n	<li>изменение цвета эмали, белые или коричневые точки;</li>\r\n	<li>чувствительность к горячему и холодному;</li>\r\n	<li>появляются болевые ощущения при механическом нажатии, употреблении сладкого;</li>\r\n	<li>запах изо рта;</li>\r\n	<li>в запущенной стадии видна кариозная полость, в которую забиваются кусочки еды.</li>\r\n</ol><h3>Чем опасно заболевание</h3><ol>\r\n	<li>воспаление, которое сопровождается постоянными болями;</li>\r\n	<li>развивается периодонтит, который приводит к потере зуба;</li>\r\n	<li>когда инфекция проникает в глубокие слои, возникает киста, абсцесс, флегмона, что грозит общей интоксикацией и сепсисом.</li>\r\n</ol><p><img src=\"https://avantis56.ru/uploads/image/bd5b5e7713374a0ce2f4bfd9c3301a9e.jpg\">\r\n</p><h2>Этапы развития</h2><h3>Стадия пятна</h3><p>Сначала на поверхности эмали образуется белесое или сероватое пятно. Пациент не ощущает никакого дискомфорта и <a href=\"https://avantis56.ru/lechenie-zubov\">лечение зубов</a> проходит безболезненно. Оно проводится с помощью специальных препаратов без сверления.\r\n</p><h3>Поверхностный кариес</h3><p>Постепенно кариозная площадь увеличивается вширь и вглубь. Эмаль в этом месте истончается, повышается чувствительность к горячей или холодной пище. Поврежденный участок удаляют лазером или бормашиной в зависимости от уровня поражения. Далее следует реминерализация.\r\n</p><h3>Средний кариес</h3><p>Инфекция проникает в дентин. Развивается воспаление, которое затрагивает нервы. Регулярно возникает сильная боль. Вылечить можно, только полностью удалив поражение механическим способом. После бормашины обязательна обработка антисептиками.\r\n</p><h3>Глубокий кариес</h3><p>Развивается пульпит и другие заболевания. Есть риск полностью потерять зуб. Зараженную часть убирают, устанавливают изолирующую прокладку, затем пломбу.\r\n</p><h2><img src=\"https://avantis56.ru/uploads/image/1dd288db39619147f4de0e3f6416bbd8.jpg\"><br></h2><h2>Методы лечения кариеса</h2><p>В традиционных стратегиях была принята хирургическая модель терапии: после удаления, создается более геометрически совершенная полость, заполненная наиболее совместимым и искусственным материалом. Стоматологи проводят лечение кариеса в Оренбурге подобными методами в большинстве случаев.\r\n</p><ol>\r\n	<li>Фтор является наиболее эффективным средством для предотвращения и локализации очагов болезни. Состав укрепляет эмаль, делая её более устойчивой к кислотным атакам.</li>\r\n	<li><a href=\"https://avantis56.ru/koronki-vkladki\">Коронки</a> используются при значительном разрушении. Кариозная часть высверливается, а коронка располагается над оставшимся участком. Цена за один зуб будет зависеть от степени разрушения коронковой части.</li>\r\n	<li>Если разрушение распространилось на пульпу, его заменяют искусственным материалом, который будет удерживать зуб на месте.</li>\r\n	<li>Глубокое поражение зуба требует полного удаления. Потеря некоторых единиц ряда может повлиять на форму и функцию остальных, без <a href=\"https://avantis56.ru/implantaciya-zubov\">имплантатов</a> не обойтись.</li>\r\n</ol><iframe src=\"https://vk.com/video_ext.php?oid=-174309102&id=456239017&hash=bd2c21fc20118ec6\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"\">\r\n</iframe><h2>Признаки правильно установленной пломбы</h2><ol>\r\n	<li>цвет совпадает с окраской окружающих тканей;</li>\r\n	<li>форма воспроизводит естественные очертания;</li>\r\n	<li>не меняется прикус, нет дискомфорта при жевании;</li>\r\n	<li>поверхность ровная, без выступающих частей, нависающих краев.</li>\r\n</ol><h2><img src=\"https://avantis56.ru/uploads/image/eb4d52fb994786fb4e90ed7dee00da33.jpg\"><br></h2><h2>Лучше предотвратить, чем лечить</h2><p>Неправильная гигиена, ослабленный иммунитет, недостаток некоторых микроэлементов, повышенное содержание  углеводов и сахара в пище - основные причины заболевания.\r\n</p><p>Профилактика кариеса включает следующие рекомендации:\r\n</p><ol>\r\n	<li>чистить зубы дважды в день;</li>\r\n	<li>применять пасту с фтором;</li>\r\n	<li>дополнительно к пасте использовать ополаскиватели и флосс;</li>\r\n	<li>ограничить употребление сладкого;</li>\r\n	<li>принимать поливитаминные комплексы;</li>\r\n	<li>отказаться от курения и алкогольных напитков.</li>\r\n</ol><p>Специалисты также рекомендуют проводить систематическую чистку ультразвуком. В клинике «Авантис» проводят <a href=\"https://avantis56.ru/professionalnaya-gigiena-polosti-rta-air-flow\">профессиональную гигиену</a>  и удаление кариеса с помощью новейших технологий мирового уровня.\r\n</p>', 'лечение кариеса оренбург', 'Лечение кариеса по доступным ценам. Лечение кариеса у взрослых в Оренбурге | Частная стоматология «Авантис»', 1, 0, 75, '', 'services', 'Лечение кариеса в Оренбурге | Частная стоматология «Авантис»', NULL, '<div class=\"thumb\"><img src=\"https://avantis56.ru/uploads/image/ec30e12bef1121fa91e329d2b1c2db87.jpg\" alt=\"Лечение кариеса\">\r\n</div>\r\n<p>Самой распространенной причиной зубной боли является кариес. Большинство пациентов обращается в клиники Оренбурга именно с этой проблемой. Это многофакторная болезнь, вызываемая возбудителем и факторами окружающей среды. \r\n</p>', '<table class=\"table table-bordered\">\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<p>Наименование услуги\r\n		</p>\r\n	</td>\r\n	<td colspan=\"2\">Цена в рублях\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\">\r\n		<p>Наложение одной пломбы из композита светового отверждения\r\n		</p>\r\n	</td>\r\n	<td colspan=\"2\">1500\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\">\r\n		<p>Наложение пломбы при разрушении зуба более ½ коронковой части зуба\r\n		</p>\r\n	</td>\r\n	<td colspan=\"2\">2000\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\">\r\n		<p>Наложение пломбы при полном разрушении коронковой части зуба(без учета штифтов)\r\n		</p>\r\n	</td>\r\n	<td colspan=\"2\">2200\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan=\"2\">\r\n		<p>Изготовление композитного винира (на один зуб)\r\n		</p>\r\n	</td>\r\n	<td colspan=\"2\">2500\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL),
(77, 1, 'ru', NULL, '2020-06-10 12:44:09', '2020-06-10 12:45:44', 1, 1, '', 'Информация о правах и обязанностях граждан в сфере охраны здоровья', 'informaciya-o-pravah-i-obyazannostyah-grazhdan-v-sfere-ohrany-zdorovya', '<p>В соответствии с Федеральным законом от 21.11.2011 № 323-ФЗ \"Об основах охраны здоровья граждан в Российской Федерации» граждане имеют следующие права в сфере охраны здоровья:</p>  <p>- Каждый имеет право на охрану здоровья (ст. 18)</p>  <p>- Каждый имеет право на медицинскую помощь (ст.19). Каждый имеет право на медицинскую помощь в гарантированном объеме, оказываемую без взимания платы в соответствии с программой государственных гарантий бесплатного оказания гражданам медицинской помощи, а также на получение платных медицинских услуг и иных услуг, в том числе в соответствии с договором добровольного медицинского страхования. Для лиц без гражданства и иностранных граждан медицинское обслуживание осуществляется в соответствии с действующим законодательством.</p>  <p>- Пациент имеет право на:</p>  <p>1) выбор врача и выбор медицинской организации в соответствии с законодательством:</p>  <p>2)профилактику, диагностику, лечение, медицинскую реабилитацию в медицинских организациях в условиях, соответствующих санитарногигиеническим требованиям;</p>  <p>3) получение консультаций врачей-специалистов;</p>  <p>4) облегчение боли, связанной с заболеванием и (или) медицинским вмешательством, доступными методами и лекарственными препаратами;</p>  <p>5) получение информации о своих правах и обязанностях, состоянии своего здоровья, о факторах, влияющих на состояние здоровья, выбор лиц, которым в интересах пациента может быть передана информация о состоянии его здоровья;</p>  <p>6)информированное добровольное согласие на медицинское вмешательство;</p>  <p>7) защиту сведений, составляющих врачебную тайну;</p>  <p>8) отказ от медицинского вмешательства;</p>  <p>9) возмещение вреда, причиненного здоровью при оказании ему медицинской помощи, получение льгот при медицинском обслуживании в случаях, предусмотренных законодательством.</p>  <p> Отказ в оказании медицинской помощи в соответствии с программой государственных гарантий бесплатного оказания гражданам медицинской помощи и взимание платы за ее оказание медицинской организацией, участвующей в реализации этой программы, и медицинскими работниками такой медицинской организации не допускаются</p>  <p> По вопросам бесплатного оказания медицинской помощи в рамках обязательного медицинского страхования граждане вправе обратиться:</p>  <p>- Территориальный фонд обязательного медицинского страхования Оренбургской области, адрес: 460014 г. Оренбург, пер. Фабричный, д. 19</p>  <p>телефон: 8(3532) 98 -15-02 Факс: 8(3532) 98-15-75 Email: office@orenfoms.ru</p>  <p> - Согласно Федерального закона от 02.05.2006 № 59-ФЗ \"О порядке рассмотрения обращений граждан Российской Федерации\" граждане имеют</p>  <p>право обращаться лично, а также направлять индивидуальные и коллективные обращения, включая обращения объединений граждан, в том числе юридических лиц, в государственные и муниципальные учреждения и иные организации, на которые возложено осуществление публично значимых функций, и их должностным лицам.</p>  <p>Обязанности граждан в сфере охраны здоровья</p>  <p>1. Граждане обязаны заботиться о сохранении своего здоровья.</p>  <p>2. Граждане в случаях, предусмотренных законодательством Российской Федерации, обязаны проходить медицинские осмотры, а граждане, страдающие заболеваниями, представляющими опасность для окружающих, в случаях, предусмотренных законодательством Российской Федерации, обязаны проходить медицинское обследование и лечение, а также заниматься профилактикой этих заболеваний.</p>  <p>3. Граждане, находящиеся на лечении, обязаны соблюдать режим лечения, в том числе определенный на период их временной нетрудоспособности, и правила поведения пациента в медицинских организациях.</p>', '', '', 1, 0, 78, '', '', '', NULL, '', '', '', NULL),
(78, 1, 'ru', NULL, '2020-06-10 12:54:14', '2020-06-10 12:54:39', 1, 1, '', 'Адреса и телефоны контролирующих органов', 'adresa-i-telefony-kontroliruyushchih-organov', '<p><strong>Министерство здравоохранения Оренбургской области</strong>\r\n</p><p>Официальный сайт: <a href=\"http://www.minzdrav.orb.ru/index.php\">www.minzdrav.orb.ru</a>\r\n</p><p>Адрес: 460006, г. Оренбург, ул. Терешковой, д. 33\r\n</p><p>E-mail: <a href=\"mailto:minzdrav@mail.orb.ru\">minzdrav@mail.orb.ru</a>\r\n</p><p>Блог министра здравоохранения: <a href=\"http://www.minzdrav.orb.ru/blog/\">www.minzdrav.orb.ru/blog</a>\r\n</p><p>Отдел по организации приема граждан и рассмотрению обращений граждан:  (3532) 91-15-09\r\n</p><p>Горячая линия по обращению граждан по вопросам медицинской помощи на территории Оренбургской области: 8-800-200-5603, (3532) 44-89-38\r\n</p><p>Факс: (3532) 37-54-76\r\n</p><hr><p><strong>Территориальный орган Росздравнадзора по Оренбургской области</strong>\r\n</p><p>Официальный сайт: <a href=\"http://56reg.roszdravnadzor.ru/\">56reg.roszdravnadzor.ru</a>\r\n</p><p>Горячая линия:  8 (3532) 77-57-88\r\n</p><p>Телефон: 8 (3532) 77-57-88, 8 (3532) 77-75-96, 8 (3532) 77-82-91\r\n</p><p>E-mail: <a href=\"mailto:rzn56r@reg56.roszdravnadzor.ru\">rzn56r@reg56.roszdravnadzor.ru</a>\r\n</p><p>Адрес: 460000, г. Оренбург, ул. Гая, 14\r\n</p><hr><p><strong>Управление Федеральной службы по надзору в сфере защиты прав потребителей и благополучия человека по Оренбургской области</strong>\r\n</p><p>Официальный сайт: <a href=\"http://56.rospotrebnadzor.ru/\">56.rospotrebnadzor.ru</a>\r\n</p><p>Адрес: 460021, г. Оренбург, ул. 60 лет Октября, 2/1\r\n</p><p>Телефон приемной: 8 (3532) 33-37-98\r\n</p><p>Электронная почта: <a href=\"mailto:oren-rpn@esoo.ru\">oren-rpn@esoo.ru</a>\r\n</p><hr><p><strong>Территориальный фонд обязательного медицинского страхования Оренбургской области </strong>(по вопросам бесплатного оказания медицинской помощи в рамках обязательного медицинского страхования):\r\n</p><p>Официальный сайт: <a href=\"http://www.orenfoms.ru/\">orenfoms.ru</a>\r\n</p><p>Адрес: 460014 Оренбургская область, город Оренбург, переулок Фабричный, дом 19\r\n</p><p>Телефон приемной: +7(3532)98-15-02, факс: +7(3532)98-15-75\r\n</p><p>Электронная почта: <a href=\"mailto:office@orenfoms.ru\">office@orenfoms.ru</a>\r\n</p>', '', '', 1, 0, 79, '', '', '', NULL, '', '', '', NULL),
(79, 1, 'ru', NULL, '2020-06-10 13:06:32', '2020-06-10 13:07:28', 1, 1, '', 'Дата государственной регистрации, сведения об учредителе', 'data-gosudarstvennoy-registracii-svedeniya-ob-uchreditele', '<p>==[[w:GalleryWidget|galleryId=11;view=gallerylicense]]==</p>', '', '', 1, 0, 80, '', '', '', NULL, '', '', '', NULL),
(80, NULL, 'ru', 12, '2020-07-13 11:19:52', '2020-07-17 11:45:11', 1, 1, 'Методы исправления прикуса', 'Что такое миошина?', 'chto-takoe-mioshina', '<h2>Шинотерапия в стоматологии</h2><p>Лечебно-диагностическая шина - медицинский инструмент, изготавливаемый из полимерных материалов гипоаллергенного характера. Внешне она напоминает каппу боксеров, имеет изогнутый вид, соответствующий геометрии человеческих челюстей. Обладает «эффектом памяти». В зависимости от разновидности дисфункции челюстного аппарата, шина может быть надета как на верхнюю, так и на нижнюю челюсть пациента.\r\n</p><p>Основные характеристики шинотерапии:\r\n</p><ol>\r\n	<li>Высокие адаптивные показатели – пациент без последствий в оптимальные сроки привыкает к ношению аппарата, шина не вызывает дискомфорта.</li>\r\n	<li>Подтвержденная эффективность – в отличие от иных видов терапевтического лечения прикуса данный аппарат позволяет увидеть результаты в первые 30 дней лечения.</li>\r\n	<li>Многофункциональность – выравнивает челюстную дугу, распределяет нагрузку при жевании, помогает снизить напряжение в мышечной ткани, способна изменять положение конкретных зубных элементов.\r\n	</li>\r\n</ol><p>Конструкция изготавливается по индивидуальному оттиску, что обеспечивает не только комфорт при работе челюстей, но и надежную фиксацию самой «каппы». Основные виды шин в стоматологии:\r\n</p><h3>Артропатическая</h3><p>Применяется для лечения пациентов с дисфункцией височно-нижнечелюстного сустава, который хрустит, причиняет боль и дискомфорт при открывании рта.\r\n</p><h3>Комбинированная</h3><p>Необходима для инициальной терапии одновременно в двух направлениях – для изменения прикуса и решения проблем в нейромышечной стоматологии.\r\n</p><h3>Миошина</h3><p>Используется в случаях, когда нарушена функциональность лицевых мышц: при жевании пациент испытывает дискомфорт и скованность, наблюдает неконтролируемые спазмы или сжатия челюстей. Это наиболее часто используемая конструкция, которая обладает целым перечнем показаний к применению.\r\n</p><h2>Применение миошин</h2><p>Шины этой категории представляют собой небольшое устройство, созданное из полимеров и устанавливаемое в основном на верхнюю челюсть. Это дугообразный аппарат, снимающий гипертонус жевательных мышц. Они получают так называемую разгрузку, что заметно уменьшает истираемость зубов, пропадают боли и перенапряжение, усталость в момент жевания.\r\n</p><p>Показания:\r\n</p><ol>\r\n	<li>Щелчки, резкие звуки, хруст в челюстях при открывании рта.</li>\r\n	<li>Болевые синдромы – в том числе в области ротовой полости и даже головные боли.</li>\r\n	<li>Парафункции (неосознанная необходимость открывания, закрывания челюстей).</li>\r\n	<li>Бруксизм (неконтролируемый скрежет зубами).</li>\r\n	<li>Чрезмерная утомляемость жевательных мышц, в том числе при разговорах.</li>\r\n	<li>Нарушение прикуса.</li>\r\n</ol><p>Универсальность устройства позволяет устанавливать его практически любому пациенту независимо от возраста, половых признаков и анатомических особенностей. Врач подберет стабилизирующую миошину, которая не вызовет дискомфорта при постоянном ношении. Противопоказания: воспалительные процессы в ротовой полости, чрезмерная активность языка и губ, что приводит к ее смещению и отсутствию терапевтического эффекта.\r\n</p>', '', 'Миошина второй категории - стоматологи клиники «Авантис» рассказывают о современных стоматологических методах и технологиях.', 1, 0, 81, '', 'blog', 'Миошина второй категории: для чего она предназначена - Блог на сайте клиники «Авантис»', '3ab97ff1a76b0b3d4bbe36101f510292.jpg', '<p>Применения миошин как подготовительный этап в исправлении прикуса.</p>', '<p>Ряд ортопедических работ, таких как накладки виниров или установка коронок, требуют проведения инициальной или начальной терапии. Пациентам необходимо исправить прикус, решить проблемы неконтролируемых мышечных спазмов и перенапряжения мышц в челюстном суставе.\r\n</p><p> Только после этого можно проводить протезирование, будучи уверенным в том, что результатом процедуры станет бесперебойная функциональность зубов, высокие эстетические показатели и корректная работа челюстей.</p><p> В нейромышечной стоматологии за подготовительный этап отвечает шинотерапия.\r\n</p>', '', NULL),
(81, NULL, 'ru', 12, '2020-07-17 11:59:14', '2020-07-17 15:55:27', 1, 1, 'Ученые заявили о вреде зубных паст с частицами угля', 'Зубные пасты с частицами угля', 'zubnye-pasty-s-chasticami-uglya', '<p>Производители утверждают, что уголь помогает избавиться от патогенных организмов и следов от чая и кофе, оказывает бактерицидный эффект, улучшает pH-баланс и борется с неприятным запахом изо рта. Однако ученые выяснили, что такие пасты плохо влияют на здоровье зубов, а маркетинговые описания не подтверждены исследованиями.\r\n</p><div>\r\n<h3>Почему пасты с древесным углем вредны</h3>\r\n<ul>\r\n	<li>Эрозия зубов;</li>\r\n	<li>Риск развития кариеса;</li>\r\n	<li>Воспаление десен (гингивит) и пародонтит.</li>\r\n</ul>\r\n<p>Британские исследователи изучили 50 образцов, и всего в 8% из них содержался фтор. Проведенная проверка показала, что в составе образцов не содержится достаточного количества отбеливающих веществ, а высокая абразивность негативно влияет на десны и эмаль.\r\n</p>\r\n<h3>Нужно ли полностью от них отказываться?</h3>\r\n<p>Активированный уголь давно используется в медицине и имеет много преимуществ, но применять его для чистки зубов ежедневно не стоит. Если у вас нет трещин на эмали, здоровые десны и вы не носите брекеты, то лучше это делать раз в месяц и тщательно полоскать ротовую полость. В таком случае удастся сохранить баланс между положительным и отрицательным влиянием на зубы.\r\n</p>\r\n<h3>Можно ли использовать детям?</h3>\r\n<h2></h2>\r\n<p>Малышам и подросткам рекомендуется чистить зубы пастой с меньшим содержанием фтора и абразивных веществ и щадящим действием, поэтому стоит с осторожностью подходить к этому вопросу.\r\n</p>\r\n<p>Стоматологи призывают воздержаться от покупки зубных паст с частичками угля, обосновав это тем, что микрочастицы могут проникнуть в щели между зубами и пломбами и остаться там навсегда. Дэмиен Волмсли назвал образцы «опасным способом отбеливания» и призвал людей с\r\n</p></div>', '', 'Уголь активно используют при изготовлении масок для лица, булочек для бутербродов, в медицине и промышленности. Последний тренд - черные пасты и порошки с углем | Статьи клиники «Авантис»', 1, 0, 82, '', 'blog', 'Ученые заявили о вреде зубных паст с частицами угля | Статьи клиники «Авантис»', '5e368a8a3bed9cfbf1372300058366c2.jpg', '<p>Ученые выяснили, что такие пасты плохо влияют на здоровье зубов, а маркетинговые описания не подтверждены исследованиями...\r\n</p>', '<p>Уголь активно используют при изготовлении масок для лица, булочек для \r\nбутербродов, в медицине и промышленности. Последний тренд - черные пасты\r\n и порошки с углем.\r\n</p><p><strong style=\"border-bottom:1px solid #232323\" rel=\"border-bottom:1px solid #232323\">Наибольшую популярность они имеют в:</strong></p><p><strong style=\"border-bottom:1px solid #232323\" rel=\"border-bottom:1px solid #232323\">	</strong></p><ul>\r\n	<div>\r\n		<div>\r\n			<li>Великобритании</li>\r\n			<li>Японии</li>\r\n			<li>США</li>\r\n			<li>Индии</li>\r\n			<li>Таиланде</li>\r\n			<li>Литве</li>\r\n			<li>Австрии</li>\r\n			<li>Китае</li>\r\n			<li>Южной Корее</li>\r\n			<li>Швейцарии.</li>\r\n		</div>\r\n	</div>\r\n</ul>', '', NULL),
(82, NULL, 'ru', 12, '2020-07-17 12:05:14', '2020-07-17 15:43:57', 1, 1, 'Отбеливающие полоски разрушают дентин', 'Отбеливающие полоски', 'otbelivayushchie-poloski', '<h3>Кто организовал исследование?</h3><p>Вызывают деструкцию зубов накладки с перекисью водорода. Выводы сделали работники Стоктонского университета, находящегося в Нью-Джерси. Они провели опыты, озвучив их итоги на собрании Американского общества биохимии и молекулярной биологии. В 2019 году оно проходило с 6 по 9 апреля в городе Орландо штата Флорида.\r\n</p><h3>Суть эксперимента</h3><p>В прошлые годы проверки ограничивались изучением воздействия отбеливающих лент на эмаль, поэтому процедура считалась безвредной. Это самая твердая ткань в организме человека. Ее сравнивают с алмазом, поэтому при процедуре она не повреждается. Страдает дентин. Биохимия твердых тканей зуба представлена четырьмя слоями:\r\n</p><ul>\r\n	<li>эмаль</li>\r\n	<li>дентин</li>\r\n	<li>десна</li>\r\n	<li>пульпа</li>\r\n</ul><p>Работы экспериментаторов были нацелены на то, чтобы определить, как сильно портится дентин после осветления (располагается он под эмалью и на 90% состоит из коллагена).\r\n</p><p>Для проведения испытания отобрали участников со здоровой ротовой полостью. Ученые пытались решить 2 задачи:\r\n</p><ol>\r\n	<li>Дать полноценную характеристику влияния перекиси на дентин.</li>\r\n	<li> Оценить степень разрушения его фибрилл.</li>\r\n</ol><p>Удалось выяснить, что коллагеновые соединения действительно страдают. Они не растворяются, а дробятся, делясь на мелкие фрагменты.\r\n</p><p>Резюме\r\n</p><h3>В целом, результаты получили неутешительные.<br> Оказалось, что молекулы H2O2 свободно проникают не только в эмаль.<br> Регулярное использование пластырей приводит к резкому снижению белка, в том числе, и в дентине.</h3><p>Удостовериться в правильности выводов помогла вторая работа. Ученые подвергли обработке чистый коллаген. Анализ, проведенный методом электрофореза, показал, что протеиновые связи стали слабыми. Это негативным образом отражается на состоянии зубов.\r\n</p><p>Исследователи не оценивали возможность восстановления соединительных тканей с течением времени. В будущем специалисты предпримут попытку понять, как перекись водорода влияет на остальные белковые компоненты дентина.\r\n</p>', '', 'Американцы ежегодно тратят миллиарды долларов на средства, помогающие добиться белоснежной улыбки. Однако в погоне за красотой можно нанести вред здоровью. Этот факт доказан экспериментальным путем | Статьи клиники «Авантис»', 1, 0, 83, '', 'blog', 'Отбеливающие полоски разрушают дентин | Новости клиники «Авантис»', 'f8104054c3cf150a76b0a4f5885a9702.jpg', '<p>Американцы ежегодно тратят миллиарды долларов на средства, помогающие \r\nдобиться белоснежной улыбки. Однако в погоне за красотой...\r\n</p>', '<p>Американцы ежегодно тратят миллиарды долларов на средства, помогающие \r\nдобиться белоснежной улыбки. Однако в погоне за красотой можно нанести \r\nвред здоровью. Этот факт доказан экспериментальным путем.\r\n</p>', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_page_page_image`
--

CREATE TABLE `avantis_page_page_image` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_review`
--

CREATE TABLE `avantis_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `text` text NOT NULL,
  `moderation` int(11) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `useremail` varchar(256) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_review`
--

INSERT INTO `avantis_review` (`id`, `user_id`, `date_created`, `text`, `moderation`, `username`, `image`, `useremail`, `category_id`, `position`) VALUES
(57, NULL, '2019-06-19 23:49:25', 'gfg', 2, 'gfg', NULL, 'ewkrrf@gmail.com', NULL, 1),
(58, NULL, '2019-10-11 14:11:16', '213213', 2, '1231', NULL, '231312312@dfsf.ew', NULL, 2),
(59, NULL, '2020-02-23 19:09:05', 'Быстрая и надёжная доставка грузов из Азии  \r\nТранспортно-экспедиторская компания Маэстро Шиппинг Компани , Владивосток \r\nтел.:89644431130 \r\nэл.почта:mtmd2-nhk(at..)msc.com.ru', 2, 'Vlad4819', NULL, 'mail@msc.com.ru', NULL, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_sitemap_page`
--

CREATE TABLE `avantis_sitemap_page` (
  `id` int(11) NOT NULL,
  `url` varchar(250) NOT NULL,
  `changefreq` varchar(20) NOT NULL,
  `priority` float NOT NULL DEFAULT '0.5',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_sitemap_page`
--

INSERT INTO `avantis_sitemap_page` (`id`, `url`, `changefreq`, `priority`, `status`) VALUES
(1, '/', 'daily', 0.5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_attribute`
--

CREATE TABLE `avantis_store_attribute` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_attribute_group`
--

CREATE TABLE `avantis_store_attribute_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_attribute_option`
--

CREATE TABLE `avantis_store_attribute_option` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_category`
--

CREATE TABLE `avantis_store_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_coupon`
--

CREATE TABLE `avantis_store_coupon` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `registered_user` tinyint(4) NOT NULL DEFAULT '0',
  `free_shipping` tinyint(4) NOT NULL DEFAULT '0',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_per_user` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_delivery`
--

CREATE TABLE `avantis_store_delivery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT NULL,
  `available_from` float(10,2) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `separate_payment` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_delivery_payment`
--

CREATE TABLE `avantis_store_delivery_payment` (
  `delivery_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_order`
--

CREATE TABLE `avantis_store_order` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_method_id` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `payment_time` datetime DEFAULT NULL,
  `payment_details` text,
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `separate_delivery` tinyint(4) DEFAULT '0',
  `status_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(1024) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `note` varchar(1024) NOT NULL DEFAULT '',
  `modified` datetime DEFAULT NULL,
  `zipcode` varchar(30) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `house` varchar(50) DEFAULT NULL,
  `apartment` varchar(10) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_order_coupon`
--

CREATE TABLE `avantis_store_order_coupon` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_order_product`
--

CREATE TABLE `avantis_store_order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `variants` text,
  `variants_text` varchar(1024) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_order_status`
--

CREATE TABLE `avantis_store_order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_store_order_status`
--

INSERT INTO `avantis_store_order_status` (`id`, `name`, `is_system`, `color`) VALUES
(1, 'Новый', 1, 'default'),
(2, 'Принят', 1, 'info'),
(3, 'Выполнен', 1, 'success'),
(4, 'Удален', 1, 'danger');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_payment`
--

CREATE TABLE `avantis_store_payment` (
  `id` int(11) NOT NULL,
  `module` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `settings` text,
  `currency_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_producer`
--

CREATE TABLE `avantis_store_producer` (
  `id` int(11) NOT NULL,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product`
--

CREATE TABLE `avantis_store_product` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_attribute_value`
--

CREATE TABLE `avantis_store_product_attribute_value` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_category`
--

CREATE TABLE `avantis_store_product_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_image`
--

CREATE TABLE `avantis_store_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_image_group`
--

CREATE TABLE `avantis_store_product_image_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_link`
--

CREATE TABLE `avantis_store_product_link` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_link_type`
--

CREATE TABLE `avantis_store_product_link_type` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_store_product_link_type`
--

INSERT INTO `avantis_store_product_link_type` (`id`, `code`, `title`) VALUES
(1, 'similar', 'Похожие'),
(2, 'related', 'Сопутствующие');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_product_variant`
--

CREATE TABLE `avantis_store_product_variant` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_type`
--

CREATE TABLE `avantis_store_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_store_type_attribute`
--

CREATE TABLE `avantis_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_user_tokens`
--

CREATE TABLE `avantis_user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_user_tokens`
--

INSERT INTO `avantis_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(30, 1, '0rGse1qsZsWJ9S49bcyP1xRSh90~CbvR', 2, 1, '2019-06-04 14:24:59', '2019-06-04 14:25:57', '127.0.0.1', '2019-06-05 14:24:59'),
(181, 1, 'tYz0dh7aR~yh13CxRSUaZWwAXczU7~LL', 4, 0, '2020-07-17 14:17:52', '2020-07-17 14:17:52', '95.105.117.203', '2020-07-24 14:17:52');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_user_user`
--

CREATE TABLE `avantis_user_user` (
  `id` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '0f3891718baac8f50a911d656cdd888f0.50315800 1547369133',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_user_user`
--

INSERT INTO `avantis_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`) VALUES
(1, '2019-06-04 14:25:57', '', '', '', 'avantis', 'rouming76@gmail.com', 0, NULL, '', '', '', 1, 1, '2020-07-17 14:17:52', '2019-01-13 13:51:33', NULL, '$2y$13$dhdofU.O/1yHgan9w0d.4ez1WLKRb8kcFS75fHg598BqiGnNdTwqO', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `avantis_yupe_settings`
--

CREATE TABLE `avantis_yupe_settings` (
  `id` int(11) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `avantis_yupe_settings`
--

INSERT INTO `avantis_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', 'Клиника Avantis оказывает весь спектр стоматологических услуг.Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику тех или иных заболеваний зубов', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(2, 'yupe', 'siteName', 'Клиника Avantis', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(3, 'yupe', 'siteKeyWords', '', '2019-01-13 13:55:59', '2019-07-11 17:47:15', 1, 1),
(4, 'yupe', 'email', 'rouming76@gmail.com', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(5, 'yupe', 'theme', 'default', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(6, 'yupe', 'backendTheme', '', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2019-01-13 13:55:59', '2019-01-13 13:55:59', 1, 1),
(9, 'homepage', 'mode', '2', '2019-01-13 15:12:41', '2019-01-13 15:12:46', 1, 1),
(10, 'homepage', 'target', '1', '2019-01-13 15:12:41', '2019-01-13 15:12:50', 1, 1),
(11, 'homepage', 'limit', '', '2019-01-13 15:12:41', '2019-01-13 15:12:41', 1, 1),
(12, 'review', 'itemsperpage', '1', '2019-01-21 15:53:17', '2019-01-21 15:53:17', 1, 1),
(13, 'review', 'itemsperpagefront', '0', '2019-01-21 15:53:17', '2019-01-21 15:53:17', 1, 1),
(14, 'review', 'moderation', '1', '2019-01-21 15:53:17', '2019-01-21 15:53:17', 1, 1),
(15, 'review', 'email_notification', '', '2019-01-21 15:53:17', '2019-01-21 15:53:17', 1, 1),
(16, 'review', 'adminMenuOrder', '0', '2019-01-21 15:53:18', '2019-01-21 15:53:18', 1, 1),
(17, 'zendsearch', 'indexFiles', 'runtime.search', '2019-06-07 18:13:23', '2019-06-07 18:13:23', 1, 1),
(18, 'page', 'pageSize', '100', '2019-07-11 17:40:21', '2019-07-11 17:40:21', 1, 2),
(19, 'yupe', 'coreCacheTime', '3600', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(20, 'yupe', 'uploadPath', 'uploads', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(21, 'yupe', 'editor', 'redactor', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(22, 'yupe', 'availableLanguages', 'ru,uk_ua,en,zh_cn', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(23, 'yupe', 'allowedIp', '', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(24, 'yupe', 'hidePanelUrls', '0', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(25, 'yupe', 'logo', 'images/logo.png', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(26, 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(27, 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(28, 'yupe', 'maxSize', '5242880', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(29, 'yupe', 'defaultImage', '/images/nophoto.jpg', '2019-07-11 17:47:15', '2019-07-11 17:47:15', 1, 1),
(30, 'news', 'editor', 'redactor', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(31, 'news', 'mainCategory', '', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(32, 'news', 'uploadPath', 'news', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(33, 'news', 'allowedExtensions', 'jpg,jpeg,png,gif', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(34, 'news', 'minSize', '0', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(35, 'news', 'maxSize', '5368709120', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(36, 'news', 'rssCount', '10', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(37, 'news', 'perPage', '10', '2019-07-31 00:25:21', '2019-07-31 00:25:46', 1, 1),
(38, 'news', 'metaTitle', 'Новости и полезные статьи | Частная стоматология «Авантис»', '2019-07-31 00:25:21', '2020-07-17 16:13:56', 1, 1),
(39, 'news', 'metaDescription', 'Актуальные новости и полезные статьи о здоровье зубов. Новейшее оборудование и большой опыт помогают нам с точностью проводить диагностику и эффективно лечить заболевания зубов | Платная стоматология «Авантис»', '2019-07-31 00:25:21', '2020-07-17 16:13:56', 1, 1),
(40, 'news', 'metaKeyWords', '', '2019-07-31 00:25:21', '2019-07-31 00:25:21', 1, 1),
(41, 'menuitem', 'pageSize', '100', '2019-09-13 16:05:21', '2019-09-13 16:05:21', 1, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `avantis_blog_blog`
--
ALTER TABLE `avantis_blog_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_blog_blog_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_avantis_blog_blog_create_user` (`create_user_id`),
  ADD KEY `ix_avantis_blog_blog_update_user` (`update_user_id`),
  ADD KEY `ix_avantis_blog_blog_status` (`status`),
  ADD KEY `ix_avantis_blog_blog_type` (`type`),
  ADD KEY `ix_avantis_blog_blog_create_date` (`create_time`),
  ADD KEY `ix_avantis_blog_blog_update_date` (`update_time`),
  ADD KEY `ix_avantis_blog_blog_lang` (`lang`),
  ADD KEY `ix_avantis_blog_blog_slug` (`slug`),
  ADD KEY `ix_avantis_blog_blog_category_id` (`category_id`);

--
-- Индексы таблицы `avantis_blog_post`
--
ALTER TABLE `avantis_blog_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_blog_post_lang_slug` (`slug`,`lang`),
  ADD KEY `ix_avantis_blog_post_blog_id` (`blog_id`),
  ADD KEY `ix_avantis_blog_post_create_user_id` (`create_user_id`),
  ADD KEY `ix_avantis_blog_post_update_user_id` (`update_user_id`),
  ADD KEY `ix_avantis_blog_post_status` (`status`),
  ADD KEY `ix_avantis_blog_post_access_type` (`access_type`),
  ADD KEY `ix_avantis_blog_post_comment_status` (`comment_status`),
  ADD KEY `ix_avantis_blog_post_lang` (`lang`),
  ADD KEY `ix_avantis_blog_post_slug` (`slug`),
  ADD KEY `ix_avantis_blog_post_publish_date` (`publish_time`),
  ADD KEY `ix_avantis_blog_post_category_id` (`category_id`);

--
-- Индексы таблицы `avantis_blog_post_to_tag`
--
ALTER TABLE `avantis_blog_post_to_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `ix_avantis_blog_post_to_tag_post_id` (`post_id`),
  ADD KEY `ix_avantis_blog_post_to_tag_tag_id` (`tag_id`);

--
-- Индексы таблицы `avantis_blog_tag`
--
ALTER TABLE `avantis_blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_blog_tag_tag_name` (`name`);

--
-- Индексы таблицы `avantis_blog_user_to_blog`
--
ALTER TABLE `avantis_blog_user_to_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  ADD KEY `ix_avantis_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  ADD KEY `ix_avantis_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  ADD KEY `ix_avantis_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  ADD KEY `ix_avantis_blog_user_to_blog_blog_user_to_blog_role` (`role`);

--
-- Индексы таблицы `avantis_callback`
--
ALTER TABLE `avantis_callback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_category_category`
--
ALTER TABLE `avantis_category_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_category_category_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_avantis_category_category_parent_id` (`parent_id`),
  ADD KEY `ix_avantis_category_category_status` (`status`);

--
-- Индексы таблицы `avantis_comment_comment`
--
ALTER TABLE `avantis_comment_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_comment_comment_status` (`status`),
  ADD KEY `ix_avantis_comment_comment_model_model_id` (`model`,`model_id`),
  ADD KEY `ix_avantis_comment_comment_model` (`model`),
  ADD KEY `ix_avantis_comment_comment_model_id` (`model_id`),
  ADD KEY `ix_avantis_comment_comment_user_id` (`user_id`),
  ADD KEY `ix_avantis_comment_comment_parent_id` (`parent_id`),
  ADD KEY `ix_avantis_comment_comment_level` (`level`),
  ADD KEY `ix_avantis_comment_comment_root` (`root`),
  ADD KEY `ix_avantis_comment_comment_lft` (`lft`),
  ADD KEY `ix_avantis_comment_comment_rgt` (`rgt`);

--
-- Индексы таблицы `avantis_contentblock_content_block`
--
ALTER TABLE `avantis_contentblock_content_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_contentblock_content_block_code` (`code`),
  ADD KEY `ix_avantis_contentblock_content_block_type` (`type`),
  ADD KEY `ix_avantis_contentblock_content_block_status` (`status`);

--
-- Индексы таблицы `avantis_feedback_feedback`
--
ALTER TABLE `avantis_feedback_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_feedback_feedback_category` (`category_id`),
  ADD KEY `ix_avantis_feedback_feedback_type` (`type`),
  ADD KEY `ix_avantis_feedback_feedback_status` (`status`),
  ADD KEY `ix_avantis_feedback_feedback_isfaq` (`is_faq`),
  ADD KEY `ix_avantis_feedback_feedback_answer_user` (`answer_user`);

--
-- Индексы таблицы `avantis_gallery_gallery`
--
ALTER TABLE `avantis_gallery_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_gallery_gallery_status` (`status`),
  ADD KEY `ix_avantis_gallery_gallery_owner` (`owner`),
  ADD KEY `fk_avantis_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  ADD KEY `fk_avantis_gallery_gallery_gallery_to_category` (`category_id`);

--
-- Индексы таблицы `avantis_gallery_image_to_gallery`
--
ALTER TABLE `avantis_gallery_image_to_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  ADD KEY `ix_avantis_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  ADD KEY `ix_avantis_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`);

--
-- Индексы таблицы `avantis_image_image`
--
ALTER TABLE `avantis_image_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_image_image_status` (`status`),
  ADD KEY `ix_avantis_image_image_user` (`user_id`),
  ADD KEY `ix_avantis_image_image_type` (`type`),
  ADD KEY `ix_avantis_image_image_category_id` (`category_id`),
  ADD KEY `fk_avantis_image_image_parent_id` (`parent_id`);

--
-- Индексы таблицы `avantis_mail_mail_event`
--
ALTER TABLE `avantis_mail_mail_event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_mail_mail_event_code` (`code`);

--
-- Индексы таблицы `avantis_mail_mail_template`
--
ALTER TABLE `avantis_mail_mail_template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_mail_mail_template_code` (`code`),
  ADD KEY `ix_avantis_mail_mail_template_status` (`status`),
  ADD KEY `ix_avantis_mail_mail_template_event_id` (`event_id`);

--
-- Индексы таблицы `avantis_menu_menu`
--
ALTER TABLE `avantis_menu_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_menu_menu_code` (`code`),
  ADD KEY `ix_avantis_menu_menu_status` (`status`);

--
-- Индексы таблицы `avantis_menu_menu_item`
--
ALTER TABLE `avantis_menu_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_menu_menu_item_menu_id` (`menu_id`),
  ADD KEY `ix_avantis_menu_menu_item_sort` (`sort`),
  ADD KEY `ix_avantis_menu_menu_item_status` (`status`);

--
-- Индексы таблицы `avantis_migrations`
--
ALTER TABLE `avantis_migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_migrations_module` (`module`);

--
-- Индексы таблицы `avantis_news_news`
--
ALTER TABLE `avantis_news_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_news_news_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_avantis_news_news_status` (`status`),
  ADD KEY `ix_avantis_news_news_user_id` (`user_id`),
  ADD KEY `ix_avantis_news_news_category_id` (`category_id`),
  ADD KEY `ix_avantis_news_news_date` (`date`);

--
-- Индексы таблицы `avantis_notify_settings`
--
ALTER TABLE `avantis_notify_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_notify_settings_user_id` (`user_id`);

--
-- Индексы таблицы `avantis_page_page`
--
ALTER TABLE `avantis_page_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_page_page_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_avantis_page_page_status` (`status`),
  ADD KEY `ix_avantis_page_page_is_protected` (`is_protected`),
  ADD KEY `ix_avantis_page_page_user_id` (`user_id`),
  ADD KEY `ix_avantis_page_page_change_user_id` (`change_user_id`),
  ADD KEY `ix_avantis_page_page_menu_order` (`order`),
  ADD KEY `ix_avantis_page_page_category_id` (`category_id`);

--
-- Индексы таблицы `avantis_page_page_image`
--
ALTER TABLE `avantis_page_page_image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_review`
--
ALTER TABLE `avantis_review`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_sitemap_page`
--
ALTER TABLE `avantis_sitemap_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_sitemap_page_url` (`url`);

--
-- Индексы таблицы `avantis_store_attribute`
--
ALTER TABLE `avantis_store_attribute`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_store_attribute_name_group` (`name`,`group_id`),
  ADD KEY `ix_avantis_store_attribute_title` (`title`),
  ADD KEY `fk_avantis_store_attribute_group` (`group_id`);

--
-- Индексы таблицы `avantis_store_attribute_group`
--
ALTER TABLE `avantis_store_attribute_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_store_attribute_option`
--
ALTER TABLE `avantis_store_attribute_option`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_store_attribute_option_attribute_id` (`attribute_id`),
  ADD KEY `ix_avantis_store_attribute_option_position` (`position`);

--
-- Индексы таблицы `avantis_store_category`
--
ALTER TABLE `avantis_store_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_store_category_alias` (`slug`),
  ADD KEY `ix_avantis_store_category_parent_id` (`parent_id`),
  ADD KEY `ix_avantis_store_category_status` (`status`),
  ADD KEY `avantis_store_category_external_id_ix` (`external_id`);

--
-- Индексы таблицы `avantis_store_coupon`
--
ALTER TABLE `avantis_store_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_store_delivery`
--
ALTER TABLE `avantis_store_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_avantis_store_delivery_position` (`position`);

--
-- Индексы таблицы `avantis_store_delivery_payment`
--
ALTER TABLE `avantis_store_delivery_payment`
  ADD PRIMARY KEY (`delivery_id`,`payment_id`),
  ADD KEY `fk_avantis_store_delivery_payment_payment` (`payment_id`);

--
-- Индексы таблицы `avantis_store_order`
--
ALTER TABLE `avantis_store_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `udx_avantis_store_order_url` (`url`),
  ADD KEY `idx_avantis_store_order_user_id` (`user_id`),
  ADD KEY `idx_avantis_store_order_date` (`date`),
  ADD KEY `idx_avantis_store_order_status` (`status_id`),
  ADD KEY `idx_avantis_store_order_paid` (`paid`),
  ADD KEY `fk_avantis_store_order_delivery` (`delivery_id`),
  ADD KEY `fk_avantis_store_order_payment` (`payment_method_id`),
  ADD KEY `fk_avantis_store_order_manager` (`manager_id`);

--
-- Индексы таблицы `avantis_store_order_coupon`
--
ALTER TABLE `avantis_store_order_coupon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_avantis_store_order_coupon_order` (`order_id`),
  ADD KEY `fk_avantis_store_order_coupon_coupon` (`coupon_id`);

--
-- Индексы таблицы `avantis_store_order_product`
--
ALTER TABLE `avantis_store_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_avantis_store_order_product_order_id` (`order_id`),
  ADD KEY `idx_avantis_store_order_product_product_id` (`product_id`);

--
-- Индексы таблицы `avantis_store_order_status`
--
ALTER TABLE `avantis_store_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_store_payment`
--
ALTER TABLE `avantis_store_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_avantis_store_payment_position` (`position`);

--
-- Индексы таблицы `avantis_store_producer`
--
ALTER TABLE `avantis_store_producer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_store_producer_slug` (`slug`),
  ADD KEY `ix_avantis_store_producer_sort` (`sort`);

--
-- Индексы таблицы `avantis_store_product`
--
ALTER TABLE `avantis_store_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_store_product_alias` (`slug`),
  ADD KEY `ix_avantis_store_product_status` (`status`),
  ADD KEY `ix_avantis_store_product_type_id` (`type_id`),
  ADD KEY `ix_avantis_store_product_producer_id` (`producer_id`),
  ADD KEY `ix_avantis_store_product_price` (`price`),
  ADD KEY `ix_avantis_store_product_discount_price` (`discount_price`),
  ADD KEY `ix_avantis_store_product_create_time` (`create_time`),
  ADD KEY `ix_avantis_store_product_update_time` (`update_time`),
  ADD KEY `fk_avantis_store_product_category` (`category_id`),
  ADD KEY `avantis_store_product_external_id_ix` (`external_id`),
  ADD KEY `ix_avantis_store_product_sku` (`sku`),
  ADD KEY `ix_avantis_store_product_name` (`name`),
  ADD KEY `ix_avantis_store_product_position` (`position`);

--
-- Индексы таблицы `avantis_store_product_attribute_value`
--
ALTER TABLE `avantis_store_product_attribute_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `avantis_fk_product_attribute_product` (`product_id`),
  ADD KEY `avantis_fk_product_attribute_attribute` (`attribute_id`),
  ADD KEY `avantis_fk_product_attribute_option` (`option_value`),
  ADD KEY `avantis_ix_product_attribute_number_value` (`number_value`),
  ADD KEY `avantis_ix_product_attribute_string_value` (`string_value`);

--
-- Индексы таблицы `avantis_store_product_category`
--
ALTER TABLE `avantis_store_product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_store_product_category_product_id` (`product_id`),
  ADD KEY `ix_avantis_store_product_category_category_id` (`category_id`);

--
-- Индексы таблицы `avantis_store_product_image`
--
ALTER TABLE `avantis_store_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_avantis_store_product_image_product` (`product_id`),
  ADD KEY `fk_avantis_store_product_image_group` (`group_id`);

--
-- Индексы таблицы `avantis_store_product_image_group`
--
ALTER TABLE `avantis_store_product_image_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avantis_store_product_link`
--
ALTER TABLE `avantis_store_product_link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_store_product_link_product` (`product_id`,`linked_product_id`),
  ADD KEY `fk_avantis_store_product_link_linked_product` (`linked_product_id`),
  ADD KEY `fk_avantis_store_product_link_type` (`type_id`);

--
-- Индексы таблицы `avantis_store_product_link_type`
--
ALTER TABLE `avantis_store_product_link_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_store_product_link_type_code` (`code`),
  ADD UNIQUE KEY `ux_avantis_store_product_link_type_title` (`title`);

--
-- Индексы таблицы `avantis_store_product_variant`
--
ALTER TABLE `avantis_store_product_variant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_avantis_store_product_variant_product` (`product_id`),
  ADD KEY `idx_avantis_store_product_variant_attribute` (`attribute_id`),
  ADD KEY `idx_avantis_store_product_variant_value` (`attribute_value`);

--
-- Индексы таблицы `avantis_store_type`
--
ALTER TABLE `avantis_store_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_store_type_name` (`name`);

--
-- Индексы таблицы `avantis_store_type_attribute`
--
ALTER TABLE `avantis_store_type_attribute`
  ADD PRIMARY KEY (`type_id`,`attribute_id`),
  ADD KEY `fk_avantis_store_type_attribute_attribute` (`attribute_id`);

--
-- Индексы таблицы `avantis_user_tokens`
--
ALTER TABLE `avantis_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_avantis_user_tokens_user_id` (`user_id`);

--
-- Индексы таблицы `avantis_user_user`
--
ALTER TABLE `avantis_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_user_user_nick_name` (`nick_name`),
  ADD UNIQUE KEY `ux_avantis_user_user_email` (`email`),
  ADD KEY `ix_avantis_user_user_status` (`status`);

--
-- Индексы таблицы `avantis_yupe_settings`
--
ALTER TABLE `avantis_yupe_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_avantis_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  ADD KEY `ix_avantis_yupe_settings_module_id` (`module_id`),
  ADD KEY `ix_avantis_yupe_settings_param_name` (`param_name`),
  ADD KEY `fk_avantis_yupe_settings_user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `avantis_blog_blog`
--
ALTER TABLE `avantis_blog_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_blog_post`
--
ALTER TABLE `avantis_blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_blog_tag`
--
ALTER TABLE `avantis_blog_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_blog_user_to_blog`
--
ALTER TABLE `avantis_blog_user_to_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_callback`
--
ALTER TABLE `avantis_callback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_category_category`
--
ALTER TABLE `avantis_category_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `avantis_comment_comment`
--
ALTER TABLE `avantis_comment_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_contentblock_content_block`
--
ALTER TABLE `avantis_contentblock_content_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `avantis_feedback_feedback`
--
ALTER TABLE `avantis_feedback_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `avantis_gallery_gallery`
--
ALTER TABLE `avantis_gallery_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `avantis_gallery_image_to_gallery`
--
ALTER TABLE `avantis_gallery_image_to_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT для таблицы `avantis_image_image`
--
ALTER TABLE `avantis_image_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT для таблицы `avantis_mail_mail_event`
--
ALTER TABLE `avantis_mail_mail_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `avantis_mail_mail_template`
--
ALTER TABLE `avantis_mail_mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `avantis_menu_menu`
--
ALTER TABLE `avantis_menu_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `avantis_menu_menu_item`
--
ALTER TABLE `avantis_menu_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT для таблицы `avantis_migrations`
--
ALTER TABLE `avantis_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT для таблицы `avantis_news_news`
--
ALTER TABLE `avantis_news_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `avantis_notify_settings`
--
ALTER TABLE `avantis_notify_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_page_page`
--
ALTER TABLE `avantis_page_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT для таблицы `avantis_page_page_image`
--
ALTER TABLE `avantis_page_page_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_review`
--
ALTER TABLE `avantis_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT для таблицы `avantis_sitemap_page`
--
ALTER TABLE `avantis_sitemap_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `avantis_store_attribute`
--
ALTER TABLE `avantis_store_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_attribute_group`
--
ALTER TABLE `avantis_store_attribute_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_attribute_option`
--
ALTER TABLE `avantis_store_attribute_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_category`
--
ALTER TABLE `avantis_store_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_coupon`
--
ALTER TABLE `avantis_store_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_delivery`
--
ALTER TABLE `avantis_store_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_order`
--
ALTER TABLE `avantis_store_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_order_coupon`
--
ALTER TABLE `avantis_store_order_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_order_product`
--
ALTER TABLE `avantis_store_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_order_status`
--
ALTER TABLE `avantis_store_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `avantis_store_payment`
--
ALTER TABLE `avantis_store_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_producer`
--
ALTER TABLE `avantis_store_producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product`
--
ALTER TABLE `avantis_store_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_attribute_value`
--
ALTER TABLE `avantis_store_product_attribute_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_category`
--
ALTER TABLE `avantis_store_product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_image`
--
ALTER TABLE `avantis_store_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_image_group`
--
ALTER TABLE `avantis_store_product_image_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_link`
--
ALTER TABLE `avantis_store_product_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_link_type`
--
ALTER TABLE `avantis_store_product_link_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `avantis_store_product_variant`
--
ALTER TABLE `avantis_store_product_variant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_store_type`
--
ALTER TABLE `avantis_store_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `avantis_user_tokens`
--
ALTER TABLE `avantis_user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT для таблицы `avantis_user_user`
--
ALTER TABLE `avantis_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `avantis_yupe_settings`
--
ALTER TABLE `avantis_yupe_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `avantis_blog_blog`
--
ALTER TABLE `avantis_blog_blog`
  ADD CONSTRAINT `fk_avantis_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `avantis_user_user` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `avantis_user_user` (`id`) ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_blog_post`
--
ALTER TABLE `avantis_blog_post`
  ADD CONSTRAINT `fk_avantis_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `avantis_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_blog_post_to_tag`
--
ALTER TABLE `avantis_blog_post_to_tag`
  ADD CONSTRAINT `fk_avantis_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `avantis_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `avantis_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_blog_user_to_blog`
--
ALTER TABLE `avantis_blog_user_to_blog`
  ADD CONSTRAINT `fk_avantis_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `avantis_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_category_category`
--
ALTER TABLE `avantis_category_category`
  ADD CONSTRAINT `fk_avantis_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_comment_comment`
--
ALTER TABLE `avantis_comment_comment`
  ADD CONSTRAINT `fk_avantis_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `avantis_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_feedback_feedback`
--
ALTER TABLE `avantis_feedback_feedback`
  ADD CONSTRAINT `fk_avantis_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_gallery_gallery`
--
ALTER TABLE `avantis_gallery_gallery`
  ADD CONSTRAINT `fk_avantis_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `avantis_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_gallery_image_to_gallery`
--
ALTER TABLE `avantis_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_avantis_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `avantis_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `avantis_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_image_image`
--
ALTER TABLE `avantis_image_image`
  ADD CONSTRAINT `fk_avantis_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `avantis_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_mail_mail_template`
--
ALTER TABLE `avantis_mail_mail_template`
  ADD CONSTRAINT `fk_avantis_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `avantis_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_menu_menu_item`
--
ALTER TABLE `avantis_menu_menu_item`
  ADD CONSTRAINT `fk_avantis_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `avantis_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_news_news`
--
ALTER TABLE `avantis_news_news`
  ADD CONSTRAINT `fk_avantis_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_notify_settings`
--
ALTER TABLE `avantis_notify_settings`
  ADD CONSTRAINT `fk_avantis_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_page_page`
--
ALTER TABLE `avantis_page_page`
  ADD CONSTRAINT `fk_avantis_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `avantis_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avantis_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `avantis_store_attribute`
--
ALTER TABLE `avantis_store_attribute`
  ADD CONSTRAINT `fk_avantis_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `avantis_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_attribute_option`
--
ALTER TABLE `avantis_store_attribute_option`
  ADD CONSTRAINT `fk_avantis_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `avantis_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_category`
--
ALTER TABLE `avantis_store_category`
  ADD CONSTRAINT `fk_avantis_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `avantis_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_delivery_payment`
--
ALTER TABLE `avantis_store_delivery_payment`
  ADD CONSTRAINT `fk_avantis_store_delivery_payment_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `avantis_store_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_delivery_payment_payment` FOREIGN KEY (`payment_id`) REFERENCES `avantis_store_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_order`
--
ALTER TABLE `avantis_store_order`
  ADD CONSTRAINT `fk_avantis_store_order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `avantis_store_delivery` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_order_manager` FOREIGN KEY (`manager_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_order_payment` FOREIGN KEY (`payment_method_id`) REFERENCES `avantis_store_payment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_order_status` FOREIGN KEY (`status_id`) REFERENCES `avantis_store_order_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_order_user` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_order_coupon`
--
ALTER TABLE `avantis_store_order_coupon`
  ADD CONSTRAINT `fk_avantis_store_order_coupon_coupon` FOREIGN KEY (`coupon_id`) REFERENCES `avantis_store_coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_order_coupon_order` FOREIGN KEY (`order_id`) REFERENCES `avantis_store_order` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_order_product`
--
ALTER TABLE `avantis_store_order_product`
  ADD CONSTRAINT `fk_avantis_store_order_product_order` FOREIGN KEY (`order_id`) REFERENCES `avantis_store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_order_product_product` FOREIGN KEY (`product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_product`
--
ALTER TABLE `avantis_store_product`
  ADD CONSTRAINT `fk_avantis_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `avantis_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `avantis_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `avantis_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_product_attribute_value`
--
ALTER TABLE `avantis_store_product_attribute_value`
  ADD CONSTRAINT `avantis_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `avantis_store_attribute` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `avantis_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `avantis_store_attribute_option` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `avantis_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_product_category`
--
ALTER TABLE `avantis_store_product_category`
  ADD CONSTRAINT `fk_avantis_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `avantis_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_product_image`
--
ALTER TABLE `avantis_store_product_image`
  ADD CONSTRAINT `fk_avantis_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `avantis_store_product_image_group` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_avantis_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_product_link`
--
ALTER TABLE `avantis_store_product_link`
  ADD CONSTRAINT `fk_avantis_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `avantis_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_product_variant`
--
ALTER TABLE `avantis_store_product_variant`
  ADD CONSTRAINT `fk_avantis_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `avantis_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `avantis_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_store_type_attribute`
--
ALTER TABLE `avantis_store_type_attribute`
  ADD CONSTRAINT `fk_avantis_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `avantis_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_avantis_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `avantis_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_user_tokens`
--
ALTER TABLE `avantis_user_tokens`
  ADD CONSTRAINT `fk_avantis_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `avantis_yupe_settings`
--
ALTER TABLE `avantis_yupe_settings`
  ADD CONSTRAINT `fk_avantis_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `avantis_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
